package com.example.my.nanyibangz.ui.shouye.model;

import com.example.my.nanyibangz.bean.HomeGoodsDetailsComment;
import com.example.my.nanyibangz.bean.HomeGoodsDetailsContent;
import com.example.my.nanyibangz.http.HomeHttpHelper;
import com.example.my.nanyibangz.ui.shouye.HomeFragmentContract;

import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/15.
 */

public class HomeGoodsDetailsModel implements HomeFragmentContract.IHomeGoodsDetailsModel {
    @Override
    public void homeLoadGoodsDetailsContentData(String type, Map<String, String> map, Subscriber<HomeGoodsDetailsContent> subscriber) {
        HomeHttpHelper.getInstance().getHomeGoodsDetailsContentDatas(type,map,subscriber);
    }

    @Override
    public void homeLoadGoodsDetailsCommentData(String type, Map<String, String> map, Subscriber<HomeGoodsDetailsComment> subscriber) {
        HomeHttpHelper.getInstance().getHomeGoodsDetailsCommentDatas(type,map,subscriber);
    }
}
