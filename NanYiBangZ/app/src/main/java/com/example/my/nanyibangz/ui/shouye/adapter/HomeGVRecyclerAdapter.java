package com.example.my.nanyibangz.ui.shouye.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.HomeVPBeans;
import com.example.my.nanyibangz.ui.shouye.HomeOnItemClickListener;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

/**
 * 首页每日签到数据适配器
 */

public class HomeGVRecyclerAdapter extends RecyclerView.Adapter<HomeGVRecyclerAdapter.HomeGVViewHolder>{
    private Context context;
    private List<HomeVPBeans.DataBean> list;
    private HomeOnItemClickListener.homeSecondRVOnItemClickListener homeSecondRVOnItemClickListener;

    public void setHomeSecondRVOnItemClickListener(HomeOnItemClickListener.homeSecondRVOnItemClickListener homeSecondRVOnItemClickListener) {
        this.homeSecondRVOnItemClickListener = homeSecondRVOnItemClickListener;
    }

    public HomeGVRecyclerAdapter(Context context, List<HomeVPBeans.DataBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public HomeGVViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.home_gv_item,null);
        HomeGVViewHolder homeGVViewHolder = new HomeGVViewHolder(view);
        return homeGVViewHolder;
    }

    @Override
    public void onBindViewHolder(final HomeGVViewHolder holder, int position) {
        holder.homeGvItemSdv.setImageURI(Uri.parse(list.get(position).getTheme_image()));
        holder.homeGvItemTitle.setText(list.get(position).getTheme_name());

        //通过接口将子视图的点击事件回传出去
        //判断当前控件是否设置了监听
        if (homeSecondRVOnItemClickListener!=null){ //说明被设置了监听
            holder.homeGvItemSdv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //获取当前点击的item的下标
                    int position = holder.getLayoutPosition();
                    homeSecondRVOnItemClickListener.onItemClickListener(holder.itemView,position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class HomeGVViewHolder extends RecyclerView.ViewHolder{
        SimpleDraweeView homeGvItemSdv;
        TextView homeGvItemTitle;
        public HomeGVViewHolder(View itemView) {
            super(itemView);
            homeGvItemSdv = (SimpleDraweeView) itemView.findViewById(R.id.home_gv_item_sdv);
            homeGvItemTitle = (TextView) itemView.findViewById(R.id.home_gv_item_title);
        }
    }
}
