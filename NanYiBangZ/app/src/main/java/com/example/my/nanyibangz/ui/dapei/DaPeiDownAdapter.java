package com.example.my.nanyibangz.ui.dapei;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.DaPeiDownBean;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/10/8.
 */

public class DaPeiDownAdapter extends BaseAdapter {
    private Context context;
    private List<DaPeiDownBean.DataBean> list;

    public DaPeiDownAdapter(Context context, List<DaPeiDownBean.DataBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    ViewHolderGD holderGd=null;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.dapei_item_down, null);
            holderGd=new ViewHolderGD(convertView);
            convertView.setTag(holderGd);
        } else {
            holderGd= (ViewHolderGD) convertView.getTag();
        }
        String path=list.get(position).getBigImage();
        if (path!=null&&!path.equals("")){
            Picasso.with(context).load(path).resize(list.get(position).getWidth(),list.get(position).getHeight()).into(holderGd.ivGroupDown);
        }else {
            Picasso.with(context).load("http://im02.nanyibang.com//match//2016//09//30//1475233069_83578.jpg?_upt=3a11ddc51475984090").into(holderGd.ivGroupDown);
        }
        holderGd.tvGroupDown.setText(list.get(position).getInfo());
        return convertView;
    }

    static class ViewHolderGD {
        @Bind(R.id.iv_groupDown)
        ImageView ivGroupDown;
        @Bind(R.id.tv_groupDown)
        TextView tvGroupDown;

        ViewHolderGD(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
