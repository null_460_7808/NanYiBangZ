package com.example.my.nanyibangz.ui.shouye;

import android.view.View;

/**
 * Created by My on 2016/10/13.
 */
//通过接口回调方式定义各个RecyclerView的点击事件
public class HomeOnItemClickListener {
    public interface homeSecondRVOnItemClickListener {
        //首页每日签到的点击事件
        public void onItemClickListener(View view,int position);
    }

    public interface homeSecondSpecialBannerOnItemClickListener{
        //首页每日签到下方专场广告板的点击事件
        public void onItemClickListener(View view,int position);
    }

    public interface homeThreeSchoolChoiceOnItemClickListener{
        //首页学堂精选的Item的点击事件
        public void onItemClickListener(View view,int position);
    }
}
