package com.example.my.nanyibangz.ui.shouye.view;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseFragment;
import com.example.my.nanyibangz.bean.HomeBangBangChoice;
import com.example.my.nanyibangz.bean.HomeChoiceness;
import com.example.my.nanyibangz.bean.HomeVPBeans;
import com.example.my.nanyibangz.config.HomeUrlConfig;
import com.example.my.nanyibangz.image.ImageHelper;
import com.example.my.nanyibangz.ui.shouye.HomeFragmentContract;
import com.example.my.nanyibangz.ui.shouye.HomeOnItemClickListener;
import com.example.my.nanyibangz.ui.shouye.adapter.HomeBangBangChoiceRecyclerAdapter;
import com.example.my.nanyibangz.ui.shouye.adapter.HomeGVRecyclerAdapter;
import com.example.my.nanyibangz.ui.shouye.adapter.HomeMatchChoiceRecyclerAdapter;
import com.example.my.nanyibangz.ui.shouye.adapter.HomeSchoolChoiceRecyclerAdapter;
import com.example.my.nanyibangz.ui.shouye.adapter.HomeSpecialBannerRecyclerAdapter;
import com.example.my.nanyibangz.ui.shouye.presenter.HomeFragmentPresenter;
import com.example.my.nanyibangz.utils.CustomGridLayoutManager;
import com.example.my.nanyibangz.utils.CustomLinearLayoutManager;
import com.example.my.nanyibangz.utils.DividerGridItemDecoration;
import com.example.my.nanyibangz.utils.DividerItemDecoration;
import com.example.my.nanyibangz.utils.MyPageAdapter;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by My on 2016/10/5.
 */
public class Home_Fragment extends BaseFragment implements HomeFragmentContract.IHomeView {
    @Bind(R.id.home_iv_search)
    ImageView homeIvSearch;
    @Bind(R.id.home_iv_shoppingCart)
    ImageView homeIvShoppingCart;
    @Bind(R.id.home_toolbar)
    Toolbar homeToolbar;
    @Bind(R.id.home_vp)
    ViewPager homeVp;
    @Bind(R.id.home_layout_icon)
    LinearLayout homeLayoutIcon;
    @Bind(R.id.home_rlayout_first)
    RelativeLayout homeRlayoutFirst;
    @Bind(R.id.home_gv)
    RecyclerView homeGv;
    @Bind(R.id.home_second_rv)
    RecyclerView homeSecondRv;
    @Bind(R.id.home_rlayout_second)
    RelativeLayout homeRlayoutSecond;
    @Bind(R.id.home_tv_choice)
    TextView homeTvChoice;
    @Bind(R.id.home_sdv_topone)
    SimpleDraweeView homeSdvTopone;
    @Bind(R.id.home_sdv_toptwo)
    SimpleDraweeView homeSdvToptwo;
    @Bind(R.id.home_sdv_topthree)
    SimpleDraweeView homeSdvTopthree;
    @Bind(R.id.home_sdv_topfour)
    SimpleDraweeView homeSdvTopfour;
    @Bind(R.id.home_sdv_bottomleftone)
    SimpleDraweeView homeSdvBottomleftone;
    @Bind(R.id.home_sdv_bottomlefttwo)
    SimpleDraweeView homeSdvBottomlefttwo;
    @Bind(R.id.home_sdv_bottommore)
    ImageView homeSdvBottommore;
    @Bind(R.id.home_third_rlayout)
    RelativeLayout homeThirdRlayout;
    @Bind(R.id.home_left_line)
    TextView homeLeftLine;
    @Bind(R.id.home_tv_choiceness)
    TextView homeTvChoiceness;
    @Bind(R.id.home_right_line)
    TextView homeRightLine;
    @Bind(R.id.home_decoration)
    RelativeLayout homeDecoration;
    @Bind(R.id.home_third_choiceHeader)
    RelativeLayout homeThirdChoiceHeader;
    @Bind(R.id.home_third_rv)
    RecyclerView homeThirdRv;
    @Bind(R.id.home_third_matchChoice_rlayout)
    RelativeLayout homeThirdMatchChoiceRlayout;
    @Bind(R.id.home_left_line1)
    TextView homeLeftLine1;
    @Bind(R.id.home_tv_choiceness1)
    TextView homeTvChoiceness1;
    @Bind(R.id.home_right_line1)
    TextView homeRightLine1;
    @Bind(R.id.home_decoration1)
    RelativeLayout homeDecoration1;
    @Bind(R.id.home_third_choiceHeader1)
    RelativeLayout homeThirdChoiceHeader1;
    @Bind(R.id.home_third_rv1)
    RecyclerView homeThirdRv1;
    @Bind(R.id.home_third_schoolChoice_rlayout)
    RelativeLayout homeThirdSchoolChoiceRlayout;
    @Bind(R.id.home_rlayout_third)
    RelativeLayout homeRlayoutThird;
    @Bind(R.id.home_tv_tese)
    TextView homeTvTese;
    @Bind(R.id.home_sdv_topleft)
    SimpleDraweeView homeSdvTopleft;
    @Bind(R.id.home_sdv_topright)
    SimpleDraweeView homeSdvTopright;
    @Bind(R.id.home_sdv_bottomleft)
    SimpleDraweeView homeSdvBottomleft;
    @Bind(R.id.home_sdv_bottomright)
    SimpleDraweeView homeSdvBottomright;
    @Bind(R.id.home_rlayout_fourth)
    RelativeLayout homeRlayoutFourth;
    @Bind(R.id.home_left_line2)
    TextView homeLeftLine2;
    @Bind(R.id.home_tv_choiceness2)
    TextView homeTvChoiceness2;
    @Bind(R.id.home_right_line2)
    TextView homeRightLine2;
    @Bind(R.id.home_decoration2)
    RelativeLayout homeDecoration2;
    @Bind(R.id.home_third_choiceHeader2)
    RelativeLayout homeThirdChoiceHeader2;
    @Bind(R.id.home_fifth_rv)
    RecyclerView homeFifthRv;
    @Bind(R.id.home_rlayout_fifth)
    RelativeLayout homeRlayoutFifth;
    @Bind(R.id.home_text_continue)
    TextView homeTextContinue;
    @Bind(R.id.home_ib_yound)
    Button homeIbYound;
    @Bind(R.id.home_ib_old)
    Button homeIbOld;
    @Bind(R.id.home_rlayout_sixth)
    RelativeLayout homeRlayoutSixth;
    @Bind(R.id.home_sv)
    NestedScrollView homeSv;
    @Bind(R.id.home_fab)
    FloatingActionButton homeFab;
    @Bind(R.id.home_swiperefresh)
    SwipeRefreshLayout homeSwiperefresh;


    //首页轮播的设置
    private HomeFragmentPresenter presenter;
    private List<ImageView> imageViewList = new ArrayList<>();//轮播图片集合
    private ImageView[] icons; //指示性图标
    private Handler handler;
    private int time = 0;//设置轮播页
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, null);
        ButterKnife.bind(this, view);
        //初始化ToolBar和FAB
        initToolBar();
        //刷新
        homeRefresh();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //创建中间人传递加载数据
        presenter = new HomeFragmentPresenter(this);
        //加载回传首页轮播数据
        presenter.homeLoadVPDataToUi(getActivity());
        //加载回传首页精选数据
        presenter.homeLoadChoicenessDataToUi(getActivity());
        //加载回传首页邦邦精选数据
        presenter.homeLoadBangBangChoicenessDataToUi(getActivity());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    //回传获取首页轮播数据
    @Override
    public void homeVPDataList(List<HomeVPBeans.DataBean> list) {
        if (list != null && list.size() != 0) {
            //首页轮播接口中包含其他数据 前六项为轮播页 故固定值
            //第一部分 首页轮播
            initViewPager(list);
            homeVPSelected();
            homeVPRun();
            //第二部分 每日签到
            homeGV(list);
            //专场广告牌
            homeSecondSpecialBanner(list);
            //第四部分 特色市场
            homeFour(list);
            //2016-10-12 研究json字符串时发现 每一部分的数据都有唯一的type值来分门别类 故不再死板的截取数据
        }
    }

    //回传获取首页精选数据
    @Override
    public void homeChoicenessList(HomeChoiceness homeChoiceness) {
        if (homeChoiceness != null) {
            //品牌精选
            List<HomeChoiceness.DataBean.BrandBean> brandBeanList = homeChoiceness.getData().getBrand();
            homeThreeBrandChoice(brandBeanList);

            //搭配精选
            List<HomeChoiceness.DataBean.MatchThemesBean> matchThemesBeanList = homeChoiceness.getData().getMatchThemes();//型男穿搭&&初秋潮搭
            homeThreeMatchChoice(matchThemesBeanList);

            //学堂精选
            List<HomeChoiceness.DataBean.SchoolBean> schoolBeanList = homeChoiceness.getData().getSchool();//经典潮流发型&&精英发型推荐
            homeThreeSchoolChoice(schoolBeanList);
        }
    }

    //回传获取首页邦邦精选数据
    @Override
    public void homeBangBangChoicenessList(List<HomeBangBangChoice.DataBean.ItemDetailBean> itemDetailBeanList) {
        if (itemDetailBeanList.size() != 0 && itemDetailBeanList != null) {
            homeFive(itemDetailBeanList);
        }
    }

    //部分控件的初始化(ToolBar)和FAB
    public void initToolBar() {
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        homeToolbar.setTitle("");
        appCompatActivity.setSupportActionBar(homeToolbar);
        //表示允许向上的导航可用
        //更改toolbar的返回按钮图片
        appCompatActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        //设置toolbar的返回按钮可以使用
        appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        homeSv.scrollTo(0, 0);
//        homeGv.setFocusable(false);
        ButterKnife.bind(this, view);

        homeFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeSv.scrollTo(0, 0);
            }
        });
    }

    //第一部分
    //首页轮播ViewPager的设置
    public void initViewPager(final List<HomeVPBeans.DataBean> list) {
        //首页轮播的type值为2 字符串类型
        List<HomeVPBeans.DataBean> homeVPList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getType().equals("2")) { //提取轮播集合数据
                HomeVPBeans.DataBean dataBean = list.get(i);
                homeVPList.add(dataBean);
            }
        }
        icons = new ImageView[homeVPList.size()];
        for (int i = 0; i < homeVPList.size(); i++) {
            ImageView imageView = new ImageView(getActivity().getApplicationContext());
            imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            ImageHelper.showImage(getActivity(), list.get(i).getTheme_image(), imageView);
            final int position = i;
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = list.get(position).getTheme_link();
                    String theme_name = list.get(position).getTheme_name();
                    Intent intent = new Intent(getActivity(), HomeWebViewActivity.class);
                    intent.putExtra("url", url);
                    intent.putExtra("theme_name", theme_name);
                    startActivity(intent);
                }
            });
            imageViewList.add(imageView);
        }
        for (int i = 0; i < icons.length; i++) {
            //动态创建指示性图标
            icons[i] = new ImageView(getActivity());
            icons[i].setBackgroundResource(R.mipmap.point_normal);
            icons[i].setLayoutParams(new ViewGroup.LayoutParams(17, 17));//设置宽高
            icons[i].setTag(i);

            icons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    homeVp.setCurrentItem((Integer) view.getTag());
                    time = (Integer) view.getTag();
                }
            });
            //将参数中指定的view添加到layout布局中
            homeLayoutIcon.addView(icons[i]);
        }
        //默认设置第一个为选中状态
        icons[0].setBackgroundResource(R.mipmap.point_focus);
        //数据适配
        MyPageAdapter adapter = new MyPageAdapter(imageViewList);
        homeVp.setAdapter(adapter);
    }

    //首页轮播VP的选择事件
    public void homeVPSelected() {
        homeVp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                time = position;
                for (int i = 0; i < icons.length; i++) {
                    icons[i].setBackgroundResource(R.mipmap.point_normal);
                }
                icons[position].setBackgroundResource(R.mipmap.point_focus);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    //开启线程轮转轮播图
    public void homeVPRun() {
        final Handler handler1 = new Handler() {
        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    time++;
                    handler1.post(new Runnable() {
                        @Override
                        public void run() {
                            int currentitem = time % icons.length;
                            homeVp.setCurrentItem(currentitem);
                        }
                    });
                }
            }
        }).start();
    }

    //第二部分 每日签到适配数据
    public void homeGV(List<HomeVPBeans.DataBean> list) {
        //根据json字符串中type值来截取数据 每日签到的type为3
        final List<HomeVPBeans.DataBean> homeGVList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getType().equals("3")) {
                HomeVPBeans.DataBean dataBean = list.get(i);
                homeGVList.add(dataBean);
            }
        }
        HomeGVRecyclerAdapter adapter = new HomeGVRecyclerAdapter(getActivity(), homeGVList);
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),4);
//        gridLayoutManager.setSmoothScrollbarEnabled(true);//需要滚动到一个位置的时候,设置这个变量
//        gridLayoutManager.setAutoMeasureEnabled(true);
//        homeGv.setHasFixedSize(true);
        CustomGridLayoutManager gridLayoutManager = new CustomGridLayoutManager(getActivity(), 4);
        homeGv.setNestedScrollingEnabled(false);//顺滑滑动解决卡顿
        homeGv.setLayoutManager(gridLayoutManager);
        homeGv.setItemAnimator(new DefaultItemAnimator());
        homeGv.addItemDecoration(new DividerGridItemDecoration(getActivity(), 0, 0));
        homeGv.setAdapter(adapter);

        //点击事件的设置(RecyclerView没有点击事件 需要在适配器中通过接口回调调用)
        adapter.setHomeSecondRVOnItemClickListener(new HomeOnItemClickListener.homeSecondRVOnItemClickListener() {
            @Override
            public void onItemClickListener(View view, int position) {
                //根据不同的position值进行不同的操作(因为每日签到四个按钮都有不同的数据)
                switch (position) {
                    //每日签到
                    case 0:
                        String url = homeGVList.get(position).getTheme_link();
                        String theme_name = homeGVList.get(position).getTheme_name();
                        Intent intent = new Intent(getActivity(), HomeWebViewActivity.class);
                        intent.putExtra("url", url);
                        intent.putExtra("theme_name", theme_name);
                        startActivity(intent);
                        break;
                    //秀吧
                    case 1:
                        break;
                }
            }
        });
    }

    //专场广告牌(每日签到下方)
    public void homeSecondSpecialBanner(List<HomeVPBeans.DataBean> list) {
        //根据json字符串中type值来截取数据 每日签到的type为4
        List<HomeVPBeans.DataBean> specialBannerList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getType().equals("4")) {
                HomeVPBeans.DataBean dataBean = list.get(i);
                specialBannerList.add(dataBean);
            }
        }
        HomeSpecialBannerRecyclerAdapter adapter = new HomeSpecialBannerRecyclerAdapter(getActivity(), specialBannerList);
        //RecyclerView的设置
        //设置自定义的线型布局管理器 横向
        CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        homeSecondRv.setLayoutManager(linearLayoutManager);
        //添加item之间的间距
        homeSecondRv.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST, 10));
        homeSecondRv.setItemAnimator(new DefaultItemAnimator());
        homeSecondRv.setNestedScrollingEnabled(false);//顺滑滑动
        homeSecondRv.setAdapter(adapter);
    }

    //第三部分
    //品牌精选
    public void homeThreeBrandChoice(final List<HomeChoiceness.DataBean.BrandBean> brandBeanList) {
        if (brandBeanList != null && brandBeanList.size() != 0) {
            //加载图片
            homeSdvTopone.setImageURI(Uri.parse(brandBeanList.get(0).getBrandIcon()));
            homeSdvToptwo.setImageURI(Uri.parse(brandBeanList.get(1).getBrandIcon()));
            homeSdvTopthree.setImageURI(Uri.parse(brandBeanList.get(2).getBrandIcon()));
            homeSdvTopfour.setImageURI(Uri.parse(brandBeanList.get(3).getBrandIcon()));
            homeSdvBottomleftone.setImageURI(Uri.parse(brandBeanList.get(4).getBrandIcon()));
            homeSdvBottomlefttwo.setImageURI(Uri.parse(brandBeanList.get(5).getBrandIcon()));

            //品牌精选Item的点击事件(需要传递brandId和cate_id值去拼接Url获取数据)
            homeSdvTopone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String brandName = brandBeanList.get(0).getBrandName();
                    int brandId = brandBeanList.get(0).getBrandId();
                    Intent intent = new Intent(getActivity(), HomeBrandChoiceItemActivity.class);
                    intent.putExtra("brandName", brandName);
                    intent.putExtra("brandId", brandId);
                    startActivity(intent);
                }
            });
            homeSdvToptwo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String brandName = brandBeanList.get(1).getBrandName();
                    int brandId = brandBeanList.get(1).getBrandId();
                    Intent intent = new Intent(getActivity(), HomeBrandChoiceItemActivity.class);
                    intent.putExtra("brandName", brandName);
                    intent.putExtra("brandId", brandId);
                    startActivity(intent);
                }
            });
            homeSdvTopthree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String brandName = brandBeanList.get(2).getBrandName();
                    int brandId = brandBeanList.get(2).getBrandId();
                    Intent intent = new Intent(getActivity(), HomeBrandChoiceItemActivity.class);
                    intent.putExtra("brandName", brandName);
                    intent.putExtra("brandId", brandId);
                    startActivity(intent);
                }
            });
            homeSdvTopfour.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String brandName = brandBeanList.get(3).getBrandName();
                    int brandId = brandBeanList.get(3).getBrandId();
                    Intent intent = new Intent(getActivity(), HomeBrandChoiceItemActivity.class);
                    intent.putExtra("brandName", brandName);
                    intent.putExtra("brandId", brandId);
                    startActivity(intent);
                }
            });
            homeSdvBottomleftone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String brandName = brandBeanList.get(4).getBrandName();
                    int brandId = brandBeanList.get(4).getBrandId();
                    Intent intent = new Intent(getActivity(), HomeBrandChoiceItemActivity.class);
                    intent.putExtra("brandName", brandName);
                    intent.putExtra("brandId", brandId);
                    startActivity(intent);
                }
            });
            homeSdvBottomlefttwo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String brandName = brandBeanList.get(5).getBrandName();
                    int brandId = brandBeanList.get(5).getBrandId();
                    Intent intent = new Intent(getActivity(), HomeBrandChoiceItemActivity.class);
                    intent.putExtra("brandName", brandName);
                    intent.putExtra("brandId", brandId);
                    startActivity(intent);
                }
            });
        }
    }

    //搭配精选
    public void homeThreeMatchChoice(List<HomeChoiceness.DataBean.MatchThemesBean> matchThemesBeanList) {
        if (matchThemesBeanList.size() != 0 && matchThemesBeanList != null) {
            HomeMatchChoiceRecyclerAdapter adapter = new HomeMatchChoiceRecyclerAdapter(getActivity(), matchThemesBeanList);
            //设置自定义的线型布局管理器 横向
//            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
//            linearLayoutManager.setAutoMeasureEnabled(true);
//            linearLayoutManager.setSmoothScrollbarEnabled(true);
//            homeThirdRv.setHasFixedSize(true); //setHasFixedSize()方法用来使RecyclerView保持固定的大小，该信息被用于自身的优化
            //CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(getActivity());
            CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            homeThirdRv.setLayoutManager(linearLayoutManager);
            //添加item之间的间距
            homeThirdRv.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST, 35));
            homeThirdRv.setItemAnimator(new DefaultItemAnimator());
            homeThirdRv.setNestedScrollingEnabled(false);//顺滑滑动
            homeThirdRv.setAdapter(adapter);
        }
    }

    //学堂精选
    public void homeThreeSchoolChoice(final List<HomeChoiceness.DataBean.SchoolBean> schoolBeanList) {
        if (schoolBeanList.size() != 0 && schoolBeanList != null) {
            HomeSchoolChoiceRecyclerAdapter adapter = new HomeSchoolChoiceRecyclerAdapter(getActivity(), schoolBeanList);
            CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            homeThirdRv1.setLayoutManager(linearLayoutManager);
            //添加item之间的间距
            homeThirdRv1.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST, 35));
            homeThirdRv1.setItemAnimator(new DefaultItemAnimator());
            homeThirdRv1.setNestedScrollingEnabled(false);//顺滑滑动
            homeThirdRv1.setAdapter(adapter);

            //点击事件的设置(RecyclerView没有点击事件 需要在适配器中通过接口回调调用)
            adapter.setThreeSchoolChoiceOnItemClickListener(new HomeOnItemClickListener.homeThreeSchoolChoiceOnItemClickListener() {
                @Override
                public void onItemClickListener(View view, int position) {
                    String url = HomeUrlConfig.BASE_WEBURL + schoolBeanList.get(position).getDress_school_id();
                    String title = schoolBeanList.get(position).getTitle();
                    Intent intent = new Intent(getActivity(), HomeWebViewActivity.class);
                    intent.putExtra("url", url);
                    intent.putExtra("theme_name", title);
                    startActivity(intent);
                }
            });
        }
    }

    //第四部分 特色市场加载图片
    public void homeFour(final List<HomeVPBeans.DataBean> list) {
        //根据json字符串中的type值来截取数据 特色市场的type值为6
        final List<HomeVPBeans.DataBean> homeFourList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getType().equals("6")) {
                HomeVPBeans.DataBean dataBean = list.get(i);
                homeFourList.add(dataBean);
            }
        }

        //加载图片
        homeSdvTopleft.setImageURI(Uri.parse(homeFourList.get(0).getTheme_image()));
        homeSdvTopright.setImageURI(Uri.parse(homeFourList.get(1).getTheme_image()));
        homeSdvBottomleft.setImageURI(Uri.parse(homeFourList.get(2).getTheme_image()));
        homeSdvBottomright.setImageURI(Uri.parse(homeFourList.get(3).getTheme_image()));

        //点击事件
        //潮品专区
        homeSdvTopleft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String theme_name = homeFourList.get(0).getTheme_name();
                Intent intent = new Intent(getActivity(), HomeTideProductActivity.class);
                intent.putExtra("theme_name", theme_name);
                startActivity(intent);
            }
        });
        homeSdvTopright.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = homeFourList.get(1).getTheme_link();
                if (!url.equals("")) {
                    Intent intent = new Intent(getActivity(), HomeWebViewActivity.class);
                    intent.putExtra("url", url);
                    startActivity(intent);
                }
            }
        });
        homeSdvBottomleft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = homeFourList.get(2).getTheme_link();
                if (!url.equals("")) {
                    Intent intent = new Intent(getActivity(), HomeWebViewActivity.class);
                    intent.putExtra("url", url);
                    startActivity(intent);
                }
            }
        });
        homeSdvBottomright.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = homeFourList.get(3).getTheme_link();
                if (!url.equals("")) {
                    Intent intent = new Intent(getActivity(), HomeWebViewActivity.class);
                    intent.putExtra("url", url);
                    startActivity(intent);
                }
            }
        });
    }

    //第五部分 邦邦精选加载图片
    public void homeFive(List<HomeBangBangChoice.DataBean.ItemDetailBean> itemDetailBeanList) {
        if (itemDetailBeanList.size() != 0 && itemDetailBeanList != null) {
            HomeBangBangChoiceRecyclerAdapter adapter = new HomeBangBangChoiceRecyclerAdapter(getActivity(), itemDetailBeanList);
            CustomGridLayoutManager gridLayoutManager = new CustomGridLayoutManager(getActivity(), 2);
//            gridLayoutManager.setSmoothScrollbarEnabled(true);//需要滚动到一个位置的时候,设置这个变量
//            gridLayoutManager.setAutoMeasureEnabled(true);
//            homeFifthRv.setHasFixedSize(true);
            homeFifthRv.setNestedScrollingEnabled(false);//顺滑滑动解决卡顿
            homeFifthRv.setLayoutManager(gridLayoutManager);
            homeFifthRv.setItemAnimator(new DefaultItemAnimator());
            homeFifthRv.addItemDecoration(new DividerGridItemDecoration(getActivity(), 6, 10));
            homeFifthRv.setAdapter(adapter);
        }
    }

    //下拉刷新
    public void homeRefresh(){
        //设置下拉刷新圆圈的背景色
        homeSwiperefresh.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.colorSwipRef));
        //设置下拉刷新圆圈进度条的颜色
        homeSwiperefresh.setColorSchemeColors(
                R.color.holo_blue_light,
                R.color.holo_red_light,
                R.color.holo_orange_light,
                R.color.holo_green_light
        );
        //第一个参数scale就是就是刷新那个圆形进度是是否缩放,如果为true表示缩放,圆形进度图像就会从小到大展示出来,为false就不缩放
        //第二个参数start和end就是那刷新进度条展示的相对于默认的展示位置,start和end组成一个范围，在这个y轴范围就是那个圆形进度ProgressView展示的位置
        homeSwiperefresh.setProgressViewOffset(false, 0, (int) TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics()));

        //表示当swipeRefreshLayout刷新时触发的监听事件
        homeSwiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //加载回传首页精选数据
                        presenter.homeLoadChoicenessDataToUi(getActivity());
                        //加载回传首页邦邦精选数据
                        presenter.homeLoadBangBangChoicenessDataToUi(getActivity());
                        homeSwiperefresh.setRefreshing(false);
                    }
                },3000);
            }
        });
    }
}
