package com.example.my.nanyibangz.ui.shuaiba;

import com.example.my.nanyibangz.base.IModel;
import com.example.my.nanyibangz.base.IPresenter;
import com.example.my.nanyibangz.base.IView;
import com.example.my.nanyibangz.bean.DaPeiDownBean;
import com.example.my.nanyibangz.bean.DaPeiUpBean;
import com.example.my.nanyibangz.bean.ShuaiBaBean;

import java.util.List;
import java.util.Map;

import rx.Subscriber;

/**
 * Created by Administrator on 2016/10/8.
 * 搭配页面
 */

public class ShuaiBaContact {
    public interface View extends IView{
        public void onVerticalSucess_ShuaiBa(List<ShuaiBaBean.DataBean> dataBeanList);
        public void onVerticalFail_ShuaiBa(String msg);
    }
    public interface Model extends IModel{
        public void getVerticalShuaiBa(Map<String, String> params, Subscriber<ShuaiBaBean> subscriber);
    }
    public interface Presenter extends IPresenter {
        public void getVerticalFromNet_ShuaiBa(Map<String, String> params);
    }
}
