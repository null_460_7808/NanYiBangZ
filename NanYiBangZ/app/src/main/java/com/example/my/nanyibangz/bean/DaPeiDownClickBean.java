package com.example.my.nanyibangz.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2016/10/9.
 */

public class DaPeiDownClickBean {

    /**
     * member_type : guest
     * member_id : 0
     * login_status : error
     * login_status_msg : not login in
     */

    @SerializedName("user")
    private UserBean user;
    /**
     * collocation_id : 8959
     * big_image : http://im02.nanyibang.com/match/2016/09/30/1475233069_83578.jpg?_upt=3a11ddc51475984090
     * long_info : 秋天里将套头毛衣外穿，里面搭配一件衬衫，彰显休闲斯文的暖男气质，走在街上令女孩好感度激升，是一款适合商务和休闲的经典穿搭。
     * info : 秋季简约休闲搭
     * width : 713
     * height : 1053
     * saveCount : 309
     * tags : [{"tagId":19,"tagName":"百搭"},{"tagId":2,"tagName":"休闲"},{"tagId":4,"tagName":"简约"}]
     * singleItems : [{"_id":412855,"bas_shop_id":1060,"item_id":412855,"title":"A21毛衣针织衫男2016新款男士毛衣圆领秋季套头毛衣男外套男春秋","coupon_price":"179.00","price":"319.00","pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1WQ.INpXXXXazXpXXXXXXXXXX_!!0-item_pic.jpg_90x90.jpg","description":"上衣","isv_code":"0_android_match_19","pic_img":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1WQ.INpXXXXazXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":299850,"bas_shop_id":1060,"item_id":299850,"title":"a21男士弹力修身休闲裤男青年长裤裤子小脚裤秋季潮流黑色男裤","coupon_price":"139.00","price":"299.00","pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1ot8TLFXXXXX7XXXXXXXXXXXX_!!0-item_pic.jpg_90x90.jpg","description":"裤子","isv_code":"0_android_match_19","pic_img":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1ot8TLFXXXXX7XXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":412636,"bas_shop_id":6274,"item_id":412636,"title":"欧利萨斯秋季布洛克雕花男鞋潮流系带男士白色板鞋平底韩版休闲鞋\n","coupon_price":"139.20","price":"278.00","pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1nD5pMVXXXXbkXFXXXXXXXXXX_!!0-item_pic.jpg_90x90.jpg","description":"鞋子","isv_code":"0_android_match_19","pic_img":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1nD5pMVXXXXbkXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"}]
     * relative : [{"cate_id":2,"items":[{"_id":294646,"title":"A21男装圆领时尚撞色毛衣 长袖潮流个性男针织线衫 2015秋装新品","coupon_price":"319.00","price":"319.00","pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1bDgrKXXXXXa1XVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":285501,"title":"A21 男款几何套头毛衣 流行男装圆领秋款男士针织衫 长袖线衣毛衫","coupon_price":"149.00","price":"339.00","pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1JeAfKpXXXXaSXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":312265,"title":"A21男装高领长袖毛衣 2015秋装新品 休闲百搭纯色套头男士针织衫","coupon_price":"199.00","price":"419.00","pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1OJbKIVXXXXaXXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":333213,"title":"A21男装圆领长袖时尚撞色毛衣 2015新品男士休闲百搭针织线衫冬装","coupon_price":"149.00","price":"389.00","pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1gmerLpXXXXcZXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":333579,"title":"A21男装圆领长袖毛衣 潮男简约百搭休闲男士套头线衫2016春装新品","coupon_price":"145.00","price":"249.00","pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1LgYCLpXXXXXrXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":323687,"title":"A21 圆领长袖男士毛衣 秋款套头针织衫 时尚青年休闲针织外套线衣","coupon_price":"95.00","price":"319.00","pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1xTyzLpXXXXbfXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":333454,"title":"A21圆领套头条纹长袖毛衣 青春休闲舒适男士线衫2016春","coupon_price":"99.00","price":"249.00","pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1Sfj5LpXXXXcKXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":322116,"title":"A21男装开衫单层单层长袖毛衣 纯色百搭舒适青春休闲纯棉外套潮男","coupon_price":"79.00","price":"279.00","pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1FtKBNFXXXXapapXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":397447,"title":"A21秋装新款毛衣男长袖圆领男士针织衫青年套头毛衣条纹学生秋冬","coupon_price":"159.00","price":"289.00","pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1MquBNFXXXXXhapXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":397651,"title":"A21新品秋装条纹撞色长袖毛衣男时尚圆领套头毛衣百搭男士线衫潮","coupon_price":"169.00","price":"319.00","pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB14FYfNFXXXXXIXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":403928,"title":"A21男士半高领休闲毛衣男 冬季圆领打底衫套头拼接长袖青年学生","coupon_price":"129.00","price":"239.00","pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1.Kh8LpXXXXXSapXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":405257,"title":"A21男装舒适潮流圆领长袖毛衣 2016秋装新品时尚活力保暖针织衫男","coupon_price":"129.00","price":"269.00","pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB163sWNpXXXXXMXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":405265,"title":"A21男士纯色高领毛衣长袖针织衫男 2016秋装新款休闲百搭白色学生","coupon_price":"199.00","price":"349.00","pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1v1bdLpXXXXaRXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":411441,"title":"男士秋装毛衣休闲V领针织衫男 学生青年长袖打底衫简约上衣潮","coupon_price":"169.00","price":"289.00","pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1L0cMMVXXXXblaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"}]},{"cate_id":7,"items":[{"_id":247852,"title":"A21男装低腰小脚修身休闲裤 黑色纯棉男裤子 夏季新品 百搭舒适潮","coupon_price":"89.00","price":"269.00","pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1RE8mIpXXXXcDXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":247851,"title":"a21 男装低腰弹力修身小脚休闲长裤 潮男 夏新品2015时尚百搭裤子","coupon_price":"139.00","price":"239.00","pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB10E2FJXXXXXbFXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":261107,"title":"A21男装修身低腰小脚休闲长裤 纯色百搭棉裤子潮男 夏季新品2015","coupon_price":"129.00","price":"249.00","pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1Xz52IXXXXXX_XVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":260220,"title":"a21男士休闲裤男 修身男裤 直筒休闲裤纯棉休闲修身长裤子","coupon_price":"129.00","price":"179.00","pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1vYLxIXXXXXcKXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":285503,"title":"A21男装修身小脚休闲裤男男士裤子 青年潮男弹力长裤男裤","coupon_price":"89.00","price":"239.00","pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1Xp7jLpXXXXaDaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":287595,"title":"A21男装中腰男士休闲裤 秋季男裤纯棉直筒裤 秋冬百搭潮男长裤子","coupon_price":"139.00","price":"269.00","pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB18n2oKpXXXXXlXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":315124,"title":"A21冬季新款男装低腰修身小脚裤 黑色简约百搭舒适男休闲裤子 潮","coupon_price":"139.00","price":"269.00","pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1lUrdKXXXXXbJXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":312488,"title":"A21男装中腰直筒休闲长裤 2015冬装新品撞色印花时尚纯棉裤子男潮","coupon_price":"339.00","price":"339.00","pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1cAoDKXXXXXa3XFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":299343,"title":"A21男装修身中腰小脚哈伦裤 松紧腰针织长裤 秋季潮男运动休闲裤","coupon_price":"319.00","price":"319.00","pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB19AcqKXXXXXXpaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":310424,"title":"A21男装时尚纯色百搭束脚裤 春装休闲裤 男士针织潮男裤子","coupon_price":"139.00","price":"339.00","pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1Xf3cLpXXXXXpaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":308457,"title":"A21男装中腰纯色百搭休闲裤青少年长裤 男士时尚纯棉裤子潮男秋装","coupon_price":"129.00","price":"389.00","pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB15VyaLVXXXXXkXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":322239,"title":"A21男装修身中腰无弹小脚裤 2015秋冬新品 男士潮流休闲裤 长裤子","coupon_price":"175.00","price":"369.00","pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1iAVIKpXXXXcnXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"}]}]
     * addition : {"theme_name":"新功能推荐","theme_image":"","theme_desc":"你知道吗？【今天穿什么】功能可以根据所在地天气情况推荐穿衣哦。","theme_link":"nanyibang://page/WeatherMatchs"}
     */

    @SerializedName("data")
    private DataBean data;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class UserBean {
        @SerializedName("member_type")
        private String memberType;
        @SerializedName("member_id")
        private int memberId;
        @SerializedName("login_status")
        private String loginStatus;
        @SerializedName("login_status_msg")
        private String loginStatusMsg;

        public String getMemberType() {
            return memberType;
        }

        public void setMemberType(String memberType) {
            this.memberType = memberType;
        }

        public int getMemberId() {
            return memberId;
        }

        public void setMemberId(int memberId) {
            this.memberId = memberId;
        }

        public String getLoginStatus() {
            return loginStatus;
        }

        public void setLoginStatus(String loginStatus) {
            this.loginStatus = loginStatus;
        }

        public String getLoginStatusMsg() {
            return loginStatusMsg;
        }

        public void setLoginStatusMsg(String loginStatusMsg) {
            this.loginStatusMsg = loginStatusMsg;
        }
    }

    public static class DataBean {
        @SerializedName("collocation_id")
        private int collocationId;
        @SerializedName("big_image")
        private String bigImage;
        @SerializedName("long_info")
        private String longInfo;
        @SerializedName("info")
        private String info;
        @SerializedName("width")
        private int width;
        @SerializedName("height")
        private int height;
        @SerializedName("saveCount")
        private int saveCount;
        /**
         * theme_name : 新功能推荐
         * theme_image :
         * theme_desc : 你知道吗？【今天穿什么】功能可以根据所在地天气情况推荐穿衣哦。
         * theme_link : nanyibang://page/WeatherMatchs
         */

        @SerializedName("addition")
        private AdditionBean addition;
        /**
         * tagId : 19
         * tagName : 百搭
         */

        @SerializedName("tags")
        private List<TagsBean> tags;
        /**
         * _id : 412855
         * bas_shop_id : 1060
         * item_id : 412855
         * title : A21毛衣针织衫男2016新款男士毛衣圆领秋季套头毛衣男外套男春秋
         * coupon_price : 179.00
         * price : 319.00
         * pic_url : http://img02.taobaocdn.com/bao/uploaded/i2/TB1WQ.INpXXXXazXpXXXXXXXXXX_!!0-item_pic.jpg_90x90.jpg
         * description : 上衣
         * isv_code : 0_android_match_19
         * pic_img : http://img02.taobaocdn.com/bao/uploaded/i2/TB1WQ.INpXXXXazXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg
         */

        @SerializedName("singleItems")
        private List<SingleItemsBean> singleItems;
        /**
         * cate_id : 2
         * items : [{"_id":294646,"title":"A21男装圆领时尚撞色毛衣 长袖潮流个性男针织线衫 2015秋装新品","coupon_price":"319.00","price":"319.00","pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1bDgrKXXXXXa1XVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":285501,"title":"A21 男款几何套头毛衣 流行男装圆领秋款男士针织衫 长袖线衣毛衫","coupon_price":"149.00","price":"339.00","pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1JeAfKpXXXXaSXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":312265,"title":"A21男装高领长袖毛衣 2015秋装新品 休闲百搭纯色套头男士针织衫","coupon_price":"199.00","price":"419.00","pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1OJbKIVXXXXaXXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":333213,"title":"A21男装圆领长袖时尚撞色毛衣 2015新品男士休闲百搭针织线衫冬装","coupon_price":"149.00","price":"389.00","pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1gmerLpXXXXcZXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":333579,"title":"A21男装圆领长袖毛衣 潮男简约百搭休闲男士套头线衫2016春装新品","coupon_price":"145.00","price":"249.00","pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1LgYCLpXXXXXrXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":323687,"title":"A21 圆领长袖男士毛衣 秋款套头针织衫 时尚青年休闲针织外套线衣","coupon_price":"95.00","price":"319.00","pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1xTyzLpXXXXbfXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":333454,"title":"A21圆领套头条纹长袖毛衣 青春休闲舒适男士线衫2016春","coupon_price":"99.00","price":"249.00","pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1Sfj5LpXXXXcKXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":322116,"title":"A21男装开衫单层单层长袖毛衣 纯色百搭舒适青春休闲纯棉外套潮男","coupon_price":"79.00","price":"279.00","pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1FtKBNFXXXXapapXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":397447,"title":"A21秋装新款毛衣男长袖圆领男士针织衫青年套头毛衣条纹学生秋冬","coupon_price":"159.00","price":"289.00","pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1MquBNFXXXXXhapXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":397651,"title":"A21新品秋装条纹撞色长袖毛衣男时尚圆领套头毛衣百搭男士线衫潮","coupon_price":"169.00","price":"319.00","pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB14FYfNFXXXXXIXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":403928,"title":"A21男士半高领休闲毛衣男 冬季圆领打底衫套头拼接长袖青年学生","coupon_price":"129.00","price":"239.00","pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1.Kh8LpXXXXXSapXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":405257,"title":"A21男装舒适潮流圆领长袖毛衣 2016秋装新品时尚活力保暖针织衫男","coupon_price":"129.00","price":"269.00","pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB163sWNpXXXXXMXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":405265,"title":"A21男士纯色高领毛衣长袖针织衫男 2016秋装新款休闲百搭白色学生","coupon_price":"199.00","price":"349.00","pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1v1bdLpXXXXaRXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"},{"_id":411441,"title":"男士秋装毛衣休闲V领针织衫男 学生青年长袖打底衫简约上衣潮","coupon_price":"169.00","price":"289.00","pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1L0cMMVXXXXblaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg"}]
         */

        @SerializedName("relative")
        private List<RelativeBean> relative;

        public int getCollocationId() {
            return collocationId;
        }

        public void setCollocationId(int collocationId) {
            this.collocationId = collocationId;
        }

        public String getBigImage() {
            return bigImage;
        }

        public void setBigImage(String bigImage) {
            this.bigImage = bigImage;
        }

        public String getLongInfo() {
            return longInfo;
        }

        public void setLongInfo(String longInfo) {
            this.longInfo = longInfo;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public int getSaveCount() {
            return saveCount;
        }

        public void setSaveCount(int saveCount) {
            this.saveCount = saveCount;
        }

        public AdditionBean getAddition() {
            return addition;
        }

        public void setAddition(AdditionBean addition) {
            this.addition = addition;
        }

        public List<TagsBean> getTags() {
            return tags;
        }

        public void setTags(List<TagsBean> tags) {
            this.tags = tags;
        }

        public List<SingleItemsBean> getSingleItems() {
            return singleItems;
        }

        public void setSingleItems(List<SingleItemsBean> singleItems) {
            this.singleItems = singleItems;
        }

        public List<RelativeBean> getRelative() {
            return relative;
        }

        public void setRelative(List<RelativeBean> relative) {
            this.relative = relative;
        }

        public static class AdditionBean {
            @SerializedName("theme_name")
            private String themeName;
            @SerializedName("theme_image")
            private String themeImage;
            @SerializedName("theme_desc")
            private String themeDesc;
            @SerializedName("theme_link")
            private String themeLink;

            public String getThemeName() {
                return themeName;
            }

            public void setThemeName(String themeName) {
                this.themeName = themeName;
            }

            public String getThemeImage() {
                return themeImage;
            }

            public void setThemeImage(String themeImage) {
                this.themeImage = themeImage;
            }

            public String getThemeDesc() {
                return themeDesc;
            }

            public void setThemeDesc(String themeDesc) {
                this.themeDesc = themeDesc;
            }

            public String getThemeLink() {
                return themeLink;
            }

            public void setThemeLink(String themeLink) {
                this.themeLink = themeLink;
            }
        }

        public static class TagsBean {
            @SerializedName("tagId")
            private int tagId;
            @SerializedName("tagName")
            private String tagName;

            public int getTagId() {
                return tagId;
            }

            public void setTagId(int tagId) {
                this.tagId = tagId;
            }

            public String getTagName() {
                return tagName;
            }

            public void setTagName(String tagName) {
                this.tagName = tagName;
            }
        }

        public static class SingleItemsBean {
            @SerializedName("_id")
            private int id;
            @SerializedName("bas_shop_id")
            private int basShopId;
            @SerializedName("item_id")
            private int itemId;
            @SerializedName("title")
            private String title;
            @SerializedName("coupon_price")
            private String couponPrice;
            @SerializedName("price")
            private String price;
            @SerializedName("pic_url")
            private String picUrl;
            @SerializedName("description")
            private String description;
            @SerializedName("isv_code")
            private String isvCode;
            @SerializedName("pic_img")
            private String picImg;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getBasShopId() {
                return basShopId;
            }

            public void setBasShopId(int basShopId) {
                this.basShopId = basShopId;
            }

            public int getItemId() {
                return itemId;
            }

            public void setItemId(int itemId) {
                this.itemId = itemId;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getCouponPrice() {
                return couponPrice;
            }

            public void setCouponPrice(String couponPrice) {
                this.couponPrice = couponPrice;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getPicUrl() {
                return picUrl;
            }

            public void setPicUrl(String picUrl) {
                this.picUrl = picUrl;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getIsvCode() {
                return isvCode;
            }

            public void setIsvCode(String isvCode) {
                this.isvCode = isvCode;
            }

            public String getPicImg() {
                return picImg;
            }

            public void setPicImg(String picImg) {
                this.picImg = picImg;
            }
        }

        public static class RelativeBean {
            @SerializedName("cate_id")
            private int cateId;
            /**
             * _id : 294646
             * title : A21男装圆领时尚撞色毛衣 长袖潮流个性男针织线衫 2015秋装新品
             * coupon_price : 319.00
             * price : 319.00
             * pic_url : http://img01.taobaocdn.com/bao/uploaded/i1/TB1bDgrKXXXXXa1XVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg
             */

            @SerializedName("items")
            private List<ItemsBean> items;

            public int getCateId() {
                return cateId;
            }

            public void setCateId(int cateId) {
                this.cateId = cateId;
            }

            public List<ItemsBean> getItems() {
                return items;
            }

            public void setItems(List<ItemsBean> items) {
                this.items = items;
            }

            public static class ItemsBean {
                @SerializedName("_id")
                private int id;
                @SerializedName("title")
                private String title;
                @SerializedName("coupon_price")
                private String couponPrice;
                @SerializedName("price")
                private String price;
                @SerializedName("pic_url")
                private String picUrl;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getCouponPrice() {
                    return couponPrice;
                }

                public void setCouponPrice(String couponPrice) {
                    this.couponPrice = couponPrice;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getPicUrl() {
                    return picUrl;
                }

                public void setPicUrl(String picUrl) {
                    this.picUrl = picUrl;
                }
            }
        }
    }
}
