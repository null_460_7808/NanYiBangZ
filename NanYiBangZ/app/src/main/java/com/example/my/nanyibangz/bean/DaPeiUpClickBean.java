package com.example.my.nanyibangz.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2016/10/8.
 */

public class DaPeiUpClickBean {

    /**
     * member_type : guest
     * member_id : 0
     * login_status : error
     * login_status_msg : not login in
     */

    @SerializedName("user")
    private UserBean user;
    /**
     * collocation_id : 8956
     * big_image : http://im02.nanyibang.com/match/2016/09/30/1475231072_73742.jpg?_upt=4ac4f7ea1475919458
     * width_height : 666,982
     * like_count : 180
     * info : 秋季休闲风穿搭
     * width : 666
     * height : 982
     */

    @SerializedName("data")
    private List<DataBean> data;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class UserBean {
        @SerializedName("member_type")
        private String memberType;
        @SerializedName("member_id")
        private int memberId;
        @SerializedName("login_status")
        private String loginStatus;
        @SerializedName("login_status_msg")
        private String loginStatusMsg;

        public String getMemberType() {
            return memberType;
        }

        public void setMemberType(String memberType) {
            this.memberType = memberType;
        }

        public int getMemberId() {
            return memberId;
        }

        public void setMemberId(int memberId) {
            this.memberId = memberId;
        }

        public String getLoginStatus() {
            return loginStatus;
        }

        public void setLoginStatus(String loginStatus) {
            this.loginStatus = loginStatus;
        }

        public String getLoginStatusMsg() {
            return loginStatusMsg;
        }

        public void setLoginStatusMsg(String loginStatusMsg) {
            this.loginStatusMsg = loginStatusMsg;
        }
    }

    public static class DataBean {
        @SerializedName("collocation_id")
        private int collocationId;
        @SerializedName("big_image")
        private String bigImage;
        @SerializedName("width_height")
        private String widthHeight;
        @SerializedName("like_count")
        private int likeCount;
        @SerializedName("info")
        private String info;
        @SerializedName("width")
        private int width;
        @SerializedName("height")
        private int height;

        public int getCollocationId() {
            return collocationId;
        }

        public void setCollocationId(int collocationId) {
            this.collocationId = collocationId;
        }

        public String getBigImage() {
            return bigImage;
        }

        public void setBigImage(String bigImage) {
            this.bigImage = bigImage;
        }

        public String getWidthHeight() {
            return widthHeight;
        }

        public void setWidthHeight(String widthHeight) {
            this.widthHeight = widthHeight;
        }

        public int getLikeCount() {
            return likeCount;
        }

        public void setLikeCount(int likeCount) {
            this.likeCount = likeCount;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }
    }
}
