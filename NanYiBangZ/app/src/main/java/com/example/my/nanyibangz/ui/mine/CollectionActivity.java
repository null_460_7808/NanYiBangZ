package com.example.my.nanyibangz.ui.mine;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/10/20.
 * 我的收藏
 */

public class CollectionActivity extends BaseActivity {
    @Bind(R.id.toolbar_mine_collection)
    Toolbar toolbarMineCollection;
    @Bind(R.id.tablayout_mine_collection)
    TabLayout tablayoutMineCollection;
    @Bind(R.id.mine_collection_fab)
    FloatingActionButton mineCollectionFab;
    @Bind(R.id.viewpager_mine)
    ViewPager viewpagerMine;
    String tabTitles[]={"单品","搭配","文章"};//存储tablayout的标题
    private List<Fragment> fragmentList;//存储单品fragment，和搭配fragment，文章fragment的集合。
    @Override
    public int getLayoutId() {
        return R.layout.activity_mine_collection;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        initView();
    }

    public void initView(){
        setSupportActionBar(toolbarMineCollection);
        toolbarMineCollection.setTitle("");//不设置title为空，toolbar会出现程序名
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //给tablayout标题赋值
        for (int i=0;i<tabTitles.length;i++){
          tablayoutMineCollection.addTab(tablayoutMineCollection.newTab().setText(tabTitles[i]));
        }
        SingleFragment singleFragment=new SingleFragment();
        ArticalFragment articalFragment=new ArticalFragment();
        MatchFragment matchFragment=new MatchFragment();
        fragmentList=new ArrayList<>();
        fragmentList.add(singleFragment);
        fragmentList.add(matchFragment);
        fragmentList.add(articalFragment);
        CollectionFragmentAdapter adapter=new CollectionFragmentAdapter(getSupportFragmentManager());
        viewpagerMine.setAdapter(adapter);


        //将viewpager和tablayout绑定到一起。
        viewpagerMine.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayoutMineCollection));
        //tablayout的滚动监听事件，用于将viewpager和tablayout绑定
        tablayoutMineCollection.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpagerMine.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



    }
    //viewpager的构造方法，又来加载不同的fragment.
    public class  CollectionFragmentAdapter extends FragmentPagerAdapter{

        public CollectionFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}
