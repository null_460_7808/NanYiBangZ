package com.example.my.nanyibangz.ui.danpin;

import com.example.my.nanyibangz.bean.DanPin_Beans;
import com.example.my.nanyibangz.bean.DanPin_JingXuan_Beans;
import com.example.my.nanyibangz.http.DanPinHttpHelper;

import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/5.
 */
public class DPFragmentVPModle implements  DPFragmentVPContact.Modle{
    @Override
    public void getVerticalDanPin_Beans(Map<String, String> params, Subscriber<DanPin_Beans> subscriber) {
        DanPinHttpHelper.getInstance().getVerticalDanPin(params,subscriber);
    }

    @Override
    public void getVerticalDanPin_JingXuan_Beans(Map<String, String> params, Subscriber<DanPin_JingXuan_Beans> subscriber) {
        DanPinHttpHelper.getInstance().getVerticalDanPin_JingXuan(params,subscriber);
    }
}
