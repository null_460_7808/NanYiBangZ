package com.example.my.nanyibangz.ui.shouye.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.HomeChoiceness;
import com.example.my.nanyibangz.ui.shouye.HomeOnItemClickListener;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

/**
 * 首页学堂精选数据适配器
 */

public class HomeSchoolChoiceRecyclerAdapter extends RecyclerView.Adapter<HomeSchoolChoiceRecyclerAdapter.HomeThirdSchoolChoiceViewHolder>{
    private Context context;
    private List<HomeChoiceness.DataBean.SchoolBean> list;
    private HomeOnItemClickListener.homeThreeSchoolChoiceOnItemClickListener threeSchoolChoiceOnItemClickListener;

    public void setThreeSchoolChoiceOnItemClickListener(HomeOnItemClickListener.homeThreeSchoolChoiceOnItemClickListener threeSchoolChoiceOnItemClickListener) {
        this.threeSchoolChoiceOnItemClickListener = threeSchoolChoiceOnItemClickListener;
    }

    public HomeSchoolChoiceRecyclerAdapter(Context context, List<HomeChoiceness.DataBean.SchoolBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public HomeThirdSchoolChoiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.home_schoolchoice_item,null);
        HomeThirdSchoolChoiceViewHolder holder = new HomeThirdSchoolChoiceViewHolder(convertView);
        return holder;
    }

    @Override
    public void onBindViewHolder(final HomeThirdSchoolChoiceViewHolder holder, final int position) {
        //设置控件 数据加载
        holder.home_schoolChoice_item_sdv.setImageURI(Uri.parse(list.get(position).getImage()));
        holder.home_schoolChoice_item_tv_title.setText(list.get(position).getTitle());
        holder.home_schoolChoice_item_tv_clickCount.setText(list.get(position).getClickCount()+"");

        //通过接口将子视图的点击事件回传出去
        //判断当前控件是否设置了监听
        if (threeSchoolChoiceOnItemClickListener!=null){
            holder.home_schoolChoice_item_sdv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = holder.getLayoutPosition();
                    threeSchoolChoiceOnItemClickListener.onItemClickListener(holder.itemView,position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class HomeThirdSchoolChoiceViewHolder extends RecyclerView.ViewHolder{
        SimpleDraweeView home_schoolChoice_item_sdv;
        TextView home_schoolChoice_item_tv_title,home_schoolChoice_item_tv_clickCount,home_schoolChoice_item_tv_read;
        public HomeThirdSchoolChoiceViewHolder(View itemView) {
            super(itemView);
            home_schoolChoice_item_sdv = (SimpleDraweeView) itemView.findViewById(R.id.home_schoolChoice_item_sdv);
            home_schoolChoice_item_tv_title = (TextView) itemView.findViewById(R.id.home_schoolChoice_item_tv_title);
            home_schoolChoice_item_tv_clickCount = (TextView) itemView.findViewById(R.id.home_schoolChoice_item_tv_clickCount);
            home_schoolChoice_item_tv_read = (TextView) itemView.findViewById(R.id.home_schoolChoice_item_tv_read);
        }
    }
}
