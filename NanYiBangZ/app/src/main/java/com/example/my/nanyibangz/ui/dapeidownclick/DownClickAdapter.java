package com.example.my.nanyibangz.ui.dapeidownclick;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.DaPeiDownClickBean;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/10/9.
 */

public class DownClickAdapter extends BaseAdapter {
    private Context context;
    private List<DaPeiDownClickBean.DataBean.SingleItemsBean> list;

    public DownClickAdapter(Context context, List<DaPeiDownClickBean.DataBean.SingleItemsBean> list) {
        this.context = context;
        this.list = list;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    ViewHolderDC holderDC=null;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.dapei_down_click_item, null);
            holderDC=new ViewHolderDC(convertView);
            convertView.setTag(holderDC);
        }
        else{
            holderDC= (ViewHolderDC) convertView.getTag();
        }
        String path=list.get(position).getPicImg();
        Picasso.with(context).load(path).into(holderDC.ivDownClickPic);
        holderDC.tvDownClickType.setText(list.get(position).getDescription());
        holderDC.tvDownClickTitle.setText(list.get(position).getTitle());
        holderDC.tvCouponPriceDC.setText("¥"+list.get(position).getCouponPrice());
        //中划线：textview.getPaint().setFlags(Paint. STRIKE_THRU_TEXT_FLAG ); //中间横线
        holderDC.tvPriceDC.setText("¥"+list.get(position).getPrice());
        holderDC.tvPriceDC.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        return convertView;
    }

    static class ViewHolderDC {
        @Bind(R.id.iv_downClickPic)
        ImageView ivDownClickPic;
        @Bind(R.id.tv_downClickType)
        TextView tvDownClickType;
        @Bind(R.id.tv_downClickTitle)
        TextView tvDownClickTitle;
        @Bind(R.id.tv_coupon_priceDC)
        TextView tvCouponPriceDC;
        @Bind(R.id.tv_priceDC)
        TextView tvPriceDC;

        ViewHolderDC(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
