package com.example.my.nanyibangz.ui.shouye.presenter;

import android.content.Context;
import android.util.Log;

import com.example.my.nanyibangz.bean.HomeGoodsDetailsComment;
import com.example.my.nanyibangz.bean.HomeGoodsDetailsContent;
import com.example.my.nanyibangz.config.HomeUrlConfig;
import com.example.my.nanyibangz.ui.shouye.HomeFragmentContract;
import com.example.my.nanyibangz.ui.shouye.model.HomeGoodsDetailsModel;

import java.util.HashMap;
import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/15.
 */

public class HomeGoodsDetailsPresenter implements HomeFragmentContract.IHomeGoodsDetailsPresenter {
    private HomeFragmentContract.IHomeGoodsDetailsModel model;
    private HomeFragmentContract.IHomeGoodsDetailsView view;

    public HomeGoodsDetailsPresenter(HomeFragmentContract.IHomeGoodsDetailsView view) {
        this.view = view;
        this.model = new HomeGoodsDetailsModel();
    }

    @Override
    public void homeLoadGoodsDetailsContentDataToUi(Context context) {
        //http://api.nanyibang.com/items?age=15&channel=yingyongbao&item_id=406944&system_name=android&versionCode=219
        Map<String,String> map = new HashMap<>();
        map.put(HomeUrlConfig.Params.AGE, "15");
        map.put(HomeUrlConfig.Params.CHANNEL, "yingyongbao");
        Log.e("tag","HomeGoodsId:"+view.getHomeGoodsId());
        map.put(HomeUrlConfig.Params.ITEM_ID, view.getHomeGoodsId()+"");
        map.put(HomeUrlConfig.Params.SYSTEM_NAME, "android");
        map.put(HomeUrlConfig.Params.VERSIONCODE, "219");

        model.homeLoadGoodsDetailsContentData("items", map, new Subscriber<HomeGoodsDetailsContent>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.e("tag",e.getMessage()+"homeGoodsDetailsContent is null");
            }

            @Override
            public void onNext(HomeGoodsDetailsContent homeGoodsDetailsContent) {
                if (homeGoodsDetailsContent!=null){
                    view.homeGoodsDetailsContentData(homeGoodsDetailsContent);
                }
            }
        });
    }

    @Override
    public void homeLoadGoodsDetailsCommentDataToUi(Context context) {
        //http://api.nanyibang.com/score-comment?age=15&channel=yingyongbao&item_id=406944&system_name=android&versionCode=219
        Map<String,String> map = new HashMap<>();
        map.put(HomeUrlConfig.Params.AGE, "15");
        map.put(HomeUrlConfig.Params.CHANNEL, "yingyongbao");
        map.put(HomeUrlConfig.Params.ITEM_ID, view.getHomeGoodsId()+"");
        map.put(HomeUrlConfig.Params.SYSTEM_NAME, "android");
        map.put(HomeUrlConfig.Params.VERSIONCODE, "219");

        model.homeLoadGoodsDetailsCommentData("score-comment", map, new Subscriber<HomeGoodsDetailsComment>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.e("tag",e.getMessage()+"homeGoodsDetailsComment is null");
            }

            @Override
            public void onNext(HomeGoodsDetailsComment homeGoodsDetailsComment) {
                if (homeGoodsDetailsComment!=null){
                    view.homeGoodsDetailsCommentData(homeGoodsDetailsComment);
                }
            }
        });
    }
}
