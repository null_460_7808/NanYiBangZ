package com.example.my.nanyibangz.ui.mine;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.style.AbsoluteSizeSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseActivity;

import butterknife.Bind;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;

/**
 * Created by Administrator on 2016/10/20.
 * 登录界面
 */

public class MineLoadActivity extends BaseActivity {


    @Bind(R.id.iv_registback)
    ImageView ivRegistback;
    @Bind(R.id.tv_shuaibaTitle)
    TextView tvShuaibaTitle;
    @Bind(R.id.ed_registid)
    EditText edRegistid;
    @Bind(R.id.ed_registpass)
    EditText edRegistpass;
    @Bind(R.id.load_regist)
    TextView loadRegist;
    @Bind(R.id.load_load)
    TextView loadLoad;
    @Bind(R.id.tv_weixin)
    TextView tvWeixin;
    Handler handler;

    @Override
    public int getLayoutId() {
        return R.layout.mine_load;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
        Regist();
        Load();
    }

    private void initData() {
        // 新建一个可以添加属性的文本对象
        SpannableString ss = new SpannableString("输入注册账号");
        SpannableString ss2 = new SpannableString("输入密码");
        // 新建一个属性对象,设置文字的大小
        AbsoluteSizeSpan ass = new AbsoluteSizeSpan(14, true);
        // 附加属性到文本
        ss.setSpan(ass, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss2.setSpan(ass, 0, ss2.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        // 设置hint
        edRegistid.setHint(new SpannedString(ss)); // 一定要进行转换,否则属性会消失
        edRegistpass.setHint(new SpannableString(ss2));
    }

    private void Regist() {
        loadRegist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MineLoadActivity.this, MineRegistActivity.class);
                startActivity(intent);
            }
        });

    }
    private void Load(){
        loadLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BmobUser bm=new BmobUser();
                bm.setUsername(edRegistid.getText().toString());
                bm.setPassword(edRegistpass.getText().toString());
                bm.login(new SaveListener<BmobUser>() {
                    @Override
                    public void done(BmobUser bmobUser, BmobException e) {
                        if (e==null){

                            Toast.makeText(MineLoadActivity.this,"登录成功！",Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent();
                            intent.putExtra("name",edRegistid.getText().toString());
                            setResult(0,intent);
                            MineFragment.flag=false;
                            MineLoadActivity.this.finish();
                            //通过BmobUser user = BmobUser.getCurrentUser()获取登录成功后的本地用户信息
                            //如果是自定义用户对象MyUser，可通过MyUser user = BmobUser.getCurrentUser(MyUser.class)获取自定义用户信息
                        }else {
                            Log.e("tag","loadError!");
                            Toast.makeText(MineLoadActivity.this,"登录失败：",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }
}
