package com.example.my.nanyibangz.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by My on 2016/10/10.
 */

public class DanPinTypeClickCommentBeans {

    /**
     * member_type : guest
     * member_id : 0
     * login_status : error
     * login_status_msg : not login in
     */

    @SerializedName("user")
    private UserBean user;
    /**
     * shop_name : 美特斯邦威官方网店
     * shop_score : {"service_score":"4.7","desc_score":"4.8","dispat_score":"4.7"}
     * comments : [{"name":"1**u","content":"衣服有点薄，这几天北京风大，没法穿，太透风了，也有点味道"},{"name":"熊**宝","content":"如果不好就不会做回头客了，还是那么棒，质量很好"},{"name":"e**e","content":"很好看"},{"name":"没**来","content":"特别好"},{"name":"努**1","content":"非常好！"},{"name":"u**m","content":"挺好的"},{"name":"壹**l","content":"很好看穿上"}]
     */

    @SerializedName("data")
    private DataBean data;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class UserBean {
        @SerializedName("member_type")
        private String memberType;
        @SerializedName("member_id")
        private int memberId;
        @SerializedName("login_status")
        private String loginStatus;
        @SerializedName("login_status_msg")
        private String loginStatusMsg;

        public String getMemberType() {
            return memberType;
        }

        public void setMemberType(String memberType) {
            this.memberType = memberType;
        }

        public int getMemberId() {
            return memberId;
        }

        public void setMemberId(int memberId) {
            this.memberId = memberId;
        }

        public String getLoginStatus() {
            return loginStatus;
        }

        public void setLoginStatus(String loginStatus) {
            this.loginStatus = loginStatus;
        }

        public String getLoginStatusMsg() {
            return loginStatusMsg;
        }

        public void setLoginStatusMsg(String loginStatusMsg) {
            this.loginStatusMsg = loginStatusMsg;
        }
    }

    public static class DataBean {
        @SerializedName("shop_name")
        private String shopName;
        /**
         * service_score : 4.7
         * desc_score : 4.8
         * dispat_score : 4.7
         */

        @SerializedName("shop_score")
        private ShopScoreBean shopScore;
        /**
         * name : 1**u
         * content : 衣服有点薄，这几天北京风大，没法穿，太透风了，也有点味道
         */

        @SerializedName("comments")
        private List<CommentsBean> comments;

        public String getShopName() {
            return shopName;
        }

        public void setShopName(String shopName) {
            this.shopName = shopName;
        }

        public ShopScoreBean getShopScore() {
            return shopScore;
        }

        public void setShopScore(ShopScoreBean shopScore) {
            this.shopScore = shopScore;
        }

        public List<CommentsBean> getComments() {
            return comments;
        }

        public void setComments(List<CommentsBean> comments) {
            this.comments = comments;
        }

        public static class ShopScoreBean {
            @SerializedName("service_score")
            private String serviceScore;
            @SerializedName("desc_score")
            private String descScore;
            @SerializedName("dispat_score")
            private String dispatScore;

            public String getServiceScore() {
                return serviceScore;
            }

            public void setServiceScore(String serviceScore) {
                this.serviceScore = serviceScore;
            }

            public String getDescScore() {
                return descScore;
            }

            public void setDescScore(String descScore) {
                this.descScore = descScore;
            }

            public String getDispatScore() {
                return dispatScore;
            }

            public void setDispatScore(String dispatScore) {
                this.dispatScore = dispatScore;
            }
        }

        public static class CommentsBean {
            @SerializedName("name")
            private String name;
            @SerializedName("content")
            private String content;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }
        }
    }
}
