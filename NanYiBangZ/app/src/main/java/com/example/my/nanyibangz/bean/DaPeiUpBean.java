package com.example.my.nanyibangz.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2016/10/8.
 */

public class DaPeiUpBean {

    /**
     * member_type : guest
     * member_id : 0
     * login_status : error
     * login_status_msg : not login in
     */

    @SerializedName("user")
    private UserBean user;
    /**
     * items : [{"classify_id":1,"classify_name":"休闲","classify_icon":"http://im01.nanyibang.com/icons/2016/09/22/xiuxian.jpg"},{"classify_id":2,"classify_name":"商务","classify_icon":"http://im01.nanyibang.com/icons/2016/09/22/shangwu.jpg"},{"classify_id":3,"classify_name":"运动","classify_icon":"http://im01.nanyibang.com/icons/2016/09/22/yundong.jpg"},{"classify_id":4,"classify_name":"简约","classify_icon":"http://im01.nanyibang.com/icons/2016/09/22/jianyue.jpg"},{"classify_id":5,"classify_name":"复古","classify_icon":"http://im01.nanyibang.com/icons/2016/09/22/fugu.jpg"},{"classify_id":6,"classify_name":"英伦","classify_icon":"http://im01.nanyibang.com/icons/2016/09/22/yinglun.jpg"},{"classify_id":7,"classify_name":"日韩","classify_icon":"http://im01.nanyibang.com/icons/2016/09/22/rihan.jpg"},{"classify_id":8,"classify_name":"欧美","classify_icon":"http://im01.nanyibang.com/icons/2016/09/22/oumei.jpg"},{"classify_id":27,"classify_name":"街头范","classify_icon":"http://im01.nanyibang.com/icons/2016/09/22/jietou.jpg"},{"classify_id":28,"classify_name":"微胖界","classify_icon":"http://im01.nanyibang.com/icons/2016/09/22/dama.jpg"},{"classify_id":29,"classify_name":"设计师","classify_icon":"http://im01.nanyibang.com/icons/2016/09/22/sheji.jpg"},{"classify_id":30,"classify_name":"明星搭","classify_icon":"http://im01.nanyibang.com/icons/2016/09/22/mingxing.jpg"}]
     * kind : 1
     * name : 风格
     * filter : [{"name":"价格","type":"price","items":[{"attribute_id":1,"attribute_name":"0-300"},{"attribute_id":2,"attribute_name":"300-500"},{"attribute_id":3,"attribute_name":"500-700"},{"attribute_id":4,"attribute_name":"700以上"}]},{"name":"季节","type":"season","items":[{"attribute_id":1,"attribute_name":"春季"},{"attribute_id":2,"attribute_name":"夏季"},{"attribute_id":3,"attribute_name":"秋季"},{"attribute_id":4,"attribute_name":"冬季"}]},{"name":"上衣","type":"cate_clothes","items":[{"attribute_id":103,"attribute_name":"居家服","pid":2},{"attribute_id":134,"attribute_name":"大衣","pid":2},{"attribute_id":131,"attribute_name":"皮夹克","pid":2},{"attribute_id":138,"attribute_name":"外套","pid":2},{"attribute_id":30,"attribute_name":"羽绒服","pid":2},{"attribute_id":29,"attribute_name":"棉服","pid":2},{"attribute_id":66,"attribute_name":"长袖T恤","pid":2},{"attribute_id":80,"attribute_name":"马甲","pid":2},{"attribute_id":22,"attribute_name":"长袖衬衫","pid":2},{"attribute_id":28,"attribute_name":"夹克","pid":2},{"attribute_id":24,"attribute_name":"卫衣","pid":2},{"attribute_id":25,"attribute_name":"针织衫","pid":2},{"attribute_id":132,"attribute_name":"牛仔外套","pid":2},{"attribute_id":135,"attribute_name":"棒球服","pid":2},{"attribute_id":52,"attribute_name":"毛衣","pid":2},{"attribute_id":70,"attribute_name":"短袖T恤","pid":2},{"attribute_id":27,"attribute_name":"风衣","pid":2},{"attribute_id":105,"attribute_name":"皮衣","pid":2},{"attribute_id":23,"attribute_name":"西服","pid":2},{"attribute_id":64,"attribute_name":"冲锋衣","pid":2},{"attribute_id":62,"attribute_name":"运动服","pid":2},{"attribute_id":26,"attribute_name":"背心","pid":2},{"attribute_id":75,"attribute_name":"短袖衬衫","pid":2},{"attribute_id":101,"attribute_name":"POLO","pid":2},{"attribute_id":53,"attribute_name":"保暖内衣","pid":2}]},{"name":"裤子","type":"cate_trousers","items":[{"attribute_id":37,"attribute_name":"休闲裤","pid":7},{"attribute_id":36,"attribute_name":"牛仔裤","pid":7},{"attribute_id":128,"attribute_name":"小脚裤","pid":7},{"attribute_id":40,"attribute_name":"运动裤","pid":7},{"attribute_id":61,"attribute_name":"哈伦裤","pid":7},{"attribute_id":73,"attribute_name":"九/七分裤","pid":7},{"attribute_id":41,"attribute_name":"工装裤","pid":7},{"attribute_id":38,"attribute_name":"西裤","pid":7},{"attribute_id":130,"attribute_name":"背带裤","pid":7},{"attribute_id":74,"attribute_name":"短裤","pid":7},{"attribute_id":129,"attribute_name":"皮裤","pid":7},{"attribute_id":68,"attribute_name":"冲锋裤","pid":7}]},{"name":"鞋子","type":"cate_shoes","items":[{"attribute_id":78,"attribute_name":"登山鞋","pid":9},{"attribute_id":122,"attribute_name":"拖鞋","pid":9},{"attribute_id":55,"attribute_name":"休闲鞋","pid":9},{"attribute_id":125,"attribute_name":"男靴","pid":9},{"attribute_id":124,"attribute_name":"棉鞋","pid":9},{"attribute_id":57,"attribute_name":"高帮鞋","pid":9},{"attribute_id":58,"attribute_name":"板鞋","pid":9},{"attribute_id":121,"attribute_name":"休闲皮鞋","pid":9},{"attribute_id":123,"attribute_name":"商务皮鞋","pid":9},{"attribute_id":31,"attribute_name":"运动鞋","pid":9},{"attribute_id":126,"attribute_name":"乐福鞋","pid":9},{"attribute_id":127,"attribute_name":"豆豆鞋","pid":9},{"attribute_id":32,"attribute_name":"帆布鞋","pid":9},{"attribute_id":119,"attribute_name":"增高鞋","pid":9},{"attribute_id":120,"attribute_name":"雪地靴","pid":9},{"attribute_id":33,"attribute_name":"皮鞋","pid":9},{"attribute_id":76,"attribute_name":"凉鞋","pid":9}]}]
     */

    @SerializedName("data")
    private List<DataBean> data;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class UserBean {
        @SerializedName("member_type")
        private String memberType;
        @SerializedName("member_id")
        private int memberId;
        @SerializedName("login_status")
        private String loginStatus;
        @SerializedName("login_status_msg")
        private String loginStatusMsg;

        public String getMemberType() {
            return memberType;
        }

        public void setMemberType(String memberType) {
            this.memberType = memberType;
        }

        public int getMemberId() {
            return memberId;
        }

        public void setMemberId(int memberId) {
            this.memberId = memberId;
        }

        public String getLoginStatus() {
            return loginStatus;
        }

        public void setLoginStatus(String loginStatus) {
            this.loginStatus = loginStatus;
        }

        public String getLoginStatusMsg() {
            return loginStatusMsg;
        }

        public void setLoginStatusMsg(String loginStatusMsg) {
            this.loginStatusMsg = loginStatusMsg;
        }
    }

    public static class DataBean {
        @SerializedName("kind")
        private int kind;
        @SerializedName("name")
        private String name;
        /**
         * classify_id : 1
         * classify_name : 休闲
         * classify_icon : http://im01.nanyibang.com/icons/2016/09/22/xiuxian.jpg
         */

        @SerializedName("items")
        private List<ItemsBean> items;
        /**
         * name : 价格
         * type : price
         * items : [{"attribute_id":1,"attribute_name":"0-300"},{"attribute_id":2,"attribute_name":"300-500"},{"attribute_id":3,"attribute_name":"500-700"},{"attribute_id":4,"attribute_name":"700以上"}]
         */

        @SerializedName("filter")
        private List<FilterBean> filter;

        public int getKind() {
            return kind;
        }

        public void setKind(int kind) {
            this.kind = kind;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public List<FilterBean> getFilter() {
            return filter;
        }

        public void setFilter(List<FilterBean> filter) {
            this.filter = filter;
        }

        public static class ItemsBean {
            @SerializedName("classify_id")
            private int classifyId;
            @SerializedName("classify_name")
            private String classifyName;
            @SerializedName("classify_icon")
            private String classifyIcon;

            public int getClassifyId() {
                return classifyId;
            }

            public void setClassifyId(int classifyId) {
                this.classifyId = classifyId;
            }

            public String getClassifyName() {
                return classifyName;
            }

            public void setClassifyName(String classifyName) {
                this.classifyName = classifyName;
            }

            public String getClassifyIcon() {
                return classifyIcon;
            }

            public void setClassifyIcon(String classifyIcon) {
                this.classifyIcon = classifyIcon;
            }
        }

        public static class FilterBean {
            @SerializedName("name")
            private String name;
            @SerializedName("type")
            private String type;
            /**
             * attribute_id : 1
             * attribute_name : 0-300
             */

            @SerializedName("items")
            private List<ItemsBean> items;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public List<ItemsBean> getItems() {
                return items;
            }

            public void setItems(List<ItemsBean> items) {
                this.items = items;
            }

            public static class ItemsBean {
                @SerializedName("attribute_id")
                private int attributeId;
                @SerializedName("attribute_name")
                private String attributeName;

                public int getAttributeId() {
                    return attributeId;
                }

                public void setAttributeId(int attributeId) {
                    this.attributeId = attributeId;
                }

                public String getAttributeName() {
                    return attributeName;
                }

                public void setAttributeName(String attributeName) {
                    this.attributeName = attributeName;
                }
            }
        }
    }
}
