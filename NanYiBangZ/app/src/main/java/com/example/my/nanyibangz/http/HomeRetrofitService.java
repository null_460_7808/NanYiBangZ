package com.example.my.nanyibangz.http;

import com.example.my.nanyibangz.bean.HomeBangBangChoice;
import com.example.my.nanyibangz.bean.HomeChoiceness;
import com.example.my.nanyibangz.bean.HomeGoodsDetailsComment;
import com.example.my.nanyibangz.bean.HomeGoodsDetailsContent;
import com.example.my.nanyibangz.bean.HomeThreeBrandChoiceItem;
import com.example.my.nanyibangz.bean.HomeTideProduct;
import com.example.my.nanyibangz.bean.HomeVPBeans;
import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by My on 2016/10/7.
 */

public interface HomeRetrofitService {
    //首页轮播
    //http://api.nanyibang.com/theme?age=15&channel=yingyongbao&system_name=android&type=2%2C3%2C4%2C5%2C6%2C7&versionCode=219
    //@GET("theme?age=15&channel=yingyongbao&system_name=android&type=2%2C3%2C4%2C5%2C6%2C7&versionCode=219")
    //Observable<HomeVPBeans> getHomeVPData();
    @GET("{type}?")
    Observable<HomeVPBeans> getHomeVPData(@Path("type") String type,@QueryMap Map<String,String> map);

    //首页精选(品牌精选 搭配精选 学堂精选部分)
    //http://api.nanyibang.com/index-jingxuan?age=15&channel=yingyongbao&system_name=android&versionCode=219
    @GET("{type}?")
    Observable<HomeChoiceness> getHomeChoiceness(@Path("type") String type,@QueryMap Map<String,String> map);

    //首页品牌精选Item的内容
    //http://api.nanyibang.com/brand-items?age=15&brand_id=8566&cate_id=1&channel=yingyongbao&page=1&system_name=android&versionCode=219
    @GET("{type}?")
    Observable<HomeThreeBrandChoiceItem> getHomeThreeBrandChoiceItem(@Path("type") String type,@QueryMap Map<String,String> map);

    //首页品牌精选内容中的Item内容(商品详情)
    //商品详情中的商品信息
    //http://api.nanyibang.com/items?age=15&channel=yingyongbao&item_id=406944&system_name=android&versionCode=219
    @GET("{type}?")
    Observable<HomeGoodsDetailsContent> getHomeGoodsDetailsContent(@Path("type") String type,@QueryMap Map<String,String> map);
    //商品详情中的店铺评论
    //http://api.nanyibang.com/score-comment?age=15&channel=yingyongbao&item_id=406944&system_name=android&versionCode=219
    @GET("{type}?")
    Observable<HomeGoodsDetailsComment> getHomeGoodsDetailsComment(@Path("type") String type,@QueryMap Map<String,String> map);

    //首页邦邦精选
    //http://api.nanyibang.com/campaign?age=15&campaignId=5&campaignType=jingxuan&channel=yingyongbao&page=1&system_name=android&versionCode=219
    @GET("{type}?")
    Observable<HomeBangBangChoice> getHomeBangBangChoiceness(@Path("type") String type,@QueryMap Map<String,String> map);

    //首页特色市场(潮品专区、日常洗护、精品小街、情侣专区)
    //http://api.nanyibang.com/campaign?age=15&campaignId=6&campaignType=chaopin&channel=yingyongbao&page=1&system_name=android&versionCode=219
    @GET("{type}?")
    Observable<HomeTideProduct> getHomeTideProduct(@Path("type") String type,@QueryMap Map<String,String> map);
}
