package com.example.my.nanyibangz.ui.shouye.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.HomeVPBeans;
import com.example.my.nanyibangz.ui.shouye.view.HomeWebViewActivity;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

/**
 * 首页每日签到专场广告板数据适配器
 */

public class HomeSpecialBannerRecyclerAdapter extends RecyclerView.Adapter<HomeSpecialBannerRecyclerAdapter.HomeSecondSpecialBannerViewHolder>{
    private Context context;
    private List<HomeVPBeans.DataBean> list;

    public HomeSpecialBannerRecyclerAdapter(Context context, List<HomeVPBeans.DataBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public HomeSecondSpecialBannerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.home_specialbanner_item,null);
        HomeSecondSpecialBannerViewHolder holder = new HomeSecondSpecialBannerViewHolder(convertView);
        return holder;
    }

    @Override
    public void onBindViewHolder(HomeSecondSpecialBannerViewHolder holder, final int position) {
        //数据加载
        holder.home_specialbanner_item_sdv.setImageURI(Uri.parse(list.get(position).getTheme_image()));
        holder.home_specialbanner_item_sdv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = list.get(position).getTheme_link();
                String theme_name = list.get(position).getTheme_name();
                Intent intent = new Intent(context, HomeWebViewActivity.class);
                intent.putExtra("url",url);
                intent.putExtra("theme_name",theme_name);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class HomeSecondSpecialBannerViewHolder extends RecyclerView.ViewHolder{
        SimpleDraweeView home_specialbanner_item_sdv;
        public HomeSecondSpecialBannerViewHolder(View itemView) {
            super(itemView);
            home_specialbanner_item_sdv = (SimpleDraweeView) itemView.findViewById(R.id.home_specialbanner_item_sdv);
        }
    }
}
