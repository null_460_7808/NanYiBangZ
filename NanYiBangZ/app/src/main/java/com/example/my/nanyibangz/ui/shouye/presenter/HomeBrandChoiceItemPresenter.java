package com.example.my.nanyibangz.ui.shouye.presenter;

import android.content.Context;
import android.util.Log;

import com.example.my.nanyibangz.bean.HomeThreeBrandChoiceItem;
import com.example.my.nanyibangz.config.HomeUrlConfig;
import com.example.my.nanyibangz.ui.shouye.HomeFragmentContract;
import com.example.my.nanyibangz.ui.shouye.model.HomeBrandChoiceItemModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/13.
 */

public class HomeBrandChoiceItemPresenter implements HomeFragmentContract.IHomeBrandChoicePresenter {
    private HomeFragmentContract.IHomeBrandChoiceModel model;
    private HomeFragmentContract.IHomeBrandChoiceView view;

    public HomeBrandChoiceItemPresenter(HomeFragmentContract.IHomeBrandChoiceView view) {
        this.view = view;
        this.model = new HomeBrandChoiceItemModel();
    }

    //加载首页品牌精选Item数据
    @Override
    public void homeLoadBrandChoiceItemDataToUi(Context context) {
        final Map<String,List<HomeThreeBrandChoiceItem.DataBean>> dataMap = new HashMap<>();
        //http://api.nanyibang.com/brand-items?age=15&brand_id=8566&cate_id=1&channel=yingyongbao&page=1&system_name=android&versionCode=219
        Map<String,String> map = new HashMap<>();
        map.put(HomeUrlConfig.Params.AGE, "15");
        Log.e("tag",view.getHomeBrandChoiceItemBrandId()+"");
        map.put(HomeUrlConfig.Params.BRAND_ID, view.getHomeBrandChoiceItemBrandId()+"");
        map.put(HomeUrlConfig.Params.CATE_ID, "1");
        map.put(HomeUrlConfig.Params.CHANNEL, "yingyongbao");
        map.put(HomeUrlConfig.Params.PAGE, "1");
        map.put(HomeUrlConfig.Params.SYSTEM_NAME, "android");
        map.put(HomeUrlConfig.Params.VERSIONCODE, "219");
        model.homeLoadBrandChoiceItemData("brand-items", map, new Subscriber<HomeThreeBrandChoiceItem>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.e("tag",e.getMessage()+"homeThreeBrandChoiceItem is null");
            }

            @Override
            public void onNext(HomeThreeBrandChoiceItem homeThreeBrandChoiceItem) {
                if (homeThreeBrandChoiceItem!=null){
                    dataMap.put("clothes",homeThreeBrandChoiceItem.getData());
                }
            }
        });

        Map<String,String> map1 = new HashMap<>();
        map1.put(HomeUrlConfig.Params.AGE, "15");
        map1.put(HomeUrlConfig.Params.BRAND_ID, view.getHomeBrandChoiceItemBrandId()+"");
        map1.put(HomeUrlConfig.Params.CATE_ID, "2");
        map1.put(HomeUrlConfig.Params.CHANNEL, "yingyongbao");
        map1.put(HomeUrlConfig.Params.PAGE, "1");
        map1.put(HomeUrlConfig.Params.SYSTEM_NAME, "android");
        map1.put(HomeUrlConfig.Params.VERSIONCODE, "219");
        model.homeLoadBrandChoiceItemData("brand-items", map1, new Subscriber<HomeThreeBrandChoiceItem>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.e("tag",e.getMessage()+"homeThreeBrandChoiceItem1 is null");
            }

            @Override
            public void onNext(HomeThreeBrandChoiceItem homeThreeBrandChoiceItem) {
                if (homeThreeBrandChoiceItem!=null){
                    dataMap.put("pants",homeThreeBrandChoiceItem.getData());
                    view.homeBrandChoiceItemDataMap(dataMap);
                }
            }
        });
    }
}
