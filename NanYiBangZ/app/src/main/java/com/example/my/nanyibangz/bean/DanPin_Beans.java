package com.example.my.nanyibangz.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by My on 2016/10/6.
 */
public class DanPin_Beans {


    /**
     * member_type : guest
     * login_status : error
     * login_status_msg : not login in
     */

    @SerializedName("user")
    private UserBean user;
    /**
     * classID : 2
     * name : 上衣
     * categories : [{"name":"长袖T恤","cate_id":66,"cateimg":"http://im01.nanyibang.com/cateimg/2016/09/14/changt.jpg","filter":[{"name":"领型","type":"key_110","items":[{"attributeID":2037,"attributeName":"圆领"},{"attributeID":2038,"attributeName":"V领"},{"attributeID":2039,"attributeName":"翻领"},{"attributeID":2040,"attributeName":"立领"},{"attributeID":2041,"attributeName":"衬衫领"},{"attributeID":2042,"attributeName":"连帽"},{"attributeID":2043,"attributeName":"高领"},{"attributeID":2044,"attributeName":"其他"}]},{"name":"版型","type":"key_111","items":[{"attributeID":2060,"attributeName":"修身"},{"attributeID":2061,"attributeName":"直筒"},{"attributeID":2062,"attributeName":"宽松"},{"attributeID":2063,"attributeName":"常规"},{"attributeID":2064,"attributeName":"紧身"},{"attributeID":2065,"attributeName":"超修身"}]},{"name":"材质","type":"key_112","items":[{"attributeID":2085,"attributeName":"棉"},{"attributeID":2086,"attributeName":"针织布"},{"attributeID":2087,"attributeName":"棉毛布"},{"attributeID":2088,"attributeName":"棉氨"},{"attributeID":2089,"attributeName":"梭织布"},{"attributeID":2090,"attributeName":"涤纶"},{"attributeID":2097,"attributeName":"其他"}]},{"name":"适用季节","type":"key_113","items":[{"attributeID":2124,"attributeName":"秋季"},{"attributeID":2125,"attributeName":"春季"},{"attributeID":2126,"attributeName":"四季"},{"attributeID":2127,"attributeName":"夏季"},{"attributeID":2128,"attributeName":"冬季"}]}]},{"name":"长袖衬衫","cate_id":22,"cateimg":"http://im01.nanyibang.com/cateimg/2016/09/14/changchen.jpg","filter":[{"name":"面料","type":"key_2","items":[{"attributeID":2335,"attributeName":"棉"},{"attributeID":2336,"attributeName":"牛津纺"},{"attributeID":2337,"attributeName":"涤纶"},{"attributeID":2338,"attributeName":"牛仔布"},{"attributeID":2339,"attributeName":"法兰绒（磨毛）"},{"attributeID":2340,"attributeName":"羊毛布"},{"attributeID":2341,"attributeName":"棉混纺布"},{"attributeID":2342,"attributeName":"麻"},{"attributeID":2343,"attributeName":"蚕丝"},{"attributeID":2344,"attributeName":"莫代尔"},{"attributeID":2345,"attributeName":"针织布"},{"attributeID":2346,"attributeName":"亚麻布"}]},{"name":"颜色","type":"key_37","items":[{"attributeID":2234,"attributeName":"纯色"},{"attributeID":2235,"attributeName":"格纹"},{"attributeID":2236,"attributeName":"印花"},{"attributeID":2237,"attributeName":"迷彩"},{"attributeID":2238,"attributeName":"字母"}]},{"name":"领型","type":"key_38","items":[{"attributeID":2254,"attributeName":"方领"},{"attributeID":2255,"attributeName":"尖领"},{"attributeID":2256,"attributeName":"扣领尖领"},{"attributeID":2257,"attributeName":"立领"},{"attributeID":2258,"attributeName":"温莎领"},{"attributeID":2259,"attributeName":"双层领"},{"attributeID":2274,"attributeName":"圆角领"},{"attributeID":2275,"attributeName":"圆角领"},{"attributeID":2276,"attributeName":"暗扣领"},{"attributeID":2277,"attributeName":"无领"}]},{"name":"版型","type":"key_39","items":[{"attributeID":2284,"attributeName":"直筒"},{"attributeID":2285,"attributeName":"宽松"},{"attributeID":2286,"attributeName":"超修身"},{"attributeID":2287,"attributeName":"特宽型"}]},{"name":"袖长","type":"key_40","items":[{"attributeID":2299,"attributeName":"中袖"},{"attributeID":2300,"attributeName":"七分袖"},{"attributeID":2301,"attributeName":"长袖"}]},{"name":"厚薄","type":"key_41","items":[{"attributeID":2316,"attributeName":"常规"},{"attributeID":2317,"attributeName":"薄款"},{"attributeID":2318,"attributeName":"厚"},{"attributeID":2319,"attributeName":"加厚"}]}]},{"name":"夹克","cate_id":28,"cateimg":"http://im01.nanyibang.com/cateimg/2016/01/29/jiake_cateB.png","filter":[{"name":"材质","type":"key_34","items":[{"attributeID":2370,"attributeName":"涤纶"},{"attributeID":2371,"attributeName":"棉"},{"attributeID":2372,"attributeName":"锦纶"},{"attributeID":2373,"attributeName":"牛仔布"},{"attributeID":2374,"attributeName":"PU皮"},{"attributeID":2375,"attributeName":"聚酯纤维"},{"attributeID":2381,"attributeName":"人造革"},{"attributeID":2382,"attributeName":"尼龙"},{"attributeID":2383,"attributeName":"毛呢布"},{"attributeID":2384,"attributeName":"卫衣布"},{"attributeID":2385,"attributeName":"腈纶"}]},{"name":"领型","type":"key_35","items":[{"attributeID":2358,"attributeName":"连帽"},{"attributeID":2359,"attributeName":"翻领"},{"attributeID":2360,"attributeName":"立领"},{"attributeID":2361,"attributeName":"棒球领"},{"attributeID":2409,"attributeName":"可脱卸帽"},{"attributeID":2410,"attributeName":"方领"},{"attributeID":2411,"attributeName":"圆领"},{"attributeID":2412,"attributeName":"无领"}]},{"name":"版型","type":"key_36","items":[{"attributeID":2353,"attributeName":"修身"},{"attributeID":2354,"attributeName":"直筒"},{"attributeID":2355,"attributeName":"宽松"},{"attributeID":2356,"attributeName":"超修身"},{"attributeID":2357,"attributeName":"特宽型"}]},{"name":"适用场景","type":"key_2386","items":[{"attributeID":2399,"attributeName":"休闲"},{"attributeID":2400,"attributeName":"上班"},{"attributeID":2401,"attributeName":"运动"},{"attributeID":2402,"attributeName":"居家"},{"attributeID":2403,"attributeName":"旅游"},{"attributeID":2404,"attributeName":"宴会"},{"attributeID":2407,"attributeName":"沙滩"},{"attributeID":2408,"attributeName":"雨天"}]}]},{"name":"卫衣","cate_id":24,"cateimg":"http://im01.nanyibang.com/cateimg/2016/09/14/weiyi.jpg","filter":[{"name":"图案","type":"key_28","items":[{"attributeID":2291,"attributeName":"纯色"},{"attributeID":2292,"attributeName":"几何图案"},{"attributeID":2293,"attributeName":"动物图案"},{"attributeID":2294,"attributeName":"条纹"},{"attributeID":2295,"attributeName":"人物"},{"attributeID":2296,"attributeName":"植物花卉"},{"attributeID":2297,"attributeName":"字母数字"},{"attributeID":2298,"attributeName":"其他"}]},{"name":"材质","type":"key_29","items":[{"attributeID":2320,"attributeName":"棉"},{"attributeID":2321,"attributeName":"针织布"},{"attributeID":2322,"attributeName":"棉毛布"},{"attributeID":2323,"attributeName":"毛圈布"},{"attributeID":2324,"attributeName":"摇粒绒"},{"attributeID":2325,"attributeName":"太空棉"},{"attributeID":2332,"attributeName":"卫衣布"},{"attributeID":2333,"attributeName":"拉绒布"},{"attributeID":2334,"attributeName":"其他"}]},{"name":"领型","type":"key_30","items":[{"attributeID":2347,"attributeName":"连帽"},{"attributeID":2348,"attributeName":"圆领"},{"attributeID":2349,"attributeName":"立领"},{"attributeID":2350,"attributeName":"翻领"},{"attributeID":2351,"attributeName":"高领"},{"attributeID":2352,"attributeName":"V领"}]},{"name":"版型","type":"key_31","items":[{"attributeID":2362,"attributeName":"修身"},{"attributeID":2363,"attributeName":"宽松"},{"attributeID":2364,"attributeName":"常规"}]},{"name":"款式","type":"key_32","items":[{"attributeID":2365,"attributeName":"套头"},{"attributeID":2366,"attributeName":"开衫"}]},{"name":"厚薄","type":"key_33","items":[{"attributeID":2367,"attributeName":"加绒"},{"attributeID":2368,"attributeName":"常规"},{"attributeID":2369,"attributeName":"薄"}]}]},{"name":"针织衫","cate_id":25,"cateimg":"http://im01.nanyibang.com/cateimg/2016/09/14/zhenzhi.jpg","filter":[{"name":"领型","type":"key_114","items":[{"attributeID":2422,"attributeName":"圆领"},{"attributeID":2423,"attributeName":"V领"},{"attributeID":2424,"attributeName":"立领"},{"attributeID":2425,"attributeName":"半高领"},{"attributeID":2426,"attributeName":"高翻领"},{"attributeID":2427,"attributeName":"半高拉链"},{"attributeID":2452,"attributeName":"高领"},{"attributeID":2453,"attributeName":"棒球领"},{"attributeID":2454,"attributeName":"衬衫领"}]},{"name":"版型","type":"key_115","items":[{"attributeID":2462,"attributeName":"直筒"},{"attributeID":2463,"attributeName":"宽松"},{"attributeID":2464,"attributeName":"超修身"},{"attributeID":2465,"attributeName":"特宽型"}]},{"name":"主材含量","type":"key_116","items":[{"attributeID":2504,"attributeName":"纯棉"},{"attributeID":2505,"attributeName":"针织布"},{"attributeID":2506,"attributeName":"棉"},{"attributeID":2507,"attributeName":"棉混纺布"},{"attributeID":2508,"attributeName":"羊毛混纺"},{"attributeID":2509,"attributeName":"羊毛"},{"attributeID":2529,"attributeName":"其他"}]},{"name":"适用场景","type":"key_117","items":[{"attributeID":2530,"attributeName":"休闲"},{"attributeID":2531,"attributeName":"日常"},{"attributeID":2532,"attributeName":"商务"},{"attributeID":2533,"attributeName":"上班"},{"attributeID":2534,"attributeName":"运动"},{"attributeID":2535,"attributeName":"居家"},{"attributeID":2543,"attributeName":"逛街"},{"attributeID":2544,"attributeName":"旅游"},{"attributeID":2545,"attributeName":"沙滩"},{"attributeID":2546,"attributeName":"出游"},{"attributeID":2547,"attributeName":"校园"}]}]},{"name":"牛仔外套","cate_id":132,"cateimg":"http://im01.nanyibang.com/cateimg/2016/01/29/niuzai_cateB.png","filter":[{"name":"版型","type":"key_123","items":[{"attributeID":2196,"attributeName":"修身"},{"attributeID":2197,"attributeName":"直筒"},{"attributeID":2198,"attributeName":"宽松"},{"attributeID":2199,"attributeName":"超修身"}]},{"name":"领型","type":"key_124","items":[{"attributeID":2213,"attributeName":"翻领"},{"attributeID":2214,"attributeName":"方领"},{"attributeID":2215,"attributeName":"立领"},{"attributeID":2216,"attributeName":"棒球领"},{"attributeID":2217,"attributeName":"尖领"},{"attributeID":2218,"attributeName":"连帽"},{"attributeID":2220,"attributeName":"圆领"},{"attributeID":2221,"attributeName":"可脱卸帽"}]},{"name":"主材含量","type":"key_125","items":[{"attributeID":2248,"attributeName":"牛仔布"},{"attributeID":2249,"attributeName":"棉"},{"attributeID":2250,"attributeName":"色丁布"},{"attributeID":2251,"attributeName":"涤纶"},{"attributeID":2252,"attributeName":"罗纹布"},{"attributeID":2253,"attributeName":"格子布"},{"attributeID":2260,"attributeName":"其他"},{"attributeID":2261,"attributeName":"混纺"},{"attributeID":2262,"attributeName":"仿呢料"},{"attributeID":2263,"attributeName":"卫衣布"},{"attributeID":2264,"attributeName":"棉涤"},{"attributeID":2265,"attributeName":"色织布"},{"attributeID":2266,"attributeName":"毛呢布"},{"attributeID":2267,"attributeName":"牛津纺"}]},{"name":"适用场景","type":"key_126","items":[{"attributeID":2225,"attributeName":"休闲"},{"attributeID":2226,"attributeName":"旅游"},{"attributeID":2227,"attributeName":"上班"},{"attributeID":2228,"attributeName":"运动"},{"attributeID":2229,"attributeName":"居家"},{"attributeID":2230,"attributeName":"日常"},{"attributeID":2239,"attributeName":"舞蹈"},{"attributeID":2240,"attributeName":"宴会"}]}]},{"name":"棒球服","cate_id":135,"cateimg":"http://im01.nanyibang.com/cateimg/2016/01/29/bangqiu_cateB.png","filter":[{"name":"领型","type":"key_118","items":[{"attributeID":2049,"attributeName":"棒球领"},{"attributeID":2050,"attributeName":"立领"},{"attributeID":2051,"attributeName":"圆领"},{"attributeID":2052,"attributeName":"连帽"},{"attributeID":2053,"attributeName":"翻领"},{"attributeID":2058,"attributeName":"方领"},{"attributeID":2059,"attributeName":"V领"}]},{"name":"版型","type":"key_119","items":[{"attributeID":2072,"attributeName":"修身"},{"attributeID":2073,"attributeName":"直筒"},{"attributeID":2074,"attributeName":"宽松"},{"attributeID":2075,"attributeName":"超修身"}]},{"name":"主材含量","type":"key_120","items":[{"attributeID":2076,"attributeName":"尼龙"},{"attributeID":2077,"attributeName":"太空棉"},{"attributeID":2078,"attributeName":"摇粒绒"},{"attributeID":2079,"attributeName":"绒布"},{"attributeID":2080,"attributeName":"混纺"},{"attributeID":2081,"attributeName":"腈纶"},{"attributeID":2098,"attributeName":"拉绒布"},{"attributeID":2099,"attributeName":"仿呢料"},{"attributeID":2100,"attributeName":"涤棉"},{"attributeID":2101,"attributeName":"涤粘"},{"attributeID":2102,"attributeName":"聚酯纤维"},{"attributeID":2103,"attributeName":"毛圈布"},{"attributeID":2110,"attributeName":"涂层布"},{"attributeID":2111,"attributeName":"罗纹布"},{"attributeID":2112,"attributeName":"色丁布"},{"attributeID":2113,"attributeName":"卫衣布"},{"attributeID":2114,"attributeName":"灯芯绒"},{"attributeID":2115,"attributeName":"棉涤"},{"attributeID":2129,"attributeName":"PU皮"},{"attributeID":2130,"attributeName":"针织布"},{"attributeID":2131,"attributeName":"毛呢布"},{"attributeID":2132,"attributeName":"格子布"},{"attributeID":2133,"attributeName":"牛仔布"},{"attributeID":2134,"attributeName":"纯羊毛（95%以上）"},{"attributeID":2140,"attributeName":"棉"}]},{"name":"热门风格","type":"key_121","items":[{"attributeID":2153,"attributeName":"嘻哈"},{"attributeID":2154,"attributeName":"英式学院"},{"attributeID":2155,"attributeName":"中国风"},{"attributeID":2156,"attributeName":"日系复古"},{"attributeID":2157,"attributeName":"基础大众"},{"attributeID":2158,"attributeName":"工装军旅"},{"attributeID":2159,"attributeName":"美式休闲"},{"attributeID":2160,"attributeName":"小清新"},{"attributeID":2161,"attributeName":"精致韩风"},{"attributeID":2162,"attributeName":"青春活力"},{"attributeID":2163,"attributeName":"潮"},{"attributeID":2164,"attributeName":"商务休闲"}]},{"name":"适用场景","type":"key_122","items":[{"attributeID":2170,"attributeName":"休闲"},{"attributeID":2171,"attributeName":"上班"},{"attributeID":2172,"attributeName":"运动"},{"attributeID":2173,"attributeName":"旅游"},{"attributeID":2174,"attributeName":"居家"},{"attributeID":2175,"attributeName":"宴会"},{"attributeID":2176,"attributeName":"沙滩"},{"attributeID":2177,"attributeName":"日常"}]}]},{"name":"毛衣","cate_id":52,"cateimg":"http://im01.nanyibang.com/cateimg/2016/01/29/maoyi_cateB.png","filter":[{"name":"材质","type":"key_21","items":[{"attributeID":2181,"attributeName":"纯棉"},{"attributeID":2182,"attributeName":"羊毛"},{"attributeID":2183,"attributeName":"羊毛混纺"},{"attributeID":2184,"attributeName":"棉混纺布"},{"attributeID":2185,"attributeName":"人造纤维"},{"attributeID":2186,"attributeName":"针织布"},{"attributeID":2190,"attributeName":"羊绒混纺"},{"attributeID":2191,"attributeName":"腈纶针织"},{"attributeID":2192,"attributeName":"纯羊绒（95%以上）"},{"attributeID":2193,"attributeName":"色织布"},{"attributeID":2194,"attributeName":"棉毛布"},{"attributeID":2195,"attributeName":"涤纶"}]},{"name":"领型","type":"key_22","items":[{"attributeID":2207,"attributeName":"圆领"},{"attributeID":2208,"attributeName":"V领"},{"attributeID":2209,"attributeName":"立领"},{"attributeID":2210,"attributeName":"翻领"},{"attributeID":2211,"attributeName":"衬衫领"},{"attributeID":2212,"attributeName":"高翻领"},{"attributeID":2219,"attributeName":"半高领"}]},{"name":"毛线粗细","type":"key_23","items":[{"attributeID":2222,"attributeName":"细毛线"},{"attributeID":2223,"attributeName":"粗毛线"},{"attributeID":2224,"attributeName":"常规毛线"}]},{"name":"厚薄","type":"key_24","items":[{"attributeID":2231,"attributeName":"常规"},{"attributeID":2232,"attributeName":"加厚"},{"attributeID":2233,"attributeName":"薄款"}]},{"name":"版型","type":"key_25","items":[{"attributeID":2245,"attributeName":"修身"},{"attributeID":2246,"attributeName":"直筒"},{"attributeID":2247,"attributeName":"宽松"}]},{"name":"热门风格","type":"key_26","items":[{"attributeID":2268,"attributeName":"英式学院"},{"attributeID":2269,"attributeName":"中国风"},{"attributeID":2270,"attributeName":"日系复古"},{"attributeID":2271,"attributeName":"基础大众"},{"attributeID":2272,"attributeName":"美式休闲"},{"attributeID":2273,"attributeName":"小清新"},{"attributeID":2278,"attributeName":"欧美简约"},{"attributeID":2279,"attributeName":"精致韩风"},{"attributeID":2280,"attributeName":"青春活力"},{"attributeID":2281,"attributeName":"潮"},{"attributeID":2282,"attributeName":"商务休闲"},{"attributeID":2283,"attributeName":"商务正装"}]},{"name":"款式细节","type":"key_27","items":[{"attributeID":2288,"attributeName":"提花"},{"attributeID":2289,"attributeName":"拼色"},{"attributeID":2290,"attributeName":"扭花"}]}]},{"name":"短袖T恤","cate_id":70,"cateimg":"http://im01.nanyibang.com/cateimg/2016/09/05/duant.jpg","filter":[{"name":"领型","type":"key_100","items":[{"attributeID":1565,"attributeName":"圆领"},{"attributeID":1566,"attributeName":"V领"},{"attributeID":1567,"attributeName":"衬衫领"},{"attributeID":1568,"attributeName":"立领"},{"attributeID":1569,"attributeName":"高领"},{"attributeID":1570,"attributeName":"连帽"},{"attributeID":1577,"attributeName":"门筒领"},{"attributeID":1578,"attributeName":"堆堆领"}]},{"name":"材质","type":"key_101","items":[{"attributeID":1617,"attributeName":"棉"},{"attributeID":1618,"attributeName":"针织布"},{"attributeID":1619,"attributeName":"棉毛布"},{"attributeID":1620,"attributeName":"棉氨"},{"attributeID":1621,"attributeName":"珠地网眼"},{"attributeID":1622,"attributeName":"汗布"},{"attributeID":1625,"attributeName":"梭织布"},{"attributeID":1626,"attributeName":"其他"}]},{"name":"版型","type":"key_102","items":[{"attributeID":1665,"attributeName":"宽松"},{"attributeID":1666,"attributeName":"直筒"},{"attributeID":1667,"attributeName":"常规"},{"attributeID":1668,"attributeName":"修身"},{"attributeID":1669,"attributeName":"紧身"},{"attributeID":1670,"attributeName":"超修身"}]}]},{"name":"风衣","cate_id":27,"cateimg":"http://im01.nanyibang.com/cateimg/2016/01/29/fengyi_cateB.png","filter":[{"name":"衣长","type":"key_142","items":[{"attributeID":2045,"attributeName":"中长款"},{"attributeID":2046,"attributeName":"长款"},{"attributeID":2047,"attributeName":"常规"},{"attributeID":2048,"attributeName":"短款"}]},{"name":"版型","type":"key_143","items":[{"attributeID":2054,"attributeName":"修身"},{"attributeID":2055,"attributeName":"直筒"},{"attributeID":2056,"attributeName":"宽松"},{"attributeID":2057,"attributeName":"特宽型"}]},{"name":"主材含量","type":"key_144","items":[{"attributeID":2066,"attributeName":"棉"},{"attributeID":2067,"attributeName":"涤纶"},{"attributeID":2068,"attributeName":"羊毛"},{"attributeID":2069,"attributeName":"毛呢大衣料"},{"attributeID":2070,"attributeName":"涤棉"},{"attributeID":2071,"attributeName":"腈纶"},{"attributeID":2091,"attributeName":"麻"},{"attributeID":2092,"attributeName":"棉麻"},{"attributeID":2093,"attributeName":"牛仔布"},{"attributeID":2094,"attributeName":"纯羊绒（95%以上）"},{"attributeID":2095,"attributeName":"仿毛料（TR)"},{"attributeID":2096,"attributeName":"涤粘"}]},{"name":"适用场景","type":"key_145","items":[{"attributeID":2118,"attributeName":"休闲"},{"attributeID":2119,"attributeName":"上班"},{"attributeID":2120,"attributeName":"旅游"},{"attributeID":2121,"attributeName":"运动"},{"attributeID":2122,"attributeName":"居家"},{"attributeID":2123,"attributeName":"宴会"},{"attributeID":2135,"attributeName":"舞蹈"},{"attributeID":2136,"attributeName":"沙滩"},{"attributeID":2137,"attributeName":"雨天"},{"attributeID":2138,"attributeName":"婚礼"},{"attributeID":2139,"attributeName":"日常"}]}]},{"name":"皮衣","cate_id":105,"cateimg":"http://im01.nanyibang.com/cateimg/2016/01/29/piyi_cateB.png","filter":[{"name":"面料","type":"key_49","items":[{"attributeID":2434,"attributeName":"PU"},{"attributeID":2435,"attributeName":"羊皮毛一体"},{"attributeID":2436,"attributeName":"绵羊皮"},{"attributeID":2437,"attributeName":"皮草"}]},{"name":"领型","type":"key_50","items":[{"attributeID":2440,"attributeName":"立领"},{"attributeID":2441,"attributeName":"翻领"},{"attributeID":2442,"attributeName":"连帽"},{"attributeID":2443,"attributeName":"可脱卸帽"},{"attributeID":2444,"attributeName":"棒球领"},{"attributeID":2445,"attributeName":"西装领"}]},{"name":"版型","type":"key_51","items":[{"attributeID":2457,"attributeName":"修身"},{"attributeID":2458,"attributeName":"直筒"},{"attributeID":2459,"attributeName":"宽松"}]},{"name":"门襟","type":"key_52","items":[{"attributeID":2580,"attributeName":"拉链"},{"attributeID":2581,"attributeName":"单排扣"}]}]},{"name":"西服","cate_id":23,"cateimg":"http://im01.nanyibang.com/cateimg/2016/01/29/xizhuang_cateB.png","filter":[{"name":"版型","type":"key_127","items":[{"attributeID":2489,"attributeName":"修身"},{"attributeID":2490,"attributeName":"直筒"},{"attributeID":2491,"attributeName":"宽松型"},{"attributeID":2492,"attributeName":"超修身"}]},{"name":"材质","type":"key_128","items":[{"attributeID":2493,"attributeName":"涤纶"},{"attributeID":2494,"attributeName":"棉"},{"attributeID":2495,"attributeName":"羊毛"},{"attributeID":2496,"attributeName":"针织布"},{"attributeID":2497,"attributeName":"腈纶"},{"attributeID":2498,"attributeName":"麻"},{"attributeID":2510,"attributeName":"羊绒"},{"attributeID":2511,"attributeName":"锦涤"},{"attributeID":2512,"attributeName":"棉麻"},{"attributeID":2513,"attributeName":"牛津纺"},{"attributeID":2514,"attributeName":"牛仔布"},{"attributeID":2515,"attributeName":"棉氨"}]},{"name":"季节","type":"key_129","items":[{"attributeID":2522,"attributeName":"秋季"},{"attributeID":2523,"attributeName":"冬季"},{"attributeID":2524,"attributeName":"春季"},{"attributeID":2525,"attributeName":"夏季"},{"attributeID":2526,"attributeName":"四季"}]},{"name":"场合","type":"key_130","items":[{"attributeID":2536,"attributeName":"上班"},{"attributeID":2537,"attributeName":"休闲"},{"attributeID":2538,"attributeName":"婚礼"},{"attributeID":2539,"attributeName":"宴会"},{"attributeID":2540,"attributeName":"旅游"},{"attributeID":2541,"attributeName":"居家"},{"attributeID":2542,"attributeName":"舞蹈"}]},{"name":"门襟","type":"key_131","items":[{"attributeID":2549,"attributeName":"一粒双排扣"},{"attributeID":2550,"attributeName":"两粒双排扣"},{"attributeID":2551,"attributeName":"三粒双排扣"},{"attributeID":2552,"attributeName":"多粒双排扣"},{"attributeID":2553,"attributeName":"一粒单排扣"},{"attributeID":2554,"attributeName":"两粒单排扣"},{"attributeID":2558,"attributeName":"三粒单排扣"},{"attributeID":2559,"attributeName":"多粒单排扣"},{"attributeID":2560,"attributeName":"拉链门襟"},{"attributeID":2561,"attributeName":"无扣"}]},{"name":"后开衩方式","type":"key_132","items":[{"attributeID":2562,"attributeName":"双开衩"},{"attributeID":2563,"attributeName":"无开衩"},{"attributeID":2564,"attributeName":"后中开衩"},{"attributeID":2565,"attributeName":"双开衩"},{"attributeID":2566,"attributeName":"无开衩"},{"attributeID":2567,"attributeName":"后中开衩"}]},{"name":"领型","type":"key_133","items":[{"attributeID":2568,"attributeName":"窄领型（7cm以下）"},{"attributeID":2569,"attributeName":"规则领型（领宽7-9cm）"},{"attributeID":2570,"attributeName":"宽领型（9-11cm）"},{"attributeID":2571,"attributeName":"加宽领型（11cm以上）"}]},{"name":"厚薄","type":"key_134","items":[{"attributeID":2572,"attributeName":"加绒加厚"},{"attributeID":2573,"attributeName":"常规"},{"attributeID":2574,"attributeName":"薄"},{"attributeID":2575,"attributeName":"厚"},{"attributeID":2576,"attributeName":"常规"},{"attributeID":2577,"attributeName":"厚"},{"attributeID":2578,"attributeName":"薄"},{"attributeID":2579,"attributeName":"加绒加厚"}]}]},{"name":"背心","cate_id":26,"cateimg":"http://im01.nanyibang.com/cateimg/2016/09/05/beixin.jpg","filter":[{"name":"材质","type":"key_103","items":[{"attributeID":1974,"attributeName":"棉"},{"attributeID":1975,"attributeName":"针织料"},{"attributeID":1976,"attributeName":"针织"},{"attributeID":1977,"attributeName":"锦棉"},{"attributeID":1978,"attributeName":"莫代尔"},{"attributeID":1979,"attributeName":"棉涤"},{"attributeID":1986,"attributeName":"涤纶"},{"attributeID":1987,"attributeName":"其他"}]},{"name":"版型","type":"key_104","items":[{"attributeID":1952,"attributeName":"宽松"},{"attributeID":1953,"attributeName":"直筒"},{"attributeID":1954,"attributeName":"修身"},{"attributeID":1955,"attributeName":"超修身"},{"attributeID":1956,"attributeName":"紧身"},{"attributeID":1957,"attributeName":"常规"},{"attributeID":1960,"attributeName":"特宽型"}]},{"name":"适用场景","type":"key_105","items":[{"attributeID":1988,"attributeName":"休闲"},{"attributeID":1989,"attributeName":"居家"},{"attributeID":1990,"attributeName":"沙滩"},{"attributeID":1991,"attributeName":"睡衣"}]}]},{"name":"短袖衬衫","cate_id":75,"cateimg":"http://im01.nanyibang.com/cateimg/2016/09/05/duanchen.jpg","filter":[{"name":"颜色","type":"key_43","items":[{"attributeID":1898,"attributeName":"纯色"},{"attributeID":1899,"attributeName":"格子"},{"attributeID":1900,"attributeName":"条纹"},{"attributeID":1901,"attributeName":"印花"}]},{"name":"面料","type":"key_44","items":[{"attributeID":1908,"attributeName":"棉"},{"attributeID":1909,"attributeName":"棉麻"},{"attributeID":1910,"attributeName":"牛仔布"}]},{"name":"领型","type":"key_45","items":[{"attributeID":1830,"attributeName":"方领"},{"attributeID":1831,"attributeName":"尖领"},{"attributeID":1832,"attributeName":"扣领尖领"},{"attributeID":1833,"attributeName":"立领"},{"attributeID":1834,"attributeName":"温莎领"},{"attributeID":1835,"attributeName":"翻领"},{"attributeID":1858,"attributeName":"温莎领"},{"attributeID":1859,"attributeName":"无领"},{"attributeID":1860,"attributeName":"圆角领"},{"attributeID":1861,"attributeName":"暗扣领"},{"attributeID":1862,"attributeName":"双层领"}]},{"name":"版型","type":"key_46","items":[{"attributeID":1875,"attributeName":"标准"},{"attributeID":1876,"attributeName":"宽松"},{"attributeID":1877,"attributeName":"特宽型"},{"attributeID":1878,"attributeName":"修身"}]},{"name":"厚薄","type":"key_47","items":[{"attributeID":1890,"attributeName":"常规"},{"attributeID":1891,"attributeName":"薄款"},{"attributeID":1892,"attributeName":"厚"},{"attributeID":1893,"attributeName":"加厚"}]}]},{"name":"POLO","cate_id":101,"cateimg":"http://im01.nanyibang.com/cateimg/2016/09/05/polo.jpg","filter":[{"name":"版型","type":"key_106","items":[{"attributeID":2010,"attributeName":"修身"},{"attributeID":2011,"attributeName":"直筒"},{"attributeID":2012,"attributeName":"宽松"},{"attributeID":2013,"attributeName":"常规"},{"attributeID":2014,"attributeName":"紧身"},{"attributeID":2015,"attributeName":"超修身"},{"attributeID":2016,"attributeName":"特宽型"}]},{"name":"材质","type":"key_107","items":[{"attributeID":2023,"attributeName":"棉"},{"attributeID":2024,"attributeName":"珠地网眼"},{"attributeID":2025,"attributeName":"针织布"},{"attributeID":2026,"attributeName":"棉毛布"},{"attributeID":2027,"attributeName":"棉氨"},{"attributeID":2028,"attributeName":"棉涤"},{"attributeID":2548,"attributeName":"其他"}]},{"name":"适用季节","type":"key_108","items":[{"attributeID":2007,"attributeName":"春季"},{"attributeID":2008,"attributeName":"夏季"},{"attributeID":2009,"attributeName":"秋季"}]},{"name":"袖长","type":"key_109","items":[{"attributeID":1998,"attributeName":"短袖"},{"attributeID":1999,"attributeName":"长袖"}]}]},{"name":"保暖内衣","cate_id":53,"cateimg":"http://im01.nanyibang.com/cateimg/2016/01/29/neiyi_cateB.png","filter":[{"name":"领型","type":"key_146","items":[{"attributeID":2302,"attributeName":"圆领"},{"attributeID":2303,"attributeName":"V领"},{"attributeID":2304,"attributeName":"高圆领"},{"attributeID":2305,"attributeName":"高领"},{"attributeID":2306,"attributeName":"低圆领"}]},{"name":"层数","type":"key_147","items":[{"attributeID":2307,"attributeName":"单层"},{"attributeID":2308,"attributeName":"双层"},{"attributeID":2309,"attributeName":"三层"}]},{"name":"材质","type":"key_148","items":[{"attributeID":2310,"attributeName":"棉"},{"attributeID":2311,"attributeName":"针织"},{"attributeID":2312,"attributeName":"发热纤维"},{"attributeID":2313,"attributeName":"莫代尔"},{"attributeID":2314,"attributeName":"腈纶"},{"attributeID":2315,"attributeName":"LYCRA莱卡"}]},{"name":"克重","type":"key_149","items":[{"attributeID":2326,"attributeName":"450g及以上"},{"attributeID":2327,"attributeName":"401g(含)-450g(含)"},{"attributeID":2328,"attributeName":"351g(含)-400g(含)"},{"attributeID":2329,"attributeName":"301g(含)-350g(含)"},{"attributeID":2330,"attributeName":"250g(含)-300g(不含)"},{"attributeID":2331,"attributeName":"180克(含)-250克(不含)"}]}]}]
     */

    @SerializedName("data")
    private List<DataBean> data;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class UserBean {
        @SerializedName("member_type")
        private String memberType;
        @SerializedName("login_status")
        private String loginStatus;
        @SerializedName("login_status_msg")
        private String loginStatusMsg;

        public String getMemberType() {
            return memberType;
        }

        public void setMemberType(String memberType) {
            this.memberType = memberType;
        }

        public String getLoginStatus() {
            return loginStatus;
        }

        public void setLoginStatus(String loginStatus) {
            this.loginStatus = loginStatus;
        }

        public String getLoginStatusMsg() {
            return loginStatusMsg;
        }

        public void setLoginStatusMsg(String loginStatusMsg) {
            this.loginStatusMsg = loginStatusMsg;
        }
    }

    public static class DataBean {
        @SerializedName("classID")
        private int classID;
        @SerializedName("name")
        private String name;
        /**
         * name : 长袖T恤
         * cate_id : 66
         * cateimg : http://im01.nanyibang.com/cateimg/2016/09/14/changt.jpg
         * filter : [{"name":"领型","type":"key_110","items":[{"attributeID":2037,"attributeName":"圆领"},{"attributeID":2038,"attributeName":"V领"},{"attributeID":2039,"attributeName":"翻领"},{"attributeID":2040,"attributeName":"立领"},{"attributeID":2041,"attributeName":"衬衫领"},{"attributeID":2042,"attributeName":"连帽"},{"attributeID":2043,"attributeName":"高领"},{"attributeID":2044,"attributeName":"其他"}]},{"name":"版型","type":"key_111","items":[{"attributeID":2060,"attributeName":"修身"},{"attributeID":2061,"attributeName":"直筒"},{"attributeID":2062,"attributeName":"宽松"},{"attributeID":2063,"attributeName":"常规"},{"attributeID":2064,"attributeName":"紧身"},{"attributeID":2065,"attributeName":"超修身"}]},{"name":"材质","type":"key_112","items":[{"attributeID":2085,"attributeName":"棉"},{"attributeID":2086,"attributeName":"针织布"},{"attributeID":2087,"attributeName":"棉毛布"},{"attributeID":2088,"attributeName":"棉氨"},{"attributeID":2089,"attributeName":"梭织布"},{"attributeID":2090,"attributeName":"涤纶"},{"attributeID":2097,"attributeName":"其他"}]},{"name":"适用季节","type":"key_113","items":[{"attributeID":2124,"attributeName":"秋季"},{"attributeID":2125,"attributeName":"春季"},{"attributeID":2126,"attributeName":"四季"},{"attributeID":2127,"attributeName":"夏季"},{"attributeID":2128,"attributeName":"冬季"}]}]
         */

        @SerializedName("categories")
        private List<CategoriesBean> categories;

        public int getClassID() {
            return classID;
        }

        public void setClassID(int classID) {
            this.classID = classID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<CategoriesBean> getCategories() {
            return categories;
        }

        public void setCategories(List<CategoriesBean> categories) {
            this.categories = categories;
        }

        public static class CategoriesBean {
            @SerializedName("name")
            private String name;
            @SerializedName("cate_id")
            private int cateId;
            @SerializedName("cateimg")
            private String cateimg;
            /**
             * name : 领型
             * type : key_110
             * items : [{"attributeID":2037,"attributeName":"圆领"},{"attributeID":2038,"attributeName":"V领"},{"attributeID":2039,"attributeName":"翻领"},{"attributeID":2040,"attributeName":"立领"},{"attributeID":2041,"attributeName":"衬衫领"},{"attributeID":2042,"attributeName":"连帽"},{"attributeID":2043,"attributeName":"高领"},{"attributeID":2044,"attributeName":"其他"}]
             */

            @SerializedName("filter")
            private List<FilterBean> filter;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getCateId() {
                return cateId;
            }

            public void setCateId(int cateId) {
                this.cateId = cateId;
            }

            public String getCateimg() {
                return cateimg;
            }

            public void setCateimg(String cateimg) {
                this.cateimg = cateimg;
            }

            public List<FilterBean> getFilter() {
                return filter;
            }

            public void setFilter(List<FilterBean> filter) {
                this.filter = filter;
            }

            public static class FilterBean {
                @SerializedName("name")
                private String name;
                @SerializedName("type")
                private String type;
                /**
                 * attributeID : 2037
                 * attributeName : 圆领
                 */

                @SerializedName("items")
                private List<ItemsBean> items;

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public List<ItemsBean> getItems() {
                    return items;
                }

                public void setItems(List<ItemsBean> items) {
                    this.items = items;
                }

                public static class ItemsBean {
                    @SerializedName("attributeID")
                    private int attributeID;
                    @SerializedName("attributeName")
                    private String attributeName;

                    public int getAttributeID() {
                        return attributeID;
                    }

                    public void setAttributeID(int attributeID) {
                        this.attributeID = attributeID;
                    }

                    public String getAttributeName() {
                        return attributeName;
                    }

                    public void setAttributeName(String attributeName) {
                        this.attributeName = attributeName;
                    }
                }
            }
        }
    }
}
