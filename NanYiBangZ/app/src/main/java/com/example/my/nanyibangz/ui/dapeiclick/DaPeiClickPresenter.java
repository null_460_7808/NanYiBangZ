package com.example.my.nanyibangz.ui.dapeiclick;

import com.example.my.nanyibangz.bean.DaPeiDownBean;

import java.util.List;
import java.util.Map;

import rx.Subscriber;

/**
 * Created by Administrator on 2016/10/8.
 */

public class DaPeiClickPresenter implements DaPeiClickContact.Presenter {
    private DaPeiClickContact.Model model;
    private DaPeiClickContact.View view;

    public DaPeiClickPresenter(DaPeiClickContact.View view) {
         model=new DaPeiClickModel();
        this.view = view;
    }

    @Override
    public void getVerticalFrmNet_DaPeiClick(Map<String, String> map) {
        model.getVerticalDaPeiClick(map, new Subscriber<DaPeiDownBean>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(DaPeiDownBean daPeiDownBean) {
                List<DaPeiDownBean.DataBean> dataBeanList=daPeiDownBean.getData();
                view.onVerticalSucess_DaPeiBeanClick(dataBeanList);
            }
        });
    }
}
