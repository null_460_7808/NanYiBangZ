package com.example.my.nanyibangz.http;

import android.support.annotation.NonNull;

import com.example.my.nanyibangz.bean.DaPeiDownBean;
import com.example.my.nanyibangz.bean.DaPeiDownClickBean;
import com.example.my.nanyibangz.bean.DaPeiUpBean;
import com.example.my.nanyibangz.bean.DanPin_Beans;
import com.example.my.nanyibangz.config.DaPeiUrlConfig;
import com.example.my.nanyibangz.config.DanPinUrlConfig;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by My on 2016/10/7.
 */

public class DaPeiHttpHelper {
    private static volatile DaPeiHttpHelper singleton;

    private DaPeiHttpHelper() {
    }
    //httpHelper对象单例模式
    public static DaPeiHttpHelper getInstance() {
        if (singleton == null) {
            synchronized (DaPeiHttpHelper.class) {
                if (singleton == null) {
                    singleton = new DaPeiHttpHelper();
                }
            }
        }
        return singleton;
    }
    //获取连接网络的retrofit对象的方法
    @NonNull
    private Retrofit getRetrofit() {
        return new Retrofit
                .Builder()
                //配置基础的url
                .baseUrl(DaPeiUrlConfig.Path.BASE_URL)
                //配置提交或者返回的参数的造型方式为gson
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().serializeNulls().create()))
                //返回值可以使用Obserable
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                //使用https时需要配置
                .client(genericClient())
                .build();
    }

    /**
     *
     * 注意！
     * 自定义了一个得到网络访问httpclient的一个方法。（不然没法添加头部）
     * @return
     */
    public static OkHttpClient genericClient() {
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request()
                                .newBuilder()
                                .addHeader("x-content", "2eae7bd7cbe2b6fa67c2189c7f581db2_b1d2ab26434d4193ab7076968b6c1af4")
                                .addHeader("x-android", "22")
                                .addHeader("x-nanyibang","2.4.0")
                                .build();
                        return chain.proceed(request);
                    }
                })
                .build();

        return httpClient;
    }

    //创建连接网络的retrofitService的方法。
    private DaPeiRetrofitService getRetrofitService() {
        return getRetrofit().create(DaPeiRetrofitService.class);
    }


    //搭配Up连接网络获取对应的字符处的方法。
    public void getVerticalDaPei(Map<String, String> params, Subscriber<DaPeiUpBean> subscriber) {
        getRetrofitService()
                .getVerticalDaPei(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
    //搭配Down连接网络获取对应的字符处的方法。
    public void getVerticalDaPeiDown(Map<String, String> params, Subscriber<DaPeiDownBean> subscriber) {
        getRetrofitService()
                .getVerticalDaPeiDown(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
    //搭配UpClick连接网络获取对应的字符处的方法。
    public void getVerticalDaPeiClick(Map<String, String> params, Subscriber<DaPeiDownBean> subscriber) {
        getRetrofitService()
                .getVerticalDaPeiClick(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
    //搭配DownClick连接网络获取对应的字符处的方法。
    public void getVerticalDaPeiDownClick(Map<String, String> params, Subscriber<DaPeiDownClickBean> subscriber) {
        getRetrofitService()
                .getVerticalDaPeiDownClick(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
    //搭配DownClick连接网络获取对应的字符处的方法。
    public void getVerticalDaPeiUpTwoClick(Map<String, String> params, Subscriber<DaPeiDownClickBean> subscriber) {
        getRetrofitService()
                .getVerticalDaPeiUpTwoClick(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
    //搭配DownClick连接网络获取对应的字符处的方法。
    public void getVerticalDaPeiRefreshClick(Map<String, String> params, Subscriber<DaPeiDownClickBean.DataBean.RelativeBean> subscriber) {
        getRetrofitService()
                .getVerticalDaPeiRefreshClick(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }


}
