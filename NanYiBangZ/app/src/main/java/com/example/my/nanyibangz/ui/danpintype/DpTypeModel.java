package com.example.my.nanyibangz.ui.danpintype;

import com.example.my.nanyibangz.bean.DanPinTypeBeans;
import com.example.my.nanyibangz.http.DanPinHttpHelper;

import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/9.
 */

public class DpTypeModel implements DpTypeContact.Model {
    @Override
    public void getVerticalDpTypeBeans(Map<String, String> params, Subscriber<DanPinTypeBeans> subscriber) {
        DanPinHttpHelper.getInstance().getVerticalDanPin_TypeClick(params,subscriber);
    }
}
