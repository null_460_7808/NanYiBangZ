package com.example.my.nanyibangz.ui.danpinserachsingle;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseFragment;
import com.example.my.nanyibangz.bean.DanPinSerachHotSingleBeans;
import com.example.my.nanyibangz.utils.FlowLayout;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by My on 2016/10/16.
 */

public class DpSerachSingleFragment extends BaseFragment implements DpSerachSingleContact.View {
    @Bind(R.id.flowlayout_hot_reserach)
    FlowLayout flowlayoutHotReserach;
    private DpSerachSinglePresent presenter;
    private LayoutInflater layoutInflater;//用来得到，dapin_hot_serach_single_txt.xml布局
  //  private String [] txtViews=null;//用来存放实体得到没每个字符串（例如飞行夹克，牛仔夹克，卫衣，等）
    private  List<DanPinSerachHotSingleBeans.DataBean.TagsBean> list;//（例如飞行夹克，牛仔夹克，卫衣，等）的集合
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.danpin_serach_dapin_fragment, null);
        ButterKnife.bind(this, view);
        InitView();//初始化控件
        return view;
    }

    //初始化控件
    public void InitView() {
        layoutInflater=LayoutInflater.from(getActivity());
        presenter=new DpSerachSinglePresent(this);
        presenter.getVerticalSerachHotSingleBeansFromNet();
    }

    //用增强for循环遍历list并生成textview,传到 流式布局中
    public void InitData(){
        for (DanPinSerachHotSingleBeans.DataBean.TagsBean beans:list){

            TextView tv= (TextView) layoutInflater.inflate(R.layout.danpin_hot_serach_single_txt,flowlayoutHotReserach,false);
            tv.setText(beans.getTitle());
            flowlayoutHotReserach.addView(tv);//添加到父view
        }
    }





    @Override
    public void getVerticalSerachHotSingleSuccess(DanPinSerachHotSingleBeans danPinSerachHotSingleBeans) {

        if (danPinSerachHotSingleBeans!=null){
            list=danPinSerachHotSingleBeans.getData().getTags();
            InitData();
        }


    }

    @Override
    public void getVerticalSerachHotSingleFailed(String msg) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
