package com.example.my.nanyibangz.ui.danpintypeclick;

import com.example.my.nanyibangz.bean.DanPinTypeClickBeans;
import com.example.my.nanyibangz.bean.DanPinTypeClickCommentBeans;
import com.example.my.nanyibangz.http.DanPinHttpHelper;

import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/10.
 */

public class DanPinTypeClickLowerModel implements DanPinTypeClickLowerContact.Model {
    //商品信息
    @Override
    public void getVerticalTypeLowerBeans(Map<String, String> params, Subscriber<DanPinTypeClickBeans> subscriber) {
        DanPinHttpHelper.getInstance().getVerticalDanPin_TypeClickLower(params,subscriber);
    }

    //商品评论
    @Override
    public void getVerticalGoodsCommentBeans(Map<String, String> params, Subscriber<DanPinTypeClickCommentBeans> subscriber) {
        DanPinHttpHelper.getInstance().getVerticalDanPin_Comment(params,subscriber);
    }
}
