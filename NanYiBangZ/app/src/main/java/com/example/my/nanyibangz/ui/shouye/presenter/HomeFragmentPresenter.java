package com.example.my.nanyibangz.ui.shouye.presenter;

import android.content.Context;
import android.util.Log;

import com.example.my.nanyibangz.bean.HomeBangBangChoice;
import com.example.my.nanyibangz.bean.HomeChoiceness;
import com.example.my.nanyibangz.bean.HomeVPBeans;
import com.example.my.nanyibangz.config.HomeUrlConfig;
import com.example.my.nanyibangz.ui.shouye.HomeFragmentContract;
import com.example.my.nanyibangz.ui.shouye.model.HomeFragmentModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/7.
 */

public class HomeFragmentPresenter implements HomeFragmentContract.IHomePresenter {
    private HomeFragmentContract.IHomeModel homeModel;
    private HomeFragmentContract.IHomeView homeView;

    public HomeFragmentPresenter(HomeFragmentContract.IHomeView homeView) {
        this.homeView = homeView;
        this.homeModel = new HomeFragmentModel();
    }

    //加载首页轮播数据(首页轮播、每日签到、特色市场部分)
    @Override
    public void homeLoadVPDataToUi(Context context) {
        //http://api.nanyibang.com/theme?age=15&channel=yingyongbao&system_name=android&type=2%2C3%2C4%2C5%2C6%2C7&versionCode=219
        Map<String,String> map = new HashMap<>();
        map.put(HomeUrlConfig.Params.AGE, "15");
        map.put(HomeUrlConfig.Params.CHANNEL, "yingyongbao");
        map.put(HomeUrlConfig.Params.SYSTEM_NAME, "android");
        map.put(HomeUrlConfig.Params.TYPE, "2,3,4,5,6,7");
        map.put(HomeUrlConfig.Params.VERSIONCODE, "219");
        homeModel.homeLoadVPData("theme",map, new Subscriber<HomeVPBeans>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.e("tag",e.getMessage()+"homeVPBeans is null");
            }

            @Override
            public void onNext(HomeVPBeans homeVPBeans) {
                if (homeVPBeans!=null){
                    List<HomeVPBeans.DataBean> list = homeVPBeans.getData();
                    homeView.homeVPDataList(list);
                }
            }
        });
    }

    //加载首页精选数据(品牌精选、搭配精选、学堂精选部分)
    @Override
    public void homeLoadChoicenessDataToUi(Context context) {
        //http://api.nanyibang.com/index-jingxuan?age=15&channel=yingyongbao&system_name=android&versionCode=219
        Map<String,String> map = new HashMap<>();
        map.put(HomeUrlConfig.Params.AGE, "15");
        map.put(HomeUrlConfig.Params.CHANNEL, "yingyongbao");
        map.put(HomeUrlConfig.Params.SYSTEM_NAME, "android");
        map.put(HomeUrlConfig.Params.VERSIONCODE, "219");
        homeModel.homeLoadChoicenessData("index-jingxuan", map, new Subscriber<HomeChoiceness>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.e("tag",e.getMessage()+"homeChoiceness is null");
            }

            @Override
            public void onNext(HomeChoiceness homeChoiceness) {
                if (homeChoiceness!=null){
                    //精选json串中包含三部分的数据 故回传整个HomeChoiceness实例
                    homeView.homeChoicenessList(homeChoiceness);
                }
            }
        });
    }

    //加载首页邦邦精选数据
    @Override
    public void homeLoadBangBangChoicenessDataToUi(Context context) {
        //http://api.nanyibang.com/campaign?age=15&campaignId=5&campaignType=jingxuan&channel=yingyongbao&page=1&system_name=android&versionCode=219
        Map<String,String> map = new HashMap<>();
        map.put(HomeUrlConfig.Params.AGE, "15");
        map.put(HomeUrlConfig.Params.CAMPAIGNID, "5");
        map.put(HomeUrlConfig.Params.CAMPAIGNTYPE, "jingxuan");
        map.put(HomeUrlConfig.Params.CHANNEL, "yingyongbao");
        map.put(HomeUrlConfig.Params.PAGE, "1");
        map.put(HomeUrlConfig.Params.SYSTEM_NAME, "android");
        map.put(HomeUrlConfig.Params.VERSIONCODE, "219");

        homeModel.homeLoadBangBangChoicenessData("campaign", map, new Subscriber<HomeBangBangChoice>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.e("tag",e.getMessage()+"homeBangBangChoice is null");
            }

            @Override
            public void onNext(HomeBangBangChoice homeBangBangChoice) {
                if (homeBangBangChoice!=null){
                    //邦邦精选json字符串中只包含该部分的数据 故改成回传list没有回传homeBangBangChoice
                    List<HomeBangBangChoice.DataBean.ItemDetailBean> list = homeBangBangChoice.getData().getItemDetail();
                    homeView.homeBangBangChoicenessList(list);
                }
            }
        });
    }
}
