package com.example.my.nanyibangz.ui.shouye.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.my.nanyibangz.R;

/**
 * 用于设置各个RecyclerView或者其它控件的子item中Url的加载
 */

public class HomeWebViewActivity extends AppCompatActivity{
    private WebView home_wv;
    private Toolbar webview_toolbar;
    private ImageView webview_back;
    private TextView webview_title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_webview);
        initWebView();
        webViewGetData();
        webViewClickBack();
    }

    //WebView的基本设置
    public void initWebView(){
        home_wv = (WebView) findViewById(R.id.home_wv);
        webview_toolbar = (Toolbar) findViewById(R.id.webview_toolbar);
        webview_title = (TextView) findViewById(R.id.webview_title);
        webview_back = (ImageView) findViewById(R.id.webview_back);

        home_wv.getSettings().setJavaScriptEnabled(true);
        home_wv.getSettings().setAllowContentAccess(true);
        home_wv.getSettings().setAllowFileAccessFromFileURLs(true);
        home_wv.getSettings().setAppCacheEnabled(true);
        home_wv.getSettings().setLoadWithOverviewMode(true);
        home_wv.getSettings().setUseWideViewPort(true);
        home_wv.getSettings().setPluginState(WebSettings.PluginState.ON);
        home_wv.getSettings().setDomStorageEnabled(true);
        //2016-10-15 解决WebView中点击事件响应问题
        home_wv.getSettings().setBuiltInZoomControls(true);
        home_wv.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        home_wv.getSettings().setSavePassword(true);
        home_wv.getSettings().setSaveFormData(true);
        home_wv.getSettings().setGeolocationEnabled(true);
        home_wv.getSettings().setGeolocationDatabasePath("/data/data/org.itri.html5webview/databases/");
        home_wv.requestFocus();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            home_wv.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        home_wv.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }

    //获取Intent中的数据设置控件
    public void webViewGetData(){
        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
        String theme_name = intent.getStringExtra("theme_name");
        if (theme_name!=null&&!theme_name.equals("")){
            webview_title.setText(theme_name);
        }
        if (!url.equals("")){
            home_wv.loadUrl(url);
        }
    }

    //返回按钮的点击事件
    public void webViewClickBack(){
        webview_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
