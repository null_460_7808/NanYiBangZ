package com.example.my.nanyibangz.ui.danpintype;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseActivity;
import com.example.my.nanyibangz.bean.DanPinTypeBeans;
import com.example.my.nanyibangz.config.DanPinUrlConfig;
import com.example.my.nanyibangz.ui.danpintypeclick.DanPinTypeClickLowerActivity;
import com.example.my.nanyibangz.utils.CustomGridView;
import com.example.my.nanyibangz.utils.CustomProgressDialog;
import com.example.my.nanyibangz.utils.CustomerNestScrollView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;

/**
 * 主要功能，接收DPragenemntVp传递过来的cate_Id,获取并且显示单品的type的gridview每一项点击事件后得到的数据。(例如点击
 * 长袖T恤，跳转显示长袖T恤这一type的所有数据)
 * 开发者：朱留宝
 * Created by My on 2016/10/9.
 */

public class DpTypeActivity extends BaseActivity implements DpTypeContact.View {

    @Bind(R.id.txt_default)
    TextView txtDefault;//推荐
    @Bind(R.id.img_default)
    ImageView imgDefault;//推荐下面图片滚动条
    @Bind(R.id.ll_dp_type_default)
    LinearLayout llDpTypeDefault;//包括推荐的线性布局
    @Bind(R.id.txt_lower)
    TextView txtLower;//最低
    @Bind(R.id.img_lower)
    ImageView imgLower;//最低下面图片滚动条
    @Bind(R.id.ll_dp_type_lower)
    LinearLayout llDpTypeLower;//包括最低的线性布局
    @Bind(R.id.txt_high)
    TextView txtHigh;//最高
    @Bind(R.id.img_high)
    ImageView imgHigh;//最高下面图片滚动条
    @Bind(R.id.ll_dp_type_high)
    LinearLayout llDpTypeHigh;//包括最高的线性布局
    @Bind(R.id.txt_hot)
    TextView txtHot;//最热
    @Bind(R.id.img_hot)
    ImageView imgHot;//最热下面图片滚动条
    @Bind(R.id.ll_dp_type_hot)
    LinearLayout llDpTypeHot;
    @Bind(R.id.dp_type_click_cgv)
    CustomGridView dpTypeClickCgv;
    String cate_id = "";//用于拼接type每一项的地址。
    String typeName = "";//用于接收传递过来的单品type名字;
    @Bind(R.id.typename)
    TextView typename;
    @Bind(R.id.appbar)
    AppBarLayout appbar;
    @Bind(R.id.fab)
    FloatingActionButton fab;
    @Bind(R.id.main_content)
    CoordinatorLayout mainContent;
    @Bind(R.id.img_type_back)
    ImageView imgTypeBack;
    @Bind(R.id.txt_pulldpup)
    TextView txtPulldpup;
    @Bind(R.id.ll_typedpfooter)
    LinearLayout llTypedpfooter;
    @Bind(R.id.nsv_dp_type_click)
    CustomerNestScrollView nsvDpTypeClick;//自定义的NestedScrollView监听事件。
    private DpTypePresenter presenter;//用于操作单品type的gridview的每一项点击事件的中间者
    private String Type = "default";//用于点击推荐,最高，最低，最热时，赋予不同的值以便拼接地址,默认是推荐=default
    private List<DanPinTypeBeans.DataBean> list;//用来接收传递的danpintypebeans集合。
    CustomProgressDialog dialog;//自定义的dialog对话框，用于gridview加载数据中的动画展示。
    private  int page=1;//带代表页数
    DanPinTypeClickAdapter danPinTypeClickAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showmeidialog();
        initview(); //接收DpFragmentVp传递过来的cate_id参数
        netUrlMap(page); //拼接得到访问网络的地址map()集合
        Type_Click_Back();//点击返回按钮跳转到之前的单品（DpFragmentVP对应的界面）

    }

    @Override
    protected void onStart() {
        super.onStart();
        SelectClick();    //点击推荐，最低，最热，最高等时的相应事件，（加载不同的数据，并且改变相应的字体颜色，和滚动条）
        scrollBottom();//监听自定义的nestedScrollView滚动控件是否滑动到底部。
        scrollUpAndDown(); //监听自定义的nestedScrollBottom的上滑，和下滑事件。
    }
    /**
     * 显示加载数据中动画
     *
     * @param
     */
    public void showmeidialog() {
        dialog = new CustomProgressDialog(this, "正在加载中", R.drawable.frame);
        dialog.show();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_dp_type_click;
    }

    //接收DpFragmentVp传递过来的cate_id参数
    public void initview() {
        Intent intent = getIntent();
        typeName = intent.getStringExtra("typename");
        cate_id = intent.getStringExtra("cateId");
        typename.setText(typeName);
    }

    //拼接得到访问网络的地址map()集合
    public void netUrlMap(int page) {
        Map<String, String> params = new HashMap<>();
        //http://api.nanyibang.com/single-product?age=19&cate_id=22&channel=oppo&hkm_sign2=fa93417d5b99379e9afaa73907e2dc10&
        // member_id=292720&member_type=member&page=1&random_key=56786&selectType=default&system_name=android&versionCode=219
        params.put(DanPinUrlConfig.Params.Age, 19 + "");
        params.put(DanPinUrlConfig.Params.Cate_Id, cate_id);
        params.put(DanPinUrlConfig.Params.Channel, DanPinUrlConfig.DefaultVaule.Channel_VALUE);
        params.put(DanPinUrlConfig.Params.Hkm_sign2, DanPinUrlConfig.DefaultVaule.Hkm_sign2_VALUE);
        params.put(DanPinUrlConfig.Params.Member_id, DanPinUrlConfig.DefaultVaule.Member_id_VALUE);
        params.put(DanPinUrlConfig.Params.Member_type, DanPinUrlConfig.DefaultVaule.Member_type_VALUE);
        params.put(DanPinUrlConfig.Params.Page, page+"");
        params.put(DanPinUrlConfig.Params.Random_key, DanPinUrlConfig.DefaultVaule.Random_key_VALUE);
        params.put(DanPinUrlConfig.Params.selectType, Type);
        params.put(DanPinUrlConfig.Params.System_name, DanPinUrlConfig.DefaultVaule.System_name_VALUE);
        params.put(DanPinUrlConfig.Params.VersionCode, DanPinUrlConfig.DefaultVaule.VersionCode_VALUE);
        presenter = new DpTypePresenter(this);
        presenter.getVetticalFromNetDpTypeBeans(params);
    }

    @Override
    public void getVerticalSuccessDpTypeBeans(List<DanPinTypeBeans.DataBean> danPinTypeClickBeansList) {
        if (danPinTypeClickBeansList.size()!=0) {
           llTypedpfooter.setVisibility(View.GONE);
            if(page==1){
                list = danPinTypeClickBeansList;
                GridViewItemClick();  //dpTypeClickCgv。中每一项的点击事件
               danPinTypeClickAdapter = new DanPinTypeClickAdapter(this, danPinTypeClickBeansList);
                dpTypeClickCgv.setAdapter(danPinTypeClickAdapter);
            }else {
                list.addAll(danPinTypeClickBeansList);
                danPinTypeClickAdapter.notifyDataSetChanged();

            }

            dialog.cancel();

        }else {
            txtPulldpup.setText("没有更多数据");
        }


    }

    @Override
    public void getVeticalFailedDpTypeBeans(String msg) {

    }

    //点击推荐，最低，最热，最高等时的相应事件，（加载不同的数据，并且改变相应的字体颜色，和滚动条）
    public void SelectClick() {
        //点击推荐时的相应事件（推荐字体颜色改变，加载推荐的数据集合）
        llDpTypeDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showmeidialog();
                txtDefault.setTextColor(getResources().getColor(R.color.colorToolbar));
                imgDefault.setVisibility(View.VISIBLE);
                txtHigh.setTextColor(getResources().getColor(R.color.color_type_click));
                imgHigh.setVisibility(View.INVISIBLE);
                txtLower.setTextColor(getResources().getColor(R.color.color_type_click));
                imgLower.setVisibility(View.INVISIBLE);
                txtHot.setTextColor(getResources().getColor(R.color.color_type_click));
                imgHot.setVisibility(View.INVISIBLE);
                Type = "default";//点击推荐时给type赋值。default
                netUrlMap(1);//重新访问网络请求数据

            }
        });
        //点击最低时的相应事件（推荐字体颜色改变，加载推荐的数据集合）
        llDpTypeLower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showmeidialog();
                txtDefault.setTextColor(getResources().getColor(R.color.color_type_click));
                imgDefault.setVisibility(View.INVISIBLE);
                txtHigh.setTextColor(getResources().getColor(R.color.color_type_click));
                imgHigh.setVisibility(View.INVISIBLE);
                txtLower.setTextColor(getResources().getColor(R.color.colorToolbar));
                imgLower.setVisibility(View.VISIBLE);
                txtHot.setTextColor(getResources().getColor(R.color.color_type_click));
                imgHot.setVisibility(View.INVISIBLE);
                Type = "low";//点击推荐时给type赋值。default
                netUrlMap(1);//重新访问网络请求数据
            }
        });
        //点击最高时的相应事件（推荐字体颜色改变，加载推荐的数据集合）
        llDpTypeHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showmeidialog();
                txtDefault.setTextColor(getResources().getColor(R.color.color_type_click));
                imgDefault.setVisibility(View.INVISIBLE);
                txtHigh.setTextColor(getResources().getColor(R.color.colorToolbar));
                imgHigh.setVisibility(View.VISIBLE);
                txtLower.setTextColor(getResources().getColor(R.color.color_type_click));
                imgLower.setVisibility(View.INVISIBLE);
                txtHot.setTextColor(getResources().getColor(R.color.color_type_click));
                imgHot.setVisibility(View.INVISIBLE);
                Type = "high";//点击推荐时给type赋值。default
                netUrlMap(1);//重新访问网络请求数据

            }
        });
        //点击最热时的相应事件（推荐字体颜色改变，加载推荐的数据集合）
        llDpTypeHot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showmeidialog();
                txtDefault.setTextColor(getResources().getColor(R.color.color_type_click));
                imgDefault.setVisibility(View.INVISIBLE);
                txtHigh.setTextColor(getResources().getColor(R.color.color_type_click));
                imgHigh.setVisibility(View.INVISIBLE);
                txtLower.setTextColor(getResources().getColor(R.color.color_type_click));
                imgLower.setVisibility(View.INVISIBLE);
                txtHot.setTextColor(getResources().getColor(R.color.colorToolbar));
                imgHot.setVisibility(View.VISIBLE);
                Type = "hot";//点击推荐时给type赋值。default
                netUrlMap(1);//重新访问网络请求数据

            }
        });
    }

    //点击返回按钮跳转到之前的单品（DpFragmentVP对应的界面）
    public void Type_Click_Back() {
        imgTypeBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  Intent intent=new Intent(DpTypeActivity.this, DPFragmentVp.class);
                startActivity(intent);*/
                finish();
                //设置activity跳转的动画。
                overridePendingTransition(R.anim.activity_left_in, R.anim.activity_right_out);
            }
        });

    }

    //dpTypeClickCgv。中每一项的点击事件
    public void GridViewItemClick() {
        dpTypeClickCgv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent1 = new Intent(DpTypeActivity.this, DanPinTypeClickLowerActivity.class);
                String item_id = list.get(position).getId() + "";
                String title=list.get(position).getTitle();
                String price=list.get(position).getCouponPrice()+"";
                String picUrl=list.get(position).getPicUrl();
                intent1.putExtra("itemId", item_id);//像购买商品详情界面传递，itemId,title,pic_url,price;
                intent1.putExtra("title",title);
                intent1.putExtra("price",price+"");
                intent1.putExtra("picUrl",picUrl);
                startActivity(intent1);
                overridePendingTransition(R.anim.activity_right_in, R.anim.activity_left_out);
            }
        });
    }
//监听自定义的NestedScrollView滑动到底部，
    public void scrollBottom() {
        nsvDpTypeClick.setOnScrollToBottomLintener(new CustomerNestScrollView.OnScrollToBottomListener() {
            @Override
            public void onScrollBottomListener(boolean isBottom) {
                if (isBottom == true) {
                    llTypedpfooter.setVisibility(View.VISIBLE);
                    //InitView_JingXuan((++page));
                    netUrlMap((++page));
                }

            }
        });
    }

    //监听自定义的nestedScrollBottom的上滑，和下滑事件。
    public  void scrollUpAndDown(){
        nsvDpTypeClick.setScrollListener(new CustomerNestScrollView.ScrollListener() {
            @Override
            public void scrollOritention(String oritentsion) {
                if("down".equals(oritentsion)){
                    //向下滑动
                    fab.show();//显示fab控件
                }else if("up".equals(oritentsion)){
                    fab.hide();//隐藏fab控件。
                }
            }
        });
    }
}
