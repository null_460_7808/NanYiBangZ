package com.example.my.nanyibangz.config;

/**
 * 男衣邦首页网络连接地址以及参数名称
 */
public class HomeUrlConfig {
    public static final String BASE_URL = "http://api.nanyibang.com/";
    public static final String BASE_WEBURL = "http://www.nanyibang.com/school/school.php?id=";

    //首页轮播部分Url(轮播、每日签到、特色市场)(五个字段)
    public static final String homeVPUrl = "http://api.nanyibang.com/theme?age=15&channel=yingyongbao&system_name=android&type=2%2C3%2C4%2C5%2C6%2C7&versionCode=219";

    //Url地址中的字段
    public static class Params{
        public static final String AGE="age";
        public static final String CHANNEL="channel";
        public static final String SYSTEM_NAME="system_name";
        public static final String TYPE="type";
        public static final String VERSIONCODE="versionCode";
        //邦邦精选部分新添字段
        public static final String CAMPAIGNID= "campaignId";
        public static final String CAMPAIGNTYPE= "campaignType";
        public static final String PAGE= "page";

        //品牌精选点击事件新添字段
        public static final String BRAND_ID = "brand_id";
        public static final String CATE_ID = "cate_id";

        //品牌精选内容中的Item点击事件新添字段(商品详情)
        public static final String ITEM_ID = "item_id";
    }

    //首页精选部分Url(品牌精选、搭配精选、学堂精选)(Url地址中无type字段 其余同上)(四个字段)
    public static final String homeChoicenessUrl = "http://api.nanyibang.com/index-jingxuan?age=15&channel=yingyongbao&system_name=android&versionCode=219";

    //首页品牌精选内容(点击事件)(七个字段)
    public static final String homeThreeBrandChoiceContentUrl = "http://api.nanyibang.com/brand-items?age=15&brand_id=8566&cate_id=1&channel=yingyongbao&page=1&system_name=android&versionCode=219";
    //首页品牌精选内容中的Item的内容 即商品详情部分
    //商品详情中的商品信息
    private static final String homeGoodsDetailsContentUrl = "http://api.nanyibang.com/items?age=15&channel=yingyongbao&item_id=406944&system_name=android&versionCode=219";
    //商品详情中的店铺评论
    private static final String homeGoodsDetailsCommentUrl = "http://api.nanyibang.com/score-comment?age=15&channel=yingyongbao&item_id=406944&system_name=android&versionCode=219";

    //首页邦邦精选Url(Url地址中无type字段 其余同上)(七个字段)
    public static final String homeBangBangChoiceUrl = "http://api.nanyibang.com/campaign?age=15&campaignId=5&campaignType=jingxuan&channel=yingyongbao&page=1&system_name=android&versionCode=219";

    //首页特色市场(潮品专区、日常洗护、精品小街、情侣专区)
    public static final String homeTideProductUrl = "http://api.nanyibang.com/campaign?age=15&campaignId=6&campaignType=chaopin&channel=yingyongbao&page=1&system_name=android&versionCode=219";
}
