package com.example.my.nanyibangz.bean;

import java.util.List;

/**
 * 首页品牌精选Item的内容
 */

public class HomeThreeBrandChoiceItem {

    /**
     * member_type : guest
     * login_status : error
     * login_status_msg : not login in
     */

    private UserBean user;
    /**
     * _id : 406944
     * title : Fun国际潮牌卫衣男秋青少年开衫修身连帽宽松长袖外套运动印花潮
     * pic_url : http://img01.taobaocdn.com/bao/uploaded/i1/TB1_x7OKXXXXXbNXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg
     * coupon_price : 559.00
     * isv_code : 0_android_brand_15
     */

    private List<DataBean> data;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class UserBean {
        private String member_type;
        private String login_status;
        private String login_status_msg;

        public String getMember_type() {
            return member_type;
        }

        public void setMember_type(String member_type) {
            this.member_type = member_type;
        }

        public String getLogin_status() {
            return login_status;
        }

        public void setLogin_status(String login_status) {
            this.login_status = login_status;
        }

        public String getLogin_status_msg() {
            return login_status_msg;
        }

        public void setLogin_status_msg(String login_status_msg) {
            this.login_status_msg = login_status_msg;
        }
    }

    public static class DataBean {
        private int _id;
        private String title;
        private String pic_url;
        private String coupon_price;
        private String isv_code;

        public int get_id() {
            return _id;
        }

        public void set_id(int _id) {
            this._id = _id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPic_url() {
            return pic_url;
        }

        public void setPic_url(String pic_url) {
            this.pic_url = pic_url;
        }

        public String getCoupon_price() {
            return coupon_price;
        }

        public void setCoupon_price(String coupon_price) {
            this.coupon_price = coupon_price;
        }

        public String getIsv_code() {
            return isv_code;
        }

        public void setIsv_code(String isv_code) {
            this.isv_code = isv_code;
        }
    }
}
