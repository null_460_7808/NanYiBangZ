package com.example.my.nanyibangz.bean;

import java.util.List;

/**
 * 商品详情的店铺评论
 */
public class HomeGoodsDetailsComment {

    /**
     * member_type : guest
     * member_id : 0
     * login_status : error
     * login_status_msg : not login in
     */

    private UserBean user;
    /**
     * shop_name : fun官方旗舰店
     * shop_score : {"service_score":"4.9","desc_score":"4.9","dispat_score":"4.9"}
     * comments : [{"name":"莫**西","content":"挺满意的。"},{"name":"幽**下","content":"货品满意，物流速度很快，给好评。"}]
     */

    private DataBean data;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class UserBean {
        private String member_type;
        private int member_id;
        private String login_status;
        private String login_status_msg;

        public String getMember_type() {
            return member_type;
        }

        public void setMember_type(String member_type) {
            this.member_type = member_type;
        }

        public int getMember_id() {
            return member_id;
        }

        public void setMember_id(int member_id) {
            this.member_id = member_id;
        }

        public String getLogin_status() {
            return login_status;
        }

        public void setLogin_status(String login_status) {
            this.login_status = login_status;
        }

        public String getLogin_status_msg() {
            return login_status_msg;
        }

        public void setLogin_status_msg(String login_status_msg) {
            this.login_status_msg = login_status_msg;
        }
    }

    public static class DataBean {
        private String shop_name;
        /**
         * service_score : 4.9
         * desc_score : 4.9
         * dispat_score : 4.9
         */

        private ShopScoreBean shop_score;
        /**
         * name : 莫**西
         * content : 挺满意的。
         */

        private List<CommentsBean> comments;

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public ShopScoreBean getShop_score() {
            return shop_score;
        }

        public void setShop_score(ShopScoreBean shop_score) {
            this.shop_score = shop_score;
        }

        public List<CommentsBean> getComments() {
            return comments;
        }

        public void setComments(List<CommentsBean> comments) {
            this.comments = comments;
        }

        public static class ShopScoreBean {
            private String service_score;
            private String desc_score;
            private String dispat_score;

            public String getService_score() {
                return service_score;
            }

            public void setService_score(String service_score) {
                this.service_score = service_score;
            }

            public String getDesc_score() {
                return desc_score;
            }

            public void setDesc_score(String desc_score) {
                this.desc_score = desc_score;
            }

            public String getDispat_score() {
                return dispat_score;
            }

            public void setDispat_score(String dispat_score) {
                this.dispat_score = dispat_score;
            }
        }

        public static class CommentsBean {
            private String name;
            private String content;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }
        }
    }
}
