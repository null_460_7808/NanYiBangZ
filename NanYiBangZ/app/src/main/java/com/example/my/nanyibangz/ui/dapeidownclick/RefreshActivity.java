package com.example.my.nanyibangz.ui.dapeidownclick;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseActivity;
import com.example.my.nanyibangz.bean.DaPeiDownClickBean;
import com.example.my.nanyibangz.config.DaPeiDownClickConfig;
import com.example.my.nanyibangz.ui.dapeirefresh.TwoClickFiveFragment;
import com.example.my.nanyibangz.ui.dapeirefresh.TwoClickFourFragment;
import com.example.my.nanyibangz.ui.dapeirefresh.TwoClickOneFragment;
import com.example.my.nanyibangz.ui.dapeirefresh.TwoClickThreeFragment;
import com.example.my.nanyibangz.ui.dapeirefresh.TwoClickTwoFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/10/11.
 */

public class RefreshActivity extends BaseActivity implements DaPeiClickDownContact.View {
    @Bind(R.id.iv_dapDownPull_back)
    ImageView ivDapDownPullBack;
    @Bind(R.id.toolbar_down)
    Toolbar toolbarDown;
    @Bind(R.id.tab_downpull)
    TabLayout tabDownpull;
    @Bind(R.id.appbar_down)
    AppBarLayout appbarDown;
    @Bind(R.id.vp_downpull)
    ViewPager vpDownpull;
    @Bind(R.id.sv_refresh)
    ScrollView svRefresh;
    private int collocation_id;
    int []str={R.drawable.choose_single_cate_2,R.drawable.choose_single_cate_5,R.drawable.choose_single_cate_7,R.drawable.choose_single_cate_9,R.drawable.choose_single_cate_71};
    private List<Fragment> fragments;
    private DownTwoClickPageAdapter  adapter;
    private List<DaPeiDownClickBean.DataBean.RelativeBean> list;
    private DaPeiClickDownContact.Presenter presenter;


    @Override
    public int getLayoutId() {
        return R.layout.dapei_pullrefresh;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        initView();
        initDate();
        initPager();
        initScroll();
        Click();
    }
    private void initView(){
        Intent intent = getIntent();
        collocation_id = intent.getExtras().getInt("collocation_id");
    }
    private void initDate(){
        presenter = new DaPeiDownClickPresenter(this);
        Map<String, String> map = new HashMap<>();
        //http://api.nanyibang.com/match-detail?age=19&channel=oppo&collocation_id=8959&hkm_sign2=7a576962b8c9e81208fbc1c50d7bde1a&member_id=292720&member_type=member&random_key=42211&system_name=android&versionCode=219
        map.put(DaPeiDownClickConfig.Params.Age, DaPeiDownClickConfig.DefaultVaule.Age_VALUE);
        map.put(DaPeiDownClickConfig.Params.Channel, DaPeiDownClickConfig.DefaultVaule.Channel_VALUE);
        map.put(DaPeiDownClickConfig.Params.Collocation_id, collocation_id + "");
        map.put(DaPeiDownClickConfig.Params.Hkm_sign2, DaPeiDownClickConfig.DefaultVaule.Hkm_sign2_VALUE);
        map.put(DaPeiDownClickConfig.Params.Member_id, DaPeiDownClickConfig.DefaultVaule.Member_id_VALUE);
        map.put(DaPeiDownClickConfig.Params.Member_type, DaPeiDownClickConfig.DefaultVaule.Member_type_VALUE);
        map.put(DaPeiDownClickConfig.Params.Random_key, DaPeiDownClickConfig.DefaultVaule.Random_key_VALUE);
        map.put(DaPeiDownClickConfig.Params.System_name, DaPeiDownClickConfig.DefaultVaule.System_name_VALUE);
        map.put(DaPeiDownClickConfig.Params.VersionCode, DaPeiDownClickConfig.DefaultVaule.VersionCode_VALUE);
        presenter.getVerticalFrmNet_DaPeiDownClick(map);
    }

    private void initTab(){
        fragments=new ArrayList<>();
        //Log.i("taga","list========="+list.size());
        for (int i=0;i<list.size();i++){
            if (list.get(i).getCateId()==2){
               /// List<DaPeiDownClickBean.DataBean.RelativeBean.ItemsBean> list1=list.get(i).getItems();
                tabDownpull.addTab(tabDownpull.newTab().setIcon(str[0]));
                TwoClickOneFragment clickFragment=new TwoClickOneFragment();
                Bundle bundle=new Bundle();
                bundle.putInt("collocation_id",collocation_id);
                clickFragment.setArguments(bundle);
                fragments.add(clickFragment);
            }
            if (list.get(i).getCateId()==5){
                tabDownpull.addTab(tabDownpull.newTab().setIcon(str[1]));
                TwoClickTwoFragment clickFragment=new TwoClickTwoFragment();
                Bundle bundle=new Bundle();
                bundle.putInt("collocation_id",collocation_id);
                clickFragment.setArguments(bundle);
                fragments.add(clickFragment);
            }
            if (list.get(i).getCateId()==7){
                tabDownpull.addTab(tabDownpull.newTab().setIcon(str[2]));
                TwoClickThreeFragment clickFragment=new TwoClickThreeFragment();
                Bundle bundle=new Bundle();
                bundle.putInt("collocation_id",collocation_id);
                clickFragment.setArguments(bundle);
                fragments.add(clickFragment);
            }
            if (list.get(i).getCateId()==9){
                tabDownpull.addTab(tabDownpull.newTab().setIcon(str[3]));
                TwoClickFourFragment clickFragment=new TwoClickFourFragment();
                Bundle bundle=new Bundle();
                bundle.putInt("collocation_id",collocation_id);
                clickFragment.setArguments(bundle);
                fragments.add(clickFragment);
            }
            if (list.get(i).getCateId()==71){
                tabDownpull.addTab(tabDownpull.newTab().setIcon(str[4]));
                TwoClickFiveFragment clickFragment=new TwoClickFiveFragment();
                Bundle bundle=new Bundle();
                bundle.putInt("collocation_id",collocation_id);
                clickFragment.setArguments(bundle);
                fragments.add(clickFragment);
            }
        }
        adapter=new DownTwoClickPageAdapter(getSupportFragmentManager(),fragments);
        vpDownpull.setAdapter(adapter);

    }
    //viewpager添加fragment
    private void initLoad(){
        fragments=new ArrayList<>();
        for (int i=0;i<list.size();i++){
            fragments.add(new TwoClickOneFragment());
        }
        adapter=new DownTwoClickPageAdapter(getSupportFragmentManager(),fragments);
        vpDownpull.setAdapter(adapter);
    }
    private void initPager(){
        // 添加viewpager的监听 参数传递tabLayout中提供的监听事件  viewpager中的page页与tab对应
        vpDownpull.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabDownpull));
        //设置tabLayout的监听事件  滑动改变tabLayout中的tab时viewpager跟随变化
        tabDownpull.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpDownpull.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    private void initScroll(){
        if (svRefresh.getScaleY()<0){
            finish();
        }
    }

    @Override
    public void onVerticalSucess_DaPeiBeanDownClick(DaPeiDownClickBean downClickBean) {
        list=downClickBean.getData().getRelative();
        initTab();
    }

    @Override
    public void onVerticalFail_DaPeiDownClick(String msg) {

    }

    @Override
    public void onVerticalSucess_DaPeiBeanDownClickGV(List<DaPeiDownClickBean.DataBean.SingleItemsBean> itemsBeanList) {

    }

    @Override
    public void onVerticalFail_DaPeiDownClickGV(String msg) {

    }

    @Override
    public void onVerticalSucess_DaPeiBeanDownClickRefresh(List<DaPeiDownClickBean.DataBean.RelativeBean.ItemsBean> itemsList) {

    }

    @Override
    public void onVerticalFail_DaPeiDownClickRefresh(String msg) {

    }

    private void Click(){
        ivDapDownPullBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
