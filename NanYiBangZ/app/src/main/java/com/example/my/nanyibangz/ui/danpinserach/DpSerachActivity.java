package com.example.my.nanyibangz.ui.danpinserach;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseActivity;
import com.example.my.nanyibangz.ui.danpinserachsingle.DpSerachSingleFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/** /**
 * 单品模块
 * 开发者：朱留宝 Created by 朱留宝 on 2016/10/14
 * 功能：主要负责，单品搜索功能（包括，单品，品牌的搜索），热门搜索的自定义流式布局，搜索记录。
 */

public class DpSerachActivity extends BaseActivity {
    @Bind(R.id.img_serach)
    ImageView imgSerach;//搜索按钮
    @Bind(R.id.toolbar_dp_serach)
    Toolbar toolbarDpSerach;
    @Bind(R.id.tablayout_dp_serach)
    TabLayout tablayoutDpSerach;
    @Bind(R.id.viewpager_dp_serach)
    ViewPager viewpagerDpSerach;//
    private String[]  tabTitles={"单品","品牌"};//存储tablayout的标题。
    private List<Fragment> fragmentList;//存储单品fragment，和品牌fragment的集合。


    @Override
    public int getLayoutId() {
        return R.layout.danpinserach;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        initView();
    }

    //初始化控件
    public void initView(){
       // getSupportActionBar(toolbarDpSerach);
        setSupportActionBar(toolbarDpSerach);
        toolbarDpSerach.setTitle("");//不设置title为空，toolbar会出现程序名
        getSupportActionBar().setDisplayShowTitleEnabled(false);
      // getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_dp);//更改toolbar的返回按钮图片
      /*  getSupportActionBar().setDisplayHomeAsUpEnabled(true);//设置toolbar的返回安妮可以使用
        getResources().getStringArray(R.array.danpin_serach);//得到array的数组，用来给tablayout头部赋值。*/
        //遍历strTablayout给tablayout头部赋值
        for(int i=0;i<tabTitles.length;i++){
            tablayoutDpSerach.addTab(tablayoutDpSerach.newTab().setText(tabTitles[i]));
        }
        DpSerachSingleFragment dpSerachSingleFragment=new DpSerachSingleFragment();//声明初始化搜索单品的fragment
        DpSerachBrandFragment dpSerachBrandFragment=new DpSerachBrandFragment();//声明和初始化搜索品牌的fragment
        fragmentList=new ArrayList<>();
        fragmentList.add(dpSerachSingleFragment);//将搜索单品添加到集合中
        fragmentList.add(dpSerachBrandFragment);//将搜索品牌添加到集合中。
        Log.i("zlb","listsize=="+fragmentList.size());
        SerachFragmentAdapter adapter=new SerachFragmentAdapter(getSupportFragmentManager());
        viewpagerDpSerach.setAdapter(adapter);//将adapter适配器添加到viewpager中。
        //将viewpager和tablayout绑定到一起。
        viewpagerDpSerach.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayoutDpSerach));
        //tablayout的滚动监听事件，用于将viewpager和tablayout绑定
        tablayoutDpSerach.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpagerDpSerach.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
    //viewpager的构造方法，又来加载不同的fragment.
    public  class SerachFragmentAdapter extends FragmentPagerAdapter{

        public SerachFragmentAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}
