package com.example.my.nanyibangz.ui.mine;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.bmob.v3.BmobUser;

/**
 * Created by My on 2016/10/5.
 * 模块--我的
 */
public class MineFragment extends BaseFragment {


    @Bind(R.id.iv_email)
    ImageView ivEmail;
    @Bind(R.id.tv_mine_shezhi)
    ImageView tvMineShezhi;
    @Bind(R.id.tv_user)
    TextView tvUser;
    @Bind(R.id.mine_load)
    TextView mineLoad;
    @Bind(R.id.mine_collection_ll)
    LinearLayout mineCollectionLl;
    public static boolean flag=false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my, null);
        ButterKnife.bind(this, view);
        //setUsers();
        initView();
        shezhi();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        BmobUser bmobUser = BmobUser.getCurrentUser();
        if (flag==false){
            if(bmobUser != null){
                String username=bmobUser.getUsername().toString();
                tvUser.setText(username);

            }else {
               // Toast.makeText(getActivity(),"你该登录了！",Toast.LENGTH_SHORT).show();
            }
        }else {
            tvUser.setText("游客");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Load();
        Call();
    }

    //点击我的收藏跳转到收藏界面
    public void initView() {
        mineCollectionLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CollectionActivity.class);
                startActivity(intent);
            }
        });

    }

    //点击登录跳转页面
    private void Load() {
        mineLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MineLoadActivity.class);
                startActivityForResult(intent, 0);

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String username = data.getStringExtra("name");
        tvUser.setText(username);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private void Call() {
        //邮箱
        ivEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:18811356421"));
                startActivity(intent);
            }
        });

    }
    private void shezhi(){
        //设置
                tvMineShezhi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BmobUser bmobUser = BmobUser.getCurrentUser();
                        if (flag == false) {
                            if (bmobUser != null) {
                                Intent intent = new Intent(getActivity(), MineSheZhi.class);
                                startActivity(intent);
                            }
                        } else {
                            Toast.makeText(getActivity(), "请先登录！！", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
