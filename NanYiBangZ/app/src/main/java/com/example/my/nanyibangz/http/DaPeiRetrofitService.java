package com.example.my.nanyibangz.http;

import com.example.my.nanyibangz.bean.DaPeiDownBean;
import com.example.my.nanyibangz.bean.DaPeiDownClickBean;
import com.example.my.nanyibangz.bean.DaPeiUpBean;
import com.example.my.nanyibangz.config.DaPeiDownClickConfig;
import com.example.my.nanyibangz.config.DaPeiDownUrlConfig;
import com.example.my.nanyibangz.config.DaPeiUpClickConfig;
import com.example.my.nanyibangz.config.DaPeiUrlConfig;

import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by My on 2016/10/5.
 */
public interface DaPeiRetrofitService {

    //搭配连接网络的rx-Observable接口

    @GET(DaPeiUrlConfig.Path.DaPei_URL_VERTICAL)
    Observable<DaPeiUpBean> getVerticalDaPei(@QueryMap() Map<String, String> params);
    //搭配down
    @GET(DaPeiDownUrlConfig.Path.DaPei_URL_VERTICAL)
    Observable<DaPeiDownBean> getVerticalDaPeiDown(@QueryMap() Map<String, String> params);
    //搭配—风格的点击接口
    @GET(DaPeiUpClickConfig.Path.DaPei_URL_VERTICAL)
    Observable<DaPeiDownBean> getVerticalDaPeiClick(@QueryMap() Map<String, String> params);
    //搭配down 一次点击
    @GET(DaPeiDownClickConfig.Path.DaPei_URL_VERTICAL)
    Observable<DaPeiDownClickBean> getVerticalDaPeiDownClick(@QueryMap() Map<String, String> params);
    //搭配up二次点击
    @GET(DaPeiDownClickConfig.Path.DaPei_URL_VERTICAL)
    Observable<DaPeiDownClickBean> getVerticalDaPeiUpTwoClick(@QueryMap() Map<String, String> params);
    //
    @GET(DaPeiDownClickConfig.Path.DaPei_URL_VERTICAL)
    Observable<DaPeiDownClickBean.DataBean.RelativeBean> getVerticalDaPeiRefreshClick(@QueryMap() Map<String, String> params);
}
