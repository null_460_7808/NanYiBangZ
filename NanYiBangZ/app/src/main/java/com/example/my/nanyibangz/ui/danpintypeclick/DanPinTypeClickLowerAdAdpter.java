package com.example.my.nanyibangz.ui.danpintypeclick;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

/**
 * Created by My on 2016/10/10.
 */

public class DanPinTypeClickLowerAdAdpter extends PagerAdapter {
    private List<ImageView> imageViewList;
    public  DanPinTypeClickLowerAdAdpter(List<ImageView> imageViewList){
        this.imageViewList=imageViewList;
    }
    @Override
    public int getCount() {
        return imageViewList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return  view==object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(imageViewList.get(position));
        return  imageViewList.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(imageViewList.get(position));
    }
}
