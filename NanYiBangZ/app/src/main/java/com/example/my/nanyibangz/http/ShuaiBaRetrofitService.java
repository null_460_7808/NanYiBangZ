package com.example.my.nanyibangz.http;

import com.example.my.nanyibangz.bean.ShuaiBaBean;
import com.example.my.nanyibangz.config.ShuaiBaUrlConfig;

import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by My on 2016/10/5.
 */
public interface ShuaiBaRetrofitService {
    @GET(ShuaiBaUrlConfig.Path.ShuaiBa_URL_VERTICAL)
    Observable<ShuaiBaBean> getVerticalShuaiBa(@QueryMap() Map<String, String> params);

}
