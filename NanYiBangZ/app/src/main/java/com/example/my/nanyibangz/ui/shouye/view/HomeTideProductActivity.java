package com.example.my.nanyibangz.ui.shouye.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.HomeTideProduct;
import com.example.my.nanyibangz.ui.shouye.HomeFragmentContract;
import com.example.my.nanyibangz.ui.shouye.adapter.HomeTideProductRecyclerAdapter;
import com.example.my.nanyibangz.ui.shouye.presenter.HomeTideProductPresenter;
import com.example.my.nanyibangz.utils.CustomLinearLayoutManager;
import com.example.my.nanyibangz.utils.DividerItemDecoration;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by My on 2016/10/17.
 */

public class HomeTideProductActivity extends AppCompatActivity implements HomeFragmentContract.IHomeTideProductView {
    @Bind(R.id.home_tideproduct_back)
    ImageView homeTideproductBack;
    @Bind(R.id.home_tideproduct_title)
    TextView homeTideproductTitle;
    @Bind(R.id.home_tideproduct_toolbar)
    Toolbar homeTideproductToolbar;
    @Bind(R.id.home_tideproduct_appbarlayout)
    AppBarLayout homeTideproductAppbarlayout;
    @Bind(R.id.home_tideproduct_rv)
    RecyclerView homeTideproductRv;
    @Bind(R.id.home_tideproduct_hsv)
    NestedScrollView homeTideproductHsv;
    @Bind(R.id.home_tideproduct_fab)
    FloatingActionButton homeTideproductFab;

    private HomeFragmentContract.IHomeTideProductPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_tideproduct);
        ButterKnife.bind(this);
        presenter = new HomeTideProductPresenter(HomeTideProductActivity.this);
        presenter.homeLoadGoodsDetailsContentDataToUi(this);

        //返回按钮
        backOnClick();
        //初始化控件
        initTideProductActivity();
        fabOnClick();
    }

    //返回按钮的点击事件
    public void backOnClick() {
        homeTideproductBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    //获取Intent意图中传递的值 初始化控件
    public void initTideProductActivity() {
        Intent intent = getIntent();
        String theme_name = intent.getStringExtra("theme_name");
        homeTideproductTitle.setText(theme_name);

        //Toolbar的设置
        homeTideproductToolbar.setTitle("");
        setSupportActionBar(homeTideproductToolbar);
    }

    //fab按钮的点击事件
    public void fabOnClick() {
        //FAB的设置
        homeTideproductFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeTideproductHsv.scrollTo(0, 0);
            }
        });
    }

    //回传获取首页特色市场潮品专场数据
    @Override
    public void homeTideProductData(List<HomeTideProduct.DataBean.ItemDetailBean> itemDetailBeanList) {
        if (itemDetailBeanList.size()!=0&&itemDetailBeanList!=null){
            HomeTideProductRecyclerAdapter adapter = new HomeTideProductRecyclerAdapter(HomeTideProductActivity.this,itemDetailBeanList);
            CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(HomeTideProductActivity.this, LinearLayoutManager.VERTICAL, false);
            homeTideproductRv.setLayoutManager(linearLayoutManager);
            //添加item之间的间距
            homeTideproductRv.addItemDecoration(new DividerItemDecoration(HomeTideProductActivity.this, DividerItemDecoration.VERTICAL_LIST, 35));
            homeTideproductRv.setItemAnimator(new DefaultItemAnimator());
            homeTideproductRv.setNestedScrollingEnabled(false);//顺滑滑动
            homeTideproductRv.setAdapter(adapter);

            //为RecyclerView的Item添加出场动画
            Animation animation = AnimationUtils.loadAnimation(HomeTideProductActivity.this,R.anim.home_tideproduct_item_anim);
            //创建LayoutAnimationController实例
            LayoutAnimationController layoutAnimationController = new LayoutAnimationController(animation);
            //添加动画插值器(加速变化(开始慢，越来越快))
            layoutAnimationController.setInterpolator(new AccelerateInterpolator());
            //设置每个Item启动动画的延时
            layoutAnimationController.setDelay(1.0f);
            //设置每个Item出场动画的顺序
            layoutAnimationController.setOrder(LayoutAnimationController.ORDER_NORMAL);
            homeTideproductRv.setLayoutAnimation(layoutAnimationController);
        }
    }
}
