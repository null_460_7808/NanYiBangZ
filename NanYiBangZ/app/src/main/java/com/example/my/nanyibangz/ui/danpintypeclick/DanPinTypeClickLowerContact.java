package com.example.my.nanyibangz.ui.danpintypeclick;

import com.example.my.nanyibangz.base.IModel;
import com.example.my.nanyibangz.base.IPresenter;
import com.example.my.nanyibangz.base.IView;
import com.example.my.nanyibangz.bean.DanPinTypeClickBeans;
import com.example.my.nanyibangz.bean.DanPinTypeClickCommentBeans;

import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/10.
 */

public class DanPinTypeClickLowerContact {
    public  interface  View extends IView{
        //商品信息
        public  void getVerticalTypeLowerSuccess(DanPinTypeClickBeans danPinTypeClickBeans);
        public  void getVerticalTypeLowerFailed(String msg);
        //商品评论
        public  void getVerticalGoodsCommentSuccess(DanPinTypeClickCommentBeans danPinTypeClickCommentBeans);
        public  void  getVerticalGoodsCommentFailed(String msg);
    }
    public  interface  Model extends IModel{
        //商品信息
        public  void  getVerticalTypeLowerBeans(Map<String, String> params, Subscriber<DanPinTypeClickBeans> subscriber);
        //商品评论
        public  void getVerticalGoodsCommentBeans(Map<String, String> params, Subscriber<DanPinTypeClickCommentBeans> subscriber);
    }

    public  interface Presenter extends IPresenter{
        //商品信息
        public  void getVerticalTypeLowerBeanFromNet(Map<String, String> params);
        //商品评论
        public  void getVerticalCommentBeansFromNet(Map<String, String> params);
    }
}
