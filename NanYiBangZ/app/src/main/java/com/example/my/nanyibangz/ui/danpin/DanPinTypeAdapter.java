package com.example.my.nanyibangz.ui.danpin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.DanPin_Beans;
import com.example.my.nanyibangz.image.DanPinImageHelper;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by My on 2016/10/7.
 */

public class DanPinTypeAdapter extends BaseAdapter {
    private Context context;
    private List<DanPin_Beans.DataBean.CategoriesBean> list;

    public DanPinTypeAdapter(List<DanPin_Beans.DataBean.CategoriesBean> list, Context context) {
        this.list = list;
        this.context = context;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder=null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.danpin_gv_type_item, null);
            viewHolder=new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder= (ViewHolder) convertView.getTag();

        }

        viewHolder.txtDanpinType.setText(list.get(position).getName());
        String url=list.get(position).getCateimg();
        DanPinImageHelper.showImage(context,url,viewHolder.imgDanpinType);
        return  convertView;
    }

    static class ViewHolder {
        @Bind(R.id.img_danpin_type)
        ImageView imgDanpinType;
        @Bind(R.id.txt_danpin_type)
        TextView txtDanpinType;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
