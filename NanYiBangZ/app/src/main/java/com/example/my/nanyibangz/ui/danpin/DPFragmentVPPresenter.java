package com.example.my.nanyibangz.ui.danpin;

import com.example.my.nanyibangz.bean.DanPin_Beans;
import com.example.my.nanyibangz.bean.DanPin_JingXuan_Beans;
import com.example.my.nanyibangz.bean.DataBean;

import java.util.List;
import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/5.
 */
public class DPFragmentVPPresenter implements DPFragmentVPContact.Presenter {
    private  DPFragmentVPContact.Modle modle;
    private  DPFragmentVPContact.View view;

    public DPFragmentVPPresenter(DPFragmentVPContact.View view) {
        this.view = view;
        modle=new DPFragmentVPModle();
    }

    @Override
    public void getVerticalFromNet_DanPin_Beans(Map<String, String> params) {
        modle.getVerticalDanPin_Beans(params, new Subscriber<DanPin_Beans>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                view.onVerticalFail_DanPin_Beans(""+e.getMessage());
            }

            @Override
            public void onNext(DanPin_Beans danPin_beans) {
                if (danPin_beans!=null){
                    List<DanPin_Beans.DataBean> list=danPin_beans.getData();
                    view.onVerticalSucess_DanPin_Beans(list);
                }


            }
        });
    }

    @Override
    public void getVerticalFromNet_DanPin_JingXuan_Beans(Map<String, String> params) {
        modle.getVerticalDanPin_JingXuan_Beans(params, new Subscriber<DanPin_JingXuan_Beans>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(DanPin_JingXuan_Beans danPin_jingXuan_beans) {
                List<DataBean> danPin_jingXuan_list=danPin_jingXuan_beans.getData();
              view.onVerticalSucess_DanPin_JingXuan_Beans(danPin_jingXuan_list);


            }
        });

    }
}
