package com.example.my.nanyibangz.http;

import android.support.annotation.NonNull;

import com.example.my.nanyibangz.bean.DanPinSerachHotSingleBeans;
import com.example.my.nanyibangz.bean.DanPinTypeBeans;
import com.example.my.nanyibangz.bean.DanPinTypeClickBeans;
import com.example.my.nanyibangz.bean.DanPinTypeClickCommentBeans;
import com.example.my.nanyibangz.bean.DanPin_Beans;
import com.example.my.nanyibangz.bean.DanPin_JingXuan_Beans;
import com.example.my.nanyibangz.config.DanPinUrlConfig;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by My on 2016/10/7.
 */

public class DanPinHttpHelper {
    private static volatile DanPinHttpHelper singleton;

    private DanPinHttpHelper() {
    }
    //httpHelper对象单例模式
    public static DanPinHttpHelper getInstance() {
        if (singleton == null) {
            synchronized (DanPinHttpHelper.class) {
                if (singleton == null) {
                    singleton = new DanPinHttpHelper();
                }
            }
        }
        return singleton;
    }
    //获取连接网络的retrofit对象的方法
    @NonNull
    private Retrofit getRetrofit() {
        return new Retrofit
                .Builder()
                //配置基础的url
                .baseUrl(DanPinUrlConfig.Path.BASE_URL)
                //配置提交或者返回的参数的造型方式为gson
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().serializeNulls().create()))
                //返回值可以使用Obserable
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                //使用https时需要配置
                .client(genericClient())
                .build();
    }

    /**
     *
     * 注意！
     * 自定义了一个得到网络访问httpclient的一个方法。（不然没法添加头部）
     * @return
     */
    public static OkHttpClient genericClient() {
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request()
                                .newBuilder()
                                .addHeader("x-content", "2eae7bd7cbe2b6fa67c2189c7f581db2_b1d2ab26434d4193ab7076968b6c1af4")
                                .addHeader("x-android", "22")
                                .addHeader("x-nanyibang","2.4.0")
                                .build();
                        return chain.proceed(request);
                    }
                })
                .build();

        return httpClient;
    }

    //创建连接网络的retrofitService的方法。
    private DanPinRetrofitService getRetrofitService() {
        return getRetrofit().create(DanPinRetrofitService.class);
    }


    //单品type连接网络获取对应的字符串的方法。
    public void getVerticalDanPin(Map<String, String> params, Subscriber<DanPin_Beans> subscriber) {
        getRetrofitService()
                .getVerticalDanPin(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    //单品type连接网络获取对应的字符串的方法。
    public void getVerticalDanPin_JingXuan(Map<String, String> params, Subscriber<DanPin_JingXuan_Beans> subscriber) {
        getRetrofitService()
                .getVerticalDanPin_JingXuan(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    //单品type的gridview点击获取对应的字符串的方法。
    public void getVerticalDanPin_TypeClick(Map<String, String> params, Subscriber<DanPinTypeBeans> subscriber) {
        getRetrofitService()
                .getVerticalDanPin_TypeClick(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
    //例如最低中每一项点击获取数据
    public void getVerticalDanPin_TypeClickLower(Map<String, String> params, Subscriber<DanPinTypeClickBeans> subscriber) {
        getRetrofitService()
              .getVerticalDanPin_TypeClickLower(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
    //例如最低中每一项点击获取数据
    public void getVerticalDanPin_Comment(Map<String, String> params, Subscriber<DanPinTypeClickCommentBeans> subscriber) {
        getRetrofitService()
                .getVerticalDanPin_Comment(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    //单品热门搜索
    public  void getVerticalDanPin_Serach_Hot_Single(Subscriber<DanPinSerachHotSingleBeans> subscriber){
        getRetrofitService()
                .getVeticalDanPin_Serach_Hot_Single()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

}
