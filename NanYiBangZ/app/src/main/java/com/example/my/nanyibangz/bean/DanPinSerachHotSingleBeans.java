package com.example.my.nanyibangz.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by My on 2016/10/16.
 */

public class DanPinSerachHotSingleBeans {
    /**
     * member_type : guest
     * login_status : error
     * login_status_msg : not login in
     */

    @SerializedName("user")
    private UserBean user;
    /**
     * url : https://ai.m.taobao.com/search.html?pid=mm_103896398_11948194_55580336&q=
     * tags : [{"title":"飞行夹克","info":"飞行夹克 男"},{"title":"牛仔夹克","info":"牛仔夹克 男"},{"title":"卫衣","info":"卫衣 男"},{"title":"迷彩风","info":"迷彩风 男"},{"title":"格子衬衫","info":"格子衬衫 男"},{"title":"潮T","info":"潮T 男"},{"title":"休闲裤","info":"休闲裤 男"},{"title":"运动裤","info":"运动裤 男"}]
     */

    @SerializedName("data")
    private DataBean data;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class UserBean {
        @SerializedName("member_type")
        private String memberType;
        @SerializedName("login_status")
        private String loginStatus;
        @SerializedName("login_status_msg")
        private String loginStatusMsg;

        public String getMemberType() {
            return memberType;
        }

        public void setMemberType(String memberType) {
            this.memberType = memberType;
        }

        public String getLoginStatus() {
            return loginStatus;
        }

        public void setLoginStatus(String loginStatus) {
            this.loginStatus = loginStatus;
        }

        public String getLoginStatusMsg() {
            return loginStatusMsg;
        }

        public void setLoginStatusMsg(String loginStatusMsg) {
            this.loginStatusMsg = loginStatusMsg;
        }
    }

    public static class DataBean {
        @SerializedName("url")
        private String url;
        /**
         * title : 飞行夹克
         * info : 飞行夹克 男
         */

        @SerializedName("tags")
        private List<TagsBean> tags;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public List<TagsBean> getTags() {
            return tags;
        }

        public void setTags(List<TagsBean> tags) {
            this.tags = tags;
        }

        public static class TagsBean {
            @SerializedName("title")
            private String title;
            @SerializedName("info")
            private String info;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getInfo() {
                return info;
            }

            public void setInfo(String info) {
                this.info = info;
            }
        }
    }
}
