package com.example.my.nanyibangz.http;

import com.example.my.nanyibangz.bean.HomeBangBangChoice;
import com.example.my.nanyibangz.bean.HomeChoiceness;
import com.example.my.nanyibangz.bean.HomeGoodsDetailsComment;
import com.example.my.nanyibangz.bean.HomeGoodsDetailsContent;
import com.example.my.nanyibangz.bean.HomeThreeBrandChoiceItem;
import com.example.my.nanyibangz.bean.HomeTideProduct;
import com.example.my.nanyibangz.bean.HomeVPBeans;
import com.example.my.nanyibangz.config.HomeUrlConfig;

import java.io.IOException;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by My on 2016/10/7.
 */

public class HomeHttpHelper {
    private static volatile HomeHttpHelper httpHelper;

    //无参构造
    private HomeHttpHelper() {
    }

    //单例模式
    public static HomeHttpHelper getInstance(){
        if (httpHelper == null){
            synchronized (HomeHttpHelper.class){
                if (httpHelper == null){
                    httpHelper = new HomeHttpHelper();
                }
            }
        }
        return httpHelper;
    }

    //使用RxJava+Retrofit方式加载数据并将数据传递到主线程
    //加载首页轮播数据
    public void getHomeVPDatas(String type, Map<String,String> map, Subscriber<HomeVPBeans> subscriber){
        getHomeRetrofitService().getHomeVPData(type,map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    //加载首页精选数据
    public void getHomeChoicenessDatas(String type, Map<String,String> map, Subscriber<HomeChoiceness> subscriber){
        getHomeRetrofitService().getHomeChoiceness(type, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    //加载首页品牌精选Item内容的数据
    public void getHomeThreeBrandChoiceItemDatas(String type, Map<String,String> map, Subscriber<HomeThreeBrandChoiceItem> subscriber){
        getHomeRetrofitService().getHomeThreeBrandChoiceItem(type,map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    //加载首页商品详情中的商品信息
    public void getHomeGoodsDetailsContentDatas(String type, Map<String,String> map, Subscriber<HomeGoodsDetailsContent> subscriber){
        getHomeRetrofitService().getHomeGoodsDetailsContent(type,map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
    //加载首页商品详情中的店铺评论
    public void getHomeGoodsDetailsCommentDatas(String type, Map<String,String> map, Subscriber<HomeGoodsDetailsComment> subscriber){
        getHomeRetrofitService().getHomeGoodsDetailsComment(type,map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    //加载首页邦邦精选数据
    public void getHomeBangBangChoicenessDatas(String type, Map<String,String> map, Subscriber<HomeBangBangChoice> subscriber){
        getHomeRetrofitService().getHomeBangBangChoiceness(type,map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    //加载首页特色市场潮品专区数据
    public void getHomeTideProductDatas(String type, Map<String,String> map, Subscriber<HomeTideProduct> subscriber){
        getHomeRetrofitService().getHomeTideProduct(type,map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    //获取Url接口对象HomeRetrofitService
    private HomeRetrofitService getHomeRetrofitService(){
        return getRetrofit().create(HomeRetrofitService.class);
    }

    //获取retrofit对象
    private Retrofit getRetrofit(){
        return new Retrofit.Builder()
                //配置基础的url
                .baseUrl(HomeUrlConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                //返回值可以使用Obserable
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                //使用https时需要配置
                .client(getOkHttpClient())
                //添加头部
                .build();
    }

    //为OkHttpClient添加头部信息 男衣邦接口限制 必须设置访问才能获取数据
    private OkHttpClient getOkHttpClient(){
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request()
                                .newBuilder()
                                .addHeader("x-content","2eae7bd7cbe2b6fa67c2189c7f581db2_b1d2ab26434d4193ab7076968b6c1af4")
                                .addHeader("x-android","22")
                                .addHeader("x-nanyibang","2.4.0")
                                .build();
                        return chain.proceed(request);
                    }
                }).build();
        return okHttpClient;
    }

    //原始的Retrofit使用方式 上述方法将其一一拆开 用于理解 一切都是变化形式而已
//    public void initRetrofit01(){
//        //构建Retrofit网络访问对象
//        Retrofit retrofit=new Retrofit
//                .Builder()
//                .baseUrl(Constant.BASE_URL)//设置访问的跟路径
//                .build();
//        //根据接口的字节码文件对象获取接口的对象
//        INewsBiz iNewsBiz=retrofit.create(INewsBiz.class);
//        //调用接口中定义的函数
//        Call<ResponseBody> call=iNewsBiz.getResonsebody();
//        /*enqueue 采用异步访问数据   execute()采用同步访问数据*/
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                if(response.isSuccess()){//成功的获取响应
//                    try {
//                        String result=response.body().string();//获取网络访问的字符串
//                        tv.setText(result);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//            }
//        });
//    }

    //RxJava观察者模式 用于理解
//    Observable.create(new Observable.OnSubscribe<Drawable>() {
//        @Override
//        public void call(Subscriber<? super Drawable> subscriber) {
//            Drawable drawable=getResources().getDrawable(R.mipmap.ic_launcher);
//            subscriber.onNext(drawable);
//            subscriber.onCompleted();
//        }
//    })
//            /**
//             * subscribeOn 指定事件发生的线程  就是Observable.OnSubscribe被调用时所处的线程
//             * Observable中的call方法所在线程*/
//            .subscribeOn(Schedulers.io())
//            /**
//             * 指定Subscriber(Observer 观察者对象)运行的线程 事件的消费线程*/
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe(new Observer<Drawable>() {
//        @Override
//        public void onCompleted() {
//        }
//
//        @Override
//        public void onError(Throwable e) {
//            e.printStackTrace();
//        }
//
//        @Override
//        public void onNext(Drawable drawable) {
//            iv.setImageDrawable(drawable);
//        }
//    });
}
