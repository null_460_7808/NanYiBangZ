package com.example.my.nanyibangz.ui.mine;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.style.AbsoluteSizeSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseActivity;

import butterknife.Bind;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;

/**
 * Created by Administrator on 2016/10/20.
 * 注册界面
 */

public class MineRegistActivity extends BaseActivity {


    @Bind(R.id.iv_shuaibaBacktou)
    ImageView ivShuaibaBacktou;
    @Bind(R.id.regist_phone)
    EditText registPhone;
    @Bind(R.id.regist_pass)
    EditText registPass;
    @Bind(R.id.regist_yanz)
    EditText registYanz;
    @Bind(R.id.tv_regist)
    TextView tvRegist;

    @Override
    public int getLayoutId() {
        return R.layout.mine_regist;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
        Regist();
    }

    private void initView() {

    }

    private void initData() {
        // 新建一个可以添加属性的文本对象
        SpannableString ss = new SpannableString("请输入11位手机号");
        SpannableString ss2 = new SpannableString("请输入密码");
        SpannableString ss3 = new SpannableString("请输入验证码");
        // 新建一个属性对象,设置文字的大小
        AbsoluteSizeSpan ass = new AbsoluteSizeSpan(14, true);
        // 附加属性到文本
        ss.setSpan(ass, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss2.setSpan(ass, 0, ss2.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss3.setSpan(ass, 0, ss3.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        // 设置hint
        registPhone.setHint(new SpannedString(ss)); // 一定要进行转换,否则属性会消失
        registPass.setHint(new SpannableString(ss2));
        registYanz.setHint(new SpannableString(ss3));
    }
    private void Regist(){
        tvRegist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BmobUser bu=new BmobUser();
                bu.setUsername(registPhone.getText().toString());
                bu.setPassword(registPass.getText().toString());
                bu.signUp(new SaveListener<BmobUser>() {
                    @Override
                    public void done(BmobUser myUser, BmobException e) {
                        if (e==null){
                            Toast.makeText(MineRegistActivity.this,"注册成功："+myUser.toString(),Toast.LENGTH_SHORT).show();
                            MineRegistActivity.this.finish();
                        }else {
                            Log.e("tag","saveError!");
                        }
                    }
                });
            }
        });
    }
}
