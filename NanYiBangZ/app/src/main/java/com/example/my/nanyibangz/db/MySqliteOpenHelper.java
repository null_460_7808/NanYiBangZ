package com.example.my.nanyibangz.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.my.nanyibangz.bean.CollectionBeans;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by Administrator on 2016/10/19.
 */

public class MySqliteOpenHelper extends OrmLiteSqliteOpenHelper {

    /**
     * volatile
     *  在当前的Java内存模型下，线程可以把变量保存在本地内存（比如机器的寄存器）中，而不是直接在主存中进行读写。这就可能造成一个线程在主存中修改了一个变量的值，
     *  而另外一个线程还继续使用它在寄存器中的变量值的拷贝，造成数据的不一致。
     要解决这个问题，只需要像在本程序中的这样，把该变量声明为volatile（不稳定的）即可，这就指示JVM，这个变量是不稳定的，每次使用它都到主存中进行读取。一般说来，
     多任务环境下各任务间共享的标志都应该加volatile修饰。
     Volatile修饰的成员变量在每次被线程访问时，都强迫从共享内存中重读该成员变量的值。而且，当成员变量发生变化时，强迫线程将变化值回写到共享内存。这样在任何时刻，
     两个不同的线程总是看到某个成员变量的同一个值。
     */
    private static volatile  MySqliteOpenHelper helper;
    private Dao<CollectionBeans,Long>collectionDao;//针对collectionbeans表的操作对象Dao;
    //定义数据库的名称
    private static final String DB_NAME="db_collections";
    //定义数据库的版本
    private static final int DB_VERSION=1;    /**
     * android 上下文对象， 数据库文件名称，
     * 游标工厂（如果传入null则表示使用默认的游标工厂，
     * 我们一般传入null），
     * 数据库版本号（数据库版本号要比0大，如果为0则会报错）
     *
     */

    public MySqliteOpenHelper(Context context){
        super(context,DB_NAME,null,DB_VERSION);

    }
    public MySqliteOpenHelper(Context context, String databaseName, SQLiteDatabase.CursorFactory factory, int databaseVersion) {
        super(context, databaseName, factory, databaseVersion);
    }

    /**
     * 采用单例的模式构建帮助类的对象  保证app中只存在一个helper类对象
     */


    public  static  MySqliteOpenHelper getInstance(Context context){
        if (helper==null){
            //为了避免多个线程使用，会产生多个helper对象，所以用synchronized添加锁。
          synchronized (MySqliteOpenHelper.class){
              if(helper==null){
                  helper=new MySqliteOpenHelper(context);
              }
          }
        }
        return  helper;
    }
    /**
     * 针对每个bean创建一个xxxDao对象处理当前的bean的数据库操作
     */
    public Dao<CollectionBeans,Long> getCollectionDao() throws SQLException {
        if(collectionDao==null){
            collectionDao=getDao(CollectionBeans.class);
        }
        return  collectionDao;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        //创建数据表createTable(连接对象,需要创建表的实体类字节码对象)
        try {
            TableUtils.createTable(connectionSource,CollectionBeans.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

        if (newVersion>oldVersion){
            try {
                TableUtils.dropTable(connectionSource,CollectionBeans.class,true);
                TableUtils.createTable(connectionSource,CollectionBeans.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
}
