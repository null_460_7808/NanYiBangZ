package com.example.my.nanyibangz.bean;

import java.util.List;

/**
 * 潮品专区
 */
public class HomeTideProduct {

    /**
     * member_type : guest
     * login_status : error
     * login_status_msg : not login in
     */

    private UserBean user;
    /**
     * campaignKind : {"campain_name":"潮品","campain_icon":"","campain_icon2":"","campain_color":"","show_price":false}
     * itemDetail : [{"_id":415876,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1oS.rNFXXXXcBaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"潮牌Life After Life/LAL 秋冬新品 粗花呢西装领大衣男士外套\n","coupon_price":"539.00","price":"769.00","saveCount":62,"isv_code":"0_android_chaopin_29","description":"潮牌大衣"},{"_id":335165,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1wISoLXXXXXaxXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"GENANX闪电潮牌黑白条纹风衣男 潮流中长款青年风衣大衣外套男\n","coupon_price":"528.00","price":"528.00","saveCount":105,"isv_code":"0_android_chaopin_29","description":"黑白条纹风衣"},{"_id":415875,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1DheJNpXXXXcvaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"pmao潮牌大码经典长款翻领风衣加肥加大宽松外套胖子休闲大衣男装\n","coupon_price":"429.00","price":"429.00","saveCount":101,"isv_code":"0_android_chaopin_29","description":"潮牌大码翻领风衣"},{"_id":415874,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1puIXNFXXXXcJXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"潮牌日系风衣男外套军绿色纯色中长款青少年学生秋季连帽韩版大衣\n","coupon_price":"368.00","price":"568.00","saveCount":186,"isv_code":"0_android_chaopin_29","description":"连帽韩版大衣"},{"_id":305390,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1iIw.JVXXXXa5XpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"江南先生原创男装 欧美英伦味翻领潮牌走秀风衣韩版羊毛呢大衣男","coupon_price":"228.00","price":"448.00","saveCount":34,"isv_code":"0_android_chaopin_29"},{"_id":415873,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1059uNXXXXXckaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"HOZZS/汉哲思秋冬季青年男士风衣中长款韩版连帽外套潮牌披风大衣\n","coupon_price":"969.00","price":"1299.00","saveCount":114,"isv_code":"0_android_chaopin_29","description":"潮牌披风大衣"},{"_id":405028,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1VzUzMVXXXXb5XXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"HOZZS/汉哲思秋季青年男士风衣中长款韩版棒球领外套潮牌披风大衣\n","coupon_price":"769.00","price":"769.00","saveCount":4,"isv_code":"0_android_chaopin_29","description":"潮牌披风大衣"},{"_id":415872,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1LFoKNXXXXXataFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"GENANX闪电潮牌毛呢子风衣男中长款个性印花单排扣加厚男士外套\n","coupon_price":"598.00","price":"598.00","saveCount":110,"isv_code":"0_android_chaopin_29","description":"潮牌毛呢子风衣"},{"_id":415871,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1pSjUNpXXXXcmapXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"viishow潮牌2016冬季新款 青年风衣男士休闲时尚外套修身大衣男\n","coupon_price":"458.90","price":"459.00","saveCount":50,"isv_code":"0_android_chaopin_29","description":"青年风衣"},{"_id":317664,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1e6dkJpXXXXauXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"DUSTY秋季男士复古拼接拼色撞色羊毛毛衣日系圆领套头毛衫潮牌\n","coupon_price":"329.00","price":"549.00","saveCount":11,"isv_code":"0_android_chaopin_29","description":"撞色毛衣"},{"_id":407563,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1uag9MVXXXXXpXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"【INXX】Stand by INXX 设计师潮牌冬款圆领毛衣中性风XM63071555\n","coupon_price":"552.00","price":"690.00","saveCount":1,"isv_code":"0_android_chaopin_29","description":"圆领毛衣"},{"_id":415733,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1B3mlNFXXXXb1XFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"NE伯爵卓尔2016秋冬新品 复古潮牌厚毛衣男 英伦圆领纯色图案毛衫\n","coupon_price":"269.00","price":"269.00","saveCount":156,"isv_code":"0_android_chaopin_29","description":"潮牌厚毛衣"},{"_id":415729,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1fvq3NpXXXXaRXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"UO秋冬青年韩版圆领厚毛衣男潮牌字母数字印花套头休闲黑色毛线衣\n","coupon_price":"259.00","price":"599.00","saveCount":152,"isv_code":"0_android_chaopin_29","description":"数字印花套头毛衣"},{"_id":415724,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1Rd2MNFXXXXavXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋冬款复古印花西装领呢子大衣英伦风灰色轻奢华毛呢修身外套男潮\n","coupon_price":"339.00","price":"339.00","saveCount":148,"isv_code":"0_android_chaopin_29","description":"呢子大衣"},{"_id":415719,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1dgvZNFXXXXa0XXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"NE伯爵卓尔2016秋冬品 复古男装潮牌毛衣 高领男毛衣厚针织衫\n","coupon_price":"152.00","price":"169.00","saveCount":159,"isv_code":"0_android_chaopin_29","description":"潮牌毛衣"},{"_id":415718,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1G9ikNFXXXXbmaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"NMAX大码男装潮牌 冬装加肥加大直筒裤子潮胖子时尚宽松休闲长裤\n","coupon_price":"189.00","price":"189.00","saveCount":155,"isv_code":"0_android_chaopin_29","description":"潮牌休闲长裤"},{"_id":415717,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1Awx2NXXXXXclXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"潮牌Life After Life/LAL 秋装新款 纯色束口休闲九分裤 男士裤子\n","coupon_price":"239.00","price":"419.00","saveCount":137,"isv_code":"0_android_chaopin_29","description":"休闲九分裤"},{"_id":415716,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1HMZrLXXXXXc0XpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"CNCN男装 秋季潮牌休闲裤 青年束脚裤 男哈伦裤小脚裤子CNAK31001\n","coupon_price":"690.00","price":"690.00","saveCount":148,"isv_code":"0_android_chaopin_29","description":"潮牌休闲裤"},{"_id":412566,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1uxh.NXXXXXb_XXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"GENANX闪电潮牌黑色裤子男 休闲裤直筒纯色简约百搭潮流休闲裤男\nGENANX闪电潮牌黑色裤子男 休闲裤直筒纯色简约百搭潮流休闲裤男\nGENANX闪电潮牌黑色裤子男 休闲裤直筒纯色简约百搭潮流休闲裤男\n","coupon_price":"258.00","price":"258.00","saveCount":4,"isv_code":"0_android_chaopin_29","description":"潮流休闲裤"},{"_id":415468,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1LGmsNpXXXXXJXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"YOHO有货 潮牌BLACKJACK/秋季新品 简约趣味印花长袖衬衫男士衬衣\n","coupon_price":"456.00","price":"519.00","saveCount":177,"isv_code":"0_android_chaopin_29","description":"印花长袖衬衫"}]
     */

    private DataBean data;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class UserBean {
        private String member_type;
        private String login_status;
        private String login_status_msg;

        public String getMember_type() {
            return member_type;
        }

        public void setMember_type(String member_type) {
            this.member_type = member_type;
        }

        public String getLogin_status() {
            return login_status;
        }

        public void setLogin_status(String login_status) {
            this.login_status = login_status;
        }

        public String getLogin_status_msg() {
            return login_status_msg;
        }

        public void setLogin_status_msg(String login_status_msg) {
            this.login_status_msg = login_status_msg;
        }
    }

    public static class DataBean {
        /**
         * campain_name : 潮品
         * campain_icon :
         * campain_icon2 :
         * campain_color :
         * show_price : false
         */

        private CampaignKindBean campaignKind;
        /**
         * _id : 415876
         * productType : 1
         * pic_url : http://img02.taobaocdn.com/bao/uploaded/i2/TB1oS.rNFXXXXcBaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg
         * title : 潮牌Life After Life/LAL 秋冬新品 粗花呢西装领大衣男士外套

         * coupon_price : 539.00
         * price : 769.00
         * saveCount : 62
         * isv_code : 0_android_chaopin_29
         * description : 潮牌大衣
         */

        private List<ItemDetailBean> itemDetail;

        public CampaignKindBean getCampaignKind() {
            return campaignKind;
        }

        public void setCampaignKind(CampaignKindBean campaignKind) {
            this.campaignKind = campaignKind;
        }

        public List<ItemDetailBean> getItemDetail() {
            return itemDetail;
        }

        public void setItemDetail(List<ItemDetailBean> itemDetail) {
            this.itemDetail = itemDetail;
        }

        public static class CampaignKindBean {
            private String campain_name;
            private String campain_icon;
            private String campain_icon2;
            private String campain_color;
            private boolean show_price;

            public String getCampain_name() {
                return campain_name;
            }

            public void setCampain_name(String campain_name) {
                this.campain_name = campain_name;
            }

            public String getCampain_icon() {
                return campain_icon;
            }

            public void setCampain_icon(String campain_icon) {
                this.campain_icon = campain_icon;
            }

            public String getCampain_icon2() {
                return campain_icon2;
            }

            public void setCampain_icon2(String campain_icon2) {
                this.campain_icon2 = campain_icon2;
            }

            public String getCampain_color() {
                return campain_color;
            }

            public void setCampain_color(String campain_color) {
                this.campain_color = campain_color;
            }

            public boolean isShow_price() {
                return show_price;
            }

            public void setShow_price(boolean show_price) {
                this.show_price = show_price;
            }
        }

        public static class ItemDetailBean {
            private int _id;
            private int productType;
            private String pic_url;
            private String title;
            private String coupon_price;
            private String price;
            private int saveCount;
            private String isv_code;
            private String description;

            public int get_id() {
                return _id;
            }

            public void set_id(int _id) {
                this._id = _id;
            }

            public int getProductType() {
                return productType;
            }

            public void setProductType(int productType) {
                this.productType = productType;
            }

            public String getPic_url() {
                return pic_url;
            }

            public void setPic_url(String pic_url) {
                this.pic_url = pic_url;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getCoupon_price() {
                return coupon_price;
            }

            public void setCoupon_price(String coupon_price) {
                this.coupon_price = coupon_price;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public int getSaveCount() {
                return saveCount;
            }

            public void setSaveCount(int saveCount) {
                this.saveCount = saveCount;
            }

            public String getIsv_code() {
                return isv_code;
            }

            public void setIsv_code(String isv_code) {
                this.isv_code = isv_code;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }
        }
    }
}
