package com.example.my.nanyibangz.ui.danpin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseFragment;
import com.example.my.nanyibangz.ui.danpinserach.DpSerachActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by My on 2016/10/5.
 */
public class DPFragment extends BaseFragment {
    /**
     * 单品模块
     * 开发者：朱留宝 Created by 朱留宝 on 2016/10/4
     * 功能：主要负责，单品界面的加载（包括，上衣，裤子，鞋子，配饰，男包等的分类）
     */
    @Bind(R.id.toolbar_dp)//单品-toolbar
            Toolbar toolbarDp;
    @Bind(R.id.tablayout_dp)//单品-tablayout
            TabLayout tablayoutDp;
    @Bind(R.id.viewpager_dp)//单品页--viewpager
            ViewPager viewpagerDp;
    View view;
    String[] strTablayout;//存储tablayout的头部标题
    @Bind(R.id.dp_main_serach)
    ImageView dpMainSerach;
    private List<DPFragmentVp> list;//存放DP_Fragment_Vp对象的集合
    private DP_Fragment_VP_Adapter adapter;  //viewpager的适配器对象

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dp, null);
        ButterKnife.bind(this, view);
        initview();
        ImageSerach();//点击跳转到搜索的activity
        return view;
    }

    //初始化控件
    public void initview() {
        //fragment中不能使用setSupportActionBar（）方法，想使用必须继承AppCompatActivity。
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbarDp);//设置toolbar
        toolbarDp.setTitle("");
        activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_dp);//更改toolbar的返回按钮图片
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);//设置toolbar的返回安妮可以使用
        strTablayout = getActivity().getResources().getStringArray(R.array.danpin_tablayout);//得到array的数组，用来给tablayout头部赋值。
        //遍历strTablayout给tablayout头部赋值
        for (int i = 0; i < strTablayout.length; i++) {
            tablayoutDp.addTab(tablayoutDp.newTab().setText(strTablayout[i]));
        }
        DPFragmentVp dp_fragment_vp;
        list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Bundle bundle = new Bundle();
            bundle.putInt("index", i);
            dp_fragment_vp = new DPFragmentVp();
            dp_fragment_vp.setArguments(bundle);
            list.add(dp_fragment_vp);

        }

        adapter = new DP_Fragment_VP_Adapter(getActivity().getSupportFragmentManager());
        viewpagerDp.setAdapter(adapter);//将adapter适配器添加到viewpager中。
        viewpagerDp.setOffscreenPageLimit(5);
        //将viewpager和tablayout绑定到一起。
        viewpagerDp.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayoutDp));
        //tablayout的滚动监听事件，用于将viewpager和tablayout绑定
        tablayoutDp.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpagerDp.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    //viewpager的构造方法，又来加载不同的fragment.
    public class DP_Fragment_VP_Adapter extends FragmentPagerAdapter {

        public DP_Fragment_VP_Adapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return list.get(position);
        }

        @Override
        public int getCount() {
            return list.size();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
    //点击搜索按钮，跳转至搜索的activity
    public  void ImageSerach(){
        dpMainSerach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), DpSerachActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.activity_right_in,R.anim.activity_left_out);
                //设置跳转动画，左出，右进。，
            }
        });

    }
}
