package com.example.my.nanyibangz.ui.dapeiclick;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseActivity;
import com.example.my.nanyibangz.bean.DaPeiDownClickBean;
import com.example.my.nanyibangz.config.DaPeiDownClickConfig;
import com.example.my.nanyibangz.http.DaPeiRetrofitService;
import com.example.my.nanyibangz.ui.dapeidownclick.DaPeiClickDownContact;
import com.example.my.nanyibangz.ui.dapeidownclick.DaPeiDownClickActivity;
import com.example.my.nanyibangz.ui.dapeidownclick.DaPeiDownClickPresenter;
import com.example.my.nanyibangz.ui.dapeidownclick.DownClickAdapter;
import com.example.my.nanyibangz.utils.MyGirdView;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/10/10.
 */

public class TwoClickActivity extends BaseActivity implements DaPeiClickDownContact.View {

    @Bind(R.id.iv_dapDownClick_back)
    ImageView ivDapDownClickBack;
    @Bind(R.id.iv_dapDownClick)
    ImageView ivDapDownClick;
    @Bind(R.id.tv_dapLove)
    TextView tvDapLove;
    @Bind(R.id.tv_dapDclickTitle)
    TextView tvDapDclickTitle;
    @Bind(R.id.layout_dapDown)
    LinearLayout layoutDapDown;
    @Bind(R.id.tv_infoDapDown)
    TextView tvInfoDapDown;
    @Bind(R.id.gv_dapDClick)
    MyGirdView gvDapDClick;
    private DaPeiClickDownContact.Presenter presenter;
    private int collocation_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        initView();
        initDate();
        Click();
        Log.i("tags","cid========="+collocation_id);
    }

    @Override
    public int getLayoutId() {
        return R.layout.dapei_down_click;
    }
    //接收来自DaPeiClickDown 的collocation_id；
    private void initView() {
        Intent intent = getIntent();
        collocation_id=intent.getExtras().getInt("collocation_id");
    }
    //map 连接访问路径
    private void initDate() {
        presenter = new DaPeiDownClickPresenter(this);
        Map<String, String> map = new HashMap<>();
        //http://api.nanyibang.com/match-detail?age=19&channel=oppo&collocation_id=8959&hkm_sign2=7a576962b8c9e81208fbc1c50d7bde1a&member_id=292720&member_type=member&random_key=42211&system_name=android&versionCode=219
        map.put(DaPeiDownClickConfig.Params.Age, DaPeiDownClickConfig.DefaultVaule.Age_VALUE);
        map.put(DaPeiDownClickConfig.Params.Channel, DaPeiDownClickConfig.DefaultVaule.Channel_VALUE);
        map.put(DaPeiDownClickConfig.Params.Collocation_id, collocation_id+"");
        map.put(DaPeiDownClickConfig.Params.Hkm_sign2, DaPeiDownClickConfig.DefaultVaule.Hkm_sign2_VALUE2);
        map.put(DaPeiDownClickConfig.Params.Member_id, DaPeiDownClickConfig.DefaultVaule.Member_id_VALUE);
        map.put(DaPeiDownClickConfig.Params.Member_type, DaPeiDownClickConfig.DefaultVaule.Member_type_VALUE);
        map.put(DaPeiDownClickConfig.Params.Random_key, DaPeiDownClickConfig.DefaultVaule.Random_key_VALUE2);
        map.put(DaPeiDownClickConfig.Params.System_name, DaPeiDownClickConfig.DefaultVaule.System_name_VALUE);
        map.put(DaPeiDownClickConfig.Params.VersionCode, DaPeiDownClickConfig.DefaultVaule.VersionCode_VALUE);
        presenter.getVerticalFrmNet_DaPeiDownClick(map);
        //-------------------------
        Map<String, String> maps = new HashMap<>();
        //http://api.nanyibang.com/match-detail?age=19&channel=oppo&collocation_id=8959&hkm_sign2=7a576962b8c9e81208fbc1c50d7bde1a&member_id=292720&member_type=member&random_key=42211&system_name=android&versionCode=219
        maps.put(DaPeiDownClickConfig.Params.Age, DaPeiDownClickConfig.DefaultVaule.Age_VALUE);
        maps.put(DaPeiDownClickConfig.Params.Channel, DaPeiDownClickConfig.DefaultVaule.Channel_VALUE);
        maps.put(DaPeiDownClickConfig.Params.Collocation_id, collocation_id+"");
        maps.put(DaPeiDownClickConfig.Params.Hkm_sign2, DaPeiDownClickConfig.DefaultVaule.Hkm_sign2_VALUE2);
        maps.put(DaPeiDownClickConfig.Params.Member_id, DaPeiDownClickConfig.DefaultVaule.Member_id_VALUE);
        maps.put(DaPeiDownClickConfig.Params.Member_type, DaPeiDownClickConfig.DefaultVaule.Member_type_VALUE);
        maps.put(DaPeiDownClickConfig.Params.Random_key, DaPeiDownClickConfig.DefaultVaule.Random_key_VALUE2);
        maps.put(DaPeiDownClickConfig.Params.System_name, DaPeiDownClickConfig.DefaultVaule.System_name_VALUE);
        maps.put(DaPeiDownClickConfig.Params.VersionCode, DaPeiDownClickConfig.DefaultVaule.VersionCode_VALUE);
        presenter.getVerticalFrmNet_DaPeiDownClickGV(maps);
    }

    @Override
    public void onVerticalSucess_DaPeiBeanDownClick(DaPeiDownClickBean downClickBean) {
        String path=downClickBean.getData().getBigImage();
        Log.i("tag","path========="+ path);
        int width=downClickBean.getData().getWidth();
        int height=downClickBean.getData().getHeight();
        Picasso.with(this).load(path).resize(width,height).into(ivDapDownClick);
        Log.i("zlb","count="+downClickBean.getData().getSaveCount()+"");
        tvDapLove.setText(downClickBean.getData().getSaveCount()+"");
        tvDapDclickTitle.setText(downClickBean.getData().getInfo());
        tvInfoDapDown.setText(downClickBean.getData().getLongInfo());
        //动态增加textView,不太完美
        List<DaPeiDownClickBean.DataBean.TagsBean> tagsBeen=downClickBean.getData().getTags();
        for (int i=0;i<tagsBeen.size();i++){
            TextView textView=new TextView(this);
            // ListView.LayoutParams layoutParams = new ListView.LayoutParams(ListView.LayoutParams.MATCH_PARENT, ListView.LayoutParams.WRAP_CONTENT);
            LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            textView.setLayoutParams(layoutParams);
            textView.setText(tagsBeen.get(i).getTagName());
            textView.setPadding(7,2,7,2);
            layoutParams.setMargins(6,2,6,2);
            textView.setGravity(Gravity.CENTER);
            textView.setTextColor(getResources().getColor(R.color.colorToolbar));
            textView.setBackground(getResources().getDrawable(R.drawable.border));
            textView.setTextSize(14);
            layoutDapDown.addView(textView);
        }
    }

    @Override
    public void onVerticalFail_DaPeiDownClick(String msg) {

    }

    @Override
    public void onVerticalSucess_DaPeiBeanDownClickGV(List<DaPeiDownClickBean.DataBean.SingleItemsBean> itemsBeanList) {
        DownClickAdapter adapter=new DownClickAdapter(TwoClickActivity.this,itemsBeanList);
        gvDapDClick.setAdapter(adapter);
    }

    @Override
    public void onVerticalFail_DaPeiDownClickGV(String msg) {

    }

    @Override
    public void onVerticalSucess_DaPeiBeanDownClickRefresh(List<DaPeiDownClickBean.DataBean.RelativeBean.ItemsBean>itemsList) {

    }

    @Override
    public void onVerticalFail_DaPeiDownClickRefresh(String msg) {

    }

    //点击事件的总方法;
    private void Click(){
        ivDapDownClickBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
