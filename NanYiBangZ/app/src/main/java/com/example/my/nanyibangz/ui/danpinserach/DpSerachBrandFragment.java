package com.example.my.nanyibangz.ui.danpinserach;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseFragment;

/**
 * Created by My on 2016/10/16.
 */

public class DpSerachBrandFragment extends BaseFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.danpin_serach_brand_fragment,null);
        return  view;

    }
}
