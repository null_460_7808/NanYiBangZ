package com.example.my.nanyibangz.ui.dapei;

import com.example.my.nanyibangz.base.IModel;
import com.example.my.nanyibangz.base.IPresenter;
import com.example.my.nanyibangz.base.IView;
import com.example.my.nanyibangz.bean.DaPeiDownBean;
import com.example.my.nanyibangz.bean.DaPeiUpBean;

import java.util.List;
import java.util.Map;

import rx.Subscriber;

/**
 * Created by Administrator on 2016/10/8.
 * 搭配页面
 */

public class DaPeiUpContact {
    public interface View extends IView{
        public void onVerticalSucess_DaPeiBeanUp(List<DaPeiUpBean.DataBean> itemsBeanList);
        public void onVerticalFail_DaPei(String msg);
        //搭配下部的grideView
        public void onVerticalSucess_DaPeiBeanDown(List<DaPeiDownBean.DataBean> beanList);
        public void onVerticalFail_DaPeiDown(String msg);
    }
    public interface Model extends IModel{
        public void getVerticalDaPeiUp(Map<String,String> params, Subscriber<DaPeiUpBean> subscriber);
        //搭配下部的grideView
        public void getVerticalDaPeiDown(Map<String,String> map,Subscriber<DaPeiDownBean> subscribers);
    }
    public interface Presenter extends IPresenter {
        public void getVerticalFromNet_DaPeiUp(Map<String,String> params);
        //搭配下部的grideView
        public void getVerticalFrmNet_DaPeiDown(Map<String,String> map);
    }
}
