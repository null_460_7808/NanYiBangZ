package com.example.my.nanyibangz.config;

/**
 * Created by My on 2016/10/5.
 */
public class DaPeiUrlConfig {
    /**
     *
     * 这是老师的例子
     *
     *  public static final String URL_VERTICAL="http://capi.douyucdn.cn/api/v1/getVerticalRoom?limit=20&offset=0&time=1470800460";
     public static class Path{
     //因为后台会分成生产环境和测试环境,所以域名地址必须单独提出来
     public static final String BASE_URL="http://capi.douyucdn.cn/";
     //斗鱼获取颜值列表的接口,传递参数获取的数值limit 偏移量 offset 时间time 毫秒数/1000   name
     public static final String URL_VERTICAL="api/v1/getVerticalRoom";

     }

     public static  class Params{
     public static final String LIMIT="limit";
     public static final String OFFSET="offset";
     public static final String TIME="time";
     }
     public static class DefaultVaule{
     public static final String LIMIT_VALUE="20";
     }
     */

  //  http://api.nanyibang.com/select-condition?administrativeArea=%E5%8C%97%E4%BA%AC%E5%B8%82&age=24&channel=yingyongbao&country=%E4%B8%AD%E5%9B%BD&locality=%E5%8C%97%E4%BA%AC%E5%B8%82&system_name=android&versionCode=219
    //http://api.nanyibang.com/match-classify?age=19&channel=oppo&hkm_sign2=f6ebc7b34f32060cea1a6e5d7acb5d5a&member_id=292720&member_type=member&random_key=30170&system_name=android&versionCode=219
    public static class Path{
        //因为后台会分成生产环境和测试环境,所以域名地址必须单独提出来
        //单品的基地址
        public static final String BASE_URL="http://api.nanyibang.com/";
        //单品获取颜值列表的接口
        public static final String DaPei_URL_VERTICAL="match-classify";
    }

    //变量，相当于map中的键
    public static  class Params{
        public static final String Age="age";
        public static final String Channel="channel";
        public static final String Hkm_sign2="hkm_sign2";
        public static final String Member_id="member_id";
        public static final String Member_type="member_type";
        public static final String Random_key="random_key";
        public static final String System_name="system_name";
        public static final String VersionCode="versionCode";
    }
    //http://api.nanyibang.com/match-classify?age=19&channel=oppo&hkm_sign2=f6ebc7b34f32060cea1a6e5d7acb5d5a&member_id=292720&member_type=member&random_key=30170&system_name=android&versionCode=219
    public static class DefaultVaule{
        public static final String Age_VALUE="19";
        public static final String Channel_VALUE="oppo";
        public static final String Hkm_sign2_VALUE="f6ebc7b34f32060cea1a6e5d7acb5d5a";
        public static final String Member_id_VALUE="292720";
        public static final String Member_type_VALUE="member";
        public static final String Random_key_VALUE="30170";
        public static final String VersionCode_VALUE="android";
        public static final String System_name_VALUE="219";



    }



}
