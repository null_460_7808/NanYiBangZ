package com.example.my.nanyibangz.ui.shouye.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.my.nanyibangz.MainActivity;
import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.CollectionBeans;
import com.example.my.nanyibangz.bean.HomeGoodsDetailsComment;
import com.example.my.nanyibangz.bean.HomeGoodsDetailsContent;
import com.example.my.nanyibangz.ui.shouye.HomeFragmentContract;
import com.example.my.nanyibangz.ui.shouye.adapter.HomeGoodsDetailsCommentRecyclerAdapter;
import com.example.my.nanyibangz.ui.shouye.adapter.HomeGoodsDetailsContentRecyclerAdapter;
import com.example.my.nanyibangz.ui.shouye.presenter.HomeGoodsDetailsPresenter;
import com.example.my.nanyibangz.utils.CustomLinearLayoutManager;
import com.example.my.nanyibangz.utils.DividerItemDecoration;
import com.j256.ormlite.stmt.QueryBuilder;
import com.stx.xhb.xbanner.XBanner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.sharesdk.onekeyshare.OnekeyShare;

/**
 * 展示商品详情
 */
public class HomeGoodsDetailsActivity extends AppCompatActivity implements HomeFragmentContract.IHomeGoodsDetailsView, XBanner.XBannerAdapter {


    @Bind(R.id.home_goodsdetails_vp)
    XBanner homeGoodsdetailsVp;
    @Bind(R.id.home_goodsdetails_layout_first)
    RelativeLayout homeGoodsdetailsLayoutFirst;
    @Bind(R.id.home_goodsdetails_coupon_price)
    TextView homeGoodsdetailsCouponPrice;
    @Bind(R.id.home_goodsdetails_sprice)
    TextView homeGoodsdetailsSprice;
    @Bind(R.id.home_goodsdetails_title)
    TextView homeGoodsdetailsTitle;
    @Bind(R.id.home_goodsdetails_layout_second)
    LinearLayout homeGoodsdetailsLayoutSecond;
    @Bind(R.id.home_goodsdetails_third_tv_info)
    TextView homeGoodsdetailsThirdTvInfo;
    @Bind(R.id.home_goodsdetails_third_tv_comment)
    TextView homeGoodsdetailsThirdTvComment;
    @Bind(R.id.home_goodsdetails_third_top)
    LinearLayout homeGoodsdetailsThirdTop;
    @Bind(R.id.home_goodsdetails_third_rv)
    RecyclerView homeGoodsdetailsThirdRv;
    @Bind(R.id.home_goodsdetails_third_tv_shopname)
    TextView homeGoodsdetailsThirdTvShopname;
    @Bind(R.id.home_goodsdetails_third_tv_dispat_score)
    TextView homeGoodsdetailsThirdTvDispatScore;
    @Bind(R.id.home_goodsdetails_third_tv_service_score)
    TextView homeGoodsdetailsThirdTvServiceScore;
    @Bind(R.id.home_goodsdetails_third_tv_desc_score)
    TextView homeGoodsdetailsThirdTvDescScore;
    @Bind(R.id.home_goodsdetails_third_rv1)
    RecyclerView homeGoodsdetailsThirdRv1;
    @Bind(R.id.home_goodsdetails_third_rlayout)
    RelativeLayout homeGoodsdetailsThirdRlayout;
    @Bind(R.id.home_goodsdetails_third_bottom)
    RelativeLayout homeGoodsdetailsThirdBottom;
    @Bind(R.id.home_goodsdetails_layout_third)
    RelativeLayout homeGoodsdetailsLayoutThird;
    @Bind(R.id.home_goodsdetails_layout_fourth)
    LinearLayout homeGoodsdetailsLayoutFourth;
    @Bind(R.id.home_goodsdetails_sv)
    ScrollView homeGoodsdetailsSv;
    @Bind(R.id.home_goodsdetails_wv)
    WebView homeGoodsdetailsWv;
    @Bind(R.id.home_goodsdetails_collection)
    RadioButton homeGoodsdetailsCollection;
    @Bind(R.id.home_goodsdetails_share)
    RadioButton homeGoodsdetailsShare;
    @Bind(R.id.home_goodsdetails_shoppingcart)
    RadioButton homeGoodsdetailsShoppingcart;
    @Bind(R.id.home_goodsdetails_tmall)
    RadioButton homeGoodsdetailsTmall;
    @Bind(R.id.home_goodsdetails_rg)
    RadioGroup homeGoodsdetailsRg;
    @Bind(R.id.home_goodsdetails_iv_back)
    ImageView homeGoodsdetailsIvBack;
    @Bind(R.id.home_goodsdetails_iv_cart)
    ImageView homeGoodsdetailsIvCart;
    @Bind(R.id.home_goodsdetails_rlayout_top)
    RelativeLayout homeGoodsdetailsRlayoutTop;
    //商品ID
    private int id;
    private HomeGoodsDetailsPresenter presenter;

    //商品详情轮播
    private List<String> imageUrlList;

    //收藏按钮点击事件计数
    private int clickCount;
    //将收藏所需信息置为全局变量
    private HomeGoodsDetailsContent content;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_goodsdetails);
        ButterKnife.bind(this);
        homeGoodsdetailsSv.scrollTo(0, 0);
        //按钮点击返回事件
        backOnClick();
        initGoodsDetailsActivity();
        presenter = new HomeGoodsDetailsPresenter(HomeGoodsDetailsActivity.this);
        //加载数据
        presenter.homeLoadGoodsDetailsContentDataToUi(this);
        presenter.homeLoadGoodsDetailsCommentDataToUi(this);

        //分享
        homeGoodsDetailsShared();

        //收藏功能
        //在回传数据时根据商品id 初始化收藏按钮
        if (content != null) {
            homeGoodsDetailsIsCollection(content);
            homeGoodsDetailsCollection(content);
        }
    }

    //按钮点击返回
    public void backOnClick() {
        homeGoodsdetailsIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    //获取Intent意图中传递的值 初始化控件
    public void initGoodsDetailsActivity() {
        Intent intent = getIntent();
        int item_id = intent.getIntExtra("item_id", 0);
        id = item_id;
    }

    //回传获取首页商品详情的商品信息
    @Override
    public void homeGoodsDetailsContentData(HomeGoodsDetailsContent homeGoodsDetailsContent) {
        if (homeGoodsDetailsContent != null) {
            //第一部分 商品详情轮播
            homeGoodsDeailsVP(homeGoodsDetailsContent);
            //第二部分 商品详情价格设置
            homeGoodsDeailsPrice(homeGoodsDetailsContent);
            //第三部分 商品详情的商品信息和店铺评论
            homeGoodsDetailsThirdContent(homeGoodsDetailsContent);
            homeGoodsDetailsThird(homeGoodsDetailsContent);
            //第五部分 WebView的设置和数据加载
            inithomeGoodsDetailsWebView();
            homeGoodsDetailsFifth(homeGoodsDetailsContent);

            //将加载出来的数据置为全局变量 以实现收藏功能
            content = homeGoodsDetailsContent;
        }
    }

    //回传获取首页商品详情的店铺评论
    @Override
    public void homeGoodsDetailsCommentData(HomeGoodsDetailsComment homeGoodsDetailsComment) {
        if (homeGoodsDetailsComment != null) {
            //设置旗舰店名字
            homeGoodsdetailsThirdTvShopname.setText(homeGoodsDetailsComment.getData().getShop_name());
            //设置商店评分
            homeGoodsdetailsThirdTvDispatScore.setText(homeGoodsDetailsComment.getData().getShop_score().getDispat_score());
            homeGoodsdetailsThirdTvServiceScore.setText(homeGoodsDetailsComment.getData().getShop_score().getService_score());
            homeGoodsdetailsThirdTvDescScore.setText(homeGoodsDetailsComment.getData().getShop_score().getDesc_score());
            //设置用户评论
            //获取homeGoodsDetailsComment对象中的店铺评论List集合数据
            List<HomeGoodsDetailsComment.DataBean.CommentsBean> commentsBeanList = homeGoodsDetailsComment.getData().getComments();
            //创建数据适配器
            HomeGoodsDetailsCommentRecyclerAdapter adapter = new HomeGoodsDetailsCommentRecyclerAdapter(HomeGoodsDetailsActivity.this, commentsBeanList);
            CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(HomeGoodsDetailsActivity.this, LinearLayoutManager.VERTICAL, false);
            homeGoodsdetailsThirdRv1.setLayoutManager(linearLayoutManager);
            //添加item之间的间距
            homeGoodsdetailsThirdRv1.addItemDecoration(new DividerItemDecoration(HomeGoodsDetailsActivity.this, DividerItemDecoration.VERTICAL_LIST, 15));
            homeGoodsdetailsThirdRv1.setItemAnimator(new DefaultItemAnimator());
            homeGoodsdetailsThirdRv1.setNestedScrollingEnabled(false);//顺滑滑动
            homeGoodsdetailsThirdRv1.setAdapter(adapter);
        }
    }

    @Override
    public int getHomeGoodsId() {
        return id;
    }

    //第一部分 商品详情的轮播
    public void homeGoodsDeailsVP(HomeGoodsDetailsContent homeGoodsDetailsContent) {
        //获取轮播字符串
        String imageUrl = homeGoodsDetailsContent.getData().getPic_urls();
        //将字符串截取成字符串数据获取轮播图片路径集合
        String[] imageUrls = imageUrl.split(",");//根据逗号截取
        //声明一个List集合存放每一条路径(因为XBanner这个插件加载数据只能是List集合)
        imageUrlList = new ArrayList<>();
        for (int i = 0; i < imageUrls.length; i++) {
            imageUrlList.add(imageUrls[i]);
        }
        homeGoodsdetailsVp.setData(imageUrlList);
        homeGoodsdetailsVp.setmAdapter(this);
    }

    //XBanner加载图片
    @Override
    public void loadBanner(XBanner banner, View view, int position) {
        Glide.with(this).load(imageUrlList.get(position)).into((ImageView) view);
        //ImageHelper.showImage(this,imageUrlList.get(position),view);
    }

    //第二部分 价格的设置
    public void homeGoodsDeailsPrice(HomeGoodsDetailsContent homeGoodsDetailsContent) {
        homeGoodsdetailsCouponPrice.setText("¥" + homeGoodsDetailsContent.getData().getCoupon_price());//优惠价
        homeGoodsdetailsSprice.setText("¥" + homeGoodsDetailsContent.getData().getPrice());//原价
        homeGoodsdetailsSprice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);//添加中划线
    }

    //第三部分 商品信息和店铺评论
    public void homeGoodsDetailsThird(final HomeGoodsDetailsContent homeGoodsDetailsContent) {
        //默认加载商品详情的商品信息
        //点击商品信息按钮时
        homeGoodsdetailsThirdTvInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //首先设置导航栏点击切换时的颜色
                //商品信息被选中
                homeGoodsdetailsThirdTvInfo.setBackgroundColor(Color.parseColor("#2BC0D0"));
                homeGoodsdetailsThirdTvInfo.setTextColor(Color.parseColor("#FFFFFF"));
                //店铺评论未被选中
                homeGoodsdetailsThirdTvComment.setBackgroundColor(Color.parseColor("#FFFFFF"));
                homeGoodsdetailsThirdTvComment.setTextColor(Color.parseColor("#2BC0D0"));

                //下方内容的设置
                //商品信息被选中 则加载数据并显示
                homeGoodsdetailsThirdRv.setVisibility(View.VISIBLE);

                //店铺评论未被选中 不加载数据且不显示
                homeGoodsdetailsThirdRlayout.setVisibility(View.GONE);
            }
        });
        //点击店铺评论按钮时
        homeGoodsdetailsThirdTvComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //首先设置导航栏点击切换时的颜色
                //商品信息未被选中
                homeGoodsdetailsThirdTvInfo.setBackgroundColor(Color.parseColor("#FFFFFF"));
                homeGoodsdetailsThirdTvInfo.setTextColor(Color.parseColor("#2BC0D0"));
                //店铺评论被选中
                homeGoodsdetailsThirdTvComment.setBackgroundColor(Color.parseColor("#2BC0D0"));
                homeGoodsdetailsThirdTvComment.setTextColor(Color.parseColor("#FFFFFF"));

                //商品信息未被选中 不加载数据且不显示
                homeGoodsdetailsThirdRv.setVisibility(View.GONE);

                //店铺评论被选中 则加载数据并显示(回传数据时设置)
                homeGoodsdetailsThirdRlayout.setVisibility(View.VISIBLE);
            }
        });
    }

    //加载商品信息数据(避免homeGoodsDetailsThird方法中的多次调用改变视图)
    public void homeGoodsDetailsThirdContent(HomeGoodsDetailsContent homeGoodsDetailsContent) {
        //获取homeGoodsDetailsContent对象中的商品信息List集合数据
        List<HomeGoodsDetailsContent.DataBean.SeoDescBean> seoDescBeanList = homeGoodsDetailsContent.getData().getSeo_desc();
        //创建数据适配器
        HomeGoodsDetailsContentRecyclerAdapter adapter = new HomeGoodsDetailsContentRecyclerAdapter(HomeGoodsDetailsActivity.this, seoDescBeanList);
        CustomLinearLayoutManager linearLayoutManager = new CustomLinearLayoutManager(HomeGoodsDetailsActivity.this, LinearLayoutManager.VERTICAL, false);
        homeGoodsdetailsThirdRv.setLayoutManager(linearLayoutManager);
        //添加item之间的间距
        homeGoodsdetailsThirdRv.addItemDecoration(new DividerItemDecoration(HomeGoodsDetailsActivity.this, DividerItemDecoration.VERTICAL_LIST, 10));
        homeGoodsdetailsThirdRv.setItemAnimator(new DefaultItemAnimator());
        homeGoodsdetailsThirdRv.setNestedScrollingEnabled(false);//顺滑滑动
        homeGoodsdetailsThirdRv.setAdapter(adapter);
    }

    //第五部分 WebView的加载(第四部分只是摆设 全是文字提示)
    //WebView的初始化
    public void inithomeGoodsDetailsWebView() {
        homeGoodsdetailsWv.getSettings().setJavaScriptEnabled(true);
        homeGoodsdetailsWv.getSettings().setAllowContentAccess(true);
        homeGoodsdetailsWv.getSettings().setAllowFileAccessFromFileURLs(true);
        homeGoodsdetailsWv.getSettings().setAppCacheEnabled(true);
        homeGoodsdetailsWv.getSettings().setLoadWithOverviewMode(true);
        homeGoodsdetailsWv.getSettings().setUseWideViewPort(true);
        homeGoodsdetailsWv.getSettings().setPluginState(WebSettings.PluginState.ON);
        homeGoodsdetailsWv.getSettings().setDomStorageEnabled(true);
        //2016-10-15 解决WebView中点击事件响应问题
        homeGoodsdetailsWv.getSettings().setBuiltInZoomControls(true);
        homeGoodsdetailsWv.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        homeGoodsdetailsWv.getSettings().setSavePassword(true);
        homeGoodsdetailsWv.getSettings().setSaveFormData(true);
        homeGoodsdetailsWv.getSettings().setGeolocationEnabled(true);
        homeGoodsdetailsWv.getSettings().setGeolocationDatabasePath("/data/data/org.itri.html5webview/databases/");
        homeGoodsdetailsWv.requestFocus();
        //设置webview的缩放功能  设置webview触摸放大缩小
        homeGoodsdetailsWv.getSettings().setSupportZoom(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            homeGoodsdetailsWv.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
    }

    public void homeGoodsDetailsFifth(HomeGoodsDetailsContent homeGoodsDetailsContent) {
        //获取homeGoodsDetailsContent对象中的html字符串内容
        String htmlString = homeGoodsDetailsContent.getData().getItem_description();
        homeGoodsdetailsWv.loadData(htmlString, "text/html", "utf-8");

        // 添加js交互接口类，并起别名为imagelistner(即实例化后对象名)
        // addJavaScriptInterface方式帮助我们从一个网页传递值到Android XML视图（反之亦然）。
        //内部类形式创建JavaScriptInterface接口(JavaScriptInterface接口提供一个平台让js脚本能执行其中定义的Java代码)
        homeGoodsdetailsWv.addJavascriptInterface(new JavaScriptInterface(this), "imagelistner");

        //添加监听
        homeGoodsdetailsWv.setWebViewClient(new WebViewClient() {
            //在点击请求的是链接是才会调用，重写此方法返回true表明点击网页里面的链接还是在当前的webview里跳转，不跳到浏览器那边。
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }

            //在页面加载结束时调用。
            @Override
            public void onPageFinished(WebView view, String url) {
                view.getSettings().setJavaScriptEnabled(true);
                super.onPageFinished(view, url);
                // WebView加载Html页面完成之后，为该Html页面添加监听图片的点击js函数
                addImageClickListner();
            }

            //在页面加载开始时调用。
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                view.getSettings().setJavaScriptEnabled(true);
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
            }
        });
    }

    //JS通信接口(js脚本通过这个接口来调用java代码)
    public class JavaScriptInterface {
        private Context context;

        public JavaScriptInterface(Context context) {
            this.context = context;
        }

        @JavascriptInterface
        public void openImage(String imageUrl) {
            Intent intent = new Intent();
            intent.putExtra("imageUrl", imageUrl);
            intent.setClass(context, HomeGoodsDetailsImageActivity.class);
            context.startActivity(intent);
        }
    }

    // 注入js函数监听(在WebView加载完成之后给Html页面添加js函数)
    private void addImageClickListner() {
        // 这段js函数的功能就是，遍历所有的img，并添加onclick函数
        // 函数的功能是在图片点击的时候调用本地java接口并传递url图片路径过去
        //-----------------------------------------------------------------
        //homeGoodsdetailsWv.loadUrl...Java调用js
        //window.imagelistner.openImage(this.src) js调用Java代码
        homeGoodsdetailsWv.loadUrl("javascript:(function(){" +
                "var objs = document.getElementsByTagName(\"img\"); " +
                "for(var i=0;i<objs.length;i++)  " +
                "{"
                + "    objs[i].onclick=function()  " +
                "    {  "
                + "        window.imagelistner.openImage(this.src);  " +
                "    }  " +
                "}" +
                "})()");
    }

    //分享按钮的点击事件
    public void homeGoodsDetailsShared() {
        homeGoodsdetailsShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //创建一键分享的对象
                OnekeyShare onekeyShare = new OnekeyShare();
                //关闭sso授权
                onekeyShare.disableSSOWhenAuthorize();
                onekeyShare.setTitle("标题");
                onekeyShare.setText("张利俊");
                onekeyShare.setImageUrl("");
                onekeyShare.show(HomeGoodsDetailsActivity.this);
            }
        });
    }

    //进入页面时 判断该商品是否被收藏
    public void homeGoodsDetailsIsCollection(HomeGoodsDetailsContent homeGoodsDetailsContent) {
        //首先进行查询 查询该商品是否存在
        QueryBuilder<CollectionBeans, Long> builder = MainActivity.collectionBeansDao.queryBuilder();
        try {
            List<CollectionBeans> list = builder.where().eq("item_id", homeGoodsDetailsContent.getData().getItem_id()).query();
           Log.i("qusi","size=="+list.size());
            //证明数据库中存在数据
            if (list.size() != 0 && list != null) {
                CollectionBeans collectionBeans = list.get(0);
                if (collectionBeans != null) {
                    clickCount = 1;//若存在则初始化为1
                    //将收藏按钮的背景图置为收藏状态
                    homeGoodsdetailsCollection.setChecked(true);
                }
            }
            else {
                clickCount = 0; //不存在则初始化为0
                //将收藏按钮的背景图置为无收藏状态
                homeGoodsdetailsCollection.setChecked(false);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //收藏按钮的点击事件
    public void homeGoodsDetailsCollection(final HomeGoodsDetailsContent homeGoodsDetailsContent) {
        homeGoodsdetailsCollection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //每次触发点击事件时clickCount计数
              //即指示下一步进行什么操作
                //0 --> 1 暂无收藏-->已经收藏(即添加收藏操作)
                if (clickCount % 2 == 0) {
                    Toast.makeText(HomeGoodsDetailsActivity.this,"添加到数据库",Toast.LENGTH_LONG).show();
                    clickCount=1;
                    //首先置为收藏状态
                    //homeGoodsdetailsCollection.setChecked(true);
                    //然后查询数据库中是否存在该条目
                    QueryBuilder<CollectionBeans, Long> builder = MainActivity.collectionBeansDao.queryBuilder();
                    try {
                        List<CollectionBeans> list = builder.where().eq("item_id", homeGoodsDetailsContent.getData().getItem_id()).query();
                        //证明数据库中存在数据
                        if (list.size() != 0 && list != null) {
                            CollectionBeans collectionBeans = list.get(0);
                            if (collectionBeans != null) {//存在
                                //将其删除
                                int count = MainActivity.collectionBeansDao.delete(collectionBeans);
                                if (count > 0) {
                                    Log.e("tag", "删除成功" + count);
                                }
                            } else {//不存在
                                String item_id = Integer.toString(homeGoodsDetailsContent.getData().getItem_id());
                                String title = homeGoodsDetailsContent.getData().getTitle();
                                String price = homeGoodsDetailsContent.getData().getCoupon_price();
                                String imgUrl = homeGoodsDetailsContent.getData().getPic_url();
                                CollectionBeans collection = new CollectionBeans(item_id, title, price, imgUrl);
                                int count = MainActivity.collectionBeansDao.create(collection);
                                if (count > 0) {
                                    Log.e("tag", "收藏成功" + count);
                                }
                            }
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                } else if (clickCount % 2 == 1) {//1 --> 0 已经收藏-->暂无收藏(即取消收藏操作)
                    //首先置为无收藏状态
                    clickCount=0;
                    Toast.makeText(HomeGoodsDetailsActivity.this,"删除数据库中的数据",Toast.LENGTH_LONG).show();
                    //homeGoodsdetailsCollection.setChecked(false);
                    //然后查询数据库中是否存在该条目
                    QueryBuilder<CollectionBeans, Long> builder = MainActivity.collectionBeansDao.queryBuilder();
                    try {
                        List<CollectionBeans> list = builder.where().eq("item_id", homeGoodsDetailsContent.getData().getItem_id()).query();
                        //证明数据库中存在数据
                        if (list.size() != 0 && list != null) {
                            CollectionBeans collectionBeans = list.get(0);
                            if (collectionBeans != null) {//存在
                                //将其删除
                                int count = MainActivity.collectionBeansDao.delete(collectionBeans);
                                if (count > 0) {
                                    Log.e("tag", count + "删除成功");
                                }
                            }
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
