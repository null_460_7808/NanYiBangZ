package com.example.my.nanyibangz.bean;

import java.util.List;

/**
 * 商品详情的商品信息
 */

public class HomeGoodsDetailsContent {

    /**
     * member_type : guest
     * login_status : error
     * login_status_msg : not login in
     */

    private UserBean user;
    /**
     * item_id : 406944
     * special_price : null
     * cate_id : 24
     * title : Fun国际潮牌卫衣男秋青少年开衫修身连帽宽松长袖外套运动印花潮
     * pic_url : http://img01.taobaocdn.com/bao/uploaded/i1/TB1_x7OKXXXXXbNXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg
     * pic_urls : http://img.alicdn.com/bao/uploaded/i1/TB1_x7OKXXXXXbNXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg,http://img.alicdn.com/bao/uploaded/i1/688498816/TB2sVmUgFXXXXb5XXXXXXXXXXXX_!!688498816.jpg_450x10000q75.jpg,http://img.alicdn.com/bao/uploaded/i3/688498816/TB2lzWHgFXXXXXLXpXXXXXXXXXX_!!688498816.jpg_450x10000q75.jpg,http://img.alicdn.com/bao/uploaded/i2/688498816/TB2bcCDgFXXXXaxXpXXXXXXXXXX_!!688498816.jpg_450x10000q75.jpg,http://img.alicdn.com/bao/uploaded/i1/688498816/TB2H3uFgFXXXXXNXpXXXXXXXXXX_!!688498816.jpg_450x10000q75.jpg
     * brand : Fun
     * num_iid : 23840416
     * coupon_price : 559.00
     * purchaseLink :
     * item_type : 1
     * seo_desc : [{"name":"品牌名称","value":"Fun"},{"name":"材质","value":"棉78.2% 聚酯纤维21.8%"},{"name":"基础风格","value":"青春流行"},{"name":"货号","value":"523840416237"},{"name":"款式颜色","value":"深蓝,灰色"},{"name":"适用季节","value":"春季"}]
     * open_iid : AAHG_kWCABuH20zjANhxtorI
     * price : 559.00
     * saveCount : 1
     * cuxiao_desc :
     * material : 棉78.2% 聚酯纤维21.8%
     * style : 青春流行
     * color : 深蓝,灰色
     * season : 春季
     * item_description : <head>
     <style>
     .center_image { width: 100%}
     .hide_image { display: none}
     </style>
     </head><img class='center_image' src='http://img.alicdn.com/imgextra/i4/688498816/TB2t3mJgFXXXXXTXpXXXXXXXXXX_!!688498816.jpg'><img class='center_image' src='http://img.alicdn.com/imgextra/i4/688498816/TB2qqGSgFXXXXcHXXXXXXXXXXXX_!!688498816.jpg'><img class='center_image' src='http://img.alicdn.com/imgextra/i2/688498816/TB2ltuCgFXXXXbAXpXXXXXXXXXX_!!688498816.jpg'><img class='center_image' src='http://img.alicdn.com/imgextra/i3/688498816/TB2w2mHgFXXXXa9XpXXXXXXXXXX_!!688498816.jpg'><img class='center_image' src='http://img.alicdn.com/imgextra/i2/688498816/TB2Kj5CgFXXXXbbXpXXXXXXXXXX_!!688498816.jpg'><img class='center_image' src='http://img.alicdn.com/imgextra/i3/688498816/TB2aF9ZgFXXXXa4XXXXXXXXXXXX_!!688498816.jpg'><img class='center_image' src='http://img.alicdn.com/imgextra/i3/688498816/TB2cXy8gFXXXXXvXXXXXXXXXXXX_!!688498816.jpg'><img class='center_image' src='http://img.alicdn.com/imgextra/i1/688498816/TB2HPWFgFXXXXaDXpXXXXXXXXXX_!!688498816.jpg'><img class='center_image' src='http://img.alicdn.com/imgextra/i2/688498816/TB2jgC2gFXXXXaqXXXXXXXXXXXX_!!688498816.jpg'><script>
     var imgs = document.getElementsByClassName('center_image');
     for (var i = 0; i < imgs.length; i++) {
     imgs[i]['index']=i;
     imgs[i].addEventListener('load', function() {
     if (imgs[this.index].naturalWidth < 100) {
     this.className += ' hide_image';
     }
     });
     imgs[i].addEventListener('error', function() {
     this.className += ' hide_image';
     });
     }
     </script>
     */

    private DataBean data;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class UserBean {
        private String member_type;
        private String login_status;
        private String login_status_msg;

        public String getMember_type() {
            return member_type;
        }

        public void setMember_type(String member_type) {
            this.member_type = member_type;
        }

        public String getLogin_status() {
            return login_status;
        }

        public void setLogin_status(String login_status) {
            this.login_status = login_status;
        }

        public String getLogin_status_msg() {
            return login_status_msg;
        }

        public void setLogin_status_msg(String login_status_msg) {
            this.login_status_msg = login_status_msg;
        }
    }

    public static class DataBean {
        private int item_id;
        private Object special_price;
        private int cate_id;
        private String title;
        private String pic_url;
        private String pic_urls;
        private String brand;
        private String num_iid;
        private String coupon_price;
        private String purchaseLink;
        private int item_type;
        private String open_iid;
        private String price;
        private int saveCount;
        private String cuxiao_desc;
        private String material;
        private String style;
        private String color;
        private String season;
        private String item_description;
        /**
         * name : 品牌名称
         * value : Fun
         */

        private List<SeoDescBean> seo_desc;

        public int getItem_id() {
            return item_id;
        }

        public void setItem_id(int item_id) {
            this.item_id = item_id;
        }

        public Object getSpecial_price() {
            return special_price;
        }

        public void setSpecial_price(Object special_price) {
            this.special_price = special_price;
        }

        public int getCate_id() {
            return cate_id;
        }

        public void setCate_id(int cate_id) {
            this.cate_id = cate_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPic_url() {
            return pic_url;
        }

        public void setPic_url(String pic_url) {
            this.pic_url = pic_url;
        }

        public String getPic_urls() {
            return pic_urls;
        }

        public void setPic_urls(String pic_urls) {
            this.pic_urls = pic_urls;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getNum_iid() {
            return num_iid;
        }

        public void setNum_iid(String num_iid) {
            this.num_iid = num_iid;
        }

        public String getCoupon_price() {
            return coupon_price;
        }

        public void setCoupon_price(String coupon_price) {
            this.coupon_price = coupon_price;
        }

        public String getPurchaseLink() {
            return purchaseLink;
        }

        public void setPurchaseLink(String purchaseLink) {
            this.purchaseLink = purchaseLink;
        }

        public int getItem_type() {
            return item_type;
        }

        public void setItem_type(int item_type) {
            this.item_type = item_type;
        }

        public String getOpen_iid() {
            return open_iid;
        }

        public void setOpen_iid(String open_iid) {
            this.open_iid = open_iid;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public int getSaveCount() {
            return saveCount;
        }

        public void setSaveCount(int saveCount) {
            this.saveCount = saveCount;
        }

        public String getCuxiao_desc() {
            return cuxiao_desc;
        }

        public void setCuxiao_desc(String cuxiao_desc) {
            this.cuxiao_desc = cuxiao_desc;
        }

        public String getMaterial() {
            return material;
        }

        public void setMaterial(String material) {
            this.material = material;
        }

        public String getStyle() {
            return style;
        }

        public void setStyle(String style) {
            this.style = style;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getSeason() {
            return season;
        }

        public void setSeason(String season) {
            this.season = season;
        }

        public String getItem_description() {
            return item_description;
        }

        public void setItem_description(String item_description) {
            this.item_description = item_description;
        }

        public List<SeoDescBean> getSeo_desc() {
            return seo_desc;
        }

        public void setSeo_desc(List<SeoDescBean> seo_desc) {
            this.seo_desc = seo_desc;
        }

        public static class SeoDescBean {
            private String name;
            private String value;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }
    }
}
