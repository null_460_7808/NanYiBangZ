package com.example.my.nanyibangz.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by My on 2016/10/9.
 */

public class DanPinTypeBeans {

    /**
     * member_type : guest
     * member_id : 0
     * login_status : error
     * login_status_msg : not login in
     */

    @SerializedName("user")
    private UserBean user;
    /**
     * _id : 409683
     * title : 佐丹奴衬衫 男装舒适牛津纺纯色斯文百搭立领长袖衬衣男01046028
     * pic_url : http://img01.taobaocdn.com/bao/uploaded/i1/TB1jFvkLpXXXXXuaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg
     * coupon_price : 188.00
     * isv_code : 0_android_danpin_19
     */

    @SerializedName("data")
    private List<DataBean> data;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class UserBean {
        @SerializedName("member_type")
        private String memberType;
        @SerializedName("member_id")
        private int memberId;
        @SerializedName("login_status")
        private String loginStatus;
        @SerializedName("login_status_msg")
        private String loginStatusMsg;

        public String getMemberType() {
            return memberType;
        }

        public void setMemberType(String memberType) {
            this.memberType = memberType;
        }

        public int getMemberId() {
            return memberId;
        }

        public void setMemberId(int memberId) {
            this.memberId = memberId;
        }

        public String getLoginStatus() {
            return loginStatus;
        }

        public void setLoginStatus(String loginStatus) {
            this.loginStatus = loginStatus;
        }

        public String getLoginStatusMsg() {
            return loginStatusMsg;
        }

        public void setLoginStatusMsg(String loginStatusMsg) {
            this.loginStatusMsg = loginStatusMsg;
        }
    }



    public static class DataBean {
        @SerializedName("_id")
        private int id;
        @SerializedName("title")
        private String title;
        @SerializedName("pic_url")
        private String picUrl;
        @SerializedName("coupon_price")
        private String couponPrice;
        @SerializedName("isv_code")
        private String isvCode;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPicUrl() {
            return picUrl;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }

        public String getCouponPrice() {
            return couponPrice;
        }

        public void setCouponPrice(String couponPrice) {
            this.couponPrice = couponPrice;
        }

        public String getIsvCode() {
            return isvCode;
        }

        public void setIsvCode(String isvCode) {
            this.isvCode = isvCode;
        }
    }
}
