package com.example.my.nanyibangz.ui.shouye.presenter;

import android.content.Context;
import android.util.Log;

import com.example.my.nanyibangz.bean.HomeTideProduct;
import com.example.my.nanyibangz.config.HomeUrlConfig;
import com.example.my.nanyibangz.ui.shouye.HomeFragmentContract;
import com.example.my.nanyibangz.ui.shouye.model.HomeTideProductModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/17.
 */

public class HomeTideProductPresenter implements HomeFragmentContract.IHomeTideProductPresenter{
    private HomeFragmentContract.IHomeTideProductModel model;
    private HomeFragmentContract.IHomeTideProductView view;

    public HomeTideProductPresenter(HomeFragmentContract.IHomeTideProductView view) {
        this.view = view;
        this.model = new HomeTideProductModel();
    }

    @Override
    public void homeLoadGoodsDetailsContentDataToUi(Context context) {
        //http://api.nanyibang.com/campaign?age=15&campaignId=6&campaignType=chaopin&channel=yingyongbao&page=1&system_name=android&versionCode=219
        Map<String,String> map = new HashMap<>();
        map.put(HomeUrlConfig.Params.AGE, "15");
        map.put(HomeUrlConfig.Params.CAMPAIGNID, "6");
        map.put(HomeUrlConfig.Params.CAMPAIGNTYPE, "chaopin");
        map.put(HomeUrlConfig.Params.CHANNEL, "yingyongbao");
        map.put(HomeUrlConfig.Params.PAGE, "1");
        map.put(HomeUrlConfig.Params.SYSTEM_NAME, "android");
        map.put(HomeUrlConfig.Params.VERSIONCODE, "219");
        model.homeLoadTideProductData("campaign", map, new Subscriber<HomeTideProduct>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.e("tag","homeTideProduct is null");
            }

            @Override
            public void onNext(HomeTideProduct homeTideProduct) {
                if (homeTideProduct!=null){
                    List<HomeTideProduct.DataBean.ItemDetailBean> list = homeTideProduct.getData().getItemDetail();
                    view.homeTideProductData(list);
                }
            }
        });
    }
}
