package com.example.my.nanyibangz.ui.shouye.model;

import com.example.my.nanyibangz.bean.HomeGoodsDetailsContent;
import com.example.my.nanyibangz.bean.HomeTideProduct;
import com.example.my.nanyibangz.http.HomeHttpHelper;
import com.example.my.nanyibangz.ui.shouye.HomeFragmentContract;

import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/17.
 */

public class HomeTideProductModel implements HomeFragmentContract.IHomeTideProductModel{

    @Override
    public void homeLoadTideProductData(String type, Map<String, String> map, Subscriber<HomeTideProduct> subscriber) {
        HomeHttpHelper.getInstance().getHomeTideProductDatas(type,map,subscriber);
    }
}
