package com.example.my.nanyibangz.ui.shuaiba;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseFragment;
import com.example.my.nanyibangz.bean.ShuaiBaBean;
import com.example.my.nanyibangz.config.ShuaiBaUrlConfig;
import com.example.my.nanyibangz.utils.CustomerNestScrollView;
import com.example.my.nanyibangz.utils.MyListView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/10/14.
 */

public class ShuaiBaChildFragment extends BaseFragment implements ShuaiBaContact.View {

    @Bind(R.id.lv_shuaiba)
    MyListView lvShuaiba;
    @Bind(R.id.tv_avoid)
    TextView tvAvoid;
    @Bind(R.id.lay_footer)
    LinearLayout layFooter;
    @Bind(R.id.scro_shuaiba)
    CustomerNestScrollView scroShuaiba;
    @Bind(R.id.sr_shuaiba)
    SwipeRefreshLayout srShuaiba;
    private ShuaiBaContact.Presenter presenter;
    private String index;
    private List<ShuaiBaBean.DataBean> list;
    int page = 1;
    private ShuaiBaAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shuaiba_view, null);
        ButterKnife.bind(this, view);
        initview(page);
        initAnim();
        return view;
    }
    //在代码中实现列表动画
    private void initAnim(){
        Animation animation= AnimationUtils.loadAnimation(getActivity(),R.anim.slide_right);
        LayoutAnimationController lac=new LayoutAnimationController(animation);
        lac.setDelay(0.8f);  //设置动画间隔时间
        lac.setOrder(LayoutAnimationController.ORDER_NORMAL); //设置列表的显示顺序
        lvShuaiba.setLayoutAnimation(lac);
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SwipRefresh();
        ScrollBottom();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public void initview(int page) {
        Bundle bundle = getArguments();
        index = bundle.getString("index");
        Log.i("tag", "index==============" + index);
        //http://api.nanyibang.com/dress-school?age=19&channel=xiaomi&kind=6&page=1&system_name=android&versionCode=219
        presenter = new ShuaiBaPresenter(this);
        Map<String, String> params = new HashMap<>();
        params.put(ShuaiBaUrlConfig.Params.Age, ShuaiBaUrlConfig.DefaultVaule.Age_VALUE);
        params.put(ShuaiBaUrlConfig.Params.Channel, ShuaiBaUrlConfig.DefaultVaule.Channel_VALUE);
        params.put(ShuaiBaUrlConfig.Params.Kind, index);
        params.put(ShuaiBaUrlConfig.Params.Page, page + "");
        params.put(ShuaiBaUrlConfig.Params.System_name, ShuaiBaUrlConfig.DefaultVaule.System_name_VALUE);
        params.put(ShuaiBaUrlConfig.Params.VersionCode, ShuaiBaUrlConfig.DefaultVaule.VersionCode_VALUE);
        presenter.getVerticalFromNet_ShuaiBa(params);
    }

    @Override
    public void onVerticalSucess_ShuaiBa(List<ShuaiBaBean.DataBean> dataBeanList) {
        if (dataBeanList.size() != 0) {
            if (page == 1) {
                list = dataBeanList;
                layFooter.setVisibility(View.GONE);
                adapter = new ShuaiBaAdapter(getActivity(), dataBeanList);
                lvShuaiba.setAdapter(adapter);
            } else {
                list.addAll(dataBeanList);
                adapter.notifyDataSetChanged();
            }
        } else {
            tvAvoid.setText("没有更多数据啦！");
        }
        Click();
    }

    @Override
    public void onVerticalFail_ShuaiBa(String msg) {

    }

    private void SwipRefresh() {

        srShuaiba.setProgressBackgroundColorSchemeColor(getResources().getColor(R.color.colorSwipRef));
        srShuaiba.setColorSchemeResources(R.color.color_holo_yellow, R.color.color_holo_green, R.color.color_holo_he, R.color.color_holo_he2);
        srShuaiba.setProgressViewOffset(false, 0, (int) TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources()
                        .getDisplayMetrics()));
        srShuaiba.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Log.i("tag", "datalist============" + dataBeanList.size());
                        adapter = new ShuaiBaAdapter(getActivity(), list);
                        lvShuaiba.setAdapter(adapter);
                        srShuaiba.setRefreshing(false);
                    }
                }, 3000);
            }
        });
    }

    private void ScrollBottom() {
        scroShuaiba.setOnScrollToBottomLintener(new CustomerNestScrollView.OnScrollToBottomListener() {
            @Override
            public void onScrollBottomListener(boolean isBottom) {
                if (isBottom == true) {
                    layFooter.setVisibility(View.VISIBLE);
                    initview((++page));
                }
            }
        });
    }

    private void Click() {
        lvShuaiba.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int dressId = list.get(position).getDressSchoolId();
                String title = list.get(position).getTitle();
                String link = list.get(position).getLink();
                Intent intent = new Intent(getActivity(), ShuaiBaClickActivity.class);
                intent.putExtra("dressId", dressId);
                intent.putExtra("title", title);
                intent.putExtra("link", link);
                startActivity(intent);
            }
        });
    }
}
