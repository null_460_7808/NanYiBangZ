package com.example.my.nanyibangz.ui.shouye.model;

import com.example.my.nanyibangz.bean.HomeBangBangChoice;
import com.example.my.nanyibangz.bean.HomeChoiceness;
import com.example.my.nanyibangz.bean.HomeThreeBrandChoiceItem;
import com.example.my.nanyibangz.bean.HomeVPBeans;
import com.example.my.nanyibangz.http.HomeHttpHelper;
import com.example.my.nanyibangz.ui.shouye.HomeFragmentContract;

import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/7.
 */

public class HomeFragmentModel implements HomeFragmentContract.IHomeModel {
    //加载首页轮播数据(首页轮播、每日签到、特色市场部分)
    @Override
    public void homeLoadVPData(String type, Map<String, String> map, Subscriber<HomeVPBeans> subscriber) {
       HomeHttpHelper.getInstance().getHomeVPDatas(type,map,subscriber);
    }

    //加载首页精选数据(品牌精选、搭配精选、学堂精选部分)
    @Override
    public void homeLoadChoicenessData(String type, Map<String, String> map, Subscriber<HomeChoiceness> subscriber) {
        HomeHttpHelper.getInstance().getHomeChoicenessDatas(type,map,subscriber);
    }

    //加载首页邦邦精选数据
    @Override
    public void homeLoadBangBangChoicenessData(String type, Map<String, String> map, Subscriber<HomeBangBangChoice> subscriber) {
        HomeHttpHelper.getInstance().getHomeBangBangChoicenessDatas(type,map,subscriber);
    }
}
