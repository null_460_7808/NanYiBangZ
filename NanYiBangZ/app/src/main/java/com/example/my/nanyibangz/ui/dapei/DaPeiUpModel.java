package com.example.my.nanyibangz.ui.dapei;

import com.example.my.nanyibangz.bean.DaPeiDownBean;
import com.example.my.nanyibangz.bean.DaPeiUpBean;
import com.example.my.nanyibangz.http.DaPeiHttpHelper;

import java.util.Map;

import rx.Subscriber;

/**
 * Created by Administrator on 2016/10/8.
 */

public class DaPeiUpModel implements DaPeiUpContact.Model {
    @Override
    public void getVerticalDaPeiUp(Map<String, String> params, Subscriber<DaPeiUpBean> subscriber) {
        DaPeiHttpHelper.getInstance().getVerticalDaPei(params,subscriber);
    }
    //搭配down
    @Override
    public void getVerticalDaPeiDown(Map<String, String> map, Subscriber<DaPeiDownBean> subscribers) {
        DaPeiHttpHelper.getInstance().getVerticalDaPeiDown(map,subscribers);
    }
}
