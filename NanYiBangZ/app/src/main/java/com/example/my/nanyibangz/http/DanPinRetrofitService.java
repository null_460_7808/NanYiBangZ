package com.example.my.nanyibangz.http;

import com.example.my.nanyibangz.bean.DanPinSerachHotSingleBeans;
import com.example.my.nanyibangz.bean.DanPinTypeBeans;
import com.example.my.nanyibangz.bean.DanPinTypeClickBeans;
import com.example.my.nanyibangz.bean.DanPinTypeClickCommentBeans;
import com.example.my.nanyibangz.bean.DanPin_Beans;
import com.example.my.nanyibangz.bean.DanPin_JingXuan_Beans;
import com.example.my.nanyibangz.config.DanPinUrlConfig;

import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by My on 2016/10/5.
 */
public interface DanPinRetrofitService {

    //单品连接网络的rx-Observable接口
    @GET(DanPinUrlConfig.Path.DanPin_URL_VERTICAL)
    Observable<DanPin_Beans> getVerticalDanPin(@QueryMap() Map<String, String> params);
    @GET(DanPinUrlConfig.Path.DanPin_JingXuan_URL_VERTICAL)
    Observable<DanPin_JingXuan_Beans> getVerticalDanPin_JingXuan(@QueryMap() Map<String, String> params);
    @GET(DanPinUrlConfig.Path.DanPin_Type_Click_URL_VERTiCAL)
    Observable<DanPinTypeBeans>getVerticalDanPin_TypeClick(@QueryMap() Map<String, String> params);
    @GET(DanPinUrlConfig.Path.DanPin_Type_Click_URL_Lower_VERTICAL)
    Observable<DanPinTypeClickBeans>getVerticalDanPin_TypeClickLower(@QueryMap() Map<String, String> params);
    @GET(DanPinUrlConfig.Path.DanPin_Type_Comment_URL_VERTICAL)
    Observable<DanPinTypeClickCommentBeans>getVerticalDanPin_Comment(@QueryMap() Map<String, String> params);
    //单品搜索，热门搜索的接口
    @GET(DanPinUrlConfig.Path.DanPin_Serach_Hot_Single_URL_VERTCAL)
    Observable<DanPinSerachHotSingleBeans>getVeticalDanPin_Serach_Hot_Single();
}
