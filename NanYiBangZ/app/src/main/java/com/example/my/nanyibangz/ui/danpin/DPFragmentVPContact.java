package com.example.my.nanyibangz.ui.danpin;

import com.example.my.nanyibangz.base.IModel;
import com.example.my.nanyibangz.base.IPresenter;
import com.example.my.nanyibangz.base.IView;
import com.example.my.nanyibangz.bean.DanPin_Beans;
import com.example.my.nanyibangz.bean.DanPin_JingXuan_Beans;
import com.example.my.nanyibangz.bean.DataBean;

import java.util.List;
import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/5.
 */
public class DPFragmentVPContact {

    //单品type对应的View接口
    public  interface View extends IView{
        public void onVerticalSucess_DanPin_Beans(List<DanPin_Beans.DataBean> danPin_Bean_List);
        public void onVerticalFail_DanPin_Beans(String msg);
        //单品 精选
        public void onVerticalSucess_DanPin_JingXuan_Beans(List<DataBean> danPin_JingXuan_List);
        public void onVerticalFail_DanPin_JingXuan_Beans(String msg);
    }
    //单品type对饮的Model接口，
    public interface Modle extends IModel{
        public void getVerticalDanPin_Beans(Map<String, String> params, Subscriber<DanPin_Beans> subscriber);
        public void getVerticalDanPin_JingXuan_Beans(Map<String, String> params, Subscriber<DanPin_JingXuan_Beans> subscriber);
    }
    //单品type对应Presenter接口，
    public interface Presenter extends IPresenter {
        public void getVerticalFromNet_DanPin_Beans(Map<String, String> params);
        public void getVerticalFromNet_DanPin_JingXuan_Beans(Map<String, String> params);

    }


}
