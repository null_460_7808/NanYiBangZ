package com.example.my.nanyibangz.utils;

import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;

/**
 * Created by My on 2016/10/13.
 */

public class CustomerNestScrollView extends NestedScrollView {
    private OnScrollToBottomListener onScrollToBottom;
    /**
     * 设置最小的滑动距离
     */
    private static final int   SCROLLLIMIT =5;
    /**
     * ScrollView正在向上滑动
     */
    public static final String SCROLL_UP = "up";
    /**
     * ScrollView正在向下滑动
     */
    public static final String  SCROLL_DOWN = "down";
    public CustomerNestScrollView(Context context) {
        super(context);
    }
    public CustomerNestScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomerNestScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    @Override
    protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
        super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
        if(scrollY != 0 && null != onScrollToBottom){
            onScrollToBottom.onScrollBottomListener(clampedY);
        }
    }
    public void setOnScrollToBottomLintener(OnScrollToBottomListener listener){
        onScrollToBottom = listener;
    }
    public interface OnScrollToBottomListener{
        public void onScrollBottomListener(boolean isBottom);
    }
    //用于监听上下滑动
    private ScrollListener mListener;
    //监听上下滑动
    public interface ScrollListener {
        public void scrollOritention(String oritentsion);
    }
    public void setScrollListener(ScrollListener uplistener) {
        this.mListener =uplistener;
    }
    /**
     * 监听上下滑动
     *
     * 第一个参数为变化后的X轴位置
     第二个参数为变化后的Y轴的位置
     第三个参数为原先的X轴的位置
     第四个参数为原先的Y轴的位置
     * @param l
     * @param t
     * @param oldl
     * @param oldt
     */
    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {

        super.onScrollChanged(l, t, oldl, oldt);
        if(oldt>t&& oldt-t>SCROLLLIMIT){
            //向下滑动了
            if (mListener!=null){
                mListener.scrollOritention(SCROLL_DOWN);
            }
        }else if (oldt<t){
            if (mListener!=null){
                //向上滑动了。
                mListener.scrollOritention(SCROLL_UP);
            }
        }
    }

}
