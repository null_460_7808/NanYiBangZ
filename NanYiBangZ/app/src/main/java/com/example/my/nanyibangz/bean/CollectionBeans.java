package com.example.my.nanyibangz.bean;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Administrator on 2016/10/19.
 */
//DatabaseTable标明数据库中的一张表 tableName指定表明
@DatabaseTable(tableName = "collectionbeans")
public class CollectionBeans {
    //DatabaseTable标明数据库中的一张表 tableName指定表明
    //DatabaseField  标明数据表中的列名(字段名)  generatedId = true 表示主键自增长
    @DatabaseField(columnName = "_id",generatedId = true)
    private  long _id;//数据库中的id,主键，
    @DatabaseField(columnName = "item_id", dataType= DataType.STRING)
    public String item_id;
    @DatabaseField(columnName = "title",dataType = DataType.STRING)
    public String title;
    @DatabaseField(columnName = "price",dataType =DataType.STRING)
    public String price;
    @DatabaseField(columnName ="imgUrl",dataType = DataType.STRING)
    public String  imgUrl;
    public CollectionBeans() {
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }


    public CollectionBeans(String item_id, String title, String price, String imgUrl) {
        this.item_id = item_id;
        this.title = title;
        this.price = price;
        this.imgUrl = imgUrl;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
