package com.example.my.nanyibangz.bean;

import java.util.List;

/**
 * Created by My on 2016/10/10.
 */

public class HomeBangBangChoice {

    /**
     * member_type : guest
     * login_status : error
     * login_status_msg : not login in
     */

    private UserBean user;
    /**
     * campaignKind : {"campain_name":"精选","campain_icon":"","campain_icon2":"","campain_color":"","show_price":true}
     * itemDetail : [{"_id":414759,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB17cCrNpXXXXXmXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"潮牌卫衣男2016秋季学生圆领套头男生字母印花青年长袖韩版外套男","coupon_price":"79.00","price":"139.00","saveCount":192,"isv_code":"0_android_jingxuan_15"},{"_id":400101,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1KNUUMVXXXXc3XVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋季男士长袖t恤 秋衣男装韩版圆领卫衣体恤学生打底衫上衣服秋装","coupon_price":"69.00","price":"128.00","saveCount":173,"isv_code":"0_android_jingxuan_15"},{"_id":412977,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1FT_eNpXXXXc5XFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"Gurbaks古克冬季新款毛衣圆领针织衫徽章棒球服条纹袖外套男GM859","coupon_price":"138.00","price":"268.00","saveCount":132,"isv_code":"0_android_jingxuan_15"},{"_id":410842,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1We5sNpXXXXc4XFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"YOHO有货潮牌LAL/数字贴布连帽套头卫衣男女通用 吴亦凡亲着","coupon_price":"269.00","price":"359.00","saveCount":138,"isv_code":"0_android_jingxuan_15"},{"_id":412876,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1HqWiNpXXXXbpXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"新款秋冬季韩版日系加厚男装宽松小清新条纹圆领套头毛衣针织衫潮","coupon_price":"98.00","price":"188.00","saveCount":6,"isv_code":"0_android_jingxuan_15"},{"_id":409799,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1jDx2NpXXXXXPapXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"纯色青少年男士长袖t恤秋季纯棉衣服男款青春学生修身v领春秋款潮","coupon_price":"68.00","price":"136.00","saveCount":104,"isv_code":"0_android_jingxuan_15"},{"_id":412926,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1Hnh_NpXXXXceXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"唐狮2016秋冬夹克男韩版落肩棒球服青少年棒球领外套潮","coupon_price":"219.00","price":"309.00","saveCount":188,"isv_code":"0_android_jingxuan_15"},{"_id":412924,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1aa5eNpXXXXXdapXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"美特斯邦威卫衣男2016冬装新款男MTEE迪士尼时尚卫衣713202专柜款","coupon_price":"169.00","price":"169.00","saveCount":146,"isv_code":"0_android_jingxuan_15"},{"_id":412813,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1XF9oNFXXXXa2XpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"Gurbaks秋季韩版潮流卫衣男圆领长袖打底衫3D立体方块男装GW844","coupon_price":"128.00","price":"238.00","saveCount":184,"isv_code":"0_android_jingxuan_15"},{"_id":412978,"productType":1,"pic_url":"http://img.taobaocdn.com/bao/uploaded/i8/TB1_KimNpXXXXaMXpXXYXGcGpXX_M2.SS2","title":"刺绣龙袍夹克男中国风男装男款秋装男士外套大码棉麻棒球服外衣潮","coupon_price":"149.00","price":"278.00","saveCount":68,"isv_code":"0_android_jingxuan_15"},{"_id":293202,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1acbtNXXXXXX3aXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"韩版男装牛仔外套男修身男士秋季夹克学生外衣大码牛仔衣潮褂上衣","coupon_price":"118.00","price":"335.00","saveCount":162,"isv_code":"0_android_jingxuan_15"},{"_id":412976,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB12DnGNXXXXXc0aXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"一秒一年潮男学生修身小脚哈伦裤男牛仔裤 秋季青年个性休闲长裤","coupon_price":"109.00","price":"194.00","saveCount":194,"isv_code":"0_android_jingxuan_15"},{"_id":412941,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB12VgZNpXXXXb3XXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"男裤秋季2016新款男士休闲裤修身小脚哈伦裤长裤黑色男生裤子男潮","coupon_price":"69.00","price":"128.00","saveCount":122,"isv_code":"0_android_jingxuan_15"},{"_id":412808,"productType":1,"pic_url":"http://img.taobaocdn.com/bao/uploaded/i1/TB1h8YhLpXXXXb5aXXXYXGcGpXX_M2.SS2","title":"酷拳青少年夹克男装秋季薄款韩版修身上衣迷彩潮流男士外套男秋装","coupon_price":"168.00","price":"389.00","saveCount":70,"isv_code":"0_android_jingxuan_15"},{"_id":404995,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1hUV0NXXXXXcuXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"allin2016秋季新款韩版长袖衬衫 休闲修身纯棉青年印花潮男衬衣","coupon_price":"138.00","price":"199.00","saveCount":193,"isv_code":"0_android_jingxuan_15"},{"_id":412782,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1gcEWMVXXXXcIXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"2016秋季新款男士T恤 青年百搭细条纹字母印花长袖T恤","coupon_price":"138.00","price":"138.00","saveCount":203,"isv_code":"0_android_jingxuan_15"},{"_id":412810,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB181H_KVXXXXXpaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"森马男装外套2016秋装新款男士连帽休闲夹克春秋学生青少年韩版潮","coupon_price":"119.90","price":"179.00","saveCount":140,"isv_code":"0_android_jingxuan_15"},{"_id":412781,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1zwglNXXXXXatapXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"viishow2016秋装新款男士长袖T恤圆领纯棉印花t恤时尚体恤上衣潮","coupon_price":"98.90","price":"99.00","saveCount":90,"isv_code":"0_android_jingxuan_15"},{"_id":412809,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1qI7yNpXXXXXQXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"沐檀秋装男装迷彩外套中长款茄克时尚连帽修身青少年新品夹克风衣","coupon_price":"117.90","price":"299.00","saveCount":74,"isv_code":"0_android_jingxuan_15"},{"_id":411970,"productType":2,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/1847637633/TB2B2RDaO2A11Bjy0FdXXbPwVXa_!!1847637633.jpg_450x10000q75.jpg","title":"2016秋季新品男式衬衫 休闲韩版翻领男装长袖衬衫 男","coupon_price":"89.00","price":"89.00","saveCount":171,"isv_code":"0_android_jingxuan_15"},{"_id":411974,"productType":2,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1.LrkLXXXXXaVXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"2016秋装男士长袖t恤男款上衣潮流青少年秋衣打底衫","coupon_price":"58.00","price":"58.00","saveCount":115,"isv_code":"0_android_jingxuan_15"},{"_id":406125,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1lA72KVXXXXXGXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"AMH男装韩版2016秋季新款圆领修身印花休闲青年长袖T恤男LL5869滈","coupon_price":"108.00","price":"158.00","saveCount":1,"isv_code":"0_android_jingxuan_15"},{"_id":411849,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1grN2KVXXXXaUXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋装新品男士休闲夹克 韩版修身青少年立领茄克时尚男装休闲外套","coupon_price":"119.00","price":"215.00","saveCount":152,"isv_code":"0_android_jingxuan_15"},{"_id":411206,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1WnmoJXXXXXcvapXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"2016新款秋季男士皮夹克外套男士皮衣潮流韩版青年修身大码男装潮","coupon_price":"89.00","price":"138.00","saveCount":136,"isv_code":"0_android_jingxuan_15"},{"_id":411202,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1gQMPKVXXXXcWXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"2016新款男士长袖t恤男款体恤上衣服卫衣男潮秋装圆领T恤男打底衫","coupon_price":"49.00","price":"199.00","saveCount":131,"isv_code":"0_android_jingxuan_15"},{"_id":409837,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1SqAcNXXXXXbyaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"新款2016秋季套头男装日系韩版学生青少年长袖圆领T恤潮上衣卫衣","coupon_price":"98.00","price":"208.00","saveCount":80,"isv_code":"0_android_jingxuan_15"},{"_id":411204,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1VZCWLFXXXXbVaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"男士春季新款牛仔夹克韩版修身青少年休闲褂外套男装上衣服潮","coupon_price":"78.00","price":"399.00","saveCount":156,"isv_code":"0_android_jingxuan_15"},{"_id":411199,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB16lGPLXXXXXa0XXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋季新款大码休闲格子衬衫男士长袖青少年修身韩版寸衬衣男装衣服","coupon_price":"59.00","price":"128.00","saveCount":76,"isv_code":"0_android_jingxuan_15"},{"_id":411186,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1wDbnMVXXXXc3XVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"男士休闲外套秋季新款连帽夹克男薄款青少年修身韩版大码上衣男装","coupon_price":"99.00","price":"199.00","saveCount":76,"isv_code":"0_android_jingxuan_15"},{"_id":411205,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1FofqNpXXXXcmXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"2016秋季新款韩版男装小腿裤男士修身潮流系带长裤青年哈伦休闲裤","coupon_price":"49.00","price":"178.00","saveCount":213,"isv_code":"0_android_jingxuan_15"},{"_id":411147,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB13PKANXXXXXXhaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"2016春秋季男士新款夹克休闲外套秋季青少年薄款韩版棒球服男装潮","coupon_price":"99.00","price":"399.00","saveCount":68,"isv_code":"0_android_jingxuan_15"},{"_id":410706,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1dm8gLpXXXXczXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"莎芭绮秋新品青少年潮男装纯棉薄夹克外套韩版潮流男时尚夹克外套","coupon_price":"138.00","price":"498.00","saveCount":191,"isv_code":"0_android_jingxuan_15"},{"_id":295436,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB101QeIFXXXXXxXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"男士秋装上衣服长袖T恤秋衣男装纯棉外穿小衫春秋季打底衫男t桖潮","coupon_price":"56.00","price":"199.00","saveCount":154,"isv_code":"0_android_jingxuan_15"},{"_id":409279,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1VR7vNXXXXXc4XVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋季新款牛仔外套男韩版修身复古外穿褂子服装青年学生夹克上衣潮","coupon_price":"128.00","price":"218.00","saveCount":67,"isv_code":"0_android_jingxuan_15"},{"_id":410713,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1wCjPLVXXXXaRXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"欧漱秋冬新品男士运动裤男棉质小脚收口直筒长裤 加大码篮球裤","coupon_price":"79.00","price":"198.00","saveCount":195,"isv_code":"0_android_jingxuan_15"},{"_id":409276,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB16BBQNpXXXXboXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋装新款男士长袖卫衣 韩版修身圆领套头T恤青年学生印花外套男潮","coupon_price":"39.00","price":"598.00","saveCount":72,"isv_code":"0_android_jingxuan_15"},{"_id":348792,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1A47RMVXXXXX1XXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋季牛仔衬衫男长袖春秋韩版修身休闲衬衣男生青年薄款外套潮男装","coupon_price":"69.00","price":"168.00","saveCount":149,"isv_code":"0_android_jingxuan_15"},{"_id":408167,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1e21gKVXXXXbeXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"麻吉岛秋季韩版修身连帽套头卫衣休闲帽子刺绣卫衣外套男生卫衣潮","coupon_price":"98.00","price":"158.00","saveCount":198,"isv_code":"0_android_jingxuan_15"},{"_id":408121,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1F7i8NpXXXXXxapXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"Gurbaks黑色牛仔外套男夹克短款韩版修身青少年男装潮流学生秋装","coupon_price":"149.00","price":"268.00","saveCount":158,"isv_code":"0_android_jingxuan_15"},{"_id":338968,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1WqnzLpXXXXc1XpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"佐潮男士外套秋季青少年韩版修身秋装新款男装休闲薄夹克棒球服男","coupon_price":"109.00","price":"228.00","saveCount":354,"isv_code":"0_android_jingxuan_15"},{"_id":408090,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1n_v2NXXXXXaiXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋季新款男士潮流卫衣青少年学生圆领套头韩版衣服休闲外套男装潮","coupon_price":"138.00","price":"698.00","saveCount":139,"isv_code":"0_android_jingxuan_15"},{"_id":411152,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1QREsNXXXXXb5XVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"套头卫衣男士秋季青年长袖学生韩版潮宽松休闲运动假两件连帽外套","coupon_price":"79.00","price":"128.00","saveCount":187,"isv_code":"0_android_jingxuan_15"},{"_id":338324,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1DEKhLpXXXXaAXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"衣品天成 2016秋装新品 男士t恤 潮 男装圆领印花长袖T恤6MT031","coupon_price":"88.00","price":"148.00","saveCount":197,"isv_code":"0_android_jingxuan_15"},{"_id":408096,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1k.fVLXXXXXcsXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋季长袖衬衫男青少年格子衬衣男士韩版花寸衫修身新款衣服男装潮","coupon_price":"59.00","price":"138.00","saveCount":81,"isv_code":"0_android_jingxuan_15"},{"_id":401555,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1w0YtLXXXXXbjXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋冬季新款男士圆领套头卫衣 韩版修身潮男装长袖青少年印花外套","coupon_price":"89.00","price":"168.00","saveCount":85,"isv_code":"0_android_jingxuan_15"},{"_id":407846,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1RhuENXXXXXcJaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"夹克男薄款修身2016新款韩版棒球服潮青年学生立领春秋季男士外套","coupon_price":"108.00","price":"258.00","saveCount":185,"isv_code":"0_android_jingxuan_15"},{"_id":397609,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1VBMxLXXXXXcoXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋季男士卫衣套头  青少年学生复古运动休闲长袖圆领条纹卫衣潮流","coupon_price":"78.00","price":"136.00","saveCount":83,"isv_code":"0_android_jingxuan_15"},{"_id":407804,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1MK8fLXXXXXchXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"Gurbaks圆领套头卫衣男青少年学生长袖秋装男韩版潮流男装GW804\n","coupon_price":"129.00","price":"238.00","saveCount":122,"isv_code":"0_android_jingxuan_15"},{"_id":407799,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1Ry7bLXXXXXa0XpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"卡策韩版套头卫衣男秋季新款青年修身休闲印花长袖T恤 潮K1640","coupon_price":"119.00","price":"399.00","saveCount":196,"isv_code":"0_android_jingxuan_15"},{"_id":407787,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1W8JNNXXXXXXxXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋装运动卫衣男 男士帽衫连帽卫衣开衫运动外套韩版青少年潮外套","coupon_price":"138.00","price":"218.00","saveCount":207,"isv_code":"0_android_jingxuan_15"},{"_id":407367,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB14XRKNXXXXXazapXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"唐狮2016秋装新款卫衣 男连帽抓绒字母印花运动青少年男长袖卫衣","coupon_price":"139.00","price":"179.00","saveCount":92,"isv_code":"0_android_jingxuan_15"},{"_id":407919,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1YusDMVXXXXXoXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"春秋季韩国板鞋男ulzzang休闲鞋青年鞋男透气悠闲鞋男运动鞋学生","coupon_price":"69.00","price":"299.00","saveCount":64,"isv_code":"0_android_jingxuan_15"},{"_id":407618,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1EIMMMVXXXXblaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"韩都衣舍男装2016秋季新款韩版修身圆领休闲毛衣男学生AE6049憉","coupon_price":"138.00","price":"218.00","saveCount":176,"isv_code":"0_android_jingxuan_15"},{"_id":400718,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1IjKbKFXXXXXqXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"森马长袖T恤男2016秋季男士秋装上衣宽松中学生体恤青少年打底衫","coupon_price":"79.90","price":"99.00","saveCount":171,"isv_code":"0_android_jingxuan_15"},{"_id":407809,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB12cWRMVXXXXc3XVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"苎萝秋季日系街头潮流圆领套头卫衣男运动韩版青少年字母印花外套\n","coupon_price":"120.00","price":"299.00","saveCount":172,"isv_code":"0_android_jingxuan_15"},{"_id":407856,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1Hr9MNXXXXXbLXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋季男士外套春秋韩版修身棒球服青年秋装休闲男装2016潮流男夹克","coupon_price":"118.00","price":"298.00","saveCount":88,"isv_code":"0_android_jingxuan_15"},{"_id":407810,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1mP4lLpXXXXXpXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋季日系潮牌连帽套头卫衣男士牛仔外套韩版潮流学院风权志龙同款","coupon_price":"98.00","price":"198.00","saveCount":209,"isv_code":"0_android_jingxuan_15"},{"_id":407587,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1VOKKLXXXXXXHXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"A21连帽卫衣男套头圆领秋季上衣 青年男士欧美长袖外套宽松潮","coupon_price":"149.00","price":"269.00","saveCount":106,"isv_code":"0_android_jingxuan_15"},{"_id":407711,"productType":1,"pic_url":"http://img.taobaocdn.com/bao/uploaded/i8/TB1NTVgLpXXXXXcXVXXYXGcGpXX_M2.SS2","title":"都青2016秋季原创男装韩版男士修身圆领卫衣潮男休闲套头棒球外套","coupon_price":"88.00","price":"150.00","saveCount":178,"isv_code":"0_android_jingxuan_15"},{"_id":401531,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1DWkqNXXXXXX0aXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"Baleno班尼路2016秋 青年印花针织连帽卫衣男 韩版休闲纯棉套头衫","coupon_price":"139.00","price":"239.90","saveCount":197,"isv_code":"0_android_jingxuan_15"},{"_id":365415,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1PTdBLpXXXXXMXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋装男士卫衣韩版潮学生圆领青少年修身长袖T恤衣服男装秋季外套","coupon_price":"88.00","price":"128.00","saveCount":10,"isv_code":"0_android_jingxuan_15"},{"_id":407875,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB10feYNXXXXXaWXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"361度男鞋2016秋季新款缓震透气跑步鞋时尚休闲鞋迷彩361运动鞋","coupon_price":"179.00","price":"299.00","saveCount":201,"isv_code":"0_android_jingxuan_15"},{"_id":407714,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1lwzeMVXXXXb0XFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"Gurbaks男士卫衣2016秋季新款韩版修身休闲套头卫衣宽松男装GW831\n","coupon_price":"125.00","price":"228.00","saveCount":95,"isv_code":"0_android_jingxuan_15"},{"_id":346490,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1V5CHNFXXXXa_XVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"viishow秋装男士套头连帽卫衣男款潮牌青少年上衣男生薄款外套","coupon_price":"127.00","price":"159.00","saveCount":157,"isv_code":"0_android_jingxuan_15"},{"_id":405426,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1sBeiLXXXXXXBXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"唐狮2016新款秋季裤子弹力休闲裤男男士修身小脚裤裤子男裤长裤潮\n","coupon_price":"129.00","price":"199.00","saveCount":71,"isv_code":"0_android_jingxuan_15"},{"_id":407717,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1NWjaNpXXXXbHaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"Gurbaks2016秋冬新品高档烫字长袖圆领套头男士修身黑色卫衣GW801","coupon_price":"138.00","price":"268.00","saveCount":104,"isv_code":"0_android_jingxuan_15"},{"_id":356081,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1IND3MXXXXXXvXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"美特斯邦威套头卫衣男春秋季青年纯棉针织衫学生卫衣韩版潮男装#","coupon_price":"139.00","price":"259.00","saveCount":161,"isv_code":"0_android_jingxuan_15"},{"_id":404934,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1Nrh5LpXXXXcEaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋季新款男士长袖T恤 韩版休闲圆领体恤潮男装青少年上衣服打底衫","coupon_price":"26.90","price":"69.00","saveCount":198,"isv_code":"0_android_jingxuan_15"},{"_id":404940,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1HmGjLpXXXXa_XFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋季青少年纯棉长袖T恤男士圆领潮流字母上衣学生情侣装体恤卫衣","coupon_price":"45.00","price":"99.00","saveCount":62,"isv_code":"0_android_jingxuan_15"},{"_id":404890,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1cRlMKVXXXXaDXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"长袖T恤2016秋季新款男士圆领体恤衣服青少年潮流贴布长袖T恤上衣","coupon_price":"79.00","price":"145.00","saveCount":139,"isv_code":"0_android_jingxuan_15"},{"_id":333505,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB15WyKJVXXXXbVXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"AMH男装韩版2016秋装新款青年圆领印花修身白色打底衫长袖T恤男燑","coupon_price":"59.00","price":"208.00","saveCount":121,"isv_code":"0_android_jingxuan_15"},{"_id":404318,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1M_xWLpXXXXbuXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"初秋日系男装韩版宽松青年文艺小清新条纹休闲长袖衬衫薄款外套男","coupon_price":"109.00","price":"209.00","saveCount":133,"isv_code":"0_android_jingxuan_15"},{"_id":332054,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1MBOJNFXXXXa3XXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"all in 2016春秋韩版套头圆领潮牌卫衣 男时尚骷髅头印花长袖T恤","coupon_price":"98.00","price":"135.00","saveCount":143,"isv_code":"0_android_jingxuan_15"},{"_id":402530,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1yCuNNpXXXXa9XVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"森马青少年长袖T恤男学生2016秋装新款男士休闲宽松韩版潮流男装","coupon_price":"79.90","price":"89.00","saveCount":74,"isv_code":"0_android_jingxuan_15"},{"_id":331229,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB13QSeLXXXXXaFXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"【预售】AMH男装韩版2016秋季新款圆领青年套头休闲长袖t恤男潮鹄","coupon_price":"118.00","price":"168.00","saveCount":68,"isv_code":"0_android_jingxuan_15"},{"_id":397654,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB13XaaNFXXXXayaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"allin2016秋季新款韩版男士外套 修身帅气牛仔青年学生潮男薄夹克","coupon_price":"185.00","price":"399.00","saveCount":185,"isv_code":"0_android_jingxuan_15"},{"_id":404878,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1CpVELXXXXXcIaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"男士长袖t恤 潮流秋衣体恤男装 白色修身上衣服打底衫 2016秋新款","coupon_price":"78.00","price":"148.00","saveCount":156,"isv_code":"0_android_jingxuan_15"},{"_id":393259,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1U6KFKFXXXXaTaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"衣品天成 2016秋季新款T恤 韩版时尚撞色粗条纹男士圆领长袖T恤潮","coupon_price":"108.00","price":"178.00","saveCount":190,"isv_code":"0_android_jingxuan_15"},{"_id":403829,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1dVMFMVXXXXa4apXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋季男士连帽卫衣男 套头带帽韩版潮学生潮牌帽衫情侣外套潮大码","coupon_price":"138.00","price":"238.00","saveCount":169,"isv_code":"0_android_jingxuan_15"},{"_id":403826,"productType":1,"pic_url":"http://img.taobaocdn.com/bao/uploaded/i2/TB1_k1gLXXXXXapaXXXYXGcGpXX_M2.SS2","title":"2016春秋季卫衣男青年修身韩版青少年运动服男潮学生休闲运动套装","coupon_price":"178.00","price":"298.00","saveCount":164,"isv_code":"0_android_jingxuan_15"},{"_id":403806,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1Z6qLKVXXXXXpapXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"2016秋季男士夹克休闲外套短款风衣青年男装韩版修身春秋装外衣","coupon_price":"198.00","price":"248.00","saveCount":174,"isv_code":"0_android_jingxuan_15"},{"_id":403795,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1J5jhMVXXXXapXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"AMH男装韩版2016秋季新款修身青年休闲圆领长袖男士t恤男PF5809麒","coupon_price":"118.00","price":"168.00","saveCount":136,"isv_code":"0_android_jingxuan_15"},{"_id":403850,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1xwsoMVXXXXbhaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"衣品天成 男装牛仔裤 2016秋季新品男士长裤子 水洗牛仔裤 男","coupon_price":"138.00","price":"138.00","saveCount":181,"isv_code":"0_android_jingxuan_15"},{"_id":392943,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB15jGmKVXXXXbSXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"AMH男装韩版2016秋季新款青年修身运动裤休闲裤男裤子慢跑裤潮燊","coupon_price":"138.00","price":"168.00","saveCount":89,"isv_code":"0_android_jingxuan_15"},{"_id":403065,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB16jIIMVXXXXarapXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"AMH男装韩版2016秋季新款圆领套头胶印印花长袖T恤男士潮NQ6625煷","coupon_price":"146.00","price":"198.00","saveCount":199,"isv_code":"0_android_jingxuan_15"},{"_id":403056,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1JjzMMVXXXXXJXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"美特斯邦威长袖T恤男士2016秋冬装新款韩版印花纯棉圆领针织衫潮#","coupon_price":"79.00","price":"119.00","saveCount":171,"isv_code":"0_android_jingxuan_15"},{"_id":403154,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1wpJjLXXXXXa4XFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"新款秋季休闲裤男修身直筒男裤青年弹力松紧腰长裤子黑色磨毛大码","coupon_price":"68.00","price":"198.00","saveCount":170,"isv_code":"0_android_jingxuan_15"},{"_id":403077,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1QMTmLpXXXXclXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"潮流个性人物图案圆领白色长袖t恤男士上衣潮男秋季韩版修身小衫","coupon_price":"69.00","price":"135.00","saveCount":69,"isv_code":"0_android_jingxuan_15"},{"_id":403076,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB10E8NLpXXXXXIaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"奢潮长袖T恤男修身薄款套头秋季学生装弹力体恤运动男士圆领上衣","coupon_price":"88.00","price":"88.00","saveCount":117,"isv_code":"0_android_jingxuan_15"},{"_id":403070,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1MAesMVXXXXcPaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"空城计日系青少年秋季男装新款潮流韩版条纹长袖T恤卫衣宽松休闲","coupon_price":"99.00","price":"228.00","saveCount":86,"isv_code":"0_android_jingxuan_15"},{"_id":403054,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1BKrxKVXXXXbLXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"2016秋季新款竖条纹长袖T恤男韩版青少年圆领宽松九分袖潮流t恤衫","coupon_price":"78.00","price":"148.00","saveCount":176,"isv_code":"0_android_jingxuan_15"},{"_id":403048,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1XXoKLXXXXXXBXVXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"杜恩简 T恤男秋季新品青少年体恤时尚迷彩男装 学生长袖上衣服","coupon_price":"69.00","price":"128.00","saveCount":88,"isv_code":"0_android_jingxuan_15"},{"_id":403069,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1ngxbNXXXXXXLaFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"唐森2016秋季新款男士个性条纹休闲长袖t恤男韩版修身百搭男上衣","coupon_price":"99.00","price":"188.00","saveCount":80,"isv_code":"0_android_jingxuan_15"},{"_id":403051,"productType":1,"pic_url":"http://img02.taobaocdn.com/bao/uploaded/i2/TB1__NtLXXXXXcyXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"2016秋季新款T恤韩版时尚印花人头卫衣男士圆领长袖T恤潮打底衫","coupon_price":"79.00","price":"139.00","saveCount":208,"isv_code":"0_android_jingxuan_15"},{"_id":403039,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1FankLXXXXXXoXFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"秋装新款男士长袖T恤 韩版修身迷彩青少年秋衣潮男装打底印花体恤","coupon_price":"69.00","price":"128.00","saveCount":117,"isv_code":"0_android_jingxuan_15"},{"_id":403038,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1lg04NpXXXXaRXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"KOJO长袖t恤 男装秋季新品黑白条纹上衣打底衣青年 长袖t恤 男士","coupon_price":"108.00","price":"208.00","saveCount":92,"isv_code":"0_android_jingxuan_15"},{"_id":403035,"productType":1,"pic_url":"http://img01.taobaocdn.com/bao/uploaded/i1/TB1Un7SMVXXXXXEXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"AMH男装韩版2016秋季新款青年男休闲圆领印花长袖T恤男士NR5461輣","coupon_price":"106.00","price":"168.00","saveCount":59,"isv_code":"0_android_jingxuan_15"},{"_id":403034,"productType":1,"pic_url":"http://img.taobaocdn.com/bao/uploaded/i2/TB16roDLXXXXXcTXpXXYXGcGpXX_M2.SS2","title":"2016秋季新款潮男青少年条纹印花休闲修身T恤男士圆领长袖打底衫","coupon_price":"69.00","price":"128.00","saveCount":184,"isv_code":"0_android_jingxuan_15"},{"_id":396752,"productType":1,"pic_url":"http://img04.taobaocdn.com/bao/uploaded/i4/TB1MekvNXXXXXc9XFXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"Lilbetter男士夹克 韩版修身型迷彩外衣青少年秋装潮牌褂子男外套","coupon_price":"277.90","price":"278.00","saveCount":44,"isv_code":"0_android_jingxuan_15"},{"_id":403031,"productType":1,"pic_url":"http://img03.taobaocdn.com/bao/uploaded/i3/TB1x0JANpXXXXbIaXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg","title":"美特斯邦威长袖T恤男士2016冬装新款纯棉针织衫韩版套头打底衫","coupon_price":"79.00","price":"139.00","saveCount":97,"isv_code":"0_android_jingxuan_15"}]
     */

    private DataBean data;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class UserBean {
        private String member_type;
        private String login_status;
        private String login_status_msg;

        public String getMember_type() {
            return member_type;
        }

        public void setMember_type(String member_type) {
            this.member_type = member_type;
        }

        public String getLogin_status() {
            return login_status;
        }

        public void setLogin_status(String login_status) {
            this.login_status = login_status;
        }

        public String getLogin_status_msg() {
            return login_status_msg;
        }

        public void setLogin_status_msg(String login_status_msg) {
            this.login_status_msg = login_status_msg;
        }
    }

    public static class DataBean {
        /**
         * campain_name : 精选
         * campain_icon :
         * campain_icon2 :
         * campain_color :
         * show_price : true
         */

        private CampaignKindBean campaignKind;
        /**
         * _id : 414759
         * productType : 1
         * pic_url : http://img04.taobaocdn.com/bao/uploaded/i4/TB17cCrNpXXXXXmXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg
         * title : 潮牌卫衣男2016秋季学生圆领套头男生字母印花青年长袖韩版外套男
         * coupon_price : 79.00
         * price : 139.00
         * saveCount : 192
         * isv_code : 0_android_jingxuan_15
         */

        private List<ItemDetailBean> itemDetail;

        public CampaignKindBean getCampaignKind() {
            return campaignKind;
        }

        public void setCampaignKind(CampaignKindBean campaignKind) {
            this.campaignKind = campaignKind;
        }

        public List<ItemDetailBean> getItemDetail() {
            return itemDetail;
        }

        public void setItemDetail(List<ItemDetailBean> itemDetail) {
            this.itemDetail = itemDetail;
        }

        public static class CampaignKindBean {
            private String campain_name;
            private String campain_icon;
            private String campain_icon2;
            private String campain_color;
            private boolean show_price;

            public String getCampain_name() {
                return campain_name;
            }

            public void setCampain_name(String campain_name) {
                this.campain_name = campain_name;
            }

            public String getCampain_icon() {
                return campain_icon;
            }

            public void setCampain_icon(String campain_icon) {
                this.campain_icon = campain_icon;
            }

            public String getCampain_icon2() {
                return campain_icon2;
            }

            public void setCampain_icon2(String campain_icon2) {
                this.campain_icon2 = campain_icon2;
            }

            public String getCampain_color() {
                return campain_color;
            }

            public void setCampain_color(String campain_color) {
                this.campain_color = campain_color;
            }

            public boolean isShow_price() {
                return show_price;
            }

            public void setShow_price(boolean show_price) {
                this.show_price = show_price;
            }
        }

        public static class ItemDetailBean {
            private int _id;
            private int productType;
            private String pic_url;
            private String title;
            private String coupon_price;
            private String price;
            private int saveCount;
            private String isv_code;

            public int get_id() {
                return _id;
            }

            public void set_id(int _id) {
                this._id = _id;
            }

            public int getProductType() {
                return productType;
            }

            public void setProductType(int productType) {
                this.productType = productType;
            }

            public String getPic_url() {
                return pic_url;
            }

            public void setPic_url(String pic_url) {
                this.pic_url = pic_url;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getCoupon_price() {
                return coupon_price;
            }

            public void setCoupon_price(String coupon_price) {
                this.coupon_price = coupon_price;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public int getSaveCount() {
                return saveCount;
            }

            public void setSaveCount(int saveCount) {
                this.saveCount = saveCount;
            }

            public String getIsv_code() {
                return isv_code;
            }

            public void setIsv_code(String isv_code) {
                this.isv_code = isv_code;
            }
        }
    }
}
