package com.example.my.nanyibangz.config;

/**
 * Created by My on 2016/10/5.
 */
public class DanPinUrlConfig {
    /**
     * 这是老师的例子
     * <p>
     * public static final String URL_VERTICAL="http://capi.douyucdn.cn/api/v1/getVerticalRoom?limit=20&offset=0&time=1470800460";
     * public static class Path{
     * //因为后台会分成生产环境和测试环境,所以域名地址必须单独提出来
     * public static final String BASE_URL="http://capi.douyucdn.cn/";
     * //斗鱼获取颜值列表的接口,传递参数获取的数值limit 偏移量 offset 时间time 毫秒数/1000   name
     * public static final String URL_VERTICAL="api/v1/getVerticalRoom";
     * <p>
     * }
     * <p>
     * public static  class Params{
     * public static final String LIMIT="limit";
     * public static final String OFFSET="offset";
     * public static final String TIME="time";
     * }
     * public static class DefaultVaule{
     * public static final String LIMIT_VALUE="20";
     * }
     */
    //商品评论
    //http://api.nanyibang.com/score-comment?age=19&channel=oppo&hkm_sign2=33c04d34e4ce711a0ceee9a3bea71771&item_id=406340
    // &member_id=292720&member_type=member&random_key=37634&system_name=android&versionCode=219
    //  http://api.nanyibang.com/select-condition?administrativeArea=%E5%8C%97%E4%BA%AC%E5%B8%82&age=24&channel=yingyongbao&country=%E4%B8%AD%E5%9B%BD&locality=%E5%8C%97%E4%BA%AC%E5%B8%82&system_name=android&versionCode=219
    public static class Path {
        //因为后台会分成生产环境和测试环境,所以域名地址必须单独提出来
        //单品的基地址
        public static final String BASE_URL = "http://api.nanyibang.com/";
        //单品获取颜值列表的接口
        public static final String DanPin_URL_VERTICAL = "select-condition";
        public static final String DanPin_JingXuan_URL_VERTICAL = "tuijian-product";
        public static final String DanPin_Type_Click_URL_VERTiCAL = "single-product";
        //例如最低里的每一项点击
        public static final String DanPin_Type_Click_URL_Lower_VERTICAL = "items";//推荐，最高，最低，最热都一样
        public static final String DanPin_Type_Comment_URL_VERTICAL="score-comment";
        public  static  final  String DanPin_Serach_Hot_Single_URL_VERTCAL="hot-words";
    }
    //单品精选gridview对应的地址。
    //http://api.nanyibang.com/tuijian-product?age=19&cateId=2&channel=xiaomi&page=1&system_name=android&versionCode=219

    //最低里每一项再点击。
    //http://api.nanyibang.com/items?age=19&channel=oppo&hkm_sign2=e25852511d05cec90c5cda0c6fe8a700&item_id=406340
    // &member_id=292720&member_type=member&random_key=72826&system_name=android&versionCode=219

    //变量，相当于map中的键
    public static class Params {
        public static final String AdministrativeArea = "administrativeArea";
        public static final String Age = "age";
        public static final String CateId = "cateId";
        public static final String Page = "page";
        public static final String Channel = "channel";
        public static final String Country = "country";
        public static final String Locality = "locality";
        public static final String System_name = "system_name";
        public static final String VersionCode = "versionCode";
        public static final String Hkm_sign2 = "hkm_sign2";
        public static final String Member_id = "member_id";
        public static final String Member_type = "member_type";
        public static final String selectType = "selectType";
        public static final String Random_key = "random_key";
        public static final String Cate_Id = "cate_id";
        public static final String Item_id = "item_id";


    }

    public static class DefaultVaule {
        public static final String AdministrativeArea_VALUE = "%E5%8C%97%E4%BA%AC%E5%B8%82";
        public static final String Age_VALUE = "19";
        public static final String Channel_VALUE = "xiaomi";
        public static final String Country_VALUE = "%E4%B8%AD%E5%9B%BD";
        public static final String Locality_VALUE = "%E5%8C%97%E4%BA%AC%E5%B8%82";
        public static final String VersionCode_VALUE = "219";
        public static final String System_name_VALUE = "android";

        //单品type的gridview点击每一项的地址
        //http://api.nanyibang.com/single-product?age=19&cate_id=22&channel=oppo&hkm_sign2=fa93417d5b99379e9afaa73907e2dc10&
        // member_id=292720&member_type=member&page=1&random_key=56786&selectType=default&system_name=android&versionCode=219
        public static final String Hkm_sign2_VALUE = "fa93417d5b99379e9afaa73907e2dc10";
        public static final String Member_id_VALUE = "292720";
        public static final String Member_type_VALUE = "member";
        public static final String Random_key_VALUE = "56786";
    }


}
