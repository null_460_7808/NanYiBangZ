package com.example.my.nanyibangz.ui.mine;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.UpdateListener;

/**
 * Created by Administrator on 2016/10/24.
 */

public class MineSheZhi extends BaseActivity {


    @Bind(R.id.tv_changepass)
    TextView tvChangepass;
    @Bind(R.id.tv_exit)
    TextView tvExit;

    @Override
    public int getLayoutId() {
        return R.layout.mine_shezhi;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        ClickExit();
        Change();
    }

    private void ClickExit() {
        tvExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BmobUser.logOut();   //清除缓存用户对象
                BmobUser currentUser = BmobUser.getCurrentUser(); // 现在的currentUser是null了
                Toast.makeText(MineSheZhi.this, "退出成功！！", Toast.LENGTH_SHORT).show();
                MineFragment.flag = true;
                finish();
            }
        });
    }
    private void Change(){
        tvChangepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MineSheZhi.this,MineChangPass.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
