package com.example.my.nanyibangz.ui.shuaiba;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;

/**
 * Created by Administrator on 2016/10/14.
 */

public class ShuaiBaClickActivity extends BaseActivity {


    @Bind(R.id.iv_shuaibaBack)
    ImageView ivShuaibaBack;
    @Bind(R.id.tv_shuaibaTitle)
    TextView tvShuaibaTitle;
    @Bind(R.id.iv_shuaibaMenu)
    ImageView ivShuaibaMenu;
    @Bind(R.id.wv_shuaiba)
    WebView wvShuaiba;
    @Bind(R.id.pop_main)
    LinearLayout popMain;
    private int dressId;
    private String title, url;
    private String link = "http://www.nanyibang.com/school/school.php?id=";
    private PopupWindow popupWindow;
    View customView;
    TextView tv1;

    @Override
    public int getLayoutId() {
        return R.layout.shuaiba_click;
    }

    private void initView() {
        Intent intent = getIntent();
        dressId = intent.getExtras().getInt("dressId");
        Log.i("tag", "id=============" + dressId);
        title = intent.getExtras().getString("title");
        url = link + dressId + "";
        Log.i("tag", "url---------" + url);
        tvShuaibaTitle.setText(title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        initView();
        initWebView();
        Click();
        ShareSDK.initSDK(this);//初始化sharedsdk

    }

    //WebView的基本设置
    private void initWebView() {
        //支持js脚本
        wvShuaiba.getSettings().setJavaScriptEnabled(true);
        //设置可以访问文件
        wvShuaiba.getSettings().setAllowContentAccess(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            wvShuaiba.getSettings().setAllowFileAccessFromFileURLs(true);
        }
        //WebSettings settings = webview.getSettings();
        //设置启动缓存
        wvShuaiba.getSettings().setAppCacheEnabled(true);
        //设置缓存的大小
        wvShuaiba.getSettings().setAppCacheMaxSize(1024 * 10);
        //设置缓存模式
        wvShuaiba.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);//优先使用缓存
        //settings.setCacheMode(WebSettings.LOAD_NO_CACHE);//不使用缓存

        //设置缓存，离线应用
        wvShuaiba.getSettings().setAppCacheEnabled(true);

        wvShuaiba.getSettings().setSupportZoom(true);
        //设置可以自动加载图片
        wvShuaiba.getSettings().setLoadsImagesAutomatically(true);
        //wvShuaiba.getSettings().setAppCacheEnabled(true);
        wvShuaiba.getSettings().setLoadWithOverviewMode(true);
        wvShuaiba.getSettings().setUseWideViewPort(true);
        wvShuaiba.getSettings().setPluginState(WebSettings.PluginState.ON);
        wvShuaiba.getSettings().setDomStorageEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            wvShuaiba.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        wvShuaiba.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        wvShuaiba.loadUrl(url);
    }

    private void Click() {
        ivShuaibaBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ivShuaibaMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                } else {
                    initmPopupWindowView();
                    initView2();
                    //顶部
                    popupWindow.showAsDropDown(v, 10, 22);
                    //从底部弹出
                    //popupWindow.showAtLocation(popMain, Gravity.BOTTOM, 0, 0);
                }

            }
        });
    }

    private void initmPopupWindowView() {

        // // 获取自定义布局文件pop.xml的视图
        customView = getLayoutInflater().inflate(R.layout.dialogdemo,
                null, false);
        popupWindow = new PopupWindow(customView, 320, 180);
//        popupWindow = new PopupWindow(customView,
//                ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.WRAP_CONTENT);
        popupWindow.setFocusable(true);
        //弹出的动画
        popupWindow.setAnimationStyle(R.style.Anim_scale);
        popupWindow.setOutsideTouchable(true);
        customView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (popupWindow != null && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                    popupWindow = null;
                }
                return false;
            }
        });

    }

    private void initView2() {
        tv1 = (TextView) customView.findViewById(R.id.button2);
        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Shared();
            }
        });
    }

    private void Shared() {
        //1.创建一键分享的对象
        OnekeyShare onekeyShare = new OnekeyShare();
        onekeyShare.disableSSOWhenAuthorize();//关闭sso授权
        //设置一键分享的内容
        onekeyShare.setTitle("一键分享的标题");
        onekeyShare.setText("一键分享的内容");
        onekeyShare.setImageUrl("http://img4.imgtn.bdimg.com/it/u=2291132731,375716043&fm=21&gp=0.jpg");
        onekeyShare.show(ShuaiBaClickActivity.this);
    }

}
