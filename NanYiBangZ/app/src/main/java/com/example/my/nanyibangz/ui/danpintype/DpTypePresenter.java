package com.example.my.nanyibangz.ui.danpintype;

import com.example.my.nanyibangz.bean.DanPinTypeBeans;

import java.util.List;
import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/9.
 */

public class DpTypePresenter implements DpTypeContact.Presenter {
    private  DpTypeContact.Model model;
    private  DpTypeContact.View view;
    public  DpTypePresenter(DpTypeContact.View view){
        this.view=view;
        model=new DpTypeModel();

    }
    @Override
    public void getVetticalFromNetDpTypeBeans(Map<String, String> params) {
        model.getVerticalDpTypeBeans(params, new Subscriber<DanPinTypeBeans>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(DanPinTypeBeans danPinTypeClickBeans) {
                if (danPinTypeClickBeans!=null){
                    List<DanPinTypeBeans.DataBean> list=danPinTypeClickBeans.getData();
                    view.getVerticalSuccessDpTypeBeans(list);
                }


            }
        });

    }
}
