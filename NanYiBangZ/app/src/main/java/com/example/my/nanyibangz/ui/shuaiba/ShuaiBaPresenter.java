package com.example.my.nanyibangz.ui.shuaiba;

import com.example.my.nanyibangz.bean.ShuaiBaBean;

import java.util.List;
import java.util.Map;

import rx.Subscriber;

/**
 * Created by Administrator on 2016/10/14.
 */

public class ShuaiBaPresenter implements ShuaiBaContact.Presenter {
    private ShuaiBaContact.Model model;
    private ShuaiBaContact.View view;

    public ShuaiBaPresenter(ShuaiBaContact.View view) {
        model=new ShuaiBaModel();
        this.view = view;
    }

    @Override
    public void getVerticalFromNet_ShuaiBa(Map<String, String> params) {
        model.getVerticalShuaiBa(params, new Subscriber<ShuaiBaBean>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(ShuaiBaBean shuaiBaBean) {
                List<ShuaiBaBean.DataBean> list=shuaiBaBean.getData();
                view.onVerticalSucess_ShuaiBa(list);
            }
        });
    }
}
