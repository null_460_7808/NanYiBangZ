package com.example.my.nanyibangz.ui.danpintype;

import com.example.my.nanyibangz.base.IModel;
import com.example.my.nanyibangz.base.IPresenter;
import com.example.my.nanyibangz.base.IView;
import com.example.my.nanyibangz.bean.DanPinTypeBeans;

import java.util.List;
import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/9.
 */

public class DpTypeContact {
    public interface View extends IView {
        public void getVerticalSuccessDpTypeBeans(List<DanPinTypeBeans.DataBean> danPinTypeClickBeansList);
        public  void getVeticalFailedDpTypeBeans(String msg);
    }

    public  interface Model extends IModel{
        public  void getVerticalDpTypeBeans(Map<String, String> params, Subscriber<DanPinTypeBeans> subscriber);
    }

    public  interface Presenter extends IPresenter{
        public  void getVetticalFromNetDpTypeBeans(Map<String, String> params);
    }


}
