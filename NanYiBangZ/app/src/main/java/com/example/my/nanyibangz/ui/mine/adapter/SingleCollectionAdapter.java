package com.example.my.nanyibangz.ui.mine.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.CollectionBeans;
import com.example.my.nanyibangz.image.DanPinImageHelper;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by My on 2016/10/8.
 * 朱留宝，实现单品收藏及展示。
 */

public class SingleCollectionAdapter extends BaseAdapter {
    private Context context;
    private List<CollectionBeans> list;

    public SingleCollectionAdapter(Context context, List<CollectionBeans> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.danpin_gv_type_click_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.txtDanpinTypeClickPrice.setText("¥" + list.get(position).getPrice()+"");
        viewHolder.txtDanpinTypeClickDesc.setText(list.get(position).getTitle());
        String imgUrl = list.get(position).getImgUrl();
        DanPinImageHelper.showImage_Jingping(context, imgUrl, viewHolder.imgDanpinTypeClick);
        return convertView;
    }


    static class ViewHolder {
        @Bind(R.id.img_danpin_type_click)
        ImageView imgDanpinTypeClick;
        @Bind(R.id.txt_danpin_type_click_desc)
        TextView txtDanpinTypeClickDesc;
        @Bind(R.id.txt_danpin_type_click_price)
        TextView txtDanpinTypeClickPrice;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
