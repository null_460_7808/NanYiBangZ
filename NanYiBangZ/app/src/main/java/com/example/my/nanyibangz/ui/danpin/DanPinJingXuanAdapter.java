package com.example.my.nanyibangz.ui.danpin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.DataBean;
import com.example.my.nanyibangz.image.DanPinImageHelper;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by My on 2016/10/8.
 */

public class DanPinJingXuanAdapter extends BaseAdapter {
    private Context context;
    private List<DataBean> list;

    public DanPinJingXuanAdapter(Context context, List<DataBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder=null;
        if (convertView == null) {

            convertView = LayoutInflater.from(context).inflate(R.layout.danpin_gv_jingxuan_item, null);
            viewHolder=new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder= (ViewHolder) convertView.getTag();
        }
        viewHolder.txtDanpinJingxuanPrice.setText("¥"+list.get(position).getCouponPrice());
        viewHolder.txtDanpinJingxuanDesc.setText(list.get(position).getTitle());
        String imgUrl=list.get(position).getPicUrl();
        DanPinImageHelper.showImage_Jingping(context,imgUrl,viewHolder.imgDanpinJingxuan);
        return  convertView;
    }


    static class ViewHolder {
        @Bind(R.id.img_danpin_jingxuan)
        ImageView imgDanpinJingxuan;
        @Bind(R.id.txt_danpin_jingxuan_desc)
        TextView txtDanpinJingxuanDesc;
        @Bind(R.id.txt_danpin_jingxuan_price)
        TextView txtDanpinJingxuanPrice;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
