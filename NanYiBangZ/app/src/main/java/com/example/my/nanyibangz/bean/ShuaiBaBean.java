package com.example.my.nanyibangz.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2016/10/8.
 */

public class ShuaiBaBean {

    /**
     * member_type : guest
     * login_status : error
     * login_status_msg : not login in
     */

    @SerializedName("user")
    private UserBean user;
    /**
     * dress_school_id : 559
     * title : 经典潮流发型
     * info : 4个永不退流行的经典发型，从此没有撩不到的妹！
     * image : http://im01.nanyibang.com/school/2016/09/28/SCHOOL131257.jpg
     * clickCount : 24134
     * link : http://www.nanyibang.com/utils/webpage_jump.php?module_type=school&school_id=559
     */

    @SerializedName("data")
    private List<DataBean> data;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class UserBean {
        @SerializedName("member_type")
        private String memberType;
        @SerializedName("login_status")
        private String loginStatus;
        @SerializedName("login_status_msg")
        private String loginStatusMsg;

        public String getMemberType() {
            return memberType;
        }

        public void setMemberType(String memberType) {
            this.memberType = memberType;
        }

        public String getLoginStatus() {
            return loginStatus;
        }

        public void setLoginStatus(String loginStatus) {
            this.loginStatus = loginStatus;
        }

        public String getLoginStatusMsg() {
            return loginStatusMsg;
        }

        public void setLoginStatusMsg(String loginStatusMsg) {
            this.loginStatusMsg = loginStatusMsg;
        }
    }

    public static class DataBean {
        @SerializedName("dress_school_id")
        private int dressSchoolId;
        @SerializedName("title")
        private String title;
        @SerializedName("info")
        private String info;
        @SerializedName("image")
        private String image;
        @SerializedName("clickCount")
        private int clickCount;
        @SerializedName("link")
        private String link;

        public int getDressSchoolId() {
            return dressSchoolId;
        }

        public void setDressSchoolId(int dressSchoolId) {
            this.dressSchoolId = dressSchoolId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public int getClickCount() {
            return clickCount;
        }

        public void setClickCount(int clickCount) {
            this.clickCount = clickCount;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }
    }
}
