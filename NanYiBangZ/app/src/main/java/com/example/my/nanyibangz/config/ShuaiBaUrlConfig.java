package com.example.my.nanyibangz.config;

/**
 * Created by My on 2016/10/5.
 */
public class ShuaiBaUrlConfig {
    /**
     *
     * 这是老师的例子
     *
     *  public static final String URL_VERTICAL="http://capi.douyucdn.cn/api/v1/getVerticalRoom?limit=20&offset=0&time=1470800460";
     public static class Path{
     //因为后台会分成生产环境和测试环境,所以域名地址必须单独提出来
     public static final String BASE_URL="http://capi.douyucdn.cn/";
     //斗鱼获取颜值列表的接口,传递参数获取的数值limit 偏移量 offset 时间time 毫秒数/1000   name
     public static final String URL_VERTICAL="api/v1/getVerticalRoom";

     }

     public static  class Params{
     public static final String LIMIT="limit";
     public static final String OFFSET="offset";
     public static final String TIME="time";
     }
     public static class DefaultVaule{
     public static final String LIMIT_VALUE="20";
     }
     */

    public static class Path{
        //因为后台会分成生产环境和测试环境,所以域名地址必须单独提出来
        //单品的基地址
        public static final String BASE_URL="http://api.nanyibang.com/";
        //单品获取颜值列表的接口
        public static final String ShuaiBa_URL_VERTICAL="dress-school";
    }
    //http://api.nanyibang.com/dress-school?age=19&channel=xiaomi&kind=6&page=1&system_name=android&versionCode=219

    //变量，相当于map中的键
    public static  class Params{
        public static final String Age="age";
        public static final String Page="page";
        public static final String Channel="channel";
        public static final String Kind="kind";
        public static final String System_name="system_name";
        public static final String VersionCode="versionCode";
    }
    public static class DefaultVaule{
        public static final String Age_VALUE="19";
        public static final String Page_VALUE="1";
        public static final String Channel_VALUE="xiaomi";
        public static final String Kind_VALUE="6";
        public static final String VersionCode_VALUE="219";
        public static final String System_name_VALUE="android";
    }



}
