package com.example.my.nanyibangz.ui.shouye.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.HomeChoiceness;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

/**
 * 首页搭配精选数据适配器
 */

public class HomeMatchChoiceRecyclerAdapter extends RecyclerView.Adapter<HomeMatchChoiceRecyclerAdapter.HomeThirdMatchChoiceViewHolder>{
    private Context context;
    private List<HomeChoiceness.DataBean.MatchThemesBean> list;

    public HomeMatchChoiceRecyclerAdapter(Context context, List<HomeChoiceness.DataBean.MatchThemesBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public HomeThirdMatchChoiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.home_matchchoice_item,null);
        HomeThirdMatchChoiceViewHolder holder = new HomeThirdMatchChoiceViewHolder(convertView);
        return holder;
    }

    @Override
    public void onBindViewHolder(HomeThirdMatchChoiceViewHolder holder, int position) {
        //数据加载
        holder.home_collocationChoice_item_sdv_left.setImageURI(Uri.parse(list.get(position).getMatches().get(0).getBig_image()));
        holder.home_collocationChoice_item_sdv_righttop.setImageURI(Uri.parse(list.get(position).getMatches().get(1).getBig_image()));
        holder.home_collocationChoice_item_sdv_rightbottom.setImageURI(Uri.parse(list.get(position).getMatches().get(2).getBig_image()));
        holder.home_collocationChoice_item_tv_themeDesc.setText(list.get(position).getThemeDesc());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class HomeThirdMatchChoiceViewHolder extends RecyclerView.ViewHolder{
        SimpleDraweeView home_collocationChoice_item_sdv_left,home_collocationChoice_item_sdv_righttop,home_collocationChoice_item_sdv_rightbottom;
        TextView home_collocationChoice_item_tv_themeDesc;
        public HomeThirdMatchChoiceViewHolder(View itemView) {
            super(itemView);
            home_collocationChoice_item_sdv_left = (SimpleDraweeView) itemView.findViewById(R.id.home_collocationChoice_item_sdv_left);
            home_collocationChoice_item_sdv_righttop = (SimpleDraweeView) itemView.findViewById(R.id.home_collocationChoice_item_sdv_righttop);
            home_collocationChoice_item_sdv_rightbottom = (SimpleDraweeView) itemView.findViewById(R.id.home_collocationChoice_item_sdv_rightbottom);
            home_collocationChoice_item_tv_themeDesc = (TextView) itemView.findViewById(R.id.home_collocationChoice_item_tv_themeDesc);
        }
    }
}
