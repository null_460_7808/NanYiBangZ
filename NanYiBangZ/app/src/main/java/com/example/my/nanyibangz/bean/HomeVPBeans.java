package com.example.my.nanyibangz.bean;

import java.util.List;

/**
 * Created by My on 2016/10/7.
 */

public class HomeVPBeans {

    /**
     * member_type : guest
     * login_status : error
     * login_status_msg : not login in
     */

    private UserBean user;
    /**
     * theme_id : 755
     * type : 2
     * theme_name : 型男改造第五期
     * theme_image : http://im01.nanyibang.com/school/2016/09/30/SCHOOL4726313.jpg
     * channel_show :
     * theme_desc :
     * theme_link : http://www.nanyibang.com/utils/webpage_jump.php?module_type=theme&theme_id=755
     * allow_share : 1
     */

    private List<DataBean> data;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class UserBean {
        private String member_type;
        private String login_status;
        private String login_status_msg;

        public String getMember_type() {
            return member_type;
        }

        public void setMember_type(String member_type) {
            this.member_type = member_type;
        }

        public String getLogin_status() {
            return login_status;
        }

        public void setLogin_status(String login_status) {
            this.login_status = login_status;
        }

        public String getLogin_status_msg() {
            return login_status_msg;
        }

        public void setLogin_status_msg(String login_status_msg) {
            this.login_status_msg = login_status_msg;
        }
    }

    public static class DataBean {
        private int theme_id;
        private String type;
        private String theme_name;
        private String theme_image;
        private String channel_show;
        private String theme_desc;
        private String theme_link;
        private int allow_share;

        public int getTheme_id() {
            return theme_id;
        }

        public void setTheme_id(int theme_id) {
            this.theme_id = theme_id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTheme_name() {
            return theme_name;
        }

        public void setTheme_name(String theme_name) {
            this.theme_name = theme_name;
        }

        public String getTheme_image() {
            return theme_image;
        }

        public void setTheme_image(String theme_image) {
            this.theme_image = theme_image;
        }

        public String getChannel_show() {
            return channel_show;
        }

        public void setChannel_show(String channel_show) {
            this.channel_show = channel_show;
        }

        public String getTheme_desc() {
            return theme_desc;
        }

        public void setTheme_desc(String theme_desc) {
            this.theme_desc = theme_desc;
        }

        public String getTheme_link() {
            return theme_link;
        }

        public void setTheme_link(String theme_link) {
            this.theme_link = theme_link;
        }

        public int getAllow_share() {
            return allow_share;
        }

        public void setAllow_share(int allow_share) {
            this.allow_share = allow_share;
        }
    }
}
