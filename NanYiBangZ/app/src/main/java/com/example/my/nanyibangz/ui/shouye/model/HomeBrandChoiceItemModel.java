package com.example.my.nanyibangz.ui.shouye.model;

import com.example.my.nanyibangz.bean.HomeThreeBrandChoiceItem;
import com.example.my.nanyibangz.http.HomeHttpHelper;
import com.example.my.nanyibangz.ui.shouye.HomeFragmentContract;

import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/13.
 */

public class HomeBrandChoiceItemModel implements HomeFragmentContract.IHomeBrandChoiceModel {
    //加载首页品牌精选Item数据
    @Override
    public void homeLoadBrandChoiceItemData(String type, Map<String, String> map, Subscriber<HomeThreeBrandChoiceItem> subscriber) {
        HomeHttpHelper.getInstance().getHomeThreeBrandChoiceItemDatas(type,map,subscriber);
    }
}
