package com.example.my.nanyibangz.image;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.facebook.drawee.view.SimpleDraweeView;

/**
 * Created by My on 2016/10/9.
 */

public class ImageHelper {
    public static void showImage(Context context, String url, View view){
        Glide.with(context).load(url).into((ImageView) view);
    }

    public static void shoswImageSimple(Context context, String url, View view){
        Glide.with(context).load(url).into((SimpleDraweeView) view);
    }
}
