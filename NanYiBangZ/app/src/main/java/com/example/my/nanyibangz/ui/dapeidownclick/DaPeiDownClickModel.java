package com.example.my.nanyibangz.ui.dapeidownclick;

import com.example.my.nanyibangz.bean.DaPeiDownClickBean;
import com.example.my.nanyibangz.http.DaPeiHttpHelper;

import java.util.Map;

import rx.Subscriber;

/**
 * Created by Administrator on 2016/10/9.
 */

public class DaPeiDownClickModel implements DaPeiClickDownContact.Model {
    @Override
    public void getVerticalDaPeiDownClick(Map<String, String> map, Subscriber<DaPeiDownClickBean> subscribers) {
        DaPeiHttpHelper.getInstance().getVerticalDaPeiDownClick(map,subscribers);
    }

    @Override
    public void getVerticalDaPeiDownClickGV(Map<String, String> map, Subscriber<DaPeiDownClickBean> subscribers) {
        DaPeiHttpHelper.getInstance().getVerticalDaPeiDownClick(map,subscribers);
    }

    @Override
    public void getVerticalDaPeiDownClickRefresh(Map<String, String> map, Subscriber<DaPeiDownClickBean.DataBean.RelativeBean> subscribers) {
        DaPeiHttpHelper.getInstance().getVerticalDaPeiRefreshClick(map,subscribers);
    }
}
