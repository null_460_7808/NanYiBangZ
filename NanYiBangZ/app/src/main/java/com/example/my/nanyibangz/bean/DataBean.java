package com.example.my.nanyibangz.bean;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 2016/10/19.
 * 单凭精选的具体实体类
 */

public class DataBean {
    @SerializedName("item_id")
    private int itemId;
    @SerializedName("_id")
    private int id;
    @SerializedName("pic_url")
    private String picUrl;
    @SerializedName("title")
    private String title;
    @SerializedName("coupon_price")
    private String couponPrice;
    @SerializedName("open_iid")
    private String openIid;
    @SerializedName("productType")
    private int productType;
    @SerializedName("isv_code")
    private String isvCode;

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(String couponPrice) {
        this.couponPrice = couponPrice;
    }

    public String getOpenIid() {
        return openIid;
    }

    public void setOpenIid(String openIid) {
        this.openIid = openIid;
    }

    public int getProductType() {
        return productType;
    }

    public void setProductType(int productType) {
        this.productType = productType;
    }

    public String getIsvCode() {
        return isvCode;
    }

    public void setIsvCode(String isvCode) {
        this.isvCode = isvCode;
    }
}
