package com.example.my.nanyibangz.ui.shouye.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.HomeThreeBrandChoiceItem;
import com.example.my.nanyibangz.ui.shouye.HomeFragmentContract;
import com.example.my.nanyibangz.ui.shouye.adapter.HomeBrandChoiceRecyclerAdapter;
import com.example.my.nanyibangz.ui.shouye.presenter.HomeBrandChoiceItemPresenter;
import com.example.my.nanyibangz.utils.CustomGridLayoutManager;
import com.example.my.nanyibangz.utils.DividerGridItemDecoration;

import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 用于设置各个RecyclerView或者其它控件的子item数据加载及展示
 */
public class HomeBrandChoiceItemActivity extends AppCompatActivity implements HomeFragmentContract.IHomeBrandChoiceView {

    @Bind(R.id.home_brandchoiceitem_back)
    ImageView homeBrandchoiceitemBack;
    @Bind(R.id.home_brandchoiceitem_title)
    TextView homeBrandchoiceitemTitle;
    @Bind(R.id.home_brandchoiceitem_toolbar)
    Toolbar homeBrandchoiceitemToolbar;
    @Bind(R.id.home_brandchoiceitem_tab)
    TabLayout homeBrandchoiceitemTab;
    @Bind(R.id.home_brandchoiceitem_appbarlayout)
    AppBarLayout homeBrandchoiceitemAppbarlayout;
    @Bind(R.id.home_brandchoiceitem_rv)
    RecyclerView homeBrandchoiceitemRv;
    @Bind(R.id.home_brandchoiceitem_hsv)
    NestedScrollView homeBrandchoiceitemHsv;
    @Bind(R.id.home_brandchoiceitem_fab)
    FloatingActionButton homeBrandchoiceitemFab;
    private HomeBrandChoiceItemPresenter presenter;
    //获取首页品牌精选Item的brandId值
    private int id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_brandchoiceitem);
        ButterKnife.bind(this);
        //点击返回上一级
        backOnClick();
        //初始化控件(TabLayout)
        initBrandChoiceItemActivity();
        fabOnClick();
        presenter = new HomeBrandChoiceItemPresenter(this);
        presenter.homeLoadBrandChoiceItemDataToUi(this);
    }

    //返回按钮的点击事件
    public void backOnClick() {
        homeBrandchoiceitemBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    //获取Intent意图中传递的值 初始化控件
    public void initBrandChoiceItemActivity() {
        Intent intent = getIntent();
        String brandName = intent.getStringExtra("brandName");
        int brandId = intent.getIntExtra("brandId", 0);
        if (!brandName.equals("")) {
            homeBrandchoiceitemTitle.setText(brandName);
        }
        id = brandId;

        //Toolbar的设置
        homeBrandchoiceitemToolbar.setTitle("");
        setSupportActionBar(homeBrandchoiceitemToolbar);

        //TabLayout的设置
        String[] titles = {"上衣", "裤子"};
        for (int i = 0; i < titles.length; i++) {
            homeBrandchoiceitemTab.addTab(homeBrandchoiceitemTab.newTab().setText(titles[i]));
        }
    }

    //回传获取首页品牌精选Item数据
    @Override
    public void homeBrandChoiceItemDataMap(Map<String, List<HomeThreeBrandChoiceItem.DataBean>> map) {
        if (map != null && map.size() != 0) {
            List<HomeThreeBrandChoiceItem.DataBean> clothesList = map.get("clothes");
            List<HomeThreeBrandChoiceItem.DataBean> pantsList = map.get("pants");
            Log.e("tag88", clothesList.size() + "");
            Log.e("tag77", pantsList.size() + "");
            final HomeBrandChoiceRecyclerAdapter clothesAdapter = new HomeBrandChoiceRecyclerAdapter(HomeBrandChoiceItemActivity.this, clothesList);
            final HomeBrandChoiceRecyclerAdapter pantsAdapter = new HomeBrandChoiceRecyclerAdapter(HomeBrandChoiceItemActivity.this, pantsList);
            //RecyclerView的设置
            CustomGridLayoutManager gridLayoutManager = new CustomGridLayoutManager(this, 2);
            homeBrandchoiceitemRv.setNestedScrollingEnabled(false);//顺滑滑动解决卡顿
            homeBrandchoiceitemRv.setLayoutManager(gridLayoutManager);
            homeBrandchoiceitemRv.setItemAnimator(new DefaultItemAnimator());
            homeBrandchoiceitemRv.addItemDecoration(new DividerGridItemDecoration(this, 6, 10));
            //默认设置加载上衣
            homeBrandchoiceitemRv.setAdapter(clothesAdapter);

            //TabLayout的选择事件
            homeBrandchoiceitemTab.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    int position = tab.getPosition();
                    switch (position) {
                        case 0:
                            homeBrandchoiceitemRv.setAdapter(clothesAdapter);
                            break;
                        case 1:
                            homeBrandchoiceitemRv.setAdapter(pantsAdapter);
                            break;
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
        }
    }

    //获取首页品牌精选Item的brandId值
    @Override
    public int getHomeBrandChoiceItemBrandId() {
        return id;
    }

    //fab按钮的点击事件
    public void fabOnClick() {
        //FAB的设置
        homeBrandchoiceitemFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeBrandchoiceitemHsv.scrollTo(0, 0);
            }
        });
    }
}
