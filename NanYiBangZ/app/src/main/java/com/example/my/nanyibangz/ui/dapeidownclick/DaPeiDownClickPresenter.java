package com.example.my.nanyibangz.ui.dapeidownclick;

import android.util.Log;

import com.example.my.nanyibangz.bean.DaPeiDownClickBean;

import java.util.List;
import java.util.Map;

import rx.Subscriber;

/**
 * Created by Administrator on 2016/10/9.
 */

public class DaPeiDownClickPresenter implements DaPeiClickDownContact.Presenter{
    private DaPeiClickDownContact.Model model;
    private DaPeiClickDownContact.View view;

    public DaPeiDownClickPresenter(DaPeiClickDownContact.View view) {
        model=new DaPeiDownClickModel();
        this.view = view;
    }

    @Override
    public void getVerticalFrmNet_DaPeiDownClick(Map<String, String> map) {
        model.getVerticalDaPeiDownClick(map, new Subscriber<DaPeiDownClickBean>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(DaPeiDownClickBean daPeiDownClickBean) {
                Log.i("zlb","beans=="+daPeiDownClickBean);
                view.onVerticalSucess_DaPeiBeanDownClick(daPeiDownClickBean);
            }
        });

    }

    @Override
    public void getVerticalFrmNet_DaPeiDownClickGV(Map<String, String> map) {
        model.getVerticalDaPeiDownClickGV(map, new Subscriber<DaPeiDownClickBean>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(DaPeiDownClickBean downClickBean) {
                List<DaPeiDownClickBean.DataBean.SingleItemsBean> itemsBeanList=downClickBean.getData().getSingleItems();
                view.onVerticalSucess_DaPeiBeanDownClickGV(itemsBeanList);
            }
        });
    }

    @Override
    public void getVerticalFrmNet_DaPeiDownClickRefresh(Map<String, String> map) {
        model.getVerticalDaPeiDownClickRefresh(map, new Subscriber<DaPeiDownClickBean.DataBean.RelativeBean>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(DaPeiDownClickBean.DataBean.RelativeBean relativeBean) {
                List<DaPeiDownClickBean.DataBean.RelativeBean.ItemsBean> list=relativeBean.getItems();
                view.onVerticalSucess_DaPeiBeanDownClickRefresh(list);
            }
        });
    }
}
