package com.example.my.nanyibangz.ui.shouye.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.HomeGoodsDetailsContent;

import java.util.List;

/**
 * 商品详情的商品信息数据适配器
 */

public class HomeGoodsDetailsContentRecyclerAdapter extends RecyclerView.Adapter<HomeGoodsDetailsContentRecyclerAdapter.HomeGoodsDetailsContentViewHolder>{
    private Context context;
    private List<HomeGoodsDetailsContent.DataBean.SeoDescBean> list;

    public HomeGoodsDetailsContentRecyclerAdapter(Context context, List<HomeGoodsDetailsContent.DataBean.SeoDescBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public HomeGoodsDetailsContentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.home_goodsdetails_content,null);
        HomeGoodsDetailsContentViewHolder holder = new HomeGoodsDetailsContentViewHolder(convertView);
        return holder;
    }

    @Override
    public void onBindViewHolder(HomeGoodsDetailsContentViewHolder holder, final int position) {
        //设置控件 数据加载
        holder.homegoodsdetails_content_tv_name.setText(list.get(position).getName()+":");
        holder.homegoodsdetails_content_tv_value.setText(list.get(position).getValue());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class HomeGoodsDetailsContentViewHolder extends RecyclerView.ViewHolder{
        TextView homegoodsdetails_content_tv_name,homegoodsdetails_content_tv_value;
        public HomeGoodsDetailsContentViewHolder(View itemView) {
            super(itemView);
            homegoodsdetails_content_tv_name = (TextView) itemView.findViewById(R.id.homegoodsdetails_content_tv_name);
            homegoodsdetails_content_tv_value = (TextView) itemView.findViewById(R.id.homegoodsdetails_content_tv_value);
        }
    }
}
