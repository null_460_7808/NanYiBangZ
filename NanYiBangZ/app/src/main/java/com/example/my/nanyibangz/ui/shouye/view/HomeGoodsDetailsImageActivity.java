package com.example.my.nanyibangz.ui.shouye.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.utils.ZoomableImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by My on 2016/10/16.
 */

public class HomeGoodsDetailsImageActivity extends AppCompatActivity {
    @Bind(R.id.home_goodsdetails_image_ziv)
    ZoomableImageView homeGoodsdetailsImageZiv;

    //Intent意图中传递过来的图片路径
    private String imageUrl = null;
    private Handler handler;
    //自定义图片单击事件需要一个静态的该Activity实例
    public static HomeGoodsDetailsImageActivity homeGoodsDetailsImageActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homegoodsdetailsimage);
        ButterKnife.bind(this);
        homeGoodsDetailsImageActivity = this;
        imageUrl = getIntent().getStringExtra("imageUrl");

        //开启子线程下载图片
        new Thread(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap = getBitmap(imageUrl);
                Message message = Message.obtain();
                message.what = 1;
                message.obj = bitmap;
                handler.sendMessage(message);
            }
        }).start();

        //Handler接收消息并进行处理
        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what){
                    case 1:
                        Bitmap bitmap = (Bitmap) msg.obj;
                        homeGoodsdetailsImageZiv.setImageBitmap(bitmap);
                        break;
                }
            }
        };
    }

    //根据图片路径加载图片
    //方式一：根据图片地址返回bitmap位图文件
    public static Bitmap getBitmap(String imageUrl){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(3000);
            conn.setConnectTimeout(5000);
            conn.setDoInput(true);
            conn.connect();
            int code = conn.getResponseCode();
            if (code == 200){
                InputStream is = conn.getInputStream();
                int len = 0;
                byte b[] = new byte[1024];
                while ((len = is.read(b))!=-1){
                    baos.write(b,0,len);
                }
            }
            Bitmap bitmap = BitmapFactory.decodeByteArray(baos.toByteArray(),0,baos.toByteArray().length);
            return bitmap;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //方式二：根据图片地址返回Drawable
    public static Drawable getDrawable(String imageUrl){
        try {
            URL url = new URL(imageUrl);
            InputStream inputStream = (InputStream) url.getContent();
            Drawable drawable = Drawable.createFromStream(inputStream, "src");
            return drawable;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
