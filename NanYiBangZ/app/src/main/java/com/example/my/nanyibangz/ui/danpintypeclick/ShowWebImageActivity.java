package com.example.my.nanyibangz.ui.danpintypeclick;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseActivity;
import com.example.my.nanyibangz.utils.ZoomableImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import butterknife.Bind;

public class ShowWebImageActivity extends BaseActivity {
    /**
     * 在一个activity中销毁指定activity
     * 一定要设置主题，否者会出现报没有主题的错误。
     */

    public static ShowWebImageActivity showWebImageActivity;
    @Bind(R.id.show_webimage_imageview)
    ZoomableImageView showWebimageImageview;
    private TextView imageTextView = null;
    private String imagePath = null;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

          if(msg.what==1){
              Drawable d= (Drawable) msg.obj;
              Log.i("zbb","drawable==h"+d);
              showWebimageImageview.setImageBitmap(((BitmapDrawable)d).getBitmap());
           }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showWebImageActivity = this;

        this.imagePath = getIntent().getStringExtra("image");
        Log.i("zbb", "image==" + this.imagePath);
        try {
            loadImageFromUrl(this.imagePath);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getLayoutId() {

        return R.layout.show_webimage;
    }

    //传入地址，下载图片，drawable,
    public void loadImageFromUrl(final String url) throws IOException {

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    URL m = new URL(url);
                    InputStream i = (InputStream) m.getContent();
                    Drawable d = Drawable.createFromStream(i, "src");
                    Message message = Message.obtain();
                    message.what = 1;
                    message.obj = d ;
                    handler.sendMessage(message);

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("zlb", "zlb===");
    }
}
