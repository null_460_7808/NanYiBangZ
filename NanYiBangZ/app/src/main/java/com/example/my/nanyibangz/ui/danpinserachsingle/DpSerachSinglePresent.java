package com.example.my.nanyibangz.ui.danpinserachsingle;

import com.example.my.nanyibangz.bean.DanPinSerachHotSingleBeans;

import rx.Subscriber;

/**
 * Created by My on 2016/10/16.
 */

public class DpSerachSinglePresent implements DpSerachSingleContact.Presenter {
    private DpSerachSingleContact.View view;
    private  DpSerachSingleContact.Model model;
    public  DpSerachSinglePresent(DpSerachSingleContact.View view){
        this.view=view;
        model= new DpSerachSingleModel();

    }

    @Override
    public void getVerticalSerachHotSingleBeansFromNet() {
        model.getVerticalSerachHotSingleBeans(new Subscriber<DanPinSerachHotSingleBeans>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(DanPinSerachHotSingleBeans danPinSerachHotSingleBeans) {
                if (danPinSerachHotSingleBeans!=null){
                    view.getVerticalSerachHotSingleSuccess(danPinSerachHotSingleBeans);
                }

            }
        });

    }
}
