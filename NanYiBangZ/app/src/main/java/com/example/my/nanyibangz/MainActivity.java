package com.example.my.nanyibangz;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.my.nanyibangz.bean.CollectionBeans;
import com.example.my.nanyibangz.db.MySqliteOpenHelper;
import com.example.my.nanyibangz.ui.danpin.DPFragment;
import com.example.my.nanyibangz.ui.dapei.DaPeiFragment;
import com.example.my.nanyibangz.ui.mine.MineFragment;
import com.example.my.nanyibangz.ui.mine.MineLoadActivity;
import com.example.my.nanyibangz.ui.shouye.view.Home_Fragment;
import com.example.my.nanyibangz.ui.shuaiba.SB_Fragment;
import com.j256.ormlite.dao.Dao;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobUser;
import java.sql.SQLException;

public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    //存放四个按钮的RadioGroup
    private RadioGroup rgbottom;
    //五个Fragment
    private Home_Fragment fragment_shouYe;
    private DPFragment fragment_danPin;
    private DaPeiFragment fragment_daPei;
    private SB_Fragment fragment_shuaiBa;
    private MineFragment fragment_mine;
    //fragment管理器
    FragmentManager Manager;
    private DrawerLayout mDrawerLayout;//侧滑菜单
    private NavigationView mNavigationView;//导航条

    //数据库
    public static MySqliteOpenHelper helper;
    public static volatile Dao<CollectionBeans,Long> collectionBeansDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initHelper();
        initview();
        initdata();
        //Login();
        //第一：默认初始化
        Bmob.initialize(this, "7891cabc67eb18cf062fff1284ca892e");
    }

    @Override
    protected void onResume() {
        super.onResume();
        BmobUser bmobUser = BmobUser.getCurrentUser();
        if(bmobUser != null){
            // 允许用户使用应用
        }else{
            //缓存用户对象为空时， 可打开用户注册界面…
        }
    }

    private void Login(){
        BmobUser user=BmobUser.getCurrentUser();
        if (user!=null){
            Toast.makeText(MainActivity.this,"欢迎回来"+user.getUsername()+"会员",Toast.LENGTH_SHORT);
        }else {
            Intent intent=new Intent(this, MineLoadActivity.class);
            startActivity(intent);
        }
    }

    //初始化数据库
    public void initHelper(){
        helper = MySqliteOpenHelper.getInstance(MainActivity.this);
        try {
            collectionBeansDao = helper.getDao(CollectionBeans.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //初始化控件
    public void initview() {
//        sp = getSharedPreferences("user", MODE_PRIVATE);
        //初始化数据库

        rgbottom= (RadioGroup) findViewById(R.id.rg_main);
        rgbottom.setOnCheckedChangeListener(this);
        Manager=getSupportFragmentManager();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.id_drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.id_nv_menu);
        mNavigationView.setItemIconTintList(null);
        /**
         *不写下面代码，navigationView中菜单的图片默认是灰色的，
         *如果我想让图片就是显示他本身的颜色该怎么办呢？在Java代码中调用如下方法：
         */

        setupDrawerContent(mNavigationView);
    }
    //默认加载第一个fragment_shouye
    public void initdata() {
        Setselection(0);
    }
    private void Setselection(int index) {
        //FragmentTransaction对fragment进行添加，删除，替换，等其他执行操作
        FragmentTransaction ft=Manager.beginTransaction();
        hindFragment(ft);
        //需要加载的页面
        switch (index){
            case 0:
                if (fragment_shouYe==null){
                    fragment_shouYe=new Home_Fragment();
                    ft.add(R.id.ll_fragment,fragment_shouYe);
                }else{
                    ft.show(fragment_shouYe);
                }
                break;
            case 1:
                if (fragment_danPin==null){
                    fragment_danPin=new DPFragment();
                    ft.add(R.id.ll_fragment,fragment_danPin);
                }else{
                    ft.show(fragment_danPin);
                }
                break;
            case 2:
                if (fragment_daPei==null){
                    fragment_daPei=new DaPeiFragment();
                    ft.add(R.id.ll_fragment,fragment_daPei);
                }else{
                    ft.show(fragment_daPei);
                }
                break;
            case 3:
                if (fragment_shuaiBa==null){
                    fragment_shuaiBa=new SB_Fragment();
                    ft.add(R.id.ll_fragment,fragment_shuaiBa);
                }else{
                    ft.show(fragment_shuaiBa);
                }
                break;
            case 4:
                if (fragment_mine==null){
                    fragment_mine=new MineFragment();
                    ft.add(R.id.ll_fragment,fragment_mine);
                }else{
                    ft.show(fragment_mine);
                }
                break;
        }
        ft.commit();
    }
    private void hindFragment(FragmentTransaction ft) {
        //如果fragment不为空则保存当前fragment状态
        if (fragment_shouYe!=null){
            ft.hide(fragment_shouYe);
        }
        if (fragment_danPin!=null){
            ft.hide(fragment_danPin);
        }
        if (fragment_daPei!=null){
            ft.hide(fragment_daPei);
        }
        if (fragment_shuaiBa!=null){
            ft.hide(fragment_shuaiBa);
        }
        if(fragment_mine!=null){
            ft.hide(fragment_mine);
        }
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId){
            //首页
            case  R.id.rbt_main_shouye:
                Setselection(0);
                break;
            case  R.id.rbt_main_danpin:
                Setselection(1);
                break;
            case  R.id.rbt_main_dapei:
                Setselection(2);
                break;
            case  R.id.rbt_main_shuaiba:
                Setselection(3);
                break;
            case  R.id.rbt_main_mine:
                Setselection(4);
                break;
        }
    }
    //方法来设置当导航项被点击时的回调.
    private void setupDrawerContent(NavigationView navigationView)
    {
        //方法来设置当导航项被点击时的回调.
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener()
                {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem)
                    {
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();//关闭抽屉
                        return true;
                    }
                });
    }

   /* //添加menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_navigation,menu);
        return  true;
    }*/
    //菜单项单击事件
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            mDrawerLayout.openDrawer(GravityCompat.START);
            return true ;
        }
        return super.onOptionsItemSelected(item);
    }
}
