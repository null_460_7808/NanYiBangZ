package com.example.my.nanyibangz.image;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by My on 2016/10/5.
 */
public class DaPeiImageHelper {

        public static void showImage(Context context, String url, View view){
            Picasso.with(context).load(url).into((ImageView) view);
        }

}
