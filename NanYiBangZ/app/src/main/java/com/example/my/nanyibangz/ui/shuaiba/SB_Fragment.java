package com.example.my.nanyibangz.ui.shuaiba;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseFragment;
import com.example.my.nanyibangz.ui.dapeidownclick.DownTwoClickPageAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by My on 2016/10/5.
 */
public class SB_Fragment extends BaseFragment {
    @Bind(R.id.toolbar_shuaiba)
    Toolbar toolbarShuaiba;
    @Bind(R.id.tablayout_shuaiba)
    TabLayout tablayoutShuaiba;
    @Bind(R.id.ap_shuaiba)
    AppBarLayout apShuaiba;
    @Bind(R.id.tv_shuaiba_send)
    TextView tvShuaibaSend;
    @Bind(R.id.viewpager_shuaiba)
    /*
    * 马伟  此为帅吧部分
    * */
            ViewPager viewpagerShuaiba;

    private String[] titles = {"最热", "搭配", "发型", "护理", "健身", "明星"};
    private List<Fragment> fragments;
    private DownTwoClickPageAdapter adapter;
    private String[] kind = {"6", "1", "2", "3", "7", "5"};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shuaiba, null);
        ButterKnife.bind(this, view);
        initView();
        initData();
        initPager();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Click();
    }

    private void initView() {
        //fragment中不能使用setSupportActionBar（）方法，想使用必须继承AppCompatActivity。
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbarShuaiba);//设置toolbar
        toolbarShuaiba.setTitle("");
        //activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_dp);//更改toolbar的返回按钮图片
        activity.getSupportActionBar().setDisplayShowTitleEnabled(false);//设置toolbar的返回安妮可以使用

        for (int i = 0; i < titles.length; i++) {
            tablayoutShuaiba.addTab(tablayoutShuaiba.newTab().setText(titles[i]));
        }
    }

    //利用bundle 传值，由此可以节省fragment的创建
    private void initData() {
        ShuaiBaChildFragment childFragment;
        fragments = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            Bundle bundle = new Bundle();
            bundle.putString("index", kind[i]);
            childFragment = new ShuaiBaChildFragment();
            childFragment.setArguments(bundle);
            fragments.add(childFragment);
        }
        adapter = new DownTwoClickPageAdapter(getChildFragmentManager(), fragments);
        viewpagerShuaiba.setAdapter(adapter);

    }

    private void initPager() {
        // 添加viewpager的监听 参数传递tabLayout中提供的监听事件  viewpager中的page页与tab对应
        viewpagerShuaiba.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayoutShuaiba));
        //设置tabLayout的监听事件  滑动改变tabLayout中的tab时viewpager跟随变化
        tablayoutShuaiba.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpagerShuaiba.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
    private void  Click(){
        tvShuaibaSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),ShuaiBaTouGaoActivity.class);
                startActivity(intent);
            }
        });
    }
}
