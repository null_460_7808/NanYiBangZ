package com.example.my.nanyibangz.ui.dapei;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.DaPeiUpBean;
import com.example.my.nanyibangz.image.DaPeiImageHelper;
import com.example.my.nanyibangz.utils.RoundImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/10/8.
 */

public class DaPeiUpAdapter extends BaseAdapter {
    private Context context;
    private List<DaPeiUpBean.DataBean.ItemsBean> list;

    public DaPeiUpAdapter(Context context, List<DaPeiUpBean.DataBean.ItemsBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    ViewHolderGr holderGr=null;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.dapei_item_up, null);
            holderGr=new ViewHolderGr(convertView);
            convertView.setTag(holderGr);
        }else {
            holderGr= (ViewHolderGr) convertView.getTag();
        }
        DaPeiImageHelper.showImage(context,list.get(position).getClassifyIcon(),holderGr.ivGroupUp);
        //Picasso.with(context).load(list.get(position).getClassifyIcon()).into(holderGr.ivGroupUp);
        holderGr.tvGroupUp.setText(list.get(position).getClassifyName());
        return convertView;
    }

    static class ViewHolderGr {
        @Bind(R.id.iv_groupUp)
        RoundImageView ivGroupUp;
        @Bind(R.id.tv_groupUp)
        TextView tvGroupUp;

        ViewHolderGr(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
