package com.example.my.nanyibangz.ui.shuaiba;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/10/19.
 */

public class ShuaiBaTouGaoActivity extends BaseActivity {
    @Bind(R.id.iv_shuaibaBacktou)
    ImageView ivShuaibaBackTou;
    @Bind(R.id.wv_shuaiba_tougao)
    WebView wvShuaiba;
    private String url= "http://www.nanyibang.com/school/school.php?id=62";

    @Override
    public int getLayoutId() {
        return R.layout.shuaiba_tougao;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        initWebView();
    }
    //WebView的基本设置
    private void initWebView() {
        //支持js脚本
        wvShuaiba.getSettings().setJavaScriptEnabled(true);
        //设置可以访问文件
        wvShuaiba.getSettings().setAllowContentAccess(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            wvShuaiba.getSettings().setAllowFileAccessFromFileURLs(true);
        }
        //WebSettings settings = webview.getSettings();
        //设置启动缓存
        wvShuaiba.getSettings().setAppCacheEnabled(true);
        //设置缓存的大小
        wvShuaiba.getSettings().setAppCacheMaxSize(1024 * 10);
        //设置缓存模式
        wvShuaiba.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);//优先使用缓存
        //settings.setCacheMode(WebSettings.LOAD_NO_CACHE);//不使用缓存

        //设置缓存，离线应用
        wvShuaiba.getSettings().setAppCacheEnabled(true);

        wvShuaiba.getSettings().setSupportZoom(true);
        //设置可以自动加载图片
        wvShuaiba.getSettings().setLoadsImagesAutomatically(true);
        //wvShuaiba.getSettings().setAppCacheEnabled(true);
        wvShuaiba.getSettings().setLoadWithOverviewMode(true);
        wvShuaiba.getSettings().setUseWideViewPort(true);
        wvShuaiba.getSettings().setPluginState(WebSettings.PluginState.ON);
        wvShuaiba.getSettings().setDomStorageEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            wvShuaiba.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        wvShuaiba.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        wvShuaiba.loadUrl(url);
        ivShuaibaBackTou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
