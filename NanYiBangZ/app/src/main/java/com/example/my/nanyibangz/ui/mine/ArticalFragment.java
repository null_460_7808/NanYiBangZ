package com.example.my.nanyibangz.ui.mine;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseFragment;

/**
 * Created by Administrator on 2016/10/20.
 * 我的收藏--文章
 */

public class ArticalFragment extends BaseFragment{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.artical_fragment,null);
        return  view;
    }
}
