package com.example.my.nanyibangz.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by My on 2016/10/8.
 */

public class DanPin_JingXuan_Beans {


    /**
     * member_type : guest
     * login_status : error
     * login_status_msg : not login in
     *
     * //DatabaseTable标明数据库中的一张表 tableName指定表明
     //DatabaseField  标明数据表中的列名(字段名)  generatedId = true 表示主键自增长
     @DatabaseField(columnName = "_id",generatedId = true)
     private long _id;
     @DatabaseField(columnName = "age",dataType = DataType.INTEGER)
     private int age;
     @DatabaseField(columnName = "name",dataType = DataType.STRING)
     private String name;
     @DatabaseField(columnName = "sex",dataType = DataType.STRING)
     *
     */
     //DatabaseTable标明数据库中的一张表 tableName指定表明
    //DatabaseField  标明数据表中的列名(字段名)  generatedId = true 表示主键自增长

    @SerializedName("user")
    private UserBean user;
    /**
     * item_id : 410415
     * _id : 410415
     * pic_url : http://img01.taobaocdn.com/bao/uploaded/i1/TB1JYmoMVXXXXXEXpXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg
     * title : AMH男装韩版2016秋季新款男士圆领修身潮流休闲长袖T恤QU5108翎

     * coupon_price : 118.00
     * open_iid : AAH__kWCABuH20zjAN1T7RzR
     * productType : 1
     * isv_code : 0_android_tuiBig_19
     */

    @SerializedName("data")
    private List<DataBean> data;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class UserBean {
        @SerializedName("member_type")
        private String memberType;
        @SerializedName("login_status")
        private String loginStatus;
        @SerializedName("login_status_msg")
        private String loginStatusMsg;

        public String getMemberType() {
            return memberType;
        }

        public void setMemberType(String memberType) {
            this.memberType = memberType;
        }

        public String getLoginStatus() {
            return loginStatus;
        }

        public void setLoginStatus(String loginStatus) {
            this.loginStatus = loginStatus;
        }

        public String getLoginStatusMsg() {
            return loginStatusMsg;
        }

        public void setLoginStatusMsg(String loginStatusMsg) {
            this.loginStatusMsg = loginStatusMsg;
        }
    }

   /* public static class DataBean {
        @SerializedName("item_id")
        private int itemId;
        @SerializedName("_id")
        private int id;
        @SerializedName("pic_url")
        private String picUrl;
        @SerializedName("title")
        private String title;
        @SerializedName("coupon_price")
        private String couponPrice;
        @SerializedName("open_iid")
        private String openIid;
        @SerializedName("productType")
        private int productType;
        @SerializedName("isv_code")
        private String isvCode;

        public int getItemId() {
            return itemId;
        }

        public void setItemId(int itemId) {
            this.itemId = itemId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPicUrl() {
            return picUrl;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCouponPrice() {
            return couponPrice;
        }

        public void setCouponPrice(String couponPrice) {
            this.couponPrice = couponPrice;
        }

        public String getOpenIid() {
            return openIid;
        }

        public void setOpenIid(String openIid) {
            this.openIid = openIid;
        }

        public int getProductType() {
            return productType;
        }

        public void setProductType(int productType) {
            this.productType = productType;
        }

        public String getIsvCode() {
            return isvCode;
        }

        public void setIsvCode(String isvCode) {
            this.isvCode = isvCode;
        }
    }*/
}
