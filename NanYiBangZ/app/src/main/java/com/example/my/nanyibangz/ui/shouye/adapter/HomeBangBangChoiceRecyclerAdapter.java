package com.example.my.nanyibangz.ui.shouye.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.HomeBangBangChoice;
import com.example.my.nanyibangz.image.ImageHelper;
import com.example.my.nanyibangz.ui.shouye.view.HomeGoodsDetailsActivity;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

/**
 * 首页邦邦精选数据适配器
 */

public class HomeBangBangChoiceRecyclerAdapter extends RecyclerView.Adapter<HomeBangBangChoiceRecyclerAdapter.HomeFifthBangBangChoiceViewHolder>{
    private Context context;
    private List<HomeBangBangChoice.DataBean.ItemDetailBean> list;

    public HomeBangBangChoiceRecyclerAdapter(Context context, List<HomeBangBangChoice.DataBean.ItemDetailBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public HomeFifthBangBangChoiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.home_bangbangchoice_item,null);
        HomeFifthBangBangChoiceViewHolder holder = new HomeFifthBangBangChoiceViewHolder(convertView);
        return holder;
    }

    @Override
    public void onBindViewHolder(HomeFifthBangBangChoiceViewHolder holder, final int position) {
        //设置控件 数据加载
        //holder.home_bangbangchoice_item_sdv.setImageURI(Uri.parse(list.get(position).getPic_url()));
        ImageHelper.shoswImageSimple(context,list.get(position).getPic_url(),holder.home_bangbangchoice_item_sdv);
        holder.home_bangbangchoice_item_tv_title.setText(list.get(position).getTitle());
        holder.home_bangbangchoice_item_tv_couponPrice.setText("¥"+list.get(position).getCoupon_price());
        holder.home_bangbangchoice_item_tv_saveCount.setText(list.get(position).getSaveCount()+"");

        holder.home_bangbangchoice_item_sdv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int item_id = list.get(position).get_id();
                Intent intent = new Intent(context, HomeGoodsDetailsActivity.class);
                intent.putExtra("item_id",item_id);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class HomeFifthBangBangChoiceViewHolder extends RecyclerView.ViewHolder{
        SimpleDraweeView home_bangbangchoice_item_sdv;
        //ImageView home_bangbangchoice_item_sdv;
        TextView home_bangbangchoice_item_tv_title,home_bangbangchoice_item_tv_couponPrice,home_bangbangchoice_item_tv_saveCount;
        ImageView home_bangbangchoice_item_iv_collection;
        public HomeFifthBangBangChoiceViewHolder(View itemView) {
            super(itemView);
            home_bangbangchoice_item_sdv = (SimpleDraweeView) itemView.findViewById(R.id.home_bangbangchoice_item_sdv);
            //home_bangbangchoice_item_sdv = (ImageView) itemView.findViewById(R.id.home_bangbangchoice_item_sdv);
            home_bangbangchoice_item_tv_title = (TextView) itemView.findViewById(R.id.home_bangbangchoice_item_tv_title);
            home_bangbangchoice_item_tv_couponPrice = (TextView) itemView.findViewById(R.id.home_bangbangchoice_item_tv_couponPrice);
            home_bangbangchoice_item_tv_saveCount = (TextView) itemView.findViewById(R.id.home_bangbangchoice_item_tv_saveCount);
            home_bangbangchoice_item_iv_collection = (ImageView) itemView.findViewById(R.id.home_bangbangchoice_item_iv_collection);
        }
    }
}
