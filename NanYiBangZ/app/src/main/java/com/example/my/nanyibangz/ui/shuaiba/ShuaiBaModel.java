package com.example.my.nanyibangz.ui.shuaiba;

import com.example.my.nanyibangz.bean.ShuaiBaBean;
import com.example.my.nanyibangz.http.ShuaiBaHttpHelper;

import java.util.Map;

import rx.Subscriber;

/**
 * Created by Administrator on 2016/10/14.
 */

public class ShuaiBaModel implements ShuaiBaContact.Model {
    @Override
    public void getVerticalShuaiBa(Map<String, String> params, Subscriber<ShuaiBaBean> subscriber) {
        ShuaiBaHttpHelper.getInstance().getVerticalShuaiBa(params,subscriber);
    }
}
