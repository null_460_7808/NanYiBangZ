package com.example.my.nanyibangz.ui.danpintypeclick;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseFragment;
import com.example.my.nanyibangz.bean.DanPinTypeClickBeans;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by My on 2016/10/10.
 */

public class DanPinGoodInfoFragment extends BaseFragment {
    @Bind(R.id.txt_type_brand)
    TextView txtTypeBrand;
    @Bind(R.id.txt_type_material)
    TextView txtTypeMaterial;
    @Bind(R.id.txt_type_style)
    TextView txtTypeStyle;
    @Bind(R.id.txt_type_numId)
    TextView txtTypeNumId;
    @Bind(R.id.txt_type_color)
    TextView txtTypeColor;
    @Bind(R.id.txt_type_season)
    TextView txtTypeSeason;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.danpin_type_goods_info, null);
        Log.e("asd","asd");
        ButterKnife.bind(this, view);
        initview();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
    //接收传递过来的Bundle值，初始化控件赋值，
    public void initview(){
        Bundle bundle=getArguments();
        Log.i("bun","bundle==="+bundle);
        if(bundle!=null){
            DanPinTypeClickBeans.DataBean danPinTypeClickBeans= (DanPinTypeClickBeans.DataBean) bundle.getSerializable("index");
            txtTypeBrand.setText(danPinTypeClickBeans.getBrand());
            txtTypeColor.setText(danPinTypeClickBeans.getColor());
            txtTypeMaterial.setText(danPinTypeClickBeans.getMaterial());
            txtTypeNumId.setText(danPinTypeClickBeans.getNumIid()+"");
            txtTypeSeason.setText(danPinTypeClickBeans.getSeason());
            txtTypeStyle.setText(danPinTypeClickBeans.getStyle());
        }
    }
}
