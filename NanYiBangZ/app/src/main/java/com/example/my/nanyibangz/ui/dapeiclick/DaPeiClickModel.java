package com.example.my.nanyibangz.ui.dapeiclick;

import com.example.my.nanyibangz.bean.DaPeiDownBean;
import com.example.my.nanyibangz.http.DaPeiHttpHelper;

import java.util.Map;

import rx.Subscriber;

/**
 * Created by Administrator on 2016/10/8.
 */

public class DaPeiClickModel implements DaPeiClickContact.Model {
    @Override
    public void getVerticalDaPeiClick(Map<String, String> map, Subscriber<DaPeiDownBean> subscribers) {
        DaPeiHttpHelper.getInstance().getVerticalDaPeiClick(map,subscribers);
    }
}
