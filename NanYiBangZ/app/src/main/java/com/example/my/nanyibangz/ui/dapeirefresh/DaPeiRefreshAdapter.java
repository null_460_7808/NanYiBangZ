package com.example.my.nanyibangz.ui.dapeirefresh;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.DaPeiDownClickBean;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/10/11.
 */

public class DaPeiRefreshAdapter extends BaseAdapter {
    private Context context;
    private List<DaPeiDownClickBean.DataBean.RelativeBean.ItemsBean> list;

    public DaPeiRefreshAdapter(Context context, List<DaPeiDownClickBean.DataBean.RelativeBean.ItemsBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    ViewHolderDR holderDR=null;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.dapei_refresh_item, null);
            holderDR=new ViewHolderDR(convertView);
            convertView.setTag(holderDR);
        }else{
            holderDR= (ViewHolderDR) convertView.getTag();
        }
        Picasso.with(context).load(list.get(position).getPicUrl()).into(holderDR.ivDapRefresh);
        holderDR.tvDapRefreshTitle.setText(list.get(position).getTitle());
        holderDR.tvDapRefreshPrice.setText(list.get(position).getCouponPrice());
        return convertView;
    }

    static class ViewHolderDR {
        @Bind(R.id.iv_dapRefresh)
        ImageView ivDapRefresh;
        @Bind(R.id.tv_dapRefreshTitle)
        TextView tvDapRefreshTitle;
        @Bind(R.id.tv_dapRefreshPrice)
        TextView tvDapRefreshPrice;

        ViewHolderDR(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
