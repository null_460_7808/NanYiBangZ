package com.example.my.nanyibangz.image;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.my.nanyibangz.R;
import com.squareup.picasso.Picasso;

/**
 * Created by My on 2016/10/5.
 */
public class DanPinImageHelper {
        public static void showImage(Context context, String url, View view){
            Picasso.with(context).load(url).into((ImageView) view);
        }
    public static void showImage_Jingping(Context context, String url, View view){
        Glide.with(context).load(url).placeholder(R.drawable.defalut_place_hold_image_nanyibang).into((ImageView) view);
    }


}
