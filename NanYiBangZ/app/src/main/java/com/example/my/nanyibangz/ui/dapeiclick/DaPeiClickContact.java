package com.example.my.nanyibangz.ui.dapeiclick;

import com.example.my.nanyibangz.base.IModel;
import com.example.my.nanyibangz.base.IPresenter;
import com.example.my.nanyibangz.base.IView;
import com.example.my.nanyibangz.bean.DaPeiDownBean;

import java.util.List;
import java.util.Map;

import rx.Subscriber;

/**
 * 搭配页面click
 * Created by Administrator on 2016/10/8.
 */

public class DaPeiClickContact {
    public interface View extends IView{
        public void onVerticalSucess_DaPeiBeanClick(List<DaPeiDownBean.DataBean> dataBeanList);
        public void onVerticalFail_DaPeiClick(String msg);
    }
    public interface Model extends IModel{
        public void getVerticalDaPeiClick(Map<String,String> map, Subscriber<DaPeiDownBean> subscribers);
    }
    public interface Presenter extends IPresenter{
        public void getVerticalFrmNet_DaPeiClick(Map<String,String> map);
    }
}
