package com.example.my.nanyibangz.ui.dapeirefresh;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.DaPeiDownClickBean;
import com.example.my.nanyibangz.config.DaPeiDownClickConfig;
import com.example.my.nanyibangz.ui.dapeidownclick.DaPeiClickDownContact;
import com.example.my.nanyibangz.ui.dapeidownclick.DaPeiDownClickPresenter;
import com.example.my.nanyibangz.utils.MyGirdView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/10/10.
 */

public class TwoClickThreeFragment extends Fragment implements DaPeiClickDownContact.View {
    @Bind(R.id.gv_dapRefresh)
    MyGirdView gvDapRefresh;
    private int collocation_id;
    private DaPeiClickDownContact.Presenter presenter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dapei_refresh, null);
        ButterKnife.bind(this, view);
        initView();
        initDate();
        return view;
    }
    private void initView(){
        Bundle bundle=getArguments();
        collocation_id=bundle.getInt("collocation_id");
    }
    private void initDate(){
        presenter = new DaPeiDownClickPresenter(this);
        Map<String, String> map = new HashMap<>();
        //http://api.nanyibang.com/match-detail?age=19&channel=oppo&collocation_id=8959&hkm_sign2=7a576962b8c9e81208fbc1c50d7bde1a&member_id=292720&member_type=member&random_key=42211&system_name=android&versionCode=219
        map.put(DaPeiDownClickConfig.Params.Age, DaPeiDownClickConfig.DefaultVaule.Age_VALUE);
        map.put(DaPeiDownClickConfig.Params.Channel, DaPeiDownClickConfig.DefaultVaule.Channel_VALUE);
        map.put(DaPeiDownClickConfig.Params.Collocation_id, collocation_id + "");
        map.put(DaPeiDownClickConfig.Params.Hkm_sign2, DaPeiDownClickConfig.DefaultVaule.Hkm_sign2_VALUE);
        map.put(DaPeiDownClickConfig.Params.Member_id, DaPeiDownClickConfig.DefaultVaule.Member_id_VALUE);
        map.put(DaPeiDownClickConfig.Params.Member_type, DaPeiDownClickConfig.DefaultVaule.Member_type_VALUE);
        map.put(DaPeiDownClickConfig.Params.Random_key, DaPeiDownClickConfig.DefaultVaule.Random_key_VALUE);
        map.put(DaPeiDownClickConfig.Params.System_name, DaPeiDownClickConfig.DefaultVaule.System_name_VALUE);
        map.put(DaPeiDownClickConfig.Params.VersionCode, DaPeiDownClickConfig.DefaultVaule.VersionCode_VALUE);
        presenter.getVerticalFrmNet_DaPeiDownClick(map);
    }
    private void initLoad(){}

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onVerticalSucess_DaPeiBeanDownClick(DaPeiDownClickBean downClickBean) {

    }

    @Override
    public void onVerticalFail_DaPeiDownClick(String msg) {

    }

    @Override
    public void onVerticalSucess_DaPeiBeanDownClickGV(List<DaPeiDownClickBean.DataBean.SingleItemsBean> itemsBeanList) {

    }

    @Override
    public void onVerticalFail_DaPeiDownClickGV(String msg) {

    }

    @Override
    public void onVerticalSucess_DaPeiBeanDownClickRefresh(List<DaPeiDownClickBean.DataBean.RelativeBean.ItemsBean> itemsList) {
        List<DaPeiDownClickBean.DataBean.RelativeBean.ItemsBean> list=itemsList;
        DaPeiRefreshAdapter adapter=new DaPeiRefreshAdapter(getActivity(),itemsList);
        gvDapRefresh.setAdapter(adapter);
    }

    @Override
    public void onVerticalFail_DaPeiDownClickRefresh(String msg) {

    }
}
