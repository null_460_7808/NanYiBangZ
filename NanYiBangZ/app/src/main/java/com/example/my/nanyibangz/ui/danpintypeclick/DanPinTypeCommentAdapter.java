package com.example.my.nanyibangz.ui.danpintypeclick;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.DanPinTypeClickCommentBeans;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by My on 2016/10/12.
 */

public class DanPinTypeCommentAdapter extends BaseAdapter {
    private List<DanPinTypeClickCommentBeans.DataBean.CommentsBean> list;
    private Context context;

    public DanPinTypeCommentAdapter(Context context, List<DanPinTypeClickCommentBeans.DataBean.CommentsBean> list) {
        this.list = list;
        this.context = context;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder=null;

        if (convertView == null) {

            convertView = LayoutInflater.from(context).inflate(R.layout.danpin_type_goods_comment_item, null);
            viewHolder=new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else {
            viewHolder= (ViewHolder) convertView.getTag();
        }
        viewHolder.txtCommentContent.setText(list.get(position).getContent());
        viewHolder.txtCommentUserName.setText(list.get(position).getName());
        return convertView;
    }

    static class ViewHolder {
        @Bind(R.id.txt_comment_user_name)
        TextView txtCommentUserName;
        @Bind(R.id.txt_comment_content)
        TextView txtCommentContent;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
