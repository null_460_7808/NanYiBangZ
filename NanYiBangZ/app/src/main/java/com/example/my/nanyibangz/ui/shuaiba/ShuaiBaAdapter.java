package com.example.my.nanyibangz.ui.shuaiba;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.ShuaiBaBean;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/10/14.
 */

public class ShuaiBaAdapter extends BaseAdapter {
    private Context context;
    private List<ShuaiBaBean.DataBean> list;
    private Animation animation;
    public ShuaiBaAdapter(Context context, List<ShuaiBaBean.DataBean> list) {
        this.context = context;
        this.list = list;
        //animation= AnimationUtils.loadAnimation(context,R.anim.slide_right2);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    ViewHolderSB holderSB=null;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.shuaiba_item, null);
            holderSB=new ViewHolderSB(convertView);
            convertView.setTag(holderSB);
        }else {
            holderSB= (ViewHolderSB) convertView.getTag();
        }
        // 如果是第一次加载该view，则使用动画
        //convertView.startAnimation(animation);
        Picasso.with(context).load(list.get(position).getImage()).into(holderSB.ivSchoolImage);
        holderSB.tvSchoolTitle.setText(list.get(position).getTitle());
        holderSB.tvSchoolNum.setText(list.get(position).getClickCount()+"");
        return convertView;
    }

    static class ViewHolderSB {
        @Bind(R.id.iv_school_image)
        ImageView ivSchoolImage;
        @Bind(R.id.tv_school_title)
        TextView tvSchoolTitle;
        @Bind(R.id.tv_school_num)
        TextView tvSchoolNum;

        ViewHolderSB(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
