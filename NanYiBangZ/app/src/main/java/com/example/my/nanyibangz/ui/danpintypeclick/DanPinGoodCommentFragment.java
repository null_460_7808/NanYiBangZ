package com.example.my.nanyibangz.ui.danpintypeclick;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseFragment;
import com.example.my.nanyibangz.bean.DanPinTypeClickBeans;
import com.example.my.nanyibangz.bean.DanPinTypeClickCommentBeans;
import com.example.my.nanyibangz.config.DanPinUrlConfig;
import com.example.my.nanyibangz.utils.MyListView;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by My on 2016/10/11.
 */

public class DanPinGoodCommentFragment extends BaseFragment implements DanPinTypeClickLowerContact.View{
    @Bind(R.id.txt_shop_name)
    TextView txtShopName;
    @Bind(R.id.txt_dispat_score)
    TextView txtDispatScore;
    @Bind(R.id.txt_service_score)
    TextView txtServiceScore;
    @Bind(R.id.txt_desc_score)
    TextView txtDescScore;
    @Bind(R.id.lv_type_comment)
    MyListView lvTypeComment;
    String item_Id;//用来接收bundel传递来的item_id值
    private  DanPinTypeClickLowerPrenster presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.danpin_type_goods_comment, null);
        ButterKnife.bind(this, view);
        initview();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
    //接收bundle传递过来的item_id值
    public void   initview(){
        Bundle bundle=getArguments();
        if(bundle!=null){
            item_Id=bundle.getString("IndexId");
            Log.i("dp","item_ID=="+item_Id);
            netMap();
        }
    }

    //拼接网络地址
    public  void netMap(){
        Map<String, String> params = new HashMap<>();
        params.put(DanPinUrlConfig.Params.Age, 19 + "");
        params.put(DanPinUrlConfig.Params.Channel, DanPinUrlConfig.DefaultVaule.Channel_VALUE);
        params.put(DanPinUrlConfig.Params.Hkm_sign2, DanPinUrlConfig.DefaultVaule.Hkm_sign2_VALUE);
        params.put(DanPinUrlConfig.Params.Item_id,item_Id);
        params.put(DanPinUrlConfig.Params.Member_id, DanPinUrlConfig.DefaultVaule.Member_id_VALUE);
        params.put(DanPinUrlConfig.Params.Member_type, DanPinUrlConfig.DefaultVaule.Member_type_VALUE);
        params.put(DanPinUrlConfig.Params.Random_key, DanPinUrlConfig.DefaultVaule.Random_key_VALUE);
        params.put(DanPinUrlConfig.Params.System_name, DanPinUrlConfig.DefaultVaule.System_name_VALUE);
        params.put(DanPinUrlConfig.Params.VersionCode, DanPinUrlConfig.DefaultVaule.VersionCode_VALUE);
        presenter = new DanPinTypeClickLowerPrenster(this);
        presenter.getVerticalCommentBeansFromNet(params);
    }

    @Override
    public void getVerticalTypeLowerSuccess(DanPinTypeClickBeans danPinTypeClickBeans) {

    }

    @Override
    public void getVerticalTypeLowerFailed(String msg) {

    }

    //商品评论
    @Override
    public void getVerticalGoodsCommentSuccess(DanPinTypeClickCommentBeans danPinTypeClickCommentBeans) {

        DanPinTypeCommentAdapter danPinTypeCommentAdapter=new DanPinTypeCommentAdapter(getActivity(),danPinTypeClickCommentBeans.getData().getComments());
        Log.i("dp","size=="+danPinTypeClickCommentBeans.getData().getComments().size());
        lvTypeComment.setAdapter(danPinTypeCommentAdapter);
        txtDescScore.setText(danPinTypeClickCommentBeans.getData().getShopScore().getDescScore()+"");
        txtDispatScore.setText(danPinTypeClickCommentBeans.getData().getShopScore().getDispatScore()+"");
        txtServiceScore.setText(danPinTypeClickCommentBeans.getData().getShopScore().getServiceScore()+"");
        txtShopName.setText(danPinTypeClickCommentBeans.getData().getShopName());


    }

    @Override
    public void getVerticalGoodsCommentFailed(String msg) {

    }
}
