package com.example.my.nanyibangz.ui.dapei;

import android.util.Log;

import com.example.my.nanyibangz.bean.DaPeiDownBean;
import com.example.my.nanyibangz.bean.DaPeiUpBean;

import java.util.List;
import java.util.Map;

import rx.Subscriber;

/**
 * Created by Administrator on 2016/10/8.
 */

public class DaPeiUpPresenter implements DaPeiUpContact.Presenter {
    private DaPeiUpContact.Model model;
    private DaPeiUpContact.View view;

    public DaPeiUpPresenter(DaPeiUpContact.View view) {
        this.view = view;
        model=new DaPeiUpModel();

    }

    @Override
    public void getVerticalFromNet_DaPeiUp(Map<String, String> params) {
        model.getVerticalDaPeiUp(params, new Subscriber<DaPeiUpBean>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                view.onVerticalFail_DaPei(""+e.getMessage());
            }

            @Override
            public void onNext(DaPeiUpBean daPeiUpBean) {
                List<DaPeiUpBean.DataBean> list=daPeiUpBean.getData();
                view.onVerticalSucess_DaPeiBeanUp(list);
            }
        });
    }

    @Override
    public void getVerticalFrmNet_DaPeiDown(Map<String, String> map) {
        model.getVerticalDaPeiDown(map, new Subscriber<DaPeiDownBean>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.i("zlb","zlb=="+false);

            }

            @Override
            public void onNext(DaPeiDownBean daPeiDownBean) {
                Log.i("zlb","dap"+daPeiDownBean.getData().size());
                List<DaPeiDownBean.DataBean> list=daPeiDownBean.getData();
                view.onVerticalSucess_DaPeiBeanDown(list);
            }
        });
    }
}
