package com.example.my.nanyibangz.ui.shouye.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.HomeGoodsDetailsComment;

import java.util.List;

/**
 * 商品详情的店铺评论数据适配器
 */

public class HomeGoodsDetailsCommentRecyclerAdapter extends RecyclerView.Adapter<HomeGoodsDetailsCommentRecyclerAdapter.HomeGoodsDetailsCommentViewHolder>{
    private Context context;
    private List<HomeGoodsDetailsComment.DataBean.CommentsBean> list;

    public HomeGoodsDetailsCommentRecyclerAdapter(Context context, List<HomeGoodsDetailsComment.DataBean.CommentsBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public HomeGoodsDetailsCommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.home_goodsdetails_comment,null);
        HomeGoodsDetailsCommentViewHolder holder = new HomeGoodsDetailsCommentViewHolder(convertView);
        return holder;
    }

    @Override
    public void onBindViewHolder(HomeGoodsDetailsCommentViewHolder holder, final int position) {
        //设置控件 数据加载
        holder.homegoodsdetails_comment_tv_name.setText(list.get(position).getName());
        holder.homegoodsdetails_comment_tv_title.setText(list.get(position).getContent());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class HomeGoodsDetailsCommentViewHolder extends RecyclerView.ViewHolder{
        TextView homegoodsdetails_comment_tv_name,homegoodsdetails_comment_tv_title;
        public HomeGoodsDetailsCommentViewHolder(View itemView) {
            super(itemView);
            homegoodsdetails_comment_tv_name = (TextView) itemView.findViewById(R.id.homegoodsdetails_comment_tv_name);
            homegoodsdetails_comment_tv_title = (TextView) itemView.findViewById(R.id.homegoodsdetails_comment_tv_title);
        }
    }
}
