package com.example.my.nanyibangz.ui.danpintypeclick;

import android.util.Log;

import com.example.my.nanyibangz.bean.DanPinTypeClickBeans;
import com.example.my.nanyibangz.bean.DanPinTypeClickCommentBeans;

import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/10.
 */

public class DanPinTypeClickLowerPrenster implements DanPinTypeClickLowerContact.Presenter {
   private  DanPinTypeClickLowerContact.Model model;
    private  DanPinTypeClickLowerContact.View view;
    public  DanPinTypeClickLowerPrenster(DanPinTypeClickLowerContact.View view){
        this.view=view;
        model=new DanPinTypeClickLowerModel();

    }

    @Override
    public void getVerticalTypeLowerBeanFromNet(Map<String, String> params) {
       model.getVerticalTypeLowerBeans(params, new Subscriber<DanPinTypeClickBeans>() {
            @Override
            public void onCompleted() {
                Log.e("item","onerrro  onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Log.e("item","onerrro"+e.getMessage());


            }

            @Override
            public void onNext(DanPinTypeClickBeans danPinTypeClickBeans) {
                Log.e("item","onerrro  onNext");
                if(danPinTypeClickBeans!=null){
                    view.getVerticalTypeLowerSuccess(danPinTypeClickBeans);
                }


            }
        });

    }

    //商品评论
    @Override
    public void getVerticalCommentBeansFromNet(Map<String, String> params) {
        model.getVerticalGoodsCommentBeans(params, new Subscriber<DanPinTypeClickCommentBeans>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(DanPinTypeClickCommentBeans danPinTypeClickCommentBeans) {
                if(danPinTypeClickCommentBeans!=null){
                    view.getVerticalGoodsCommentSuccess(danPinTypeClickCommentBeans);
                }

            }
        });

    }
}
