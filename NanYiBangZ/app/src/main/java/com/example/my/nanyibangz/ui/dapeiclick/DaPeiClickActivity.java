package com.example.my.nanyibangz.ui.dapeiclick;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseActivity;
import com.example.my.nanyibangz.bean.DaPeiDownBean;
import com.example.my.nanyibangz.config.DaPeiUpClickConfig;
import com.example.my.nanyibangz.ui.dapei.DaPeiDownAdapter;
import com.example.my.nanyibangz.utils.MyGirdView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/10/8.
 */

public class DaPeiClickActivity extends BaseActivity implements DaPeiClickContact.View {


    @Bind(R.id.tv_dapClick_title)
    TextView tvDapClickTitle;
    @Bind(R.id.iv_dapClick_back)
    ImageView ivDapClickBack;
    @Bind(R.id.toolbar_dpc)
    Toolbar toolbarDpc;
    @Bind(R.id.gv_dapClick)
    MyGirdView gvDapClick;
    private DaPeiClickContact.Presenter presenter;
    private int classify_id;
    private String classify_name;
    private List<DaPeiDownBean.DataBean> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        initView();
        Intent intent = getIntent();
        classify_name = intent.getExtras().getString("classify_name");
        classify_id = intent.getExtras().getInt("classify_id");
        tvDapClickTitle.setText(classify_name);
        initDate();
        Click();
    }

    @Override
    public int getLayoutId() {
        return R.layout.dapei_click;
    }

    //tooBar设置
    private void initView() {
        //fragment中不能使用setSupportActionBar（）方法，想使用必须继承AppCompatActivity。
        AppCompatActivity activity = DaPeiClickActivity.this;
        activity.setSupportActionBar(toolbarDpc);//设置toolbar
        toolbarDpc.setTitle("");
        activity.getSupportActionBar().setDisplayShowTitleEnabled(false);//
    }

    private void initDate() {
        //http://api.nanyibang.com/match-list?age=19&channel=oppo&classify_id=1&hkm_sign2=0a8c93313012ce4f09eedb2a5f575f50&kind=1&member_id=292720&member_type=member&page=1&random_key=52363&system_name=android&versionCode=219
        presenter = new DaPeiClickPresenter(this);
        Map<String, String> map = new HashMap<>();
        map.put(DaPeiUpClickConfig.Params.Age, DaPeiUpClickConfig.DefaultVaule.Age_VALUE);
        map.put(DaPeiUpClickConfig.Params.Channel, DaPeiUpClickConfig.DefaultVaule.Channel_VALUE);
        map.put(DaPeiUpClickConfig.Params.Classify_id, classify_id + "");
        map.put(DaPeiUpClickConfig.Params.Hkm_sign2, DaPeiUpClickConfig.DefaultVaule.Hkm_sign2_VALUE);
        map.put(DaPeiUpClickConfig.Params.Kind, DaPeiUpClickConfig.DefaultVaule.Kind_VALUE);
        map.put(DaPeiUpClickConfig.Params.Member_id, DaPeiUpClickConfig.DefaultVaule.Member_id_VALUE);
        map.put(DaPeiUpClickConfig.Params.Member_type, DaPeiUpClickConfig.DefaultVaule.Member_type_VALUE);
        map.put(DaPeiUpClickConfig.Params.Page, DaPeiUpClickConfig.DefaultVaule.Page_VALUE);
        map.put(DaPeiUpClickConfig.Params.Random_key, DaPeiUpClickConfig.DefaultVaule.Random_key_VALUE);
        map.put(DaPeiUpClickConfig.Params.System_name, DaPeiUpClickConfig.DefaultVaule.System_name_VALUE);
        map.put(DaPeiUpClickConfig.Params.VersionCode, DaPeiUpClickConfig.DefaultVaule.VersionCode_VALUE);
        Log.i("tag", map + "");
        presenter.getVerticalFrmNet_DaPeiClick(map);
    }

    @Override
    public void onVerticalSucess_DaPeiBeanClick(List<DaPeiDownBean.DataBean> dataBeanList) {
        Log.i("tag", dataBeanList + "");
        DaPeiDownAdapter adapter = new DaPeiDownAdapter(DaPeiClickActivity.this, dataBeanList);
        gvDapClick.setAdapter(adapter);
        list = dataBeanList;
    }

    @Override
    public void onVerticalFail_DaPeiClick(String msg) {

    }

    //本页面的点击事件汇
    private void Click() {
        ivDapClickBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        gvDapClick.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int collocation_id = list.get(position).getCollocationId();
                Intent intent = new Intent(DaPeiClickActivity.this, TwoClickActivity.class);
                intent.putExtra("collocation_id", collocation_id);
                startActivity(intent);
            }
        });
    }
}
