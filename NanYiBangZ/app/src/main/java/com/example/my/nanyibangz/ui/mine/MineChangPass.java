package com.example.my.nanyibangz.ui.mine;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.UpdateListener;

/**
 * Created by Administrator on 2016/10/24.
 */

public class MineChangPass extends BaseActivity {
    @Bind(R.id.ed_registpass1)
    EditText edRegistpass1;
    @Bind(R.id.ed_registpass2)
    EditText edRegistpass2;
    @Bind(R.id.changpass)
    TextView changpass;

    @Override
    public int getLayoutId() {
        return R.layout.mine_changpass;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        Load();
    }
    private void Load(){
        changpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pass1=edRegistpass1.getText().toString();
                String pass2=edRegistpass2.getText().toString();
                BmobUser.updateCurrentUserPassword(pass1, pass2, new UpdateListener() {

                    @Override
                    public void done(BmobException e) {
                        if(e==null){
                            Toast.makeText(MineChangPass.this, "密码修改成功，可以用新密码进行登录啦！！", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(MineChangPass.this,MineLoadActivity.class);
                            startActivity(intent);
                            finish();

                        }else{
                            Toast.makeText(MineChangPass.this, "失败:" + e.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    }

                });
            }
        });
    }
}
