package com.example.my.nanyibangz.ui.shouye.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.HomeGoodsDetailsContent;
import com.example.my.nanyibangz.bean.HomeTideProduct;
import com.example.my.nanyibangz.ui.shouye.view.HomeGoodsDetailsActivity;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

/**
 * 商品详情的商品信息数据适配器
 */

public class HomeTideProductRecyclerAdapter extends RecyclerView.Adapter<HomeTideProductRecyclerAdapter.HomeTideProductViewHolder>{
    private Context context;
    private List<HomeTideProduct.DataBean.ItemDetailBean> list;

    public HomeTideProductRecyclerAdapter(Context context, List<HomeTideProduct.DataBean.ItemDetailBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public HomeTideProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.home_tideproduct_item,null);
        HomeTideProductViewHolder holder = new HomeTideProductViewHolder(convertView);
        return holder;
    }

    @Override
    public void onBindViewHolder(HomeTideProductViewHolder holder, final int position) {
        //设置控件 数据加载
        holder.home_tideproduct_item_sdv.setImageURI(Uri.parse(list.get(position).getPic_url()));
        holder.home_tideproduct_item_tv_saveCount.setText(list.get(position).getSaveCount()+"");
        holder.home_tideproduct_item_tv_title.setText(list.get(position).getDescription());
        holder.home_tideproduct_item_tv_description.setText(list.get(position).getTitle());
        String price = list.get(position).getPrice();
        String price1 = price.substring(0,price.indexOf("."));
        holder.home_tideproduct_item_tv_price.setText("¥"+price1);

        //点击事件
        holder.home_tideproduct_item_sdv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int item_id = list.get(position).get_id();
                Intent intent = new Intent(context, HomeGoodsDetailsActivity.class);
                intent.putExtra("item_id",item_id);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class HomeTideProductViewHolder extends RecyclerView.ViewHolder{
        SimpleDraweeView home_tideproduct_item_sdv;
        ImageView home_tideproduct_item_iv_love;
        TextView home_tideproduct_item_tv_saveCount,home_tideproduct_item_tv_title,home_tideproduct_item_tv_description,home_tideproduct_item_tv_price;
        public HomeTideProductViewHolder(View itemView) {
            super(itemView);
            home_tideproduct_item_sdv  = (SimpleDraweeView) itemView.findViewById(R.id.home_tideproduct_item_sdv);
            home_tideproduct_item_iv_love = (ImageView) itemView.findViewById(R.id.home_tideproduct_item_iv_love);
            home_tideproduct_item_tv_saveCount = (TextView) itemView.findViewById(R.id.home_tideproduct_item_tv_saveCount);
            home_tideproduct_item_tv_title = (TextView) itemView.findViewById(R.id.home_tideproduct_item_tv_title);
            home_tideproduct_item_tv_description = (TextView) itemView.findViewById(R.id.home_tideproduct_item_tv_description);
            home_tideproduct_item_tv_price = (TextView) itemView.findViewById(R.id.home_tideproduct_item_tv_price);
        }
    }
}
