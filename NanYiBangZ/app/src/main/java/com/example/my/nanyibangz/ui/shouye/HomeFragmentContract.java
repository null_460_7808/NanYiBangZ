package com.example.my.nanyibangz.ui.shouye;

import android.content.Context;

import com.example.my.nanyibangz.base.IModel;
import com.example.my.nanyibangz.base.IPresenter;
import com.example.my.nanyibangz.base.IView;
import com.example.my.nanyibangz.bean.HomeBangBangChoice;
import com.example.my.nanyibangz.bean.HomeChoiceness;
import com.example.my.nanyibangz.bean.HomeGoodsDetailsComment;
import com.example.my.nanyibangz.bean.HomeGoodsDetailsContent;
import com.example.my.nanyibangz.bean.HomeThreeBrandChoiceItem;
import com.example.my.nanyibangz.bean.HomeTideProduct;
import com.example.my.nanyibangz.bean.HomeVPBeans;
import com.example.my.nanyibangz.utils.MyPageAdapter;

import java.util.List;
import java.util.Map;

import rx.Subscriber;

/**
 * Created by My on 2016/10/7.
 */

public class HomeFragmentContract {
    public interface IHomeView extends IView{
        public void homeVPDataList(List<HomeVPBeans.DataBean> list);//回传首页轮播数据
        public void homeChoicenessList(HomeChoiceness homeChoiceness);//回传首页精选数据
        public void homeBangBangChoicenessList(List<HomeBangBangChoice.DataBean.ItemDetailBean> itemDetailBeanList);//回传首页邦邦精选数据
    }


   public interface IHomeModel extends IModel{
       public void homeLoadVPData(String type, Map<String,String> map, Subscriber<HomeVPBeans> subscriber);//加载首页轮播数据
       public void homeLoadChoicenessData(String type, Map<String,String> map, Subscriber<HomeChoiceness> subscriber);//加载首页精选数据
       public void homeLoadBangBangChoicenessData(String type, Map<String,String> map, Subscriber<HomeBangBangChoice> subscriber);//加载首页邦邦精选数据
    }

    public interface IHomePresenter extends IPresenter{
        public void homeLoadVPDataToUi(Context context);//将加载的轮播数据传递给View
        public void homeLoadChoicenessDataToUi(Context context);//将加载的精选数据传递给View
        public void homeLoadBangBangChoicenessDataToUi(Context context);//将加载的邦邦精选数据传递给View
    }

    //首页品牌精选Item内容
    public interface IHomeBrandChoiceView extends IView{
        public void homeBrandChoiceItemDataMap(Map<String,List<HomeThreeBrandChoiceItem.DataBean>> map);//回传首页品牌精选Item内容数据
        public int getHomeBrandChoiceItemBrandId();//获取品牌精选Item的品牌ID值
    }

    public interface IHomeBrandChoiceModel extends IModel{
        public void homeLoadBrandChoiceItemData(String type, Map<String,String> map, Subscriber<HomeThreeBrandChoiceItem> subscriber);//加载首页品牌精选Item数据
    }

    public interface IHomeBrandChoicePresenter extends IPresenter{
        public void homeLoadBrandChoiceItemDataToUi(Context context);//将加载的品牌精选Item数据传递给View
    }

    //首页商品详情内容
    public interface IHomeGoodsDetailsView extends IView{
        public void homeGoodsDetailsContentData(HomeGoodsDetailsContent homeGoodsDetailsContent);//回传首页商品详情的商品信息数据
        public void homeGoodsDetailsCommentData(HomeGoodsDetailsComment homeGoodsDetailsComment);//回传首页商品详情的店铺评论数据
        public int getHomeGoodsId();//获取商品ID
    }

    public interface IHomeGoodsDetailsModel extends IModel{
        public void homeLoadGoodsDetailsContentData(String type, Map<String,String> map, Subscriber<HomeGoodsDetailsContent> subscriber);//加载首页商品详情的商品信息数据
        public void homeLoadGoodsDetailsCommentData(String type, Map<String,String> map, Subscriber<HomeGoodsDetailsComment> subscriber);//加载首页商品详情的商品信息数据
    }

    public interface IHomeGoodsDetailsPresenter extends IPresenter{
        public void homeLoadGoodsDetailsContentDataToUi(Context context);//将加载的首页商品详情的商品信息数据传递给View
        public void homeLoadGoodsDetailsCommentDataToUi(Context context);//将加载的首页商品详情的店铺评论数据传递给View
    }

    //首页特色市场潮品专区
    public interface IHomeTideProductView extends IView{
        public void homeTideProductData(List<HomeTideProduct.DataBean.ItemDetailBean> itemDetailBeanList);//回传首页特色市场潮品专区数据
    }

    public interface IHomeTideProductModel extends IModel{
        public void homeLoadTideProductData(String type, Map<String,String> map, Subscriber<HomeTideProduct> subscriber);//加载首页特色市场潮品专区数据
    }

    public interface IHomeTideProductPresenter extends IPresenter{
        public void homeLoadGoodsDetailsContentDataToUi(Context context);//将加载的首页特色市场潮品专区数据传递给View
    }
}
