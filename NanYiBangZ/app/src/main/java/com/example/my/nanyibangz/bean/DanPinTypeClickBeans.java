package com.example.my.nanyibangz.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by My on 2016/10/10.
 */

public class DanPinTypeClickBeans implements Serializable{

    /**
     * member_type : guest
     * member_id : 0
     * login_status : error
     * login_status_msg : not login in
     */

    @SerializedName("user")
    private UserBean user;
    /**
     * item_id : 6226
     * special_price : null
     * cate_id : 22
     * title : 秒杀29元！最后100件！秋季时尚修身衬衣磨毛格子男士装长袖衬衫
     * pic_url : http://img04.taobaocdn.com/bao/uploaded/i4/TB1H3ZSNXXXXXXRXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg
     * pic_urls : http://img.alicdn.com/bao/uploaded/i4/TB1H3ZSNXXXXXXRXXXXXXXXXXXX_!!0-item_pic.jpg_450x10000q75.jpg,http://img.alicdn.com/bao/uploaded/i3/656010309/TB253.raFHzQeBjSZFHXXbwZpXa_!!656010309.jpg_450x10000q75.jpg,http://img.alicdn.com/bao/uploaded/i1/656010309/TB2FpRsXhLA11Bjy0FeXXbmzFXa_!!656010309.jpg_450x10000q75.jpg,http://img.alicdn.com/bao/uploaded/i4/656010309/TB2wFXrXj2C11BjSszgXXaKlpXa_!!656010309.jpg_450x10000q75.jpg,http://img.alicdn.com/bao/uploaded/i4/656010309/TB2re_waRLzQeBjSZFoXXc5gFXa_!!656010309.jpg_450x10000q75.jpg
     * brand : TOPOT
     * num_iid : 87414805
     * coupon_price : 29.00
     * purchaseLink :
     * item_type : 1
     * open_iid : AAHn_kWCABuH20zjAKXb8K9T
     * price : 239.00
     * saveCount : 0
     * cuxiao_desc :
     * material : 聚酯纤维100%
     * style : 时尚都市
     * color : 军绿色,深卡其布色,巧克力色,桔色,浅黄色,浅绿色,粉红色,紫色,紫罗兰,深灰色,深紫色,深蓝色,黄色
     * season : 秋季
     * item_description : <head>
     <style>
     .center_image { width: 100%}
     .hide_image { display: none}
     </style>
     </head><img class='center_image' src='https://img.alicdn.com/imgextra/i4/656010309/TB2OYr1uXXXXXb3XpXXXXXXXXXX_!!656010309.jpg'><img class='center_image' src='https://img.alicdn.com/imgextra/i2/656010309/TB208UcuXXXXXadXpXXXXXXXXXX_!!656010309.jpg'><img class='center_image' src='https://img.alicdn.com/imgextra/i4/656010309/TB20iQsuXXXXXa_XXXXXXXXXXXX_!!656010309.jpg'><img class='center_image' src='https://img.alicdn.com/imgextra/i2/656010309/TB2I8UquXXXXXbZXXXXXXXXXXXX_!!656010309.jpg'><img class='center_image' src='https://img.alicdn.com/imgextra/i4/656010309/TB2nrgAuXXXXXanXXXXXXXXXXXX_!!656010309.jpg'><img class='center_image' src='https://img.alicdn.com/imgextra/i1/656010309/TB2DTMCuXXXXXXZXXXXXXXXXXXX_!!656010309.jpg'><img class='center_image' src='https://img.alicdn.com/imgextra/i2/656010309/TB2B3Y1uXXXXXcjXpXXXXXXXXXX_!!656010309.jpg'><img class='center_image' src='https://img.alicdn.com/imgextra/i3/656010309/TB2KjAguXXXXXXMXpXXXXXXXXXX_!!656010309.jpg'><img class='center_image' src='https://img.alicdn.com/imgextra/i4/656010309/TB2fshcXb75VKBjy0FcXXbAkXXa_!!656010309.jpg'><script>
     var imgs = document.getElementsByClassName('center_image');
     for (var i = 0; i < imgs.length; i++) {
     imgs[i]['index']=i;
     imgs[i].addEventListener('load', function() {
     if (imgs[this.index].naturalWidth < 100) {
     this.className += ' hide_image';
     }
     });
     imgs[i].addEventListener('error', function() {
     this.className += ' hide_image';
     });
     }
     </script>
     */

    @SerializedName("data")
    private DataBean data;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class UserBean {
        @SerializedName("member_type")
        private String memberType;
        @SerializedName("member_id")
        private int memberId;
        @SerializedName("login_status")
        private String loginStatus;
        @SerializedName("login_status_msg")
        private String loginStatusMsg;

        public String getMemberType() {
            return memberType;
        }

        public void setMemberType(String memberType) {
            this.memberType = memberType;
        }

        public int getMemberId() {
            return memberId;
        }

        public void setMemberId(int memberId) {
            this.memberId = memberId;
        }

        public String getLoginStatus() {
            return loginStatus;
        }

        public void setLoginStatus(String loginStatus) {
            this.loginStatus = loginStatus;
        }

        public String getLoginStatusMsg() {
            return loginStatusMsg;
        }

        public void setLoginStatusMsg(String loginStatusMsg) {
            this.loginStatusMsg = loginStatusMsg;
        }
    }

    public static class DataBean  implements Serializable{
        @SerializedName("item_id")
        private int itemId;
        @SerializedName("special_price")
        private Object specialPrice;
        @SerializedName("cate_id")
        private int cateId;
        @SerializedName("title")
        private String title;
        @SerializedName("pic_url")
        private String picUrl;
        @SerializedName("pic_urls")
        private String picUrls;
        @SerializedName("brand")
        private String brand;
        @SerializedName("num_iid")
        private String numIid;
        @SerializedName("coupon_price")
        private String couponPrice;
        @SerializedName("purchaseLink")
        private String purchaseLink;
        @SerializedName("item_type")
        private int itemType;
        @SerializedName("open_iid")
        private String openIid;
        @SerializedName("price")
        private String price;
        @SerializedName("saveCount")
        private int saveCount;
        @SerializedName("cuxiao_desc")
        private String cuxiaoDesc;
        @SerializedName("material")
        private String material;
        @SerializedName("style")
        private String style;
        @SerializedName("color")
        private String color;
        @SerializedName("season")
        private String season;
        @SerializedName("item_description")
        private String itemDescription;

        public int getItemId() {
            return itemId;
        }

        public void setItemId(int itemId) {
            this.itemId = itemId;
        }

        public Object getSpecialPrice() {
            return specialPrice;
        }

        public void setSpecialPrice(Object specialPrice) {
            this.specialPrice = specialPrice;
        }

        public int getCateId() {
            return cateId;
        }

        public void setCateId(int cateId) {
            this.cateId = cateId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPicUrl() {
            return picUrl;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }

        public String getPicUrls() {
            return picUrls;
        }

        public void setPicUrls(String picUrls) {
            this.picUrls = picUrls;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getNumIid() {
            return numIid;
        }

        public void setNumIid(String numIid) {
            this.numIid = numIid;
        }

        public String getCouponPrice() {
            return couponPrice;
        }

        public void setCouponPrice(String couponPrice) {
            this.couponPrice = couponPrice;
        }

        public String getPurchaseLink() {
            return purchaseLink;
        }

        public void setPurchaseLink(String purchaseLink) {
            this.purchaseLink = purchaseLink;
        }

        public int getItemType() {
            return itemType;
        }

        public void setItemType(int itemType) {
            this.itemType = itemType;
        }

        public String getOpenIid() {
            return openIid;
        }

        public void setOpenIid(String openIid) {
            this.openIid = openIid;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public int getSaveCount() {
            return saveCount;
        }

        public void setSaveCount(int saveCount) {
            this.saveCount = saveCount;
        }

        public String getCuxiaoDesc() {
            return cuxiaoDesc;
        }

        public void setCuxiaoDesc(String cuxiaoDesc) {
            this.cuxiaoDesc = cuxiaoDesc;
        }

        public String getMaterial() {
            return material;
        }

        public void setMaterial(String material) {
            this.material = material;
        }

        public String getStyle() {
            return style;
        }

        public void setStyle(String style) {
            this.style = style;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getSeason() {
            return season;
        }

        public void setSeason(String season) {
            this.season = season;
        }

        public String getItemDescription() {
            return itemDescription;
        }

        public void setItemDescription(String itemDescription) {
            this.itemDescription = itemDescription;
        }
    }
}
