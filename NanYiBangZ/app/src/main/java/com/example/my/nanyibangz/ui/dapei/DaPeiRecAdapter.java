package com.example.my.nanyibangz.ui.dapei;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.DaPeiDownBean;
import com.example.my.nanyibangz.ui.dapeidownclick.DaPeiClickDownContact;

import java.util.List;

/**
 * Created by Administrator on 2016/10/19.
 */

public class DaPeiRecAdapter extends RecyclerView.Adapter<DaPeiRecAdapter.MyViewHolder> implements View.OnClickListener {
    private Context context;
    private List<DaPeiDownBean.DataBean> list;
    private OnItemActionListener actionListener;


    public DaPeiRecAdapter(Context context, List<DaPeiDownBean.DataBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.dapei_item_down,null);
        MyViewHolder holder=new MyViewHolder(view);
        view.setOnClickListener(this);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Glide.with(context)
                .load(list.get(position).getBigImage())
                .override(list.get(position).getWidth(),list.get(position).getHeight())
                .into(holder.ivGroupDown);
        holder.tvGroupDown.setText(list.get(position).getInfo());
        //
        holder.itemView.setTag(list.get(position).getCollocationId()+"");
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    /**
     * Called when a view has been clicked.
     *
     */
    @Override
    public void onClick(View v) {
        if (actionListener!=null){
            actionListener.OnItemClick(v, (String) v.getTag());
        }
    }
    /**itemclicklistener监听
     * 设置itemclick
     */
    public  void setItemClickListener(OnItemActionListener listener){
        this.actionListener=listener;
    }


    /**********定义点击事件**********/
    public  interface  OnItemActionListener {
        public void OnItemClick(View view, String CollocationId);
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView ivGroupDown;
        TextView tvGroupDown;
        public MyViewHolder(View itemView) {
            super(itemView);
            ivGroupDown= (ImageView) itemView.findViewById(R.id.iv_groupDown);
            tvGroupDown= (TextView) itemView.findViewById(R.id.tv_groupDown);
        }
    }

}

