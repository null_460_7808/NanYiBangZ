package com.example.my.nanyibangz.bean;

import java.util.List;

/**
 * Created by My on 2016/10/8.
 */

public class HomeChoiceness {

    /**
     * member_type : guest
     * login_status : error
     * login_status_msg : not login in
     */

    private UserBean user;
    private DataBean data;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class UserBean {
        private String member_type;
        private String login_status;
        private String login_status_msg;

        public String getMember_type() {
            return member_type;
        }

        public void setMember_type(String member_type) {
            this.member_type = member_type;
        }

        public String getLogin_status() {
            return login_status;
        }

        public void setLogin_status(String login_status) {
            this.login_status = login_status;
        }

        public String getLogin_status_msg() {
            return login_status_msg;
        }

        public void setLogin_status_msg(String login_status_msg) {
            this.login_status_msg = login_status_msg;
        }
    }

    public static class DataBean {
        private List<?> match;
        /**
         * dress_school_id : 559
         * title : 经典潮流发型
         * info : 4个永不退流行的经典发型，从此没有撩不到的妹！
         * image : http://im01.nanyibang.com/school/2016/09/28/SCHOOL131257.jpg
         * clickCount : 24065
         * link : http://www.nanyibang.com/utils/webpage_jump.php?module_type=school&school_id=559
         */

        private List<SchoolBean> school;
        /**
         * brandId : 8646
         * brandIcon : http://im01.nanyibang.com/brand/2016/06/16/fun.jpg
         * brandName : FUN
         */

        private List<BrandBean> brand;
        /**
         * themeId : 17
         * themeDesc : 型男穿搭
         * matches : [{"collocation_id":8533,"big_image":"http://im02.nanyibang.com/match/2016/10/08/1475892858_85942.jpg?_upt=0af77a421475915910"},{"collocation_id":8696,"big_image":"http://im02.nanyibang.com/match/2016/09/26/1474857286_49392.jpg?_upt=527ae45e1475915910"},{"collocation_id":8814,"big_image":"http://im02.nanyibang.com/match/2016/09/28/1475030521_78365.jpg?_upt=7c0189d41475915910"}]
         */

        private List<MatchThemesBean> matchThemes;

        public List<?> getMatch() {
            return match;
        }

        public void setMatch(List<?> match) {
            this.match = match;
        }

        public List<SchoolBean> getSchool() {
            return school;
        }

        public void setSchool(List<SchoolBean> school) {
            this.school = school;
        }

        public List<BrandBean> getBrand() {
            return brand;
        }

        public void setBrand(List<BrandBean> brand) {
            this.brand = brand;
        }

        public List<MatchThemesBean> getMatchThemes() {
            return matchThemes;
        }

        public void setMatchThemes(List<MatchThemesBean> matchThemes) {
            this.matchThemes = matchThemes;
        }

        public static class SchoolBean {
            private int dress_school_id;
            private String title;
            private String info;
            private String image;
            private int clickCount;
            private String link;

            public int getDress_school_id() {
                return dress_school_id;
            }

            public void setDress_school_id(int dress_school_id) {
                this.dress_school_id = dress_school_id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getInfo() {
                return info;
            }

            public void setInfo(String info) {
                this.info = info;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public int getClickCount() {
                return clickCount;
            }

            public void setClickCount(int clickCount) {
                this.clickCount = clickCount;
            }

            public String getLink() {
                return link;
            }

            public void setLink(String link) {
                this.link = link;
            }
        }

        public static class BrandBean {
            private int brandId;
            private String brandIcon;
            private String brandName;

            public int getBrandId() {
                return brandId;
            }

            public void setBrandId(int brandId) {
                this.brandId = brandId;
            }

            public String getBrandIcon() {
                return brandIcon;
            }

            public void setBrandIcon(String brandIcon) {
                this.brandIcon = brandIcon;
            }

            public String getBrandName() {
                return brandName;
            }

            public void setBrandName(String brandName) {
                this.brandName = brandName;
            }
        }

        public static class MatchThemesBean {
            private int themeId;
            private String themeDesc;
            /**
             * collocation_id : 8533
             * big_image : http://im02.nanyibang.com/match/2016/10/08/1475892858_85942.jpg?_upt=0af77a421475915910
             */

            private List<MatchesBean> matches;

            public int getThemeId() {
                return themeId;
            }

            public void setThemeId(int themeId) {
                this.themeId = themeId;
            }

            public String getThemeDesc() {
                return themeDesc;
            }

            public void setThemeDesc(String themeDesc) {
                this.themeDesc = themeDesc;
            }

            public List<MatchesBean> getMatches() {
                return matches;
            }

            public void setMatches(List<MatchesBean> matches) {
                this.matches = matches;
            }

            public static class MatchesBean {
                private int collocation_id;
                private String big_image;

                public int getCollocation_id() {
                    return collocation_id;
                }

                public void setCollocation_id(int collocation_id) {
                    this.collocation_id = collocation_id;
                }

                public String getBig_image() {
                    return big_image;
                }

                public void setBig_image(String big_image) {
                    this.big_image = big_image;
                }
            }
        }
    }
}
