package com.example.my.nanyibangz.ui.dapeidownclick;

import com.example.my.nanyibangz.base.IModel;
import com.example.my.nanyibangz.base.IPresenter;
import com.example.my.nanyibangz.base.IView;
import com.example.my.nanyibangz.bean.DaPeiDownBean;
import com.example.my.nanyibangz.bean.DaPeiDownClickBean;

import java.util.List;
import java.util.Map;

import rx.Subscriber;

/**
 * 搭配页面click
 * Created by Administrator on 2016/10/8.
 */

public class DaPeiClickDownContact {
    public interface View extends IView{
        //上半部分的加载
        public void onVerticalSucess_DaPeiBeanDownClick(DaPeiDownClickBean downClickBean);
        public void onVerticalFail_DaPeiDownClick(String msg);
        //下半部分grideView的类型
        public void onVerticalSucess_DaPeiBeanDownClickGV(List<DaPeiDownClickBean.DataBean.SingleItemsBean> itemsBeanList);
        public void onVerticalFail_DaPeiDownClickGV(String msg);
        //上拉加载详情页面
        public void onVerticalSucess_DaPeiBeanDownClickRefresh(List<DaPeiDownClickBean.DataBean.RelativeBean.ItemsBean> itemsList);
        public void onVerticalFail_DaPeiDownClickRefresh(String msg);

    }
    public interface Model extends IModel{
        public void getVerticalDaPeiDownClick(Map<String, String> map, Subscriber<DaPeiDownClickBean> subscribers);
        //
        public void getVerticalDaPeiDownClickGV(Map<String, String> map, Subscriber<DaPeiDownClickBean> subscribers);
        //
        public void getVerticalDaPeiDownClickRefresh(Map<String, String> map, Subscriber<DaPeiDownClickBean.DataBean.RelativeBean> subscribers);
    }
    public interface Presenter extends IPresenter{
        public void getVerticalFrmNet_DaPeiDownClick(Map<String, String> map);
        //
        public void getVerticalFrmNet_DaPeiDownClickGV(Map<String, String> map);
        //
        public void getVerticalFrmNet_DaPeiDownClickRefresh(Map<String, String> map);
    }
}
