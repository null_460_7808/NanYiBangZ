package com.example.my.nanyibangz.ui.danpintypeclick;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.my.nanyibangz.MainActivity;
import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.base.BaseActivity;
import com.example.my.nanyibangz.bean.CollectionBeans;
import com.example.my.nanyibangz.bean.DanPinTypeClickBeans;
import com.example.my.nanyibangz.bean.DanPinTypeClickCommentBeans;
import com.example.my.nanyibangz.config.DanPinUrlConfig;
import com.j256.ormlite.stmt.QueryBuilder;
import com.squareup.picasso.Picasso;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;

/**
 * Created by My on 2016/10/10.
 */

@SuppressLint("SetJavaScriptEnabled")
public class DanPinTypeClickLowerActivity extends BaseActivity implements DanPinTypeClickLowerContact.View {

    @Bind(R.id.vp_typeclick)
    ViewPager vpTypeclick;
    @Bind(R.id.txt_typeclick_sprice)
    TextView txtTypeclickSprice;
    @Bind(R.id.txt_typeclick_coupon_price)
    TextView txtTypeclickCouponPrice;
    @Bind(R.id.txt_typeclick_title)
    TextView txtTypeclickTitle;
    @Bind(R.id.ll_type_click_fg)
    FrameLayout llTypeClickFg;
    @Bind(R.id.web_typeclick)
    WebView webTypeclick;
    @Bind(R.id.rbt_type_collection)
    CheckBox rbtTypeCollection;
    @Bind(R.id.rbt_type_share)
    CheckBox rbtTypeShare;
    @Bind(R.id.rbt_type_add_cart)
    CheckBox rbtTypeAddCart;
    @Bind(R.id.rbt_type_goto_mail)
    CheckBox rbtTypeGotoMail;
    @Bind(R.id.ll_type_point)
    LinearLayout llTypePoint;//轮播图的指示小圆点。
    @Bind(R.id.txt_type_info)
    TextView txtTypeInfo;
    @Bind(R.id.txt_type_comment)
    TextView txtTypeComment;
    @Bind(R.id.img_goodsinfo_back)
    ImageView imgGoodsinfoBack;
    private DanPinTypeClickLowerAdAdpter ad_adapter;//轮播图的adapter;
    private String Item_Id = "";//用于接收最低分类页面传递过来的，item_id，拼接地址。
    private String picUrl="";//用于接收最低分类页面传递过来的，picUrl，拼接地址。
    private String title = "";//用于接收最低分类页面传递过来的，title，拼接地址。
    private String price="";//用于接收最低分类页面传递过来的，price，拼接地址。
    private DanPinTypeClickBeans.DataBean dpbeans;//用于接收网络下载的每一项的实体对象
    private DanPinTypeClickLowerPrenster presenter;//中间者
    private List<ImageView> imageViewList;//存放imageview的list集合。
    private int curIndex = 0;//默认起始索引。
  /*  private MySqliteOpenHelper mySqliteOpenHelper;//数据库对象
    private Dao<CollectionBeans,Long> collectionBeansLongDao;//操作数据库的dao;*/
    FragmentManager fragmentManager;
    //加载头部广告
    private String content=null;//分享的内容
    private String shareImageUrl=null;//分享的图片的地址
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                final String[] imgStrs = (String[]) msg.obj;
                imageViewList = new ArrayList<>();
                for (int i = 0; i < imgStrs.length; i++) {
                    ImageView imageView = new ImageView(DanPinTypeClickLowerActivity.this);
                    Picasso.with(DanPinTypeClickLowerActivity.this).load(imgStrs[i]).resize(320, 320).into(imageView);
                    imageViewList.add(imageView);
                    final String imgUrl = imgStrs[i];//取出地址。
                    //轮播图中的点击事件。 以实现点击viewPager中轮播广告条 ，查看大图
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.putExtra("image", imgUrl);
                            intent.setClass(DanPinTypeClickLowerActivity.this, ShowWebImageActivity.class);
                            DanPinTypeClickLowerActivity.this.startActivity(intent);
                        }
                    });
                }
                initPoints(imageViewList.size());//初始化图片小圆点
                ad_adapter = new DanPinTypeClickLowerAdAdpter(imageViewList);
                vpTypeclick.setAdapter(ad_adapter);
                startAutoScroll();// 自动播放
                MyVpChagnerListenr();//广告条viewpager的滑动监听事件

            }
        }
    };

    //注解必须有
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShareSDK.initSDK(this);//初始化sharedsdk
        initview();
        netUrlMap();    //拼接访问网络的地址。
        // //给各个textview等赋值。
        ImagBack();//点击返回按钮回到上一级。
        Collection();

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_dp_typeclick_plus;
    }

    //用于接收最低页面传递过来的item_id值，拼接地址用
    public void initview() {
        Intent intent = getIntent();
        Item_Id = intent.getStringExtra("itemId");
        picUrl=intent.getStringExtra("picUrl");
        title=intent.getStringExtra("title");
        price=intent.getStringExtra("price");
        Log.i("cc","price="+price+"");
        Log.i("cc", Item_Id);
    }

    //给各个textview等赋值。
    @SuppressLint("SetJavaScriptEnabled")
    public void setData() {
        txtTypeclickSprice.setText(dpbeans.getCouponPrice()+"");
        txtTypeclickCouponPrice.setText(dpbeans.getPrice() + "");
        txtTypeclickCouponPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG); //中划线
        txtTypeclickTitle.setText(dpbeans.getTitle());
        content=dpbeans.getTitle();
        shareImageUrl=dpbeans.getPicUrl();
        String htmlString = dpbeans.getItemDescription().toString();
        //启用javascript
        webTypeclick.getSettings().setJavaScriptEnabled(true);
//给webView添加js代码
        webTypeclick.loadData(htmlString, "text/html", "utf-8");

        // 添加js交互接口类，并起别名 imagelistner  addJavaScriptInterface方式帮助我们从一个网页传递值到Android XML视图（反之亦然）。
        webTypeclick.addJavascriptInterface(new JavascriptInterface(this), "imagelistner");
        // webTypeclick.addJavascriptInterface(new JavascriptInterface(this), "imagelistner");
        webTypeclick.setWebViewClient(new MyWebViewClient());
        //加载轮播图的的地址
        String imgUrls = dpbeans.getPicUrls();
        final String[] imgs = imgUrls.split(",");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message message = Message.obtain();
                message.what = 1;
                message.obj = imgs;
                handler.sendMessage(message);
            }
        }).start();
    }

    //实现webview中图片的点击事件
    private void addImageClickListner() {
        Log.i("zb", "zlbhtml加载完成后调用listenter");
        // 这段js函数的功能就是，遍历所有的img，并添加onclick函数，函数的功能是在图片点击的时候调用本地java接口并传递url过去
        webTypeclick.loadUrl("javascript:(function(){" +
                "var objs = document.getElementsByTagName(\"img\"); " +
                "for(var i=0;i<objs.length;i++)  " +
                "{"
                + "    objs[i].onclick=function()  " +
                "    {  "
                + "        window.imagelistner.openImage(this.src);  " +
                "    }  " +
                "}" +
                "})()");
    }

    // js通信接口
    public class JavascriptInterface {

        private Context context;

        public JavascriptInterface(Context context) {
            this.context = context;
        }

        // android混淆之后webview不响应js点击事件 在js的接口上面写上@JavascriptInterface。。。找了好久，在官方文档上找到的解决办法。
        //下面注解必须加上，不然在4.2以上的机子，js注入不响应事件。
        @android.webkit.JavascriptInterface
        public void openImage(String img) {
            System.out.println(img);

            Intent intent = new Intent();
            intent.putExtra("image", img);
            intent.setClass(context, ShowWebImageActivity.class);
            context.startActivity(intent);
            System.out.println(img);
        }
    }

    //webViewClient 这个类主要帮助WebView处理各种通知、请求时间的，比如：
    private class MyWebViewClient extends WebViewClient {

        //在点击请求的是链接是才会调用，重写此方法返回true表明点击网页里面的链接还是在当前的webview里跳转，不跳到浏览器那边。
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            return super.shouldOverrideUrlLoading(view, url);
        }

        //在页面加载结束时调用。
        @Override
        public void onPageFinished(WebView view, String url) {

            view.getSettings().setJavaScriptEnabled(true);

            super.onPageFinished(view, url);
            // html加载完成之后，添加监听图片的点击js函数
            addImageClickListner();

        }

        //在页面加载开始时调用。
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            view.getSettings().setJavaScriptEnabled(true);

            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Log.i("zb", "zlb被点击了" + false);
            super.onReceivedError(view, errorCode, description, failingUrl);

        }
    }


    //拼接访问网络的地址。
    public void netUrlMap() {
        //http://api.nanyibang.com/items?age=19&channel=oppo&hkm_sign2=e25852511d05cec90c5cda0c6fe8a700&item_id=406340
        // &member_id=292720&member_type=member&random_key=72826&system_name=android&versionCode=219
        //商品评论
        //http://api.nanyibang.com/score-comment?age=19&channel=oppo&hkm_sign2=33c04d34e4ce711a0ceee9a3bea71771&item_id=406340
        // &member_id=292720&member_type=member&random_key=37634&system_name=android&versionCode=219
        Map<String, String> params = new HashMap<>();
        params.put(DanPinUrlConfig.Params.Age, 19 + "");
        params.put(DanPinUrlConfig.Params.Channel, DanPinUrlConfig.DefaultVaule.Channel_VALUE);
        params.put(DanPinUrlConfig.Params.Hkm_sign2, DanPinUrlConfig.DefaultVaule.Hkm_sign2_VALUE);
        params.put(DanPinUrlConfig.Params.Item_id, Item_Id);
        params.put(DanPinUrlConfig.Params.Member_id, DanPinUrlConfig.DefaultVaule.Member_id_VALUE);
        params.put(DanPinUrlConfig.Params.Member_type, DanPinUrlConfig.DefaultVaule.Member_type_VALUE);
        params.put(DanPinUrlConfig.Params.Random_key, DanPinUrlConfig.DefaultVaule.Random_key_VALUE);
        params.put(DanPinUrlConfig.Params.System_name, DanPinUrlConfig.DefaultVaule.System_name_VALUE);
        params.put(DanPinUrlConfig.Params.VersionCode, DanPinUrlConfig.DefaultVaule.VersionCode_VALUE);
        presenter = new DanPinTypeClickLowerPrenster(this);
        presenter.getVerticalTypeLowerBeanFromNet(params);
    }


    @Override
    public void getVerticalTypeLowerSuccess(DanPinTypeClickBeans danPinTypeClickBeans) {
        Log.i("item", "bens" + danPinTypeClickBeans);
        dpbeans = danPinTypeClickBeans.getData();
        setData();
        SendDataToInfoFg();
        GoodsInfoClick();  //商品信息
        GoodsCommentClick();//商品评论点击
        Share();//点击分享一键分享到，微信，微信朋友圈，微博，qq,qq空间。
    }

    @Override
    public void getVerticalTypeLowerFailed(String msg) {

    }

    @Override
    public void getVerticalGoodsCommentSuccess(DanPinTypeClickCommentBeans danPinTypeClickCommentBeans) {
        //商品评论
    }

    @Override
    public void getVerticalGoodsCommentFailed(String msg) {
        //商品评论

    }

    //以下是实现广告banner的轮播
    // 初始化图片轮播的小圆点和目录
    private void initPoints(int count) {
        for (int i = 0; i < count; i++) {

            ImageView iv = new ImageView(this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    20, 20);
            params.setMargins(0, 0, 20, 0);
            iv.setLayoutParams(params);

            iv.setImageResource(R.mipmap.icon_next_one_normal);

            llTypePoint.addView(iv);

        }
        ((ImageView) llTypePoint.getChildAt(curIndex))
                .setImageResource(R.mipmap.icon_next_one_select);
    }


    // 自动播放
    private void startAutoScroll() {
        ScheduledExecutorService scheduledExecutorService = Executors
                .newSingleThreadScheduledExecutor();
        // 每隔4秒钟切换一张图片
        scheduledExecutorService.scheduleWithFixedDelay(new ViewPagerTask(), 5,
                4, TimeUnit.SECONDS);
    }

    // 切换图片任务
    private class ViewPagerTask implements Runnable {
        @Override
        public void run() {

            DanPinTypeClickLowerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int count = ad_adapter.getCount();
                    vpTypeclick.setCurrentItem((curIndex + 1) % count);
                }
            });
        }
    }

    //广告viewpager滑动事件监听
    public void MyVpChagnerListenr() {
        vpTypeclick.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                ImageView imageView1 = (ImageView) llTypePoint.getChildAt(position);
                ImageView imageView2 = (ImageView) llTypePoint.getChildAt(curIndex);
                if (imageView1 != null) {
                    imageView1.setImageResource(R.mipmap.icon_next_one_select);
                }
                if (imageView2 != null) {
                    imageView2.setImageResource(R.mipmap.icon_next_one_normal);
                }
                curIndex = position;

            }

            boolean b = false;

            @Override
            public void onPageScrollStateChanged(int state) {
                //这段代码可不加，主要功能是实现切换到末尾后返回到第一张
                switch (state) {
                    case 1:// 手势滑动
                        b = false;
                        break;
                    case 2:// 界面切换中
                        b = true;
                        break;
                    case 0:// 滑动结束，即切换完毕或者加载完毕
                        // 当前为最后一张，此时从右向左滑，则切换到第一张
                        if (vpTypeclick.getCurrentItem() == vpTypeclick.getAdapter()
                                .getCount() - 1 && !b) {
                            vpTypeclick.setCurrentItem(0);
                        }
                        // 当前为第一张，此时从左向右滑，则切换到最后一张
                        else if (vpTypeclick.getCurrentItem() == 0 && !b) {
                            vpTypeclick.setCurrentItem(vpTypeclick.getAdapter()
                                    .getCount() - 1);
                        }
                        break;

                    default:
                        break;
                }
            }
        });
    }

    //将danpintypeclickbeans传递给商品信息的bundle值。
    public void SendDataToInfoFg() {
        Log.i("zlb", "mei===" + true);
        txtTypeInfo.setTextColor(DanPinTypeClickLowerActivity.this.getResources().getColor(R.color.colorSwipRef));
        txtTypeInfo.setBackground(DanPinTypeClickLowerActivity.this.getResources().getDrawable(R.color.colorToolbar));
        Bundle bundle = new Bundle();
        bundle.putSerializable("index", dpbeans);
        DanPinGoodInfoFragment danPinGoodInfoFragment = new DanPinGoodInfoFragment();
        danPinGoodInfoFragment.setArguments(bundle);
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.ll_type_click_fg, danPinGoodInfoFragment);
        transaction.commit();
    }

    //商品信息点击
    public void GoodsInfoClick() {
        txtTypeInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                txtTypeComment.setTextColor(DanPinTypeClickLowerActivity.this.getResources().getColor(R.color.colorToolbar));

                txtTypeComment.setBackground(DanPinTypeClickLowerActivity.this.getResources().getDrawable(R.drawable.type_txt_infoand_comshape));
                txtTypeInfo.setTextColor(DanPinTypeClickLowerActivity.this.getResources().getColor(R.color.colorSwipRef));
                txtTypeInfo.setBackground(DanPinTypeClickLowerActivity.this.getResources().getDrawable(R.color.colorToolbar));
                Bundle bundle = new Bundle();
                bundle.putSerializable("index", dpbeans);
                DanPinGoodInfoFragment danPinGoodInfoFragment = new DanPinGoodInfoFragment();
                danPinGoodInfoFragment.setArguments(bundle);
                fragmentManager = getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.ll_type_click_fg, danPinGoodInfoFragment);
                transaction.commit();
            }
        });
    }

    //店铺品论点击
    public void GoodsCommentClick() {
        txtTypeComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtTypeComment.setTextColor(DanPinTypeClickLowerActivity.this.getResources().getColor(R.color.colorSwipRef));
                txtTypeComment.setBackground(DanPinTypeClickLowerActivity.this.getResources().getDrawable(R.color.colorToolbar));
                txtTypeInfo.setBackground(null);
                txtTypeInfo.setBackground(DanPinTypeClickLowerActivity.this.getResources().getDrawable(R.drawable.type_txt_infoand_comshape));
                txtTypeInfo.setTextColor(DanPinTypeClickLowerActivity.this.getResources().getColor(R.color.colorToolbar));
                Bundle bundle = new Bundle();
                bundle.putString("IndexId", Item_Id);
                DanPinGoodCommentFragment danPinGoodInfoFragment = new DanPinGoodCommentFragment();
                danPinGoodInfoFragment.setArguments(bundle);
                fragmentManager = getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.ll_type_click_fg, danPinGoodInfoFragment);
                transaction.commit();
            }
        });
    }

    //点击返回键会上上一页
    public  void ImagBack(){
        imgGoodsinfoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent=new Intent(DanPinTypeClickLowerActivity.this, DpTypeActivity.class);
                startActivity(intent);*/
                finish();
                overridePendingTransition(R.anim.activity_left_in,R.anim.activity_right_out);
                //activity跳转动画，左近，右出。
            }
        });
    }

    //点击分享一键分享到，微信，微信朋友圈，微博，qq,qq空间。
    public  void Share(){
   rbtTypeShare.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               /**
                * 一键分享
                */
                   //1.创建一键分享的对象
                   OnekeyShare onekeyShare=new OnekeyShare();
                   onekeyShare.disableSSOWhenAuthorize();//关闭sso授权
                   //设置一键分享的内容
                   onekeyShare.setTitle("分享给你一个好东西");
                   onekeyShare.setText(content);
                   onekeyShare.setImageUrl(shareImageUrl+"");
                   onekeyShare.show(DanPinTypeClickLowerActivity.this);

           }
       });

    }
    //点击收藏按钮，收藏商品
    public  void Collection(){
        //下面布局已经改变，我把 radiobutton 改成了checkbox,id 名未改变，
        rbtTypeCollection.setOnClickListener(new View.OnClickListener() {
            boolean flag=true;//用于判断收藏按钮，的选中状态
            CollectionBeans collectionBeans=new CollectionBeans(Item_Id,title,price,picUrl);
            List<CollectionBeans> list;
            @Override
            public void onClick(View v) {
                /**
                 * 思路，进来先查询，此商品有没有收藏，如果没有，则收藏，如果数据库已经存在，
                 * 则弹出此商品已经收藏。
                 */

                if (flag==true) {
                    flag=false;//如果，收藏被选中则设置flag为false ,
                    //将商品的itemId,price,title,picUrl添加到数据库collectionbeans中，
                    QueryBuilder<CollectionBeans, Long> builder = MainActivity.collectionBeansDao.queryBuilder();
                    try {
                       list = builder.where().eq("item_id",Item_Id).query();
                       // list.get()
                        Log.i("cc","list.size=="+list.size());
                        if (list.size()==0){
                            if (!TextUtils.isEmpty(Item_Id) && !TextUtils.isEmpty(title) && !TextUtils.isEmpty(price) && !TextUtils.isEmpty(picUrl)){
                                int count = 0;
                                //插入数据，
                                count = MainActivity.collectionBeansDao.create(collectionBeans);
                                if (count > 0) {
                                    Toast.makeText(DanPinTypeClickLowerActivity.this,"添加收藏成功",Toast.LENGTH_LONG).show();
                                    Log.i("cc", "添加数据成功");
                                } else {
                                    Toast.makeText(DanPinTypeClickLowerActivity.this,"添加数据失败",Toast.LENGTH_LONG).show();

                                    Log.i("cc", "添加数据失败");
                                }
                            }

                        }else {
                            Toast.makeText(DanPinTypeClickLowerActivity.this,"此商品已经收藏",Toast.LENGTH_LONG).show();

                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    /*if (!TextUtils.isEmpty(Item_Id) && !TextUtils.isEmpty(title) && !TextUtils.isEmpty(price) && !TextUtils.isEmpty(picUrl)) {

                        int count = 0;
                        //插入数据，

                        try {
                            count = MainActivity.collectionBeansDao.create(collectionBeans);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        if (count > 0) {
                            Toast.makeText(DanPinTypeClickLowerActivity.this,"添加收藏成功",Toast.LENGTH_LONG).show();
                            Log.i("cc", "添加数据成功");
                        } else {
                            Toast.makeText(DanPinTypeClickLowerActivity.this,"添加数据失败",Toast.LENGTH_LONG).show();

                            Log.i("cc", "添加数据失败");
                        }
*/
                    } else if (flag==false){
                        flag=true;//改变falg的值，
                        //collectionBeansLongDao.delete(实体类对象)  删除数据
                        try {
                           /*
                           方法一、在点击事件click上面定义一个 List<CollectionBeans> list;
                           执行点击事件时，在插入数据或则删除数据之前先查询数据中是否已经存在（Item_id）对应的实体，如果存在，再次进来时，直接删除。

                           if (list.size()!=0){

                                Log.i("cc", "删除成功");
                              *//*  int count = MainActivity.collectionBeansDao.delete(list.get(0));

                                Log.i("cc","count==="+count);
                                if (count > 0) {
                                    Toast.makeText(DanPinTypeClickLowerActivity.this,"删除成功",Toast.LENGTH_LONG).show();
                                    Log.i("cc", "删除成功");
                                } else {
                                    Toast.makeText(DanPinTypeClickLowerActivity.this,"删除失败",Toast.LENGTH_LONG).show();
                                    Log.i("cc", "删除失败");
                                }*//*
                            }*/
                            /**
                             * 方法二：ORMLite原生的更新语句
                             * 在DAO的函数不满足你的灵活性时，可能用到原生的更新语句，
                             * 更新语句，必须包含保留关键字，INSERT,DELETE 或则UPDATE.
                             */
                            MainActivity.collectionBeansDao.updateRaw("DELETE FROM collectionbeans WHERE item_id="+Item_Id);
                            Toast.makeText(DanPinTypeClickLowerActivity.this,"删除成功",Toast.LENGTH_LONG).show();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }

                }

        });

    }


}
