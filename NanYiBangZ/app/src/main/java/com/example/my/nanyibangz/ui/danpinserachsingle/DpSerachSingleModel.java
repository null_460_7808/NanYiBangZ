package com.example.my.nanyibangz.ui.danpinserachsingle;

import com.example.my.nanyibangz.bean.DanPinSerachHotSingleBeans;
import com.example.my.nanyibangz.http.DanPinHttpHelper;

import rx.Subscriber;

/**
 * Created by My on 2016/10/16.
 */

public class DpSerachSingleModel implements DpSerachSingleContact.Model {
    @Override
    public void getVerticalSerachHotSingleBeans(Subscriber<DanPinSerachHotSingleBeans> subscriber) {
        DanPinHttpHelper.getInstance().getVerticalDanPin_Serach_Hot_Single(subscriber);
    }
}
