package com.example.my.nanyibangz.ui.danpinserachsingle;

import com.example.my.nanyibangz.base.IModel;
import com.example.my.nanyibangz.base.IPresenter;
import com.example.my.nanyibangz.base.IView;
import com.example.my.nanyibangz.bean.DanPinSerachHotSingleBeans;

import rx.Subscriber;

/**
 * Created by My on 2016/10/16.
 */

public class DpSerachSingleContact {
    public  interface View extends IView{
        public void getVerticalSerachHotSingleSuccess(DanPinSerachHotSingleBeans danPinSerachHotSingleBeans);
        public void getVerticalSerachHotSingleFailed(String msg);
    }
    public interface  Model extends IModel{
        public void getVerticalSerachHotSingleBeans(Subscriber<DanPinSerachHotSingleBeans> subscriber);
    }
    public interface  Presenter extends IPresenter{
        public  void getVerticalSerachHotSingleBeansFromNet();
    }
}
