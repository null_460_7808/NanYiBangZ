package com.example.my.nanyibangz.ui.shouye.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.HomeThreeBrandChoiceItem;
import com.example.my.nanyibangz.ui.shouye.view.HomeGoodsDetailsActivity;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

/**
 * 首页品牌精选Item数据适配器
 */

public class HomeBrandChoiceRecyclerAdapter extends RecyclerView.Adapter<HomeBrandChoiceRecyclerAdapter.HomeBrangChoiceItemViewHolder>{
    private Context context;
    private List<HomeThreeBrandChoiceItem.DataBean> list;

    public HomeBrandChoiceRecyclerAdapter(Context context, List<HomeThreeBrandChoiceItem.DataBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public HomeBrangChoiceItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.home_brandchoiceitem_item,null);
        HomeBrangChoiceItemViewHolder holder = new HomeBrangChoiceItemViewHolder(convertView);
        return holder;
    }

    @Override
    public void onBindViewHolder(HomeBrangChoiceItemViewHolder holder, final int position) {
        //设置控件 数据加载
        holder.home_brandchoice_item_sdv.setImageURI(Uri.parse(list.get(position).getPic_url()));
        holder.home_brandchoice_item_tv_title.setText(list.get(position).getTitle());
        holder.home_brandchoice_item_tv_couponPrice.setText("¥"+list.get(position).getCoupon_price());

        holder.home_brandchoice_item_sdv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int item_id = list.get(position).get_id();
                Intent intent = new Intent(context,HomeGoodsDetailsActivity.class);
                intent.putExtra("item_id",item_id);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class HomeBrangChoiceItemViewHolder extends RecyclerView.ViewHolder{
        SimpleDraweeView home_brandchoice_item_sdv;
        TextView home_brandchoice_item_tv_title,home_brandchoice_item_tv_couponPrice;
        public HomeBrangChoiceItemViewHolder(View itemView) {
            super(itemView);
            home_brandchoice_item_sdv = (SimpleDraweeView) itemView.findViewById(R.id.home_brandchoice_item_sdv);
            home_brandchoice_item_tv_title = (TextView) itemView.findViewById(R.id.home_brandchoice_item_tv_title);
            home_brandchoice_item_tv_couponPrice = (TextView) itemView.findViewById(R.id.home_brandchoice_item_tv_couponPrice);
        }
    }
}
