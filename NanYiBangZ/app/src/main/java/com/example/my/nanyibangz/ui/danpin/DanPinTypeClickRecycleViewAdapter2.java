package com.example.my.nanyibangz.ui.danpin;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.my.nanyibangz.R;
import com.example.my.nanyibangz.bean.DataBean;
import com.example.my.nanyibangz.image.DanPinImageHelper;

import java.util.List;

/**
 * Created by My on 2016/10/14.
 */

public class DanPinTypeClickRecycleViewAdapter2 extends RecyclerView.Adapter<DanPinTypeClickRecycleViewAdapter2.MyViewHolder>implements View.OnClickListener{
    private Context context;
    private List<DataBean> list;
    private MyItemClickListener listener;
    public DanPinTypeClickRecycleViewAdapter2(Context context,List<DataBean> list){

        this.context=context;
        this.list=list;
        Log.i("list","list=-="+list.size());
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.danpin_gv_jingxuan_item,null);
        MyViewHolder holder=new MyViewHolder(view);
        //将创建的View注册点击事件
        view.setOnClickListener(this);
        return  holder;
    }

    /**itemclicklistener监听
     * 设置itemclick
     */
    public  void setItemClickListener(MyItemClickListener listener){
        this.listener=listener;
    }

    /**
     * 点击监听
     */


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (list.size()!=0){
            holder.txt_desc.setText(list.get(position).getTitle());
            holder.txt_price.setText(list.get(position).getCouponPrice()+"");
            String imgUrl=list.get(position).getPicUrl();
            DanPinImageHelper.showImage_Jingping(context,imgUrl,holder.imageView);
            //将数据保存在itemView的Tag中，以便点击时进行获取
            holder.itemView.setTag(list.get(position).getItemId()+"");
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onClick(View v) {
        if(listener!=null){
            listener.OnItemClick(v, (String) v.getTag());
        }

    }

    class  MyViewHolder extends RecyclerView.ViewHolder{
        private TextView txt_price,txt_desc;
        private ImageView imageView;
        public MyViewHolder(View itemView) {
            super(itemView);
            txt_desc= (TextView) itemView.findViewById(R.id.txt_danpin_jingxuan_desc);
            txt_price= (TextView) itemView.findViewById(R.id.txt_danpin_jingxuan_price);
            imageView= (ImageView) itemView.findViewById(R.id.img_danpin_jingxuan);
        }
    }


    public  interface  MyItemClickListener {
        public void OnItemClick(View view, String Item_id);
    }
}
